package ch.uzh.ifi.seal.soprafs14.service;

import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GamePlayerRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GameRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserLoginLogoutRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserLoginLogoutResponseBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserRequestBean;
import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;

public class HelperTest{
	
	/*	Diese Methoden sind da um in den Test mit ein paar Attribute Tests zu kreieren
	 * 	Die Methoden greifen mit den Parametern den Tests direkt auf die Services zu
	 * */
	
	/** Helper 01: create user **/
	static public Boolean helper01CreateUser(String name, String username, UserRepository userRepo){
		
		UserRequestBean userRequestBean = new UserRequestBean();
		userRequestBean.setName(name);
		userRequestBean.setUsername(username);;
		
		return UserService.addUser(userRequestBean, userRepo);
	}
	
	/** Helper 02: login user **/
	static public UserLoginLogoutResponseBean helper02LoginUser(String name, UserRepository userRepo){
		
		Long userId = userRepo.findByName(name).getId();
		
		return UserService.loginUser(userId, userRepo);
	}
	
	/** Helper 03: logout user **/
	static public Boolean helper03LogoutUser(String name, UserRepository userRepo, GameRepository gameRepo){
		Long userId = userRepo.findByName(name).getId();
		
		UserLoginLogoutRequestBean userLoginLogoutRequestBean = new UserLoginLogoutRequestBean();
		userLoginLogoutRequestBean.setToken(userRepo.findByName(name).getToken());
		
		return UserService.logoutUser(userId, userLoginLogoutRequestBean, userRepo);
	}
	
	/** Helper 04: Create and login user **/
	static public UserLoginLogoutResponseBean helper04CreateAndLoginUser(String name, String username, UserRepository userRepo){
		helper01CreateUser(name, username, userRepo);
		return helper02LoginUser(name, userRepo);
	}
	
	/** Helper 05: Create user and game **/
	static public Boolean helper05CreateUserAndGame(String nameU, String username, String nameG, UserRepository userRepo, GameRepository gameRepo){
		helper04CreateAndLoginUser(nameU, username, userRepo);
		
		GameRequestBean gameRequestBean = new GameRequestBean();
		gameRequestBean.setName(nameG);
		gameRequestBean.setUserToken(userRepo.findByName(nameU).getToken());
		
		return GameAdminService.addGame(gameRequestBean, userRepo, gameRepo);
	}
	
	/** Helper 06: start a valid game **/
	static public Boolean helper06StartValidGame(String nameU, String nameG, UserRepository userRepo, GameRepository gameRepo){
		
		Long userId = userRepo.findByName(nameU).getId();
		Long gameId = userRepo.findByToken(userRepo.findByName(nameU).getToken()).getGames().get(0).getId();
		
		return GameStartStopDrawService.startGame(userId, gameId, userRepo, gameRepo);
	}
	
	/** Helper 07: create a game with valid user **/
	static public Boolean helper07CreateGame(String nameU, String nameG, UserRepository userRepo, GameRepository gameRepo){
		GameRequestBean gameRequestBean = new GameRequestBean();
		gameRequestBean.setName(nameG);
		gameRequestBean.setUserToken(userRepo.findByName(nameU).getToken());
	
		return GameAdminService.addGame(gameRequestBean, userRepo, gameRepo);
	}
	
	/** Helper 08: Add user to a game **/
	public static void helper08AddUser(String nameOwner, String nameU, String nameG, UserRepository userRepo, GameRepository gameRepo){
		GamePlayerRequestBean gamePlayerRequestBean = new GamePlayerRequestBean();
		gamePlayerRequestBean.setUserToken(userRepo.findByName(nameU).getToken());
		
		Long gameId = userRepo.findByName(nameOwner).getGames().get(0).getId();
		
		GamePlayerService.addPlayer(gameId, gamePlayerRequestBean, userRepo, gameRepo);
	}
	
}