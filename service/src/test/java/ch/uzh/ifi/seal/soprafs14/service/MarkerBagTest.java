package ch.uzh.ifi.seal.soprafs14.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/spring-test.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MarkerBagTest {
	
	static Logger logger = LoggerFactory.getLogger(GameAdminService.class);
	
	@Autowired
	MarkerBag markerBag;
	
	//help-list for tests
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ArrayList<String> markerTypes = new ArrayList();
	

	/** Test 1: Draw marker **/
	@Test
	public void test01GetMarker() {
		
		//fill markerTypes with markernames
		markerTypes.add("schwert");
		markerTypes.add("schild");
		markerTypes.add("wagen");
		markerTypes.add("katapult");
		markerTypes.add("standarte");
		markerTypes.add("dolch");
		markerTypes.add("joker");
		
		//check if markerTypes is full
		logger.debug("Enter first test ");
		assertThat("Testmarker is not fully loaded", markerTypes.size(), is(7));
		logger.debug("Finished first test ");
		
		//draw marker and check if a valid marker ahs been returned
		String marker = markerBag.getMarker();
		boolean checkerMarker = false;
		for (int i = 0; markerTypes.size() > i; i++ ){
			logger.debug("In the for loop, at round: " + i);
			if(markerTypes.get(i).contains(marker)){
				checkerMarker = true;
				break;
			}
			checkerMarker = true;
		}
		logger.debug("Enter second test ");
		assertTrue("Wrong marker, returned: " + marker , checkerMarker);
		logger.debug("Finished second test ");
	}
	
	/** Test 2: Get id of MarkerBag **/
	@Test(expected = AssertionError.class)
	public void test02GetMarkerID(){
		assertNotNull(markerBag.getId());
	}
	
	/** Test 3: Test all draws **/
	@Test
	public void test03DrawAllMarkers(){
		markerBag.drawMarkerPublic("joker");
		markerBag.drawMarkerPublic("schwert");
		markerBag.drawMarkerPublic("schild");
		markerBag.drawMarkerPublic("wagen");
		markerBag.drawMarkerPublic("katapult");
		markerBag.drawMarkerPublic("standarte");
		markerBag.drawMarkerPublic("dolch");
	}
	
}
