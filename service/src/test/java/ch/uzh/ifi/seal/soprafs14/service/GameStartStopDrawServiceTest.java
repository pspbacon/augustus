package ch.uzh.ifi.seal.soprafs14.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GameRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserRequestBean;
import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/spring-test.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GameStartStopDrawServiceTest {
	
	@Autowired
	private GameRepository gameRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	/** Test 1.1: start a game with valid user**/
	@Test
	public void test01_1StartGame(){
		
		//Create and login user for a new game and create game
		HelperTest.helper05CreateUserAndGame("Alfons", "A", "gameA", userRepo, gameRepo);
		assertTrue(HelperTest.helper06StartValidGame("Alfons", "gameA", userRepo, gameRepo));

		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 1.2: start a game with wrong **/
	@Test
	public void test01_2StartGame(){
		
		//Create valid user and game and destroy the game
		HelperTest.helper05CreateUserAndGame("Alfons", "A", "gameA", userRepo, gameRepo);
		assertTrue(HelperTest.helper06StartValidGame("Alfons", "gameA", userRepo, gameRepo));
		gameRepo.deleteAll();
		
		//Aflons creates a valid game
		HelperTest.helper07CreateGame("Alfons",  "gameA", userRepo, gameRepo);
		Long gameIdA2 = userRepo.findByToken(userRepo.findByName("Alfons").getToken()).getGames().get(0).getId();
		
		//Create and login user Indiana Goof
		HelperTest.helper04CreateAndLoginUser("Indiana Goof", "IG", userRepo);
		Long userIdIG = userRepo.findByName("Indiana Goof").getId();
		assertNull(GameStartStopDrawService.startGame(userIdIG, gameIdA2, userRepo, gameRepo));
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 1.3: start a game with 2 users **/
	@Test
	public void test01_3StartGame(){
		//Create and login user for a new game and create game
		HelperTest.helper05CreateUserAndGame("Alfons", "A", "gameA", userRepo, gameRepo);
		//Create and login user
		HelperTest.helper04CreateAndLoginUser("Indiana Goof", "IG", userRepo);
		
		assertTrue(HelperTest.helper06StartValidGame("Alfons", "gameA", userRepo, gameRepo));
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 2.1: stop a game with valid user **/
	@Test
	public void test02_1StopGame() throws Exception{
		
		HelperTest.helper05CreateUserAndGame("Alfons", "A", "nameA", userRepo, gameRepo);
		String userToken = userRepo.findByName("Alfons").getToken();
		Long gameId = userRepo.findByToken(userRepo.findByName("Alfons").getToken()).getGames().get(0).getId();
		
		assertTrue(GameStartStopDrawService.stopGame(gameId, userToken, userRepo, gameRepo));

		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
	
	/** Test 2.2: stop a game with not the right user **/
	public void test02_2StopGame() throws Exception{
		
		HelperTest.helper05CreateUserAndGame("Alfons", "A", "nameA", userRepo, gameRepo);
		HelperTest.helper04CreateAndLoginUser("Indiana Goof", "IG", userRepo);
		
		String userToken = userRepo.findByName("Alfons").getToken();
		Long gameId = userRepo.findByToken(userRepo.findByName("Alfons").getToken()).getGames().get(0).getId();
		
		assertFalse(GameStartStopDrawService.stopGame(gameId, userToken, userRepo, gameRepo));
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
	
	/** Test 3.1: draw cards with valid user **/
	@Test
	public void test03_1DrawCards(){
		
		HelperTest.helper05CreateUserAndGame("Kommissar Hunter", "KH", "gameKH", userRepo, gameRepo);
		assertTrue(HelperTest.helper06StartValidGame("Kommissar Hunter", "gameKH", userRepo, gameRepo));

		Long gameIdKH = userRepo.findByToken(userRepo.findByName("Kommissar Hunter").getToken()).getGames().get(0).getId();
		
		assertNotNull(GameStartStopDrawService.drawCards(gameIdKH, gameRepo));
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
	
	/** Test 3.2: dDraw cards with an invalid gameId **/
	@Test
	public void test03_2DrawCards(){
		Long gameIdInvalid = (long) 3774657;
		assertNull(GameStartStopDrawService.drawCards(gameIdInvalid, gameRepo));
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
	
	/** Test 4: generate start cards **/
	@Test(expected = NullPointerException.class)
	public void test04_1GenerateCards(){
		
		HelperTest.helper05CreateUserAndGame("Kommissar Hunter", "KH", "gameKH", userRepo, gameRepo);
		assertTrue(HelperTest.helper06StartValidGame("Kommissar Hunter", "gameKH", userRepo, gameRepo));
		
		//Long gameIdKH = userRepo.findByToken(userRepo.findByName("Kommissar Hunter").getToken()).getGames().get(0).getId();
		
		assertNull(GameStartStopDrawService.generateStartcardsPublic(gameRepo.findByName("Kommissar Hunter").getGames().get(0), userRepo.findByName("Kommissar Hunter")));
		
		//TODO: Irgendwie muss man auf das generateStartcards kommen zum testen
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
	
}