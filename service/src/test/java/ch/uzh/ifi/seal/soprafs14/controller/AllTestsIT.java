package ch.uzh.ifi.seal.soprafs14.controller;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ UserServiceControllerITT.class, GameServiceControllerITT.class })
public class AllTestsIT {}
