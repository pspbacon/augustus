package ch.uzh.ifi.seal.soprafs14.service;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/spring-test.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FastForwardServiceTest{
	
	@Autowired
	private GameRepository gameRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	/** Test 1.1 - one player **/
	@Test
	public void test01_1MobilizeAllLegions(){
		HelperTest.helper05CreateUserAndGame("Pluto", "P", "gameP", userRepo, gameRepo);
		HelperTest.helper06StartValidGame("Pluto", "gameP", userRepo, gameRepo);
		Long userId = userRepo.findByName("Pluto").getId();
		FastForwardService.mobilizeAllLegions(userRepo, userId);
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
	
	/** Test 1.2 - two players **/
	@Test
	public void test01_2MobilizeAllLegions(){
		HelperTest.helper05CreateUserAndGame("Wufy", "W", "gameW", userRepo, gameRepo);
		HelperTest.helper04CreateAndLoginUser("Dawg", "D", userRepo);
		HelperTest.helper08AddUser("Wufy", "Dawg", "gameW", userRepo, gameRepo);
		HelperTest.helper06StartValidGame("Wufy", "gameW", userRepo, gameRepo);
		
		Long userId1 = userRepo.findByName("Wufy").getId();
		FastForwardService.mobilizeAllLegions(userRepo, userId1);
		
		Long userId2 = userRepo.findByName("Dawg").getId();
		FastForwardService.mobilizeAllLegions(userRepo, userId2);
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
}