package ch.uzh.ifi.seal.soprafs14.controller;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserLoginLogoutRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserLoginLogoutResponseBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserResponseBean;

@Controller("UserServiceController")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserServiceControllerITT {


	final String TEST_SERVER = "http://localhost:26051";
	
	/** Test 1 - Testing user creation - linked to Part 1 and 2**/
	@SuppressWarnings("unchecked")
	@Test
	public void test1CreateUser(){
		/* Test 1-1 - Test if no user is created*/
		List<UserResponseBean> result0 = new RestTemplate().getForObject(TEST_SERVER + "/user", List.class);
		assertThat(result0.size(), is(0));
		//Create Test Case Mickey Mouse
		UserRequestBean requestMM = new UserRequestBean();
		requestMM.setName("Mickey Mouse");
		requestMM.setUsername("MM");
		
		/* Test 1-2 - Test if the user is created correctly */
		ResponseEntity<Boolean> newUser = new RestTemplate().postForEntity(TEST_SERVER + "/user", requestMM, Boolean.class);
		assertTrue(newUser.getBody().booleanValue());
		
		/* Test 1-3 - Test if there is one user on the server side */
		List<UserResponseBean> resultMM = new RestTemplate().getForObject(TEST_SERVER + "/user", List.class);
		assertThat(resultMM.size(), is(1));
	}
	
	/** Test 2 - Testing user login - linked to Part 1, 2 and 4 **/
	@Test
	public void test2LoginUser(){
		//Create Test Case Donald Duck
		UserRequestBean requestDD = new UserRequestBean();
		requestDD.setName("Donald Duck");
		requestDD.setUsername("DD");
		
		/* Test 2-1 - check if Donald was added */
		//Add Donald Duck to the server and check if true
		ResponseEntity<Boolean> newUser = new RestTemplate().postForEntity(TEST_SERVER + "/user", requestDD, Boolean.class);
		assertTrue(newUser.getBody().booleanValue());
		
		/* Test 2-2 -  Check if Donald Duck has a valid UserId */
		UserResponseBean[] resultDD =  new RestTemplate().getForObject(TEST_SERVER + "/user", UserResponseBean[].class);
		Long userId = null;
		String dd = "Donald Duck";
		for(UserResponseBean bean : resultDD){
			if(bean.getName().equals(dd)){
				userId = bean.getId();
			}
		}
		assertNotNull("The UserId is null",userId);
		/* Test 2-3 - Try to login with Donald Duck*/
		ResponseEntity<UserLoginLogoutResponseBean> userTokenDD = new RestTemplate().postForEntity(TEST_SERVER + "/user/" + userId + "/login", 0 ,UserLoginLogoutResponseBean.class);
		assertNotNull(userTokenDD);
	}
		
	/** Test 3 - Try a specific user request - linked to Part 1, 2 and 3 **/
	@Test
	public void test3IfUserMatch(){
		//Create Test Case Goofy
		UserRequestBean requestG = new UserRequestBean();
		requestG.setName("Goofy");
		requestG.setUsername("G");
		
		/* Test 3-1 - check if Goofy was added */
		//Add Goofy to the server and check if true
		ResponseEntity<Boolean> newUser = new RestTemplate().postForEntity(TEST_SERVER + "/user", requestG, Boolean.class);
		assertTrue(newUser.getBody().booleanValue());
		
		/* Test 3-2 -  Check if Goofy has a valid UserId */
		UserResponseBean[] resultG =  new RestTemplate().getForObject(TEST_SERVER + "/user", UserResponseBean[].class);
		Long userIdGoofy = null;
		String g = "Goofy";
		for(UserResponseBean bean : resultG){
			if(bean.getName().equals(g)){
				userIdGoofy = bean.getId();
			}
		}
		assertNotNull("The UserId is null",userIdGoofy);
		
		/* Test 3-3 - compare local user with online DB*/
		UserResponseBean onlineUser = new RestTemplate().getForObject(TEST_SERVER + "/user/" + userIdGoofy, UserResponseBean.class);
		assertEquals(requestG.getName(), onlineUser.getName());
		assertEquals(requestG.getUsername(), onlineUser.getUsername());
	}
	
	/** Test 4 - Testing user logout - linked to Part 1, 2, 4 and 5 **/
	@Test
	public void test4LogoutUser(){
		//Create Kater Karlo
		UserRequestBean requestKK = new UserRequestBean();
		requestKK.setName("Kater Karlo");
		requestKK.setUsername("KK");
		
		/* Test 4-1 - check if Kater Karlo was added */
		//Add Kater Karlo to the server and check if true
		ResponseEntity<Boolean> newUser = new RestTemplate().postForEntity(TEST_SERVER + "/user", requestKK, Boolean.class);
		assertTrue(newUser.getBody().booleanValue());
		
		/* Test 4-2 -  Check if Kater Karlo has a valid UserId */
		UserResponseBean[] resultKK =  new RestTemplate().getForObject(TEST_SERVER + "/user", UserResponseBean[].class);
		Long userIdKK = null;
		String kk = "Kater Karlo";
		for(UserResponseBean bean : resultKK){
			if(bean.getName().equals(kk)){
				userIdKK = bean.getId();
			}
		}
		assertNotNull("The UserId is null",userIdKK);
		
		/* Test 4-3 - Try to login with Kater Karlo*/
		ResponseEntity<UserLoginLogoutResponseBean> userTokenEntity = new RestTemplate().postForEntity(TEST_SERVER + "/user/" + userIdKK + "/login", 0, UserLoginLogoutResponseBean.class);
		UserLoginLogoutResponseBean userTokenKK = userTokenEntity.getBody();
		assertNotNull(userTokenKK);
		
		/* Test 4-4 - Testing user logout */
		UserLoginLogoutRequestBean beanLogoutKK = new UserLoginLogoutRequestBean();
		beanLogoutKK.setToken(userTokenKK.getUserToken());
		new RestTemplate().postForEntity(TEST_SERVER + "/user/" + userIdKK + "/logout", beanLogoutKK, null);
	}
	
	/** Test 5 - Test login and logout **/
	@Test
	public void test5LoginLogout(){
		HelperIT.helperIT01CreateUser("Robin Hood", "RH");
		String userToken = HelperIT.helperIT02LoginUser("Robin Hood");
		HelperIT.helperIT03LogoutUser("Robin Hood", userToken);
	}
	
	/** Test 6 - Testing user deletion - linked to Part 6 **/
	@Test
	public void test6delteUsers(){
		new RestTemplate().postForEntity(TEST_SERVER + "/user/delete", null, null);
	}
}
