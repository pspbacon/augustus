package ch.uzh.ifi.seal.soprafs14.controller;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FastForwardControllerIT {
	
	final String TEST_SERVER = "http://localhost:26051";
	
	/** Test 1.1: Part 1 **/
	@Test
	public void test01_1initializeFastforwarding(){
		HelperIT.helperIT01CreateUser("Lucky Luke", "LL");
		String userToken = HelperIT.helperIT02LoginUser("Lucky Luke");
		ResponseEntity<Long> response = HelperIT.helperIT100AddGame("Lucky Luke", userToken, "gameLL");
		Long gameId = response.getBody();
		Long playerId =  HelperIT.helperIT04GetUserId("Lucky Luke");
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/"+ gameId + "/player/"+ playerId +"/fastforward", null, null);
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 2.1: Part 2 **/
	@Test
	public void test02_1initializeFullcard(){
		HelperIT.helperIT01CreateUser("Lucky Luke", "LL");
		String userToken = HelperIT.helperIT02LoginUser("Lucky Luke");
		ResponseEntity<Long> response = HelperIT.helperIT100AddGame("Lucky Luke", userToken, "gameLL");
		Long gameId = response.getBody();
		Long playerId =  HelperIT.helperIT04GetUserId("Lucky Luke");
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/"+ gameId + "/player/"+ playerId +"/fullcard", null, null);
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 3.1: Part 3 **/
	@Test
	public void test03_1initializeEndgame(){
		HelperIT.helperIT01CreateUser("Lucky Luke", "LL");
		String userToken = HelperIT.helperIT02LoginUser("Lucky Luke");
		ResponseEntity<Long> response = HelperIT.helperIT100AddGame("Lucky Luke", userToken, "gameLL");
		Long gameId = response.getBody();
		Long playerId =  HelperIT.helperIT04GetUserId("Lucky Luke");
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/"+ gameId + "/player/"+ playerId +"/endGame", null, null);
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 4.1: Part 4 with valid data **/
	@Test
	public void test04_1initializeEndgame(){
		HelperIT.helperIT01CreateUser("Lucky Luke", "LL");
		String userToken = HelperIT.helperIT02LoginUser("Lucky Luke");
		ResponseEntity<Long> response = HelperIT.helperIT100AddGame("Lucky Luke", userToken, "gameLL");
		Long gameId = response.getBody();
		Long playerId =  HelperIT.helperIT04GetUserId("Lucky Luke");
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/"+ gameId + "/player/"+ playerId +"/fillcards", null, null);
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
}