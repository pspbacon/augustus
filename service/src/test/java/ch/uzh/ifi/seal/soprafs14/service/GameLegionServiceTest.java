package ch.uzh.ifi.seal.soprafs14.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ch.uzh.ifi.seal.soprafs14.controller.beans.game.TargetCardBean;
import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/spring-test.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GameLegionServiceTest {
	
	@Autowired
	private GameRepository gameRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	/** Test 1.1: get legions with invalid IDs - tests 4.1 **/
	@Test(expected = AssertionError.class) 
	public void test01_1getAvailableLegions(){
		Long gameId = (long) 0;
		Long playerId = (long) 0;
		assertEquals(AssertionError.class, GameLegionService.getAvailableLegions(gameId, playerId, gameRepo, userRepo));
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 1.2: get legions with valid IDs - tests 4.1 **/
	@Test
	public void test01_2getAvailableLegions(){
		HelperTest.helper05CreateUserAndGame("Alfons", "A", "gameA", userRepo, gameRepo);
		assertTrue(HelperTest.helper06StartValidGame("Alfons", "gameA", userRepo, gameRepo));
		
		Long gameId = userRepo.findByToken(userRepo.findByName("Alfons").getToken()).getGames().get(0).getId();
		Long playerId = userRepo.findByName("Alfons").getId();
		
		assertNotNull(GameLegionService.getAvailableLegions(gameId, playerId, gameRepo, userRepo));
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 2.1: get free legions fields with valid IDs - tests 4.2 **/
	
	// TEST IST KORRUPT IM MOMENT
	/*
	@Test
	public void test02_1getFreeLegionFields(){
		HelperTest.helper05CreateUserAndGame("Alfons", "A", "gameA", userRepo, gameRepo);
		assertTrue(HelperTest.helper06StartValidGame("Alfons", "gameA", userRepo, gameRepo));
		
		Long gameId = userRepo.findByToken(userRepo.findByName("Alfons").getToken()).getGames().get(0).getId();
		Long playerId = userRepo.findByName("Alfons").getId();
		
		assertNotNull(GameLegionService.getFreeLegionFields(gameId, playerId, gameRepo, userRepo));
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	*/
	
	/** Test 2.2: get free legions fields with valid IDs - tests 4.2 **/
	@Test(expected = AssertionError.class) 
	public void test02_2getFreeLegionFields(){
		
		Long gameId = (long) 0;
		Long playerId = (long) 0;
		assertEquals(AssertionError.class, GameLegionService.getFreeLegionFields(gameId, playerId, gameRepo, userRepo));
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 3.1: post legion fields with valid IDs - tests 4.3 **/
	@Test
	public void test03_1postLegionFields(){
		
		HelperTest.helper05CreateUserAndGame("Alfons", "A", "gameA", userRepo, gameRepo);
		assertTrue(HelperTest.helper06StartValidGame("Alfons", "gameA", userRepo, gameRepo));
		
		Long gameId = userRepo.findByToken(userRepo.findByName("Alfons").getToken()).getGames().get(0).getId();
		Long playerId = userRepo.findByName("Alfons").getId();
		
		TargetCardBean[] targetCardBeanList = null;
		
		GameLegionService.postLegionFields(gameId, playerId, targetCardBeanList, gameRepo, userRepo);
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 3.2: post legion fields with invalid IDs - tests 4.3 **/
	@Test
	public void test03_2postLegionFields(){
		
		Long gameId = (long) 0;
		Long playerId = (long) 0;
		
		TargetCardBean[] targetCardBeanList = null;
		
		GameLegionService.postLegionFields(gameId, playerId, targetCardBeanList, gameRepo, userRepo);
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 4.1: get player cards with valid ID - tests 4.4 **/
	@Test
	public void test04_1GetPlayerCards(){
		
		HelperTest.helper05CreateUserAndGame("Alfons", "A", "gameA", userRepo, gameRepo);
		assertTrue(HelperTest.helper06StartValidGame("Alfons", "gameA", userRepo, gameRepo));
		
		Long gameId = userRepo.findByToken(userRepo.findByName("Alfons").getToken()).getGames().get(0).getId();
		
		GameLegionService.getPlayerCards(gameId, gameRepo, userRepo);
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 4.2: get player cards with invalid ID - tests 4.4 **/
	@Test
	public void test04_2GetPlayerCards(){
		

		Long gameId = (long) 0;
		
		GameLegionService.getPlayerCards(gameId, gameRepo, userRepo);
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 5.1: get conquered cards with valid ID - tests 4.5 **/
	@Test
	public void test05_1GetConqueredCards(){
		
		HelperTest.helper05CreateUserAndGame("Alfons", "A", "gameA", userRepo, gameRepo);
		assertTrue(HelperTest.helper06StartValidGame("Alfons", "gameA", userRepo, gameRepo));
		
		Long gameId = userRepo.findByToken(userRepo.findByName("Alfons").getToken()).getGames().get(0).getId();
		
		GameLegionService.getConqueredCards(gameId, gameRepo, userRepo);
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 5.2: get conquered cards with invalid ID - tests 4.5 **/
	@Test(expected = NullPointerException.class)
	public void test05_2GetConqueredCards(){
		
		Long gameId = (long) 0;
		
		GameLegionService.getConqueredCards(gameId, gameRepo, userRepo);
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
}