package ch.uzh.ifi.seal.soprafs14.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ch.uzh.ifi.seal.soprafs14.model.TargetCard;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/spring-test.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TargetCardStackTest{
	
	@Autowired
	TargetCardStack targetCardStack;
	
	//Create a TargetCardStack
	TargetCardStack tcs = new TargetCardStack();
	
	/** Test 1: Test if TargetCardStack has been initialized correctly **/
	@Test
	public void test01Create(){
		//Check if exactly 88 cards have been given back
		assertThat("TargetCardStack has not a size 88" ,tcs.getCards().size(), is(88));
	}
	
	/** Test 2: Test if TargetCardStack has been initialized correctly **/
	@Test
	public void test02RemoveTopCard(){
		TargetCard tc = tcs.removeTopCard(0);
		//check if the size has been reduced by one
		assertThat("TargetCardStack has not size 88" ,tcs.getCards().size(), is(88));
		//check if the card has a valid CardNumber
		assertTrue("Drawn targetCard is not valid", tc.getNumber() <= 88 && tc.getNumber() >= 1);
		tc = null;
	}
	
	/** Test 3: Draw too many cards - getCards() **/
	@Test
	public void test03DrawTooManyCards(){
		//loop the check for 88 times, once too often
		for(int i = 88; i > 0 ; i--){
			TargetCard tc = tcs.removeTopCard(0);
			//check if the size has been reduced by one
			assertThat("TargetCardStack has not size 88",tcs.getCards().size(), is(88));
			//check if the card has a valid CardNumber
			assertTrue("Drawn targetCard is not valid", tc.getNumber() <= 88 && tc.getNumber() >= 1);
			tc = null;
		}
	}
	
	/** Test 4: Test for direct method call **/
	@Test
	public void test04GetCards() {
		assertThat("No valid card returned ", targetCardStack.getCards().isEmpty(), is(false));
	}
	
	/** Test 5: Test for direct method call - removeTopCard() **/
	@Test
	public void test05RemoveTopCard() {
		assertNotNull("No valid card returned ", targetCardStack.removeTopCard(0));
	}
	
	/** Test 6: Test for getId **/
	@Test
	public void test06getId() {
		targetCardStack.getId();
	}
}