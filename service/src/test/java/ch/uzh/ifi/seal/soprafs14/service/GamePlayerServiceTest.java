package ch.uzh.ifi.seal.soprafs14.service;

import static org.junit.Assert.assertNotNull;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GamePlayerRequestBean;
import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/spring-test.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GamePlayerServiceTest{
	
	@Autowired
	private GameRepository gameRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	/** Test 1.1: list players of a game (with one player) **/
	@Test
	public void test01_1ListPlayers(){
		HelperTest.helper05CreateUserAndGame("Pingu", "P", "PinguG", userRepo, gameRepo);
		
		Long gameId = userRepo.findByName("Pingu").getGames().get(0).getId();
		
		assertNotNull(GamePlayerService.listPlayers(gameId, gameRepo));
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
	
	/** Test 1.2: list players of a game (with two players) **/
	@Test
	public void test01_2ListPlayers(){
		HelperTest.helper05CreateUserAndGame("Pingu", "P", "PinguG", userRepo, gameRepo);
		HelperTest.helper04CreateAndLoginUser("Speedy Gonzales", "SG", userRepo);
		
		//Add Speedy Gonzales to game
		HelperTest.helper08AddUser("Pingu", "Speedy Gonzales", "PinguG", userRepo, gameRepo);
		
		Long gameId = userRepo.findByName("Pingu").getGames().get(0).getId();
		
		assertNotNull(GamePlayerService.listPlayers(gameId, gameRepo));
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
	
	/** Test 2.1: add player in a game **/
	@Test
	public void test02_1addPlayer(){		
		HelperTest.helper05CreateUserAndGame("Pingu", "P", "PinguG", userRepo, gameRepo);
		HelperTest.helper04CreateAndLoginUser("Speedy Gonzales", "SG", userRepo);
		
		//Add Speedy Gonzales to game
		HelperTest.helper08AddUser("Pingu", "Speedy Gonzales", "PinguG", userRepo, gameRepo);
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
	
	/** Test 2.2: add player in a game (non existing) **/
	@Test(expected = NullPointerException.class)
	public void test02_2addPlayer(){		
		HelperTest.helper05CreateUserAndGame("Pingu", "P", "PinguG", userRepo, gameRepo);
		
		//Add Speedy Gonzales to game
		HelperTest.helper08AddUser("Pingu", "Rambo", "PinguG", userRepo, gameRepo);
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
	
	/** Test 2.3: add player in a game, who is already in the game **/
	@Test
	public void test02_3addPlayer(){		
		HelperTest.helper05CreateUserAndGame("Pingu", "P", "PinguG", userRepo, gameRepo);
		HelperTest.helper04CreateAndLoginUser("Speedy Gonzales", "SG", userRepo);
		
		//Add Speedy Gonzales to game
		HelperTest.helper08AddUser("Pingu", "Speedy Gonzales", "PinguG", userRepo, gameRepo);
		HelperTest.helper08AddUser("Pingu", "Speedy Gonzales", "PinguG", userRepo, gameRepo);
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
	
	/** Test 2.4: add two players in a game **/
	@Test
	public void test02_4addPlayer(){		
		HelperTest.helper05CreateUserAndGame("Pingu", "P", "PinguG", userRepo, gameRepo);
		HelperTest.helper04CreateAndLoginUser("Speedy Gonzales", "SG", userRepo);
		HelperTest.helper04CreateAndLoginUser("Roadrunner", "R", userRepo);
		
		//Add Speedy Gonzales and Roadrunner to the game
		HelperTest.helper08AddUser("Pingu", "Speedy Gonzales", "PinguG", userRepo, gameRepo);
		HelperTest.helper08AddUser("Pingu", "Roadrunner", "PinguG", userRepo, gameRepo);
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
	
	/** Test 3.1: add two players in a game **/
	@Test
	public void test03_1RemovePlayer(){
		HelperTest.helper05CreateUserAndGame("Pingu", "P", "PinguG", userRepo, gameRepo);
		HelperTest.helper04CreateAndLoginUser("Speedy Gonzales", "SG", userRepo);
		HelperTest.helper08AddUser("Pingu", "Speedy Gonzales", "PinguG", userRepo, gameRepo);
		
		Long gameId = userRepo.findByName("Pingu").getGames().get(0).getId();
		GamePlayerRequestBean gamePlayerRequestBean = new GamePlayerRequestBean();
		gamePlayerRequestBean.setUserToken(userRepo.findByName("Speedy Gonzales").getToken());
		
		GamePlayerService.removePlayer(gameId, gamePlayerRequestBean, userRepo, gameRepo);
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
	
	/** Test 4.1: get player **/
	@Test
	public void test04_1GetPlayer(){
		//Create and start
		HelperTest.helper05CreateUserAndGame("Pingu", "P", "PinguG", userRepo, gameRepo);
		HelperTest.helper04CreateAndLoginUser("Speedy Gonzales", "SG", userRepo);
		HelperTest.helper08AddUser("Pingu", "Speedy Gonzales", "PinguG", userRepo, gameRepo);
		HelperTest.helper06StartValidGame("Pingu", "PinguG", userRepo, gameRepo);
		
		Long gameId = userRepo.findByName("Pingu").getGames().get(0).getId();
		Long playerId = userRepo.findByName("Pingu").getId();
		
		assertNotNull( "Didn't get the player " ,GamePlayerService.getPlayer(gameId, playerId, userRepo, gameRepo));
	}
	
	/** Test 5.1: get player status **/
	@Test
	public void test05_1GetPlayerStatus(){
		HelperTest.helper05CreateUserAndGame("Pingu", "P", "PinguG", userRepo, gameRepo);
		HelperTest.helper04CreateAndLoginUser("Speedy Gonzales", "SG", userRepo);
		HelperTest.helper08AddUser("Pingu", "Speedy Gonzales", "PinguG", userRepo, gameRepo);
		HelperTest.helper06StartValidGame("Pingu", "PinguG", userRepo, gameRepo);
		
		Long gameId = userRepo.findByName("Pingu").getGames().get(0).getId();
		
		GamePlayerService.getPlayerStatus(gameId, gameRepo);
	}
}