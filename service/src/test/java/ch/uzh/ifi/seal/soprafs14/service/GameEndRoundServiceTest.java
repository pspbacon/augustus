package ch.uzh.ifi.seal.soprafs14.service;

import static org.junit.Assert.assertNotNull;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/spring-test.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GameEndRoundServiceTest{
	
	@Autowired
	private GameRepository gameRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	/** Test 1: Test to evaluate game with invalid data - 7.1 **/
	@Test(expected = AssertionError.class)
	public void test01EvaluateInvalidRound(){
		assertNotNull(GameEndRoundService.evaluateRound(null, userRepo, gameRepo));
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}

	/** Test 2: Test to evaluate game with valid data and one player - 7.1 **/
	@Test(expected = NullPointerException.class)
	public void test02EvaluateRound(){
		
		HelperTest.helper05CreateUserAndGame("Pingu", "P", "PinguG", userRepo, gameRepo);
		HelperTest.helper06StartValidGame("Pingu", "PinguG", userRepo, gameRepo);
		
		GameEndRoundService.evaluateRound(userRepo.findByName("Pingu").getGames().get(0).getId(), userRepo, gameRepo);
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
	
	/** Test 3: Test to evaluate game with valid data and two players - 7.1 **/
	@Test(expected = NullPointerException.class)
	public void test03EvaluateRound(){
		
		HelperTest.helper05CreateUserAndGame("Pingu", "P", "PinguG", userRepo, gameRepo);
		HelperTest.helper06StartValidGame("Pingu", "PinguG", userRepo, gameRepo);
		HelperTest.helper04CreateAndLoginUser("Jame Bond", "JB", userRepo);
		
		GameEndRoundService.evaluateRound(userRepo.findByName("Pingu").getGames().get(0).getId(), userRepo, gameRepo);
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
}