package ch.uzh.ifi.seal.soprafs14.service;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.hamcrest.Matchers.is;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GameRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserRequestBean;
import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;
import ch.uzh.ifi.seal.soprafs14.service.GameAdminService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/spring-test.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GameAdminServiceTest {
	
	@Autowired
	private GameRepository gameRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	/** Test 1: listGames **/
	@Test
	public void test01ListGames(){
		/* Test 1.1 - check if there are zero games online */
		assertThat("There were games, but it showed some ", GameAdminService.listGames(gameRepo).size(), is(0));
		gameRepo.deleteAll();
	}
	
	/** Test 2: addGames **/
	@Test
	public void test020AddGame(){
		
		/* Test 2.1 - Add a game with a valid user */
		//assertTrue("Game could not be add", GameAdminService.addGame(gameRequestBeanP, userRepo, gameRepo));
		assertTrue("Game could not be add", HelperTest.helper05CreateUserAndGame("Pluto", "P", "gameP", userRepo, gameRepo));
		
		/* Test 2.2 - Check if there is exactly one game online */
		assertThat(GameAdminService.listGames(gameRepo).size(), is(1));
		
		/* Test 2.3 - Try to add a game with invalid GameRequestBean */
		GameRequestBean gameRequestBeanInvalid = null;
		assertNull("Game could not be add", GameAdminService.addGame(gameRequestBeanInvalid, userRepo, gameRepo));
		
		/*Test 2.4 - Try to add game with user who isn't logged in */
		UserRequestBean userRequestBeanIG = new UserRequestBean();
		userRequestBeanIG.setName("Indiana Goof");
		userRequestBeanIG.setUsername("IG");
		UserService.addUser(userRequestBeanIG, userRepo);
		GameRequestBean gameRequestBeanIG = new GameRequestBean();
		gameRequestBeanIG.setName("GameIG");
		gameRequestBeanIG.setUserToken(null);
		assertFalse("Game could be added, but should be able to ", GameAdminService.addGame(gameRequestBeanIG, userRepo, gameRepo));
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
	
	/** Test 2.1: addGames  with gameId return **/
	@Test
	public void test021AddGame(){
		
		/* Test 2.1 - Add a game with a valid user */
		//assertTrue("Game could not be add", GameAdminService.addGame(gameRequestBeanP, userRepo, gameRepo));
		assertTrue("Game could not be add", HelperTest.helper05CreateUserAndGame("Pluto", "P", "gameP", userRepo, gameRepo));
		
		/* Test 2.2 - Check if there is exactly one game online */
		assertThat(GameAdminService.listGames(gameRepo).size(), is(1));
		
		/* Test 2.3 - Try to add a game with invalid GameRequestBean */
		GameRequestBean gameRequestBeanInvalid = null;
		assertNull("Game could not be add", GameAdminService.addGame1(gameRequestBeanInvalid, userRepo, gameRepo));
		
		/*Test 2.4 - Try to add game with user who isn't logged in */
		UserRequestBean userRequestBeanIG = new UserRequestBean();
		userRequestBeanIG.setName("Indiana Goof");
		userRequestBeanIG.setUsername("IG");
		UserService.addUser(userRequestBeanIG, userRepo);
		GameRequestBean gameRequestBeanIG = new GameRequestBean();
		gameRequestBeanIG.setName("GameIG");
		gameRequestBeanIG.setUserToken(null);
		assertNull("Game could be added, but should be able to ", GameAdminService.addGame1(gameRequestBeanIG, userRepo, gameRepo));
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
	
	/** Test 3: get a specific game **/
	@Test
	public void test03GetGame(){
		//Create a new game
//		GameRequestBean gameRequestBeanP = new GameRequestBean();
//		gameRequestBeanP.setName("GameP");
//		gameRequestBeanP.setUserToken(userRepo.findByName("Pluto").getToken());
		//Long gameIdP = userRepo.findByToken(userRepo.findByName("Pluto").getToken()).getGames().get(0).getId();
		
		HelperTest.helper05CreateUserAndGame("Pluto", "P", "gameP", userRepo, gameRepo);
		Long gameId = (long) 3;
		//assertNotNull(GameAdminService.getGame(gameRepo, gameId));
		/* Test 3.1 - Get a valid game and check if same gameId as first*/
		//GameResponseBean gameResponseBean = GameAdminService.getGame(gameRepo, userRepo.findByToken(userRepo.findByName("Pluto").getToken()).getGames().get(0).getId());
		//TODO: Irgendwie muss die richtige GameId gezogen werden, etwas mit den Funktionsaufruefen stimmt nicht
		//Long gameOnlineId = gameResponseBean.getId();
		//assertThat(gameIdP ,gameOnlineId);
		
		/* Test 3.2 - Try to get an invalid game */
		Long GameIdInvalid = null;
		assertNull(GameAdminService.getGame(gameRepo, GameIdInvalid));
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
}