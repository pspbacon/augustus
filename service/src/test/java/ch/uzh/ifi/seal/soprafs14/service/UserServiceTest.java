package ch.uzh.ifi.seal.soprafs14.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserLoginLogoutRequestBean;
import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/spring-test.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserServiceTest{
	
	@Autowired
	private GameRepository gameRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	/** Test 1: listUsers **/
	@Test
	public void test01ListUsers(){
		assertThat("There should be no user in the DB ", UserService.listUsers(userRepo).size(), is(0));
	}
	
	/** Test 2: addUser**/
	@Test
	public void test02AddUser(){
		
		/* Test 2.1 - Check if Donald Duck could be added to the user DB */
		assertTrue(HelperTest.helper01CreateUser("Donald Duck", "DD", userRepo));
		
		/* Test 2.2 - Check if the user DB has one user */
		assertThat("There should be one user in the DB ", UserService.listUsers(userRepo).size(), is(1));
		userRepo.deleteAll();
	}
	
	/** Test 3: get specific user **/
	@Test
	public void test03GetUser(){
		assertTrue(HelperTest.helper01CreateUser("Donald Duck", "DD", userRepo));
		Long userId = userRepo.findByName("Donald Duck").getId();
		
		/* Test 3.1 - check for a valid user */
		assertNotNull("Was not able to find user with userId; " + userId, UserService.getUser(userId, userRepo));
		
		/* Test 3.1 - check for an invalid user */
		Long userIdInvalid = null;
		assertNull(UserService.getUser(userIdInvalid, userRepo));
		userRepo.deleteAll();
	}
	
	/** Test 4: login user **/
	@Test
	public void test04LoginUser(){
		HelperTest.helper01CreateUser("Donald Duck", "DD", userRepo);
		/* Test 4.1 - try to login valid user */
		assertNotNull(HelperTest.helper02LoginUser("Donald Duck", userRepo));
		
		/* Test 4.2 - try to login invalid user */
		Long userIdInvalid = null;
		assertNull(UserService.loginUser(userIdInvalid, userRepo));
		
		/* Test 4.3 - try to create and login new valid user */
		assertNotNull(HelperTest.helper04CreateAndLoginUser("James Bond1", "JB1", userRepo));
		
		/* Test 4.4 - try to create and login  new valid user */
		assertNotNull(HelperTest.helper04CreateAndLoginUser("James Bond2", "JB2", userRepo));
		userRepo.deleteAll();
	}
	
	/** Test 5: logout user **/
	@Test
	public void test05LogoutUser(){
		HelperTest.helper04CreateAndLoginUser("Donald Duck", "DD", userRepo);
		
		/* Test 5.1 - try to logout valid user*/
		assertNotNull(HelperTest.helper03LogoutUser("Donald Duck", userRepo, gameRepo));
		
		/* Test 5.2 - try invalid user */
		Long userIdInvalid = null;
		UserLoginLogoutRequestBean userLoginLogoutRequestBeanDD = new UserLoginLogoutRequestBean();
		userLoginLogoutRequestBeanDD.setToken(userRepo.findByName("Donald Duck").getToken());
		
		assertNull(UserService.logoutUser(userIdInvalid, userLoginLogoutRequestBeanDD, userRepo));
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
	
	/** Test 6: login user **/
	@Test(expected = AssertionError.class)
	public void test06LoginInvalidUser(){
		
		/* Test 4.4 - try to create and login  new valid user */
		assertNull(HelperTest.helper04CreateAndLoginUser("", "", userRepo));
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
	
	/** Test 7: logout not loged in user **/
	@Test
	public void test07LogoutUser(){
		HelperTest.helper01CreateUser("Donald Duck", "DD", userRepo);
		
		/* Test 7.1 - try to logout valid user*/
		assertNotNull(HelperTest.helper03LogoutUser("Donald Duck", userRepo, gameRepo));
		
		/* Test 7.2 - try invalid user */
		Long userIdInvalid = null;
		UserLoginLogoutRequestBean userLoginLogoutRequestBeanDD = new UserLoginLogoutRequestBean();
		userLoginLogoutRequestBeanDD.setToken(userRepo.findByName("Donald Duck").getToken());
		
		assertNull(UserService.logoutUser(userIdInvalid, userLoginLogoutRequestBeanDD, userRepo));
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
}