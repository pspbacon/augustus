package ch.uzh.ifi.seal.soprafs14.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GamePlayerRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GameRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserLoginLogoutRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserLoginLogoutResponseBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserResponseBean;

public class HelperIT{
	
	final static String TEST_SERVER = "http://localhost:26051";
	
	/** HelperIT 01: create user **/
	public static void helperIT01CreateUser(String name, String username){
		UserRequestBean request = new UserRequestBean();
		request.setName(name);
		request.setUsername(username);
		new RestTemplate().postForEntity(TEST_SERVER + "/user", request, Boolean.class);
	}
	
	/** HelperIT 02: login user **/
	public static String helperIT02LoginUser(String name){
		Long userId = null;
		UserResponseBean[] results =  new RestTemplate().getForObject(TEST_SERVER + "/user", UserResponseBean[].class);
		for(UserResponseBean bean : results){
			if(bean.getName().equals(name)){
				userId = bean.getId();
			}
		}
		ResponseEntity<UserLoginLogoutResponseBean> userTokenEntity = new RestTemplate().postForEntity(TEST_SERVER + "/user/" + userId + "/login", null, UserLoginLogoutResponseBean.class);
		return userTokenEntity.getBody().getUserToken();
	}
	
	/** HelperIT 03: logout user **/
	public static void helperIT03LogoutUser(String name, String userToken){
		Long userId = null;
		UserResponseBean[] results =  new RestTemplate().getForObject(TEST_SERVER + "/user", UserResponseBean[].class);
		for(UserResponseBean bean : results){
			if(bean.getName().equals(name)){
				userId = bean.getId();
			}
		}
		UserLoginLogoutRequestBean beanLogout = new UserLoginLogoutRequestBean();
		beanLogout.setToken(userToken);
		new RestTemplate().postForEntity(TEST_SERVER + "/user/" + userId + "/logout", beanLogout, null);
	}
	
	/** HelperIT 04: get userId **/
	public static Long helperIT04GetUserId(String name){
		Long userId = null;
		UserResponseBean[] results =  new RestTemplate().getForObject(TEST_SERVER + "/user", UserResponseBean[].class);
		for(UserResponseBean bean : results){
			if(bean.getName().equals(name)){
				userId = bean.getId();
			}
		}
		return userId;
	}
	
	/** HelperIT 10: add game **/
	public static void helperIT10AddGame(String name, String userToken, String nameGame){
		GameRequestBean game = new GameRequestBean();
		game.setName(nameGame);
		game.setUserToken(userToken);
		new RestTemplate().postForEntity(TEST_SERVER + "/game", game, void.class);
	}
	
	/** HelperIT 100: add game with return gameId **/
	public static ResponseEntity<Long> helperIT100AddGame(String name, String userToken, String nameGame){
		GameRequestBean game = new GameRequestBean();
		game.setName(nameGame);
		game.setUserToken(userToken);
		return new RestTemplate().postForEntity(TEST_SERVER + "/game1", game, Long.class);
	}
	
	/** HelperIT 11: add user to a game **/
	public static void helperIT11UsertoGame(String userToken, Long gameId){
		GamePlayerRequestBean gamePlayerRequestBean = new GamePlayerRequestBean();
		gamePlayerRequestBean.setUserToken(userToken);
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/player/add", gamePlayerRequestBean, null);
	}
	
	/** HelperIT 12: return gameId from owner **/
	public static Long helper12GameId(String owner){
		Long gameId =  null;
		
//		List<GameResponseBean[]> results = new RestTemplate().getForEntity(TEST_SERVER + "/game", void.class,GameResponseBean[].class);
//		
//		for(GameResponseBean bean : results){
//			if(bean.getOwner().equals(owner)){
//				gameId = bean.getId();
//			}
//		}
		
		gameId = new RestTemplate().getForObject(TEST_SERVER + "/game/" + owner +"/gameId", Long.class);
		return gameId;
	}
	
	
	/** HelperIT 90: destroy gameRepo and userRepo **/
	public static void helperIT90DestroyGameRepoAndUserRepo(){
		helperIT91DestroyGameRepo();
		helperIT92DestroyUserRepo();
	}
	
	/** HelperIT 91: destroy gameRepo **/
	public static void helperIT91DestroyGameRepo(){
		new RestTemplate().postForEntity(TEST_SERVER + "/game/delete", void.class, void.class);
	}
	
	/** HelperIT 92: destroy userRepo **/
	public static void helperIT92DestroyUserRepo(){
		new RestTemplate().postForEntity(TEST_SERVER + "/user/delete", void.class, void.class);
	}
	
}

//add user to game
