package ch.uzh.ifi.seal.soprafs14.controller;

import static org.junit.Assert.assertNotNull;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.web.client.RestTemplate;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class IndexControllerIT{
	
	final String TEST_SERVER = "http://localhost:26051";

	/** test 1 **/
	@Test
	public void test01enterIndex(){
		assertNotNull(IndexController.index());
	}
}