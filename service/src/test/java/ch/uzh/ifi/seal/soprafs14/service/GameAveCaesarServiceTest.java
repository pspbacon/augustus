package ch.uzh.ifi.seal.soprafs14.service;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/spring-test.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GameAveCaesarServiceTest {
	@Autowired
	private GameRepository gameRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	/** Test 1.1: **/
	@Test(expected = NullPointerException.class)
	public void test01_1something(){
		HelperTest.helper05CreateUserAndGame("Pluto", "P", "gameP", userRepo, gameRepo);
		GameAveCaesarService.aveCaesar(gameRepo.findByName("Pluto").getId(), userRepo.findByName("Pluto").getId(), 0, userRepo, gameRepo);
	
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
}