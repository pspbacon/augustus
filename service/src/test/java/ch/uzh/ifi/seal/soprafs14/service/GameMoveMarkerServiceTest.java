package ch.uzh.ifi.seal.soprafs14.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GameMoveRequestBean;
import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/spring-test.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GameMoveMarkerServiceTest {
	
	@Autowired
	private GameRepository gameRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	/** Test 1.1.1: initialize move with invalid gameId - tests 3.1.1 **/
	@Test(expected = NullPointerException.class)  
	public void test11_1InitializeMove() {
		Long gameId = (long) 0;
		GameMoveMarkerService.initializeMove(gameId, gameRepo);
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 1.1.2: initialize move with valid set up - tests 3.1.1 **/
	@Test 
	public void test11_2InitializeMove() {
		HelperTest.helper05CreateUserAndGame("Alfons", "A", "gameA", userRepo, gameRepo);
		assertTrue(HelperTest.helper06StartValidGame("Alfons", "gameA", userRepo, gameRepo));
		
		Long gameId = userRepo.findByToken(userRepo.findByName("Alfons").getToken()).getGames().get(0).getId();
		
		assertNotNull(GameMoveMarkerService.initializeMove(gameId, gameRepo));
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 1.2.1: get moves with invalid gameId - tests 3.1.2 **/
	@Test(expected = AssertionError.class)  
	public void test12_1ListMoves() {
		Long gameId = (long) 0;
		assertEquals(AssertionError.class, GameMoveMarkerService.listMoves(gameId, gameRepo));
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 1.3.1: add move with null bean - tests 3.1.3 **/
	@Test(expected = NullPointerException.class) 
	public void test13_1addMove() {
		GameMoveRequestBean gameMoveRequestBean = null;
		Long gameId = (long) 0;
		Long playerId = (long) 0;
		GameMoveMarkerService.addMove(gameId, playerId, gameMoveRequestBean, gameRepo, userRepo);
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 1.3.2: add move with valid user - tests 3.1.3 **/
	@Test
	public void test13_2addMove() {
		HelperTest.helper05CreateUserAndGame("Alfons", "A", "gameA", userRepo, gameRepo);
		assertTrue(HelperTest.helper06StartValidGame("Alfons", "gameA", userRepo, gameRepo));
		
		
		Long gameId = userRepo.findByToken(userRepo.findByName("Alfons").getToken()).getGames().get(0).getId();
		Long playerId = userRepo.findByName("Alfons").getId();
		
		GameMoveRequestBean gameMoveRequestBean = new GameMoveRequestBean();
		GameMoveMarkerService.addMove(gameId, playerId, gameMoveRequestBean, gameRepo, userRepo);
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 1.3.3: add move with valid user but not owner - tests 3.1.3 **/
	@Test
	public void test13_3addMove() {
		HelperTest.helper05CreateUserAndGame("Alfons", "A", "gameA", userRepo, gameRepo);
		assertTrue(HelperTest.helper06StartValidGame("Alfons", "gameA", userRepo, gameRepo));
		
		
		Long gameId = userRepo.findByToken(userRepo.findByName("Alfons").getToken()).getGames().get(0).getId();
		HelperTest.helper04CreateAndLoginUser("Indiana Goof", "IG", userRepo);
		Long playerId = userRepo.findByName("Indiana Goof").getId();
		
		GameMoveRequestBean gameMoveRequestBean = new GameMoveRequestBean();
		GameMoveMarkerService.addMove(gameId, playerId, gameMoveRequestBean, gameRepo, userRepo);
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 1.4.1: delete game with valid user but not owner - tests 3.1.4 **/
	@Test
	public void test14_1deleteGame() {
		HelperTest.helper05CreateUserAndGame("Alfons", "A", "gameA", userRepo, gameRepo);
		assertTrue(HelperTest.helper06StartValidGame("Alfons", "gameA", userRepo, gameRepo));
		
		
		Long gameId = userRepo.findByToken(userRepo.findByName("Alfons").getToken()).getGames().get(0).getId();
		HelperTest.helper04CreateAndLoginUser("Indiana Goof", "IG", userRepo);
		Long playerId = userRepo.findByName("Indiana Goof").getId();
		
		GameMoveMarkerService.deleteGame(gameId, playerId, gameRepo, userRepo);
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 1.4.2: delete game with valid user and owner - tests 3.1.4 **/
	@Test
	public void test14_2deleteGame() {
		HelperTest.helper05CreateUserAndGame("Alfons", "A", "gameA", userRepo, gameRepo);
		assertTrue(HelperTest.helper06StartValidGame("Alfons", "gameA", userRepo, gameRepo));
		
		Long gameId = userRepo.findByToken(userRepo.findByName("Alfons").getToken()).getGames().get(0).getId();
		Long playerId = userRepo.findByName("Alfons").getId();
		
		GameMoveMarkerService.deleteGame(gameId, playerId, gameRepo, userRepo);
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 1.5.1: delete game with valid user and owner - tests 3.1.5 **/
	@Test
	public void test14_2getMove() {
		HelperTest.helper05CreateUserAndGame("Alfons", "A", "gameA", userRepo, gameRepo);
		assertTrue(HelperTest.helper06StartValidGame("Alfons", "gameA", userRepo, gameRepo));
		
		Long gameId = userRepo.findByToken(userRepo.findByName("Alfons").getToken()).getGames().get(0).getId();
		int moveId = 0;
		
		assertNotNull(GameMoveMarkerService.getMove(gameId, moveId, gameRepo));
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 2.1.1: create marker with valid user but invalid moveId - tests 3.2.1 **/
	@Test
	public void test21_1CreateMarker() {
		HelperTest.helper05CreateUserAndGame("Alfons", "A", "gameA", userRepo, gameRepo);
		assertTrue(HelperTest.helper06StartValidGame("Alfons", "gameA", userRepo, gameRepo));
		
		Long gameId = userRepo.findByToken(userRepo.findByName("Alfons").getToken()).getGames().get(0).getId();
		Long moveId = (long) 0;
		
		GameMoveMarkerService.createMarker(gameId, moveId, gameRepo);
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
//	/** Test 2.1.2: create marker with valid user but invalid moveId - tests 3.2.1 **/
//	@Test
//	public void test21_2CreateMarker() {
//		HelperTest.helper05CreateUserAndGame("Alfons", "A", "gameA", userRepo, gameRepo);
//		assertTrue(HelperTest.helper06StartValidGame("Alfons", "gameA", userRepo, gameRepo));
//		
//		Long gameId = userRepo.findByToken(userRepo.findByName("Alfons").getToken()).getGames().get(0).getId();
//		Long moveId = (long) 0;
//		
//		GameMoveMarkerService.createMarker(gameId, moveId, gameRepo);
//		
//		userRepo.deleteAll();
//		gameRepo.deleteAll();
//	}
	
	/** Test 2.2.1: get marker of a valid game - tests 3.2.2 **/
	@Test
	public void test22_1GetMarker() {
		HelperTest.helper05CreateUserAndGame("Alfons", "A", "gameA", userRepo, gameRepo);
		assertTrue(HelperTest.helper06StartValidGame("Alfons", "gameA", userRepo, gameRepo));
		
		Long gameId = userRepo.findByToken(userRepo.findByName("Alfons").getToken()).getGames().get(0).getId();
		Long moveId = (long) 0;
		
		GameMoveMarkerService.getMarker(gameId, moveId, gameRepo);
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
	
	/** Test 2.2.2: get marker of an invalid game - tests 3.2.2 **/
	@Test
	public void test22_2GetMarker() {
		HelperTest.helper04CreateAndLoginUser("Kater Karlo", "KK", userRepo);
		
		Long gameId = null;
		Long moveId = null;
		
		GameMoveMarkerService.getMarker(gameId, moveId, gameRepo);
		
		userRepo.deleteAll();
		gameRepo.deleteAll();
	}
}