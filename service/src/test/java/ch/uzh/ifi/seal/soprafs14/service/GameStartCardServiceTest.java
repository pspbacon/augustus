package ch.uzh.ifi.seal.soprafs14.service;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ch.uzh.ifi.seal.soprafs14.controller.beans.game.TargetCardBean;
import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/spring-test.xml"})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public final class GameStartCardServiceTest {

	@Autowired
	private GameRepository gameRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	/** Test 1.1: 6.1 - get own start cards **/
	@Test
	public void test01_1DeliverStartCards(){
		HelperTest.helper05CreateUserAndGame("Felix", "F", "FelixGame", userRepo, gameRepo);
		HelperTest.helper06StartValidGame("Felix", "FelixGame", userRepo, gameRepo);
		Long gameId = userRepo.findByName("Felix").getGames().get(0).getId();
		Long userId = userRepo.findByName("Felix").getId();
		GameStartCardService.deliverStartCards(gameId, userId, userRepo);
		
		gameRepo.deleteAll();
		userRepo.deleteAll();
	}
	
	/** Test 2.1: 6.2 - post chosen start cards **/
	@Test
	public void test02_1UpdateStartCards(){
		HelperTest.helper05CreateUserAndGame("Chris", "C", "ChrisGame", userRepo, gameRepo);
		HelperTest.helper04CreateAndLoginUser("James", "J", userRepo);
		HelperTest.helper08AddUser("Chris", "James", "ChrisGame", userRepo, gameRepo);
		HelperTest.helper06StartValidGame("Chris", "ChrisGame", userRepo, gameRepo);
		
		Long playerId = userRepo.findByName("Chris").getId();
		Long gameId = userRepo.findByToken(userRepo.findByName("Chris").getToken()).getGames().get(0).getId();
		
//		List<TargetCardBean> cardIdBean = GameStartCardService.deliverStartCards(gameId, playerId, userRepo);
//		assertThat(cardIdBean.size(), is(6));
//		cardIdBean.remove(5);
//		cardIdBean.remove(4);
//		cardIdBean.remove(3);
//		
//		GameStartCardService.updateStartCards(gameId, playerId, cardIdBean, userRepo);
//		
//		gameRepo.deleteAll();
//		userRepo.deleteAll();
	}
}