package ch.uzh.ifi.seal.soprafs14.controller;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GameMoveRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GameMoveResponseBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GamePlayerRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GamePlayerResponseBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GameResponseBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.MarkerBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.RewardBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.RoundEvaluationBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.TargetCardBean;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GameServiceControllerITT {
	
	final String TEST_SERVER = "http://localhost:26051";
	
	/**** Test 1: list, add and remove game ****/
	/** Test 1.1 list games **/
	@Test
	public void test11ListGames(){
		HelperIT.helperIT91DestroyGameRepo();
		GameResponseBean[] results = new RestTemplate().getForObject(TEST_SERVER + "/game", GameResponseBean[].class);
		assertThat(results.length, is(0));
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 1.2 add game without logged in player**/
	@Test
	public void test12CreateGame(){
		HelperIT.helperIT01CreateUser("Gustav Gans", "GG");
		String userToken = HelperIT.helperIT02LoginUser("Gustav Gans");
		assertNotNull(userToken);
		HelperIT.helperIT10AddGame("Gustav Gans", userToken, "GameGG");
		
		
		GameResponseBean[] results2 = new RestTemplate().getForObject(TEST_SERVER + "/game", GameResponseBean[].class);
		assertThat(results2.length, is(1));
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 1.2.1 with gameId **/
	@Test
	public void test121CreateGame(){
		HelperIT.helperIT01CreateUser("Gustav Gans", "GG");
		String userToken = HelperIT.helperIT02LoginUser("Gustav Gans");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Gustav Gans", userToken, "GameGG");
		Long gameId = result.getBody();
		assertNotNull(gameId);
		
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	
	/** Test 1.3 get game **/
	@Test
	public void test13GetGame(){
		HelperIT.helperIT01CreateUser("Gustav Gans", "GG");
		String userToken = HelperIT.helperIT02LoginUser("Gustav Gans");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Gustav Gans", userToken, "GameGG");
		Long gameId = result.getBody();
		
//		new RestTemplate().getForObject(TEST_SERVER + "/game/" + gameId, GameResponseBean[].class);
//		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/**** Test 2: start game ****/
	/** Test 2.1 - Start a game **/
	@Test
	public void test21startGame(){
		HelperIT.helperIT01CreateUser("Gustav Gans", "GG");
		String userToken = HelperIT.helperIT02LoginUser("Gustav Gans");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Gustav Gans", userToken, "GameGG");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Gustav Gans");
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/start", userId, void.class);
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 2.2 - Stop a game **/
	@Test
	public void test22stopGame(){
		HelperIT.helperIT01CreateUser("Gustav Gans", "GG");
		String userToken = HelperIT.helperIT02LoginUser("Gustav Gans");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Gustav Gans", userToken, "GameGG");
		Long gameId = result.getBody();
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/stop", userToken, void.class);
		
		
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 2.3 - Draw cards **/
	@Test
	public void test23DrawCards(){
		HelperIT.helperIT01CreateUser("Gustav Gans", "GG");
		String userToken = HelperIT.helperIT02LoginUser("Gustav Gans");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Gustav Gans", userToken, "GameGG");
		Long gameId = result.getBody();
		
		//TODO: funktioniert noch nicht richtig
		//ResponseEntity<TargetCardBean[]> response = new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/drawablecards", null, List<TargetCardBean>);
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/******* Part 3: Move *******/
	/******* Part 3.1: getMove *******/
	
	/** Test 3.1.1 - initializeMove **/
	@Test
	public void test311initializeMove(){
		HelperIT.helperIT01CreateUser("Gustav Gans", "GG");
		String userToken = HelperIT.helperIT02LoginUser("Gustav Gans");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Gustav Gans", userToken, "GameGG");
		Long gameId = result.getBody();
		assertNotNull(new RestTemplate().getForObject(TEST_SERVER + "/game/" + gameId + "/move", MarkerBean.class));
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 3.1.2 - list moves **/
	@Test
	public void test312ListMoves(){
		HelperIT.helperIT01CreateUser("Gustav Gans", "GG");
		String userToken = HelperIT.helperIT02LoginUser("Gustav Gans");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Gustav Gans", userToken, "GameGG");
		Long gameId = result.getBody();
		
		assertNotNull(new RestTemplate().getForObject(TEST_SERVER + "/game/" + gameId + "/listMove", GameMoveResponseBean[].class));
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
//	/** Test 3.1.3 - addMove **/
//	@Test
//	public void test313AddMove(){
//		HelperIT.helperIT01CreateUser("Gustav Gans", "GG");
//		String userToken = HelperIT.helperIT02LoginUser("Gustav Gans");
//		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Gustav Gans", userToken, "GameGG");
//		Long gameId = result.getBody();
//		Long userId = HelperIT.helperIT04GetUserId("Gustav Gans");
//		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/start", userId, void.class);
//		
//		GameMoveRequestBean gameMoveRequestBean = null;
//		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/move", gameMoveRequestBean, void.class);
//		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
//	}
	
	/** Test 3.1.4 - delete game **/
	@Test
	public void test314DeleteGame(){
		HelperIT.helperIT01CreateUser("Gustav Gans", "GG");
		String userToken = HelperIT.helperIT02LoginUser("Gustav Gans");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Gustav Gans", userToken, "GameGG");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Gustav Gans");
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/start", userId, void.class);
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/delete", userId, void.class);
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 3.1.5 - get move **/
	@Test
	public void test315GetMove(){
		//TODO: Add things
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/******* Part 3.2: Marker *******/
	/** Test 3.2.1 - create marker **/
	@Test
	public void test321CreateMarker(){
		HelperIT.helperIT01CreateUser("Gustav Gans", "GG");
		String userToken = HelperIT.helperIT02LoginUser("Gustav Gans");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Gustav Gans", userToken, "GameGG");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Gustav Gans");
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/start", userId, void.class);
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/move/" + 0 + "/createMarker", null, void.class);
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 3.2.2 - get marker **/
	@Test
	public void test322GetMarker(){
		HelperIT.helperIT01CreateUser("Gustav Gans", "GG");
		String userToken = HelperIT.helperIT02LoginUser("Gustav Gans");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Gustav Gans", userToken, "GameGG");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Gustav Gans");
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/start", userId, void.class);
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/move/" + 0 + "/marker", null, String.class);
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/******* Part 4: Legionfields *******/
	/** Test 4.1 - get available legions **/
	@Test
	public void test41GetAvailableLegions(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Chef");
		
		HelperIT.helperIT01CreateUser("Stefan", "S");
		String userToken1 = HelperIT.helperIT02LoginUser("Stefan");
		HelperIT.helperIT11UsertoGame(userToken1, gameId);
		
		new RestTemplate().getForObject(TEST_SERVER + "/game/" + gameId + "/player/" + userId + "/legions", MarkerBean.class);
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 4.2 - get free legion fields **/
//	@Test
//	public void test42GetFreeLegionFields(){
//		HelperIT.helperIT01CreateUser("Chef", "C");
//		String userToken = HelperIT.helperIT02LoginUser("Chef");
//		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
//		Long gameId = result.getBody();
//		Long userId = HelperIT.helperIT04GetUserId("Chef");
//		
//		HelperIT.helperIT01CreateUser("Stefan", "S");
//		String userToken1 = HelperIT.helperIT02LoginUser("Stefan");
//		HelperIT.helperIT11UsertoGame(userToken1, gameId);
//		Long moveId = (long) 0;
//		
//		new RestTemplate().getForObject(TEST_SERVER + "/game/" + gameId + "/move/" + moveId + "/player/" + userId +"/legionsfield", TargetCardBean[].class);
//		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
//	}
	
	/** Test 4.3 - post legion fields **/
	@Test
	public void test43PostLegionFields(){
		//TODO: auschreiben
	}
	
	/** Test 4.4 - get player cards **/
	public void test44GetPlayerCards(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		
		assertNotNull(new RestTemplate().getForObject( TEST_SERVER + "/game/" + gameId + "/assignedCards", TargetCardBean[][].class));
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 4.5 - get conquered cards **/
	public void test45getConqueredCards(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		
		assertNotNull(new RestTemplate().getForObject( TEST_SERVER + "/game/" + gameId + "/conqueredCards", TargetCardBean[][].class));
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 4.6 - get conquered cards **/
	public void test46GetEffectLegionFields(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Chef");
		
		assertNotNull(new RestTemplate().getForObject( TEST_SERVER + "/game/" + gameId + "/player/" + userId +"/effectlegionfield", TargetCardBean[].class));
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 4.7 - get conquered cards **/
	public void test47GetEffectLegionCount(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Chef");
		
		assertNotNull(new RestTemplate().getForObject( TEST_SERVER + "/game/" + gameId + "/player/" + userId +"/effectlegioninfo", MarkerBean.class));
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/******* Part 5: Player *******/
	/** Test 5.1 - list players in a game **/
	@Test
	public void test51ListPlayers(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		
		new RestTemplate().getForObject(TEST_SERVER + "/game/" + gameId + "/player", GamePlayerResponseBean[].class);
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 5.2 - add player to game **/
	@Test
	public void test52AddPlayer(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		
		HelperIT.helperIT01CreateUser("Stefan", "S");
		String userToken1 = HelperIT.helperIT02LoginUser("Stefan");
		
		GamePlayerRequestBean gamePlayerRequestBean = new GamePlayerRequestBean();
		gamePlayerRequestBean.setUserToken(userToken1);
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/player/add", gamePlayerRequestBean, void.class);
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 5.3 - remove palyer from game **/
	@Test
	public void test53RemovePlayer(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		
		HelperIT.helperIT01CreateUser("Stefan", "S");
		String userToken1 = HelperIT.helperIT02LoginUser("Stefan");
		HelperIT.helperIT11UsertoGame(userToken1, gameId);
		
		GamePlayerRequestBean gamePlayerRequestBean = new GamePlayerRequestBean();
		gamePlayerRequestBean.setUserToken(userToken1);
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/player/remove", gamePlayerRequestBean, void.class);
		
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 5.4 - get player cars **/
	@Test
	public void test54GetPlayerCars(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Chef");
		
		new RestTemplate().getForObject(TEST_SERVER + "/game/" + gameId + "/player/" + userId, GamePlayerResponseBean .class);
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** 5.5 - get player status to start the game **/
	@Test
	public void test55GetStatusToStart(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		
		new RestTemplate().getForObject(TEST_SERVER + "/game/" + gameId + "/player/status", Boolean.class);
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** 5.6 - set player status **/
	@Test
	public void test56SetPlayerStatus(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Chef");
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/player/" + userId + "/status", null, void.class);
		
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/******* Part 6: Startcards *******/
	/** Test 6.1 - get own start cards **/
	@Test
	public void test61startCards(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Chef");
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/start", userId, void.class);
		
		assertNotNull(new RestTemplate().getForObject(TEST_SERVER+ "/game/" +gameId +"/player/" +userId+"/startcard", TargetCardBean[].class));
		
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 6.2 - post own start cards **/
	@Test
	public void test62UpdateStartCards(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Chef");
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/start", userId, void.class);
		
		TargetCardBean[] cards = new RestTemplate().getForObject(TEST_SERVER+ "/game/" +gameId +"/player/" +userId+"/startcard", TargetCardBean[].class);
		
		new RestTemplate().postForEntity(TEST_SERVER+ "/game/" +gameId +"/player/" +userId+"/startcard2", cards, void.class);
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/******* Part 7: End of round evaluation *******/
	/** Test 7.1 - evaluate a round **/
//	@Test
//	public void test71Endevaluation(){
//		HelperIT.helperIT01CreateUser("Chef", "C");
//		String userToken = HelperIT.helperIT02LoginUser("Chef");
//		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
//		Long gameId = result.getBody();
//		Long userId = HelperIT.helperIT04GetUserId("Chef");
//		
//		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/start", userId, void.class);
//		
//		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/roundevaluation", null, RoundEvaluationBean.class);
//		
//		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
//	}
	
	/** 7.2 - count points **/
	@Test(expected = HttpClientErrorException.class)
	public void test72CountPoints(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Chef");
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/start", userId, void.class);
		
		new RestTemplate().getForObject(TEST_SERVER + "/game/" + gameId + "/reoundevaluation", RoundEvaluationBean.class);
		
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** 7.3 - set ready **/
	@Test
	public void test73SetReady(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Chef");
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/start", userId, void.class);

		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/player/" + userId + "/ready", null, void.class);
		
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** 7.4 - deliver card stack **/
	@Test
	public void test74DeliverCardStack(){
//		HelperIT.helperIT01CreateUser("Chef", "C");
//		String userToken = HelperIT.helperIT02LoginUser("Chef");
//		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
//		Long gameId = result.getBody();
//		Long userId = HelperIT.helperIT04GetUserId("Chef");
//		
//		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/start", userId, void.class);
//
//		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/newcardstack", null, TargetCardBean[].class);
//		
//		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** 7.5 - legion effect **/
	@Test
	public void test75LegionEffect(){
//		HelperIT.helperIT01CreateUser("Chef", "C");
//		String userToken = HelperIT.helperIT02LoginUser("Chef");
//		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
//		Long gameId = result.getBody();
//		Long userId = HelperIT.helperIT04GetUserId("Chef");
//		
//		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/start", userId, void.class);
//
//		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/player/" + userId + "/legionEffect", null, TargetCardBean.class);
//		
//		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	
	/******* Part 8: Ave Caesar *******/
	/** Test 8.1 - Ave Caesar **/
	@Test(expected = HttpClientErrorException.class)
	public void test81AveCaesar(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Chef");
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/start", userId, void.class);
		Integer choosenCardNumber = 1;
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/player/" + userId+ "/start", choosenCardNumber, void.class);
		
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 8.2 - Request drawable Rewards **/
	@Test(expected = HttpClientErrorException.class)
	public void test82DrawableRewards(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Chef");
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/start", userId, void.class);
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/user/" + userId + "/rewards", null, RewardBean[].class);
		
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 8.3 - Update Conquered Rewards **/
	@Test(expected = HttpClientErrorException.class)
	public void test83updateCR(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Chef");
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/start", userId, void.class);
		
		Long rewardId = (long) 2;
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/user/" + userId + "/choosenReqrds", rewardId, RewardBean[].class);
		
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 8.4 - show assignedRewards **/
	@Test
	public void test84Show(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Chef");
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/start", userId, void.class);
		
		new RestTemplate().getForObject(TEST_SERVER + "/game/" + gameId + "/assignedRewards", RewardBean[][].class);
		
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 8.5 - Update Player Status After Immediate Effect Has Been Executed **/
	@Test
	public void test85Update(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Chef");
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/start", userId, void.class);
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/player/" + userId +"/negativeEffectDone", null, null);
		
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	/** Test 8.7 send playerCards (assigned and controlled) for negativeEffectActivity **/
	@Test
	public void test87SendPC(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Chef");
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/start", userId, void.class);
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/player/" + userId +"/negativeEffect", null, TargetCardBean[][].class);
		
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	
	/** Test 8.8 Post card if EffectId = 51 **/
	@Test(expected = HttpServerErrorException.class)
	public void test88EffectID51(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Chef");
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/start", userId, void.class);
		Integer choosenCardNumber = 1;
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/player/" + userId +"/ImmediateWin", choosenCardNumber, null);
		
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
	
	/** Test 8.9 sacrifice TargetCard **/
	@Test
	public void test89sac(){
		HelperIT.helperIT01CreateUser("Chef", "C");
		String userToken = HelperIT.helperIT02LoginUser("Chef");
		ResponseEntity<Long> result = HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
		Long gameId = result.getBody();
		Long userId = HelperIT.helperIT04GetUserId("Chef");
		
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/start", userId, void.class);
		Integer choosenCardNumber = 1;
		new RestTemplate().postForEntity(TEST_SERVER + "/game/" + gameId + "/player/" + userId +"/sacrifice", choosenCardNumber, null);
		
		HelperIT.helperIT90DestroyGameRepoAndUserRepo();
	}
	
//	/**** 10 Delete gameRepo ****/
//	/** 10.2 - get first gameId, assisting testing **/
//	@Test
//	public void test9102Assistance(){
//	HelperIT.helperIT01CreateUser("Chef", "C");
//	String userToken = HelperIT.helperIT02LoginUser("Chef");
//	HelperIT.helperIT100AddGame("Chef", userToken, "GameC");
//	new RestTemplate().getForObject(TEST_SERVER + "/game/chef/gameId", Long.class);
//	HelperIT.helperIT90DestroyGameRepoAndUserRepo();
//	}
}