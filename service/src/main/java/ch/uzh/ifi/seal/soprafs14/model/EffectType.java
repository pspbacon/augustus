package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

// Diese Enumeration spezifiziert verschiedene Zeitpunkte bzw. Dauern, wann ein Effekt von Bedeutung ist
public enum EffectType implements Serializable {
	IMMEDIATELY(1, "immediately", "unmittelbar eintretender Effekt bei Auswertung"),
	PERSISTENT(2, "persistent", "Effekt, der für den Rest des Spiels anhält"),
	POINT(3, "point", "Effekt, der für variable Punkte genutzt wird; kein Einfluss während des Spielablaufs, erst am Ende des Spiels von Bedeutung"); // momentan redundant zu gewissen VariablePoint-Klassen...
	
	private final Integer code;
	private final String label;
	private final String description;
	
	private EffectType(int code, String label, String description) {
		this.code = code;
		this.label = label;
		this.description = description;
	}
	
	public Integer getCode() {
		return code;
	}
	
	public String getLabel() {
		return label;
	}
	
	public String getDescription() {
		return description;
	}

}
