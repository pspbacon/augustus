package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.uzh.ifi.seal.soprafs14.GameConstants;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GameStatus;
import ch.uzh.ifi.seal.soprafs14.service.MarkerBag;
import ch.uzh.ifi.seal.soprafs14.service.TargetCardStack;

@Entity
public class Game implements Serializable {
	
	private static final long serialVersionUID = 1L;
    private static Logger logger = LoggerFactory.getLogger(Game.class);

	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false) 
	private String name;
	
	@Column(nullable = false) 
	private String owner;

	@Column 
	private GameStatus status;
	
	@Column 
	private Integer currentPlayer;

	// Anzahl der Zielkarten, die ein Spieler kontrollieren muss, um dieses Spiel zu gewinnen
	@Column(nullable = false)
	public final Integer numberOfTargetCardsForVictory;

	// Anzahl der Zielkarten, die einem Spieler bei Beginn dieses Spiels zur Auswahl gestellt wird
	@Column(nullable = false)
	public final Integer numberOfStartcardsGiven;

	// Anzahl der Zielkarten, welche ein Spieler von den DEFAULT_NUMBER_OF_STARTCARDS_HANDOUT Karten, die er zunaechst erhalten hat, wieder abgeben muss
	@Column(nullable = false)
	public final Integer numberOfStartcardsHandback;

	// Anzahl der Legionen, ueber die ein Spieler bei Beginn dieses Spiels verfuegt
	@Column(nullable = false)
	public final Integer numberOfLegionsStart;

	@OneToMany(mappedBy="game", cascade = CascadeType.PERSIST)
    private List<Move> moves;
    
    @ManyToMany(mappedBy="games")
    private List<User> players;
    
    @OneToOne(cascade = CascadeType.PERSIST)
    private TargetCardStack cardStackOfGame;

    // list with the currently drawable target cards
    @Column
    private List<TargetCard> drawableTargetCards;

    @OneToOne(cascade = CascadeType.PERSIST)
    private MarkerBag markerBagOfGame;
    
    // list of all available Rewards in the game (without movable rewards that have been assigned already)
    @Column
    private List<Reward> availableRewards;
    
    // used to store the last drawn marker
    // TODO: evil attribute: refactor and delete
    @Column
    private String lastDrawnMarker;
    
    @Column
    private Integer requestCount;
    
    @Column
    private String endRoundMessage;
    
    @Column
    private Integer positionInTargetCardStack;
    
    @Column
    private Boolean flagForNegativeEffect;

    public Game() {
        this.numberOfTargetCardsForVictory = GameConstants.DEFAULT_NUMBER_OF_TARGETCARDS_FOR_VICTORY;
        this.numberOfStartcardsGiven = GameConstants.DEFAULT_NUMBER_OF_STARTCARDS_HANDOUT;
        this.numberOfStartcardsHandback = GameConstants.DEFAULT_NUMBER_OF_STARTCARDS_HANDBACK;
        this.numberOfLegionsStart = GameConstants.DEFAULT_NUMBER_OF_LEGIONS_START;
    }
    
    public Game(String name, String owner) {
        this.numberOfTargetCardsForVictory = GameConstants.DEFAULT_NUMBER_OF_TARGETCARDS_FOR_VICTORY;
        this.numberOfStartcardsGiven = GameConstants.DEFAULT_NUMBER_OF_STARTCARDS_HANDOUT;
        this.numberOfStartcardsHandback = GameConstants.DEFAULT_NUMBER_OF_STARTCARDS_HANDBACK;
        this.numberOfLegionsStart = GameConstants.DEFAULT_NUMBER_OF_LEGIONS_START;
        
        this.name = name;
        this.owner = owner;
        this.status = GameStatus.PENDING;
        
        this.players = new ArrayList<User>();
        this.moves = new ArrayList<Move>();
        
        this.currentPlayer = 0;
        this.requestCount = 0;
        this.positionInTargetCardStack = -1;
        this.flagForNegativeEffect = false;
    }

	public Integer getRequestCount() {
		return requestCount;
	}

	public void setRequestCount(Integer requestCount) {
		this.requestCount = requestCount;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getOwner() {
		return owner;
	}

	public List<Move> getMoves() {
		return moves;
	}

	public void setMoves(List<Move> moves) {
		this.moves = moves;
	}

	public List<User> getPlayers() {
		return players;
	}

	public void setPlayers(List<User> players) {
		this.players = players;
	}

	public GameStatus getStatus() {
		return status;
	}

	public void initialize() {
	    // create marker bag and shuffle cards
        this.markerBagOfGame = new MarkerBag();
        this.cardStackOfGame = new TargetCardStack();
	    // fill in the standard set of available rewards for the game
        this.availableRewards = Reward.createStandardRewards();
        // draw a number of target cards (GameConstants.NUMBER_OF_DRAWABLE_TARGET_CARDS) 
	    // from deck and put them "in the middle of the table"
	    this.drawableTargetCards = new ArrayList<TargetCard>(); // TODO: should probably better be an array of size GameConstants.NUMBER_OF_DRAWABLE_TARGET_CARDS
	    for (int i = 0; i < GameConstants.NUMBER_OF_DRAWABLE_TARGET_CARDS; i++) {
	    	this.raisePositionInTargetCardStack();
	        this.drawableTargetCards.add(this.cardStackOfGame.removeTopCard(this.positionInTargetCardStack));
	    }
	    // TODO: an dieser Stelle muesste eigentlich auch die Anzahl der verfuegbaren Legionen der Spieler ausgeteilt werden
	    // this.status = GameStatus.INITIALIZED;
	}
	
	public void run() {
        this.status = GameStatus.RUNNING;
	}

	/*
	public void run() {
	    // sicherstellen, dass alle Spieler ihre Startkarten bekommen haben bzw.
	    // ihnen Karten austeilen
		this.status = GameStatus.RUNNING;
	}
	*/
	
	public void delete() {
	    this.markerBagOfGame = null;
	    this.cardStackOfGame = null;
        this.drawableTargetCards = null;
        this.availableRewards = null;
        this.players = null;
	    this.status = GameStatus.FINISHED;
	}

	public Integer getCurrentPlayer() {
		return currentPlayer;
	}

	public void setCurrentPlayer(Integer currentPlayer) {
		this.currentPlayer = currentPlayer;
	}
   
	public User getNextPlayer() {
		if (players.isEmpty())
		{
			return null;
		}
		else
		{
			return getPlayers().get((getCurrentPlayer() + 1) % getPlayers().size());
		}
	}

	public List<TargetCard> getDrawableTargetCards() {
		return this.drawableTargetCards;
	}

	// TODO: should probably not be allowed, unless when a target card gets controlled
	public void addDrawableCard() /*throws Exception*/ {
	    // TODO: make sure that inserted target cards have status UNASSIGNED
	    
        // TODO: make sure that it is not possible to insert more drawable target cards than are totally allowed (as specified by constant GameConstants.NUMBER_OF_DRAWABLE_TARGET_CARDS)
	    //if (this.drawableTargetCards.size() == GameConstants.NUMBER_OF_DRAWABLE_TARGET_CARDS) {
	    //    throw new Exception("Error: addDrawableCard(): it is not allowed to have more than " + GameConstants.NUMBER_OF_DRAWABLE_TARGET_CARDS + " target cards which would have surpassed by this operation and was thus denied.");
	    //} else {
		TargetCard cardToAdd = null;
		this.raisePositionInTargetCardStack();
		cardToAdd = cardStackOfGame.removeTopCard(this.positionInTargetCardStack);
		logger.debug("adding new card to drawableTargetCards: " + cardToAdd.getNumber());
	    drawableTargetCards.add(cardToAdd);	        
	    //}
	}

    public void removeDrawableCard(Integer removeNumber) {
    	List<TargetCard> tmpDrawableCards = new ArrayList<TargetCard>();
    	for(TargetCard tmpDrawCard : this.drawableTargetCards) {
    		if(!tmpDrawCard.getNumber().equals(removeNumber)) {
    			tmpDrawableCards.add(tmpDrawCard);
    		}
    	}
    	this.drawableTargetCards = tmpDrawableCards;
    }
    
    public TargetCard getCardFromCardStack() {
    	this.raisePositionInTargetCardStack();
        return this.cardStackOfGame.removeTopCard(this.positionInTargetCardStack);
    }
	
	public MarkerBag getMarkerBag() {
		return markerBagOfGame;
	}
	
	public void getTargetCardStack(TargetCardStack tstack) {
		this.cardStackOfGame = tstack;
	}
	
	public TargetCardStack getTargetCardStack() {
		return cardStackOfGame;
	}
	
	// TODO: refactor
	public String getLastDrawnMarker() {
	    return lastDrawnMarker;
	}
	
	// TODO: this method is evil; refactor and delete
	public void setLastDrawnMarker(String lastDrawnMarker) {
	    this.lastDrawnMarker = lastDrawnMarker;
	}

	/*
	public Marker getLastDrawnMarker() {
	    if (this.markerBagOfGame == null) {
	        return null;
	    } else {
	        return this.markerBagOfGame.getLastDrawnMarker();	        
	    }
    }
    */
	
	public void addMove(Move move) {
		this.moves.add(move);
	}

	public List<Reward> getAvailableRewards() {
		return this.availableRewards;
	}

	public List<Reward> getAvailableRewards(RewardType type) {
		//TODO: check whether this.availableRewards == null; if so throw exception
		List<Reward> rewardList = new ArrayList<Reward>();
		for (Reward myReward : this.availableRewards) {
			if (myReward.getType() == type) {
				rewardList.add(myReward);
			}
		}
		return rewardList;
	}

	/*
	 * Returns a list of all rewards available in a game that are automatically assigned to a eligible player; 
	 * this includes rewards that have never been drawn until now (in this.availableRewards) as well as movable rewards hold by players in the game
	 * 
	 * @pre		this.availableRewards != null && this.players != null
	 * @post	
	 * @param	
	 * @return	a List of all available rewards that are automatically assigned
	 */
	public List<Reward> getAllAvailableAutoRewards() {
		//TODO: check whether this.availableRewards == null; if so throw exception; same for this.players
		// TODO: try-catch structure
		
		List<Reward> rewardList = new ArrayList<Reward>();
		// iterate over all rewards of the game that have never been drawn until now and add them to the rewards list
		for (Reward myReward : this.availableRewards) {
			if (myReward.isAuto()) {
				rewardList.add(myReward);
			}
		}
		// iterate over all movable rewards that are already hold by players
		for (User aPlayer : this.players) {
			if(aPlayer.getRewardsHold() != null) {
			for (Reward aReward : aPlayer.getRewardsHold()) {
				if (aReward.isMovable() && aReward.isAuto()) {
					rewardList.add(aReward);
				}
			}
			}
		}
		// return complete list
		return rewardList;
	}

	public List<Reward> getAvailableNonAutoRewards() {
		//TODO: check whether this.availableRewards == null; if so throw exception
		List<Reward> rewardList = new ArrayList<Reward>();
		for (Reward myReward : this.availableRewards) {
			if (!myReward.isAuto()) {
				rewardList.add(myReward);
			}
		}
		return rewardList;
	}
	
	// set Reward to assigned=true and remove it from availableRewards 
	public void removeReward(Reward rewardToRemove) {
		rewardToRemove.setIsAssigned(true);
		this.availableRewards.remove(rewardToRemove);
	}
	
	/*
	 * Checks whether the reward given as argument is still available for this game and returns true iff this is the case
	 * 
	 * @pre		this.availableRewards != null
	 * @post	
	 * @param	
	 * @return	
	 */
	public Boolean isAvailable(Reward myReward) { // TODO: was passiert / sollte passieren, wenn als Argument null uebergeben wird?
		//TODO: check that this.availableRewards != null
		if (this.availableRewards.contains(myReward)) { // Vorsicht: .contains() prueft auf Referenz... TODO: test, if necessary re-implement
			return true;
		} else {
			return false;
		}
	}
	
	/*
	 * Determins the player in the game that has currently the maximum number of points
	 * 
	 * @pre		this.players != null
	 * @post	
	 * @param	
	 * @return	
	 */
	public User getUserWithMaximumPoints() throws Exception {
	    if (this.players == null) {
	        throw new Exception("Error: getUserWithMaximumPoints(): Tried to get player with maximum points for a game that has no users.");
	    }
		User playerWithMaximumPoints = null;
		Integer maximumNumberOfPoints = 0;
		for (User aPlayer : this.players) {
			if (aPlayer.getPoints() >= maximumNumberOfPoints) {
				playerWithMaximumPoints = aPlayer;
			}
		}
		return playerWithMaximumPoints;
	}

	public void setEndRoundMessage(String endRoundMessage) {
		this.endRoundMessage = endRoundMessage;
	}
	
	public String getEndRoundMessage() {
		return endRoundMessage;
	}
	
	public void raisePositionInTargetCardStack() {
		++positionInTargetCardStack;
	}
	
	public Integer getPositionInTargetCardStack() {
		return this.positionInTargetCardStack;
	}
	
	public void setFlagForNegativeEffect(Boolean flag) {
		this.flagForNegativeEffect = flag;
	}
	
	public Boolean getFlagForNegativeEffect() {
		return this.flagForNegativeEffect;
	}
}
