package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

public class RewardControlOfCommodities extends Reward implements Serializable {	
			private static final long serialVersionUID = 1L;
			
			private final Commodity commodityToControl;

			public RewardControlOfCommodities(Long id, String description, RewardType type, Integer points, Commodity commodityToControl) {
				this.id = id;
				this.description = description;
				this.type = type;
				this.points = points;
				this.isAuto = true;
				this.isMovable = true;
				this.commodityToControl = commodityToControl; 
				this.isAssigned = false;
			}
			
			public Boolean isEligible(User anUser, Game aGame) {
				// identify maximum number of commodities controlled by a player
				Integer maximumNumberOfCommodities = 0;
				for (User aPlayer : aGame.getPlayers()) {
					Integer numberOfCommoditiesOfCurrentPlayer = aPlayer.getNumberOfCommodities(this.commodityToControl);
					if (numberOfCommoditiesOfCurrentPlayer > maximumNumberOfCommodities) {
						maximumNumberOfCommodities = numberOfCommoditiesOfCurrentPlayer;
					}
				}
				// player is eligible if he has at least one commodity and number of commodities >= maximum number of commodities
				Integer commoditiesOfUser = anUser.getNumberOfCommodities(this.commodityToControl);
				return (commoditiesOfUser > 0 && commoditiesOfUser >= maximumNumberOfCommodities);
			}

}
