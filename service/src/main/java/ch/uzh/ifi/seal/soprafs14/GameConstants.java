package ch.uzh.ifi.seal.soprafs14;

public final class GameConstants {

	// hide utility class public default constructor
	private GameConstants() {
	}

	
	/* Constants for all games */
	
	public static final Integer MIN_PLAYERS = 2;
	public static final Integer MAX_PLAYERS = 6;
	
	// Anzahl, der frei verfügbaren / wählbaren Zielkarten (im physischen Spiel in der Mitte des Tisches)
	public static final Integer NUMBER_OF_DRAWABLE_TARGET_CARDS = 5;
	
	// Anzahl der Zielkarten; TODO: sollte vermutlich eher dynamisch pro Spiel anhand des Konstruktors gespeichert werden
	public static final Integer NUMBER_OF_TARGET_CARDS = 88;
	
	
	
	/* Defaults which could be overwritten for a specific game */
	
	// Default-Anzahl der Zielkarten, die ein Spieler kontrollieren muss, um ein Spiel zu gewinnen
	public static final Integer DEFAULT_NUMBER_OF_TARGETCARDS_FOR_VICTORY = 7;

	// Default-Anzahl der Zielkarten, die einem Spieler bei Spielbeginn zur Auswahl gestellt wird
	public static final Integer DEFAULT_NUMBER_OF_STARTCARDS_HANDOUT = 6;

	// Default-Anzahl der Zielkarten, welche ein Spieler von den DEFAULT_NUMBER_OF_STARTCARDS_HANDOUT Karten, die er zunaechst erhalten hat, wieder abgeben muss
	public static final Integer DEFAULT_NUMBER_OF_STARTCARDS_HANDBACK = 3;
	
	// Default-Anzahl der Legionen, ueber die ein Spieler bei Spielbeginn verfuegt
	public static final Integer DEFAULT_NUMBER_OF_LEGIONS_START = 7;

}
