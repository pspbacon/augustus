package ch.uzh.ifi.seal.soprafs14.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.uzh.ifi.seal.soprafs14.model.CardType;
import ch.uzh.ifi.seal.soprafs14.model.Commodity;
import ch.uzh.ifi.seal.soprafs14.model.Effect;
import ch.uzh.ifi.seal.soprafs14.model.EffectImmediate;
import ch.uzh.ifi.seal.soprafs14.model.EffectImmediateIncreaseLegions;
import ch.uzh.ifi.seal.soprafs14.model.EffectImmediateMobilizeLegions;
import ch.uzh.ifi.seal.soprafs14.model.EffectImmediateRemoveLegions;
import ch.uzh.ifi.seal.soprafs14.model.EffectPersistent;
import ch.uzh.ifi.seal.soprafs14.model.EffectType;
import ch.uzh.ifi.seal.soprafs14.model.ImmediateEffectCategory;
import ch.uzh.ifi.seal.soprafs14.model.Marker;
import ch.uzh.ifi.seal.soprafs14.model.ProvinceCard;
import ch.uzh.ifi.seal.soprafs14.model.SenatorCard;
import ch.uzh.ifi.seal.soprafs14.model.TargetCard;
import ch.uzh.ifi.seal.soprafs14.model.VictoryPointsFixed;
import ch.uzh.ifi.seal.soprafs14.model.VictoryPointsVariableCardType;
import ch.uzh.ifi.seal.soprafs14.model.VictoryPointsVariableEffects;
import ch.uzh.ifi.seal.soprafs14.model.VictoryPointsVariableMarker;

// initializes a fresh cardstack that is shuffled
// and has a method that returns the card on top of the stack
@Service
@Entity
public class TargetCardStack implements Serializable {

    private static final long serialVersionUID = 1L;
        
    @Id
    @GeneratedValue
    private Long id;

    private List<TargetCard> cardsStack =  new ArrayList<TargetCard>();
    
    public TargetCardStack() {
        // Create commodities
        List<Commodity> commoditiesGoldOnly = Arrays.asList(Commodity.GOLD);
        List<Commodity> commoditiesWheatOnly = Arrays.asList(Commodity.WHEAT);
        List<Commodity> commoditiesGoldAndWheat = Arrays.asList(Commodity.GOLD, Commodity.WHEAT);
        
        // Create specific marker-configurations for legionFields
        List<Marker> marker1 = Arrays.asList(Marker.SCHWERTER, Marker.SCHILD)/* 1,2 */;
        List<Marker> marker2 = Arrays.asList(Marker.SCHWERTER, Marker.SCHWERTER, Marker.SCHWERTER, Marker.SCHILD)/* 3,4 */;
        List<Marker> marker3 = Arrays.asList(Marker.SCHWERTER, Marker.SCHWERTER, Marker.STREITWAGEN)/* 5,6 */;
        List<Marker> marker4 = Arrays.asList(Marker.SCHWERTER, Marker.SCHILD, Marker.STREITWAGEN)/* 7,8 */;
        List<Marker> marker5 = Arrays.asList(Marker.SCHWERTER, Marker.SCHWERTER, Marker.SCHILD, Marker.SCHILD)/* 9,10 */;
        List<Marker> marker6 = Arrays.asList(Marker.SCHWERTER, Marker.SCHILD, Marker.STREITWAGEN)/* 11,12 */;
        List<Marker> marker7 = Arrays.asList(Marker.SCHWERTER, Marker.SCHWERTER, Marker.SCHWERTER, Marker.KATAPULT)/* 13,14 */;
        List<Marker> marker8 = Arrays.asList(Marker.SCHILD, Marker.SCHILD, Marker.STREITWAGEN)/* 15,16 */;
        List<Marker> marker9 = Arrays.asList(Marker.SCHWERTER, Marker.SCHWERTER, Marker.SCHILD, Marker.SCHILD, Marker.SCHILD)/* 17,18 */;
        List<Marker> marker10 = Arrays.asList(Marker.SCHWERTER, Marker.SCHWERTER, Marker.SCHILD, Marker.STANDARTE)/* 19,20 */;
        List<Marker> marker11 = Arrays.asList(Marker.SCHWERTER, Marker.SCHWERTER, Marker.SCHILD, Marker.STREITWAGEN, Marker.STREITWAGEN)/* 21,22,23,24,37,38 */;
        List<Marker> marker12 = Arrays.asList(Marker.SCHWERTER, Marker.SCHILD, Marker.STREITWAGEN, Marker.KATAPULT)/* 25,26,27,28,35,36 */;
        List<Marker> marker13 = Arrays.asList(Marker.SCHILD, Marker.SCHILD, Marker.SCHILD, Marker.KATAPULT)/* 29,30 */;
        List<Marker> marker14 = Arrays.asList(Marker.SCHILD, Marker.SCHILD, Marker.STREITWAGEN, Marker.STREITWAGEN)/* 31,32 */;
        List<Marker> marker15 = Arrays.asList(Marker.SCHWERTER, Marker.SCHWERTER, Marker.SCHILD, Marker.DOLCH)/* 33,34 */;
        List<Marker> marker16 = Arrays.asList(Marker.SCHWERTER, Marker.SCHWERTER, Marker.SCHILD, Marker.STREITWAGEN, Marker.KATAPULT)/* 39,40,41,42 */;
        List<Marker> marker17 = Arrays.asList(Marker.SCHWERTER, Marker.SCHILD, Marker.STREITWAGEN, Marker.STANDARTE)/* 43,44,45,46 */;
        List<Marker> marker18 = Arrays.asList(Marker.SCHILD, Marker.SCHILD, Marker.KATAPULT, Marker.KATAPULT)/* 47,48 */;
        List<Marker> marker19 = Arrays.asList(Marker.SCHWERTER, Marker.SCHWERTER, Marker.SCHILD, Marker.SCHILD, Marker.STREITWAGEN, Marker.KATAPULT)/* 49,50 */;
        List<Marker> marker20 = Arrays.asList(Marker.STREITWAGEN, Marker.STREITWAGEN, Marker.STREITWAGEN, Marker.KATAPULT)/* 51,52 */;
        List<Marker> marker21 = Arrays.asList(Marker.SCHILD, Marker.STREITWAGEN, Marker.KATAPULT, Marker.STANDARTE)/* 53,54 */;
        List<Marker> marker22 = Arrays.asList(Marker.STREITWAGEN, Marker.STREITWAGEN, Marker.KATAPULT, Marker.KATAPULT)/* 55,56 */;
        List<Marker> marker23 = Arrays.asList(Marker.SCHWERTER, Marker.SCHILD, Marker.STREITWAGEN, Marker.KATAPULT, Marker.STANDARTE)/* 57,58 */;
        List<Marker> marker24 = Arrays.asList(Marker.STREITWAGEN, Marker.KATAPULT, Marker.KATAPULT, Marker.KATAPULT)/* 59,60 */;
        List<Marker> marker25 = Arrays.asList(Marker.SCHWERTER, Marker.SCHWERTER, Marker.SCHILD, Marker.DOLCH, Marker.DOLCH)/* 61,62 */;
        List<Marker> marker26 = Arrays.asList(Marker.SCHWERTER, Marker.SCHILD, Marker.STREITWAGEN, Marker.KATAPULT, Marker.DOLCH)/* 63,64 */;
        List<Marker> marker27 = Arrays.asList(Marker.SCHILD, Marker.KATAPULT, Marker.STANDARTE, Marker.DOLCH)/* 65,66 */;
        List<Marker> marker28 = Arrays.asList(Marker.SCHILD, Marker.STREITWAGEN, Marker.STREITWAGEN, Marker.KATAPULT, Marker.STANDARTE)/* 67,68 */;
        List<Marker> marker29 = Arrays.asList(Marker.SCHWERTER, Marker.SCHWERTER, Marker.STREITWAGEN, Marker.STREITWAGEN, Marker.KATAPULT, Marker.STANDARTE)/* 69,70 */;
        List<Marker> marker30 = Arrays.asList(Marker.SCHWERTER, Marker.SCHILD, Marker.KATAPULT, Marker.STANDARTE, Marker.DOLCH)/* 71,72 */;
        List<Marker> marker31 = Arrays.asList(Marker.KATAPULT, Marker.KATAPULT, Marker.STANDARTE, Marker.STANDARTE)/* 73,74 */;
        List<Marker> marker32 = Arrays.asList(Marker.SCHWERTER, Marker.SCHWERTER, Marker.STANDARTE, Marker.STANDARTE, Marker.DOLCH)/* 75,76 */;
        List<Marker> marker33 = Arrays.asList(Marker.SCHILD, Marker.SCHILD, Marker.STREITWAGEN, Marker.STREITWAGEN, Marker.KATAPULT, Marker.STANDARTE)/* 77,78 */;
        List<Marker> marker34 = Arrays.asList(Marker.SCHWERTER, Marker.SCHWERTER, Marker.SCHILD, Marker.STANDARTE, Marker.STANDARTE, Marker.DOLCH)/* 79,80 */;
        List<Marker> marker35 = Arrays.asList(Marker.SCHWERTER, Marker.SCHILD, Marker.STREITWAGEN, Marker.KATAPULT, Marker.STANDARTE, Marker.DOLCH)/* 81,82 */;
        List<Marker> marker36 = Arrays.asList(Marker.SCHILD, Marker.KATAPULT, Marker.STANDARTE,Marker.STANDARTE, Marker.DOLCH)/* 83,84 */;
        List<Marker> marker37 = Arrays.asList(Marker.STANDARTE, Marker.STANDARTE, Marker.DOLCH, Marker.DOLCH)/* 85,86 */;
        List<Marker> marker38 = Arrays.asList(Marker.STANDARTE, Marker.STANDARTE, Marker.STANDARTE, Marker.DOLCH, Marker.DOLCH) /* 87,88 */;
        
        // Sonar-Qube-Iterator-Konstante, um von Magic Numbers abzulenken
        Integer effId = 0;
        
        // Create Effects
        /* Every card needs its own instance for effect
         * First store the effect ids with their corresponding meanings
         * before deleting them here
         */
        Effect effect01 = new Effect(effId++, "Schwerter = 1 (max 6)", EffectType.POINT);
        Effect effect02 = new Effect(effId++, "Schild =  1 (max 8)", EffectType.POINT);
        Effect effect03 = new Effect(effId++, "Streitwagen =  2 (max 10)", EffectType.POINT);
        Effect effect04 = new Effect(effId++, "Katapult =  2 (max 12)", EffectType.POINT);
        Effect effect05 = new Effect(effId++, "Europa =  2", EffectType.POINT);
        Effect effect06 = new Effect(effId++, "Senator = 2", EffectType.POINT);
        Effect effect07 = new Effect(effId++, "Kleinasien = 4", EffectType.POINT);
        Effect effect08 = new Effect(effId++, "Standarte = 3 (max 15)", EffectType.POINT);
        Effect effect09 = new Effect(effId++, "Dolch = 4 (max 20)", EffectType.POINT);
        Effect effect10 = new Effect(effId++, "Afrika =  6", EffectType.POINT);
        Effect effect11 = new Effect(effId++, "Rote Karte = 5", EffectType.POINT);

        /*
         * TODO (Important): Careful implement one effect at a time
         * 
         * Especially effects, that enable more possibilities for assigning legions should be very difficult
         * -> Idea: iterate through controlled cards before assigning open fields for assignment
         */
        // Persistente Effekte
        EffectPersistent effect20 = new EffectPersistent(20, "Schwerter = Schild", Marker.SCHWERTER, Marker.SCHILD) /* Zielkarten 9 und 10 */;
        EffectPersistent effect21 = new EffectPersistent(21, "Schild = Streitwagen", Marker.SCHILD, Marker.STREITWAGEN) /* Zielkarten 31 und 32 */;
        EffectPersistent effect22 = new EffectPersistent(22, "Streitwagen = Katapult", Marker.STREITWAGEN, Marker.KATAPULT) /* Zielkarten 55 und 56 */;
        EffectPersistent effect23 = new EffectPersistent(23, "Katapult = Standarte", Marker.KATAPULT, Marker.STANDARTE) /* Zielkarten 73 und 74 */;
        EffectPersistent effect24 = new EffectPersistent(24, "Standarte = Dolch", Marker.STANDARTE, Marker.DOLCH) /* Zielkarten 85 und 86 */;

        /*
         * TODO: player should get to playing field activity (or better to similar activity)
         * with 2 legions that can be moved and from there back to the endOfRound-activity
         * 
         * Everyone of these might take some effort, but shouldn't be too difficult
         * 
         * Undoing operations are annoying, but as the controller should be able to handle this
         */
        // Mobilisierung von Legionen auf bestimmte Felder
        EffectImmediateMobilizeLegions effect30 = new EffectImmediateMobilizeLegions(30, "Plus 2 Legionen auf Schwerter", 2, Marker.SCHWERTER);
        EffectImmediateMobilizeLegions effect31 = new EffectImmediateMobilizeLegions(31, "Plus 2 Legionen auf Standarte", 2, Marker.STANDARTE);
        EffectImmediateMobilizeLegions effect32 = new EffectImmediateMobilizeLegions(32, "Plus 2 Legionen auf Schild", 2, Marker.SCHILD);
        EffectImmediateMobilizeLegions effect33 = new EffectImmediateMobilizeLegions(33, "Plus 2 Legionen auf Dolch", 2, Marker.DOLCH);
        EffectImmediateMobilizeLegions effect34 = new EffectImmediateMobilizeLegions(34, "Plus 2 Legionen auf Streitwagen", 2, Marker.STREITWAGEN);
        EffectImmediateMobilizeLegions effect35 = new EffectImmediateMobilizeLegions(35, "Plus 2 Legionen auf Katapult", 2, Marker.KATAPULT);
        // Mobilisierung von Legionen auf beliebige Felder
        EffectImmediateMobilizeLegions effect41 = new EffectImmediateMobilizeLegions(41, "Plus 1 Legion auf beliebiges Feld", 1, Marker.JOKER);
        EffectImmediateMobilizeLegions effect42 = new EffectImmediateMobilizeLegions(42, "Plus 2 Legionen auf beliebiges Feld", 2, Marker.JOKER);

        // Freier Wechsel eines eingesetzten Legionärs (Zielkarten 43 und 44)
        EffectImmediate effect50 = new EffectImmediate(50, "Freier Wechsel eines eingesetzten Legionärs", ImmediateEffectCategory.REDISTRIBUTE_LEGIONS);
        // Zusätzliche Karte sofort fertig (Zielkarten 87 und 88)
        EffectImmediate effect51 = new EffectImmediate(51, "Eine Zielkarte des Kartenbesitzers wird sofort erfüllt", ImmediateEffectCategory.CARD_COMPLETION);
        // Kartenbesitzer erhält eine zusätzliche Zielkarte, die er erobern kann (Zielkarten 35 und 36)
        EffectImmediate effect52 = new EffectImmediate(52, "Der Kartenbesitzer erhält eine zusätzliche Zielkarte", ImmediateEffectCategory.ADD_TARGETCARD);

        // Kartenbesitzer erhält zusätzliche Legionen für seinen Vorrat
        EffectImmediateIncreaseLegions effect60 = new EffectImmediateIncreaseLegions(60, "Plus 1 Legion", 1) /* Zielkarten 5 und 6 */;
        EffectImmediateIncreaseLegions effect61 = new EffectImmediateIncreaseLegions(61, "Plus 2 Legionen", 2) /* Zielkarten 15 und 16 */;

        // Gegenspieler verlieren mobilisierte Legionen
        EffectImmediateRemoveLegions effect80 = new EffectImmediateRemoveLegions(80, "Minus 1 Legion alle anderen Spieler", 1) /* Zielkarten 37 und 38 */;
        EffectImmediateRemoveLegions effect81 = new EffectImmediateRemoveLegions(81, "Minus 2 Legionen alle anderen Spieler", 2) /* Zielkarten 41 und 42 */;

        // Gegenspieler muss umgehend alle mobilisierten Legionen von einer beliebigen seiner Zielkarten entfernen (Zielkarten 75 und 76)
        EffectImmediate effect70 = new EffectImmediate(70, "alle mobilisierten Legionen von einer beliebigen Zielkarten entfernen", ImmediateEffectCategory.REMOVE_ALL_LEGIONS);

        // Gegenspieler verlieren eine eroberte Zielkarte (aka the devil's effect; Zielkarten 81 und 82)
        EffectImmediate effect71 = new EffectImmediate(71, "Alle anderen Spieler verlieren eine eroberte Karte", ImmediateEffectCategory.CARD_LOSS);

        //TODO: Definitionen evtl. in statische Methode einer anderen Klasse auslagern, um Sonar Qube eine Freude zu machen
        
        // Erzeugen der Karten    //TODO: pruefen, ob die Karten mit denjenigen des Spiels uebereinstimmen
        TargetCard targetCard01 = new SenatorCard(1, "Gaius Antonius Scipio", new VictoryPointsFixed(3), marker1, null);
        TargetCard targetCard02 = new ProvinceCard(2, "Italia", CardType.GREEN, null, new VictoryPointsFixed(3), marker1, null);
        TargetCard targetCard03 = new ProvinceCard(3, "Asia", CardType.VIOLET, null, new VictoryPointsFixed(6), marker2, null);
        TargetCard targetCard04 = new SenatorCard(4, "Julius Aurelius Longus", new VictoryPointsFixed(6), marker2, null);
        TargetCard targetCard05 = new ProvinceCard(5, "Sicilia", CardType.GREEN, commoditiesWheatOnly, new VictoryPointsFixed(3), marker3, effect60/*plus 1 Legion für den Vorrat*/);
        TargetCard targetCard06 = new SenatorCard(6, "Marcus Calpurnius Maximus", new VictoryPointsFixed(3), marker3, effect60/*plus 1 Legion für den Vorrat*/);
        TargetCard targetCard07 = new ProvinceCard(7, "Corsica et Sardinia", CardType.GREEN, commoditiesWheatOnly, new VictoryPointsVariableMarker(1,Marker.SCHWERTER,6), marker4,
                new Effect(1, "Schwerter = 1 (max 6)", EffectType.POINT));
        TargetCard targetCard08 = new SenatorCard(8, "Sosius Postumius Merula", new VictoryPointsVariableMarker(1,Marker.SCHILD,8), marker4, 
                new Effect(1, "Schwerter = 1 (max 6)", EffectType.POINT));
        TargetCard targetCard09 = new ProvinceCard(9, "Alpes Phoeniae", CardType.GREEN, null, new VictoryPointsFixed(3), marker5, effect20/*Schwerter = Schild*/);
        TargetCard targetCard10 = new ProvinceCard(10, "Bithynia", CardType.VIOLET, commoditiesWheatOnly, new VictoryPointsFixed(3), marker5, effect20/*Schwerter = Schild*/);
        TargetCard targetCard11 = new SenatorCard(11, "Lucius Cladius Philo", new VictoryPointsVariableMarker(1,Marker.SCHILD,8), marker6, 
                new Effect(2, "Schild =  1 (max 8)", EffectType.POINT));
        TargetCard targetCard12 = new ProvinceCard(12, "Alpes Cottiae", CardType.GREEN, null, new VictoryPointsVariableMarker(1,Marker.SCHILD,8), marker6, 
                new Effect(2, "Schild =  1 (max 8)", EffectType.POINT));
        TargetCard targetCard13 = new SenatorCard(13, "Publius Domitius Fidenas", new VictoryPointsFixed(3), marker7, effect30/*plus 2 Legionen auf Schwerter*/);
        TargetCard targetCard14 = new ProvinceCard(14, "Alpes Maritimae", CardType.GREEN, null, new VictoryPointsFixed(3), marker7, effect30/*plus 2 Legionen auf Schwerter*/);
        TargetCard targetCard15 = new SenatorCard(15, "Titus Fabius Flaccinator", new VictoryPointsFixed(3), marker8, effect61/*plus 2 Legionen für den Vorrat*/);
        TargetCard targetCard16 = new ProvinceCard(16, "Hispania Baleares", CardType.GREEN, null, new VictoryPointsFixed(3), marker8, effect61/*plus 2 Legionen für den Vorrat*/);
        TargetCard targetCard17 = new ProvinceCard(17, "Galatia", CardType.VIOLET, null, new VictoryPointsFixed(8), marker9, null);
        TargetCard targetCard18 = new SenatorCard(18, "Aulus Giulius Vulso", new VictoryPointsFixed(8), marker9, null);
        TargetCard targetCard19 = new ProvinceCard(19, "Hispania Baetica", CardType.GREEN, null, new VictoryPointsFixed(3), marker10, effect31/*plus 2 Legionen auf Standarte*/);
        TargetCard targetCard20 = new SenatorCard(20, "Valerius Manlius Pansa", new VictoryPointsFixed(3), marker10, effect31/*plus 2 Legionen auf Standarte*/);
        TargetCard targetCard21 = new ProvinceCard(21, "Lycaonia", CardType.VIOLET, null, new VictoryPointsFixed(9), marker11, null);
        TargetCard targetCard22 = new SenatorCard(22, "Sextus Ottavius Curio", new VictoryPointsFixed(9), marker11, null);
        TargetCard targetCard23 = new ProvinceCard(23, "Hispania Lusitania", CardType.GREEN, null, new VictoryPointsFixed(10), marker11, null);
        TargetCard targetCard24 = new ProvinceCard(24, "Syria", CardType.ORANGE, commoditiesGoldOnly, new VictoryPointsFixed(10), marker11, null);
        TargetCard targetCard25 = new SenatorCard(25, "Tiberius Pompeus Cursor", new VictoryPointsVariableMarker(2,Marker.STREITWAGEN,10), marker12,
                new Effect(3, "Streitwagen =  2 (max 10)", EffectType.POINT));
        TargetCard targetCard26 = new ProvinceCard(26, "Hispania Tarraconesis", CardType.GREEN, commoditiesGoldOnly, new VictoryPointsVariableMarker(2,Marker.STREITWAGEN,10), marker12,
                new Effect(3, "Streitwagen =  2 (max 10)", EffectType.POINT));
        TargetCard targetCard27 = new SenatorCard(27, "Drusus Sempronius Violens", new VictoryPointsVariableMarker(3,Marker.KATAPULT,12), marker12,
                new Effect(4, "Katapult =  2 (max 12)", EffectType.POINT));
        TargetCard targetCard28 = new ProvinceCard(28, "Lycia", CardType.VIOLET, null, new VictoryPointsVariableMarker(3,Marker.KATAPULT,12), marker12,
                new Effect(4, "Katapult =  2 (max 12)", EffectType.POINT));
        TargetCard targetCard29 = new SenatorCard(29, "Faustus Sergius Caudex", new VictoryPointsFixed(4), marker13, effect32/*plus 2 Legionen auf Schild*/);
        TargetCard targetCard30 = new ProvinceCard(30, "Gallia Narbonensis", CardType.GREEN, null, new VictoryPointsFixed(4), marker13, effect32/*plus 2 Legionen auf Schild*/);
        TargetCard targetCard31 = new ProvinceCard(31, "Pisidia", CardType.VIOLET, null, new VictoryPointsFixed(4), marker14, effect21/*Schild = Streitwagen*/);
        TargetCard targetCard32 = new SenatorCard(32, "Italicus Curtius Nerva", new VictoryPointsFixed(4), marker14, effect21/*Schild = Streitwagen*/);
        TargetCard targetCard33 = new ProvinceCard(33, "Gallia Aquitania", CardType.GREEN, null, new VictoryPointsFixed(5), marker15, effect33/*plus 2 Legionen auf Dolch*/);
        TargetCard targetCard34 = new SenatorCard(34, "Appius Horatius Libo", new VictoryPointsFixed(5), marker15, effect33/*plus 2 Legionen auf Dolch*/);
        TargetCard targetCard35 = new ProvinceCard(35, "Pamphylia", CardType.VIOLET, null, new VictoryPointsFixed(5), marker12, effect52/*der Kartenbesitzer erhält eine zusätzliche Zielkarte*/);
        TargetCard targetCard36 = new SenatorCard(36, "Cirillus Sandrus Asameo", new VictoryPointsFixed(5), marker12, effect52/*der Kartenbesitzer erhält eine zusätzliche Zielkarte*/);
        TargetCard targetCard37 = new ProvinceCard(37, "Gallia Lugdunensis", CardType.GREEN, null, new VictoryPointsFixed(6), marker11, effect80/*minus 1 Legion alle anderen Spieler*/);
        TargetCard targetCard38 = new ProvinceCard(38, "Judaea", CardType.ORANGE, null, new VictoryPointsFixed(6), marker11, effect80/*minus 1 Legion alle anderen Spieler*/);
        TargetCard targetCard39 = new SenatorCard(39, "Atilius Ovidius Canina", new VictoryPointsFixed(11), marker16, null);
        TargetCard targetCard40 = new ProvinceCard(40, "Gallia Belgica", CardType.GREEN, commoditiesWheatOnly, new VictoryPointsFixed(11), marker16, null);
        TargetCard targetCard41 = new SenatorCard(41, "Servilius Petronicus Tucca", new VictoryPointsFixed(2), marker16, effect81/*minus 2 Legionen alle anderen Spieler*/);
        TargetCard targetCard42 = new ProvinceCard(42, "Brittania", CardType.GREEN, null, new VictoryPointsFixed(2), marker16, effect81/*minus 2 Legionen alle anderen Spieler*/);
        TargetCard targetCard43 = new SenatorCard(43, "Ericus Vanus Derlindenus", new VictoryPointsFixed(2), marker17, effect50/*freier Wechsel eines eingesetzten Legionärs*/);
        TargetCard targetCard44 = new ProvinceCard(44, "Cyprus", CardType.VIOLET, null, new VictoryPointsFixed(2), marker17, effect50/*freier Wechsel eines eingesetzten Legionärs*/);
        TargetCard targetCard45 = new ProvinceCard(45, "Cilicia", CardType.VIOLET, commoditiesGoldOnly, new VictoryPointsFixed(6), marker17, effect41/*plus 1 Legion auf beliebiges Feld*/);
        TargetCard targetCard46 = new SenatorCard(46, "Pius Valerius Blasio", new VictoryPointsFixed(6), marker17, effect41/*plus 1 Legion auf beliebiges Feld*/);
        TargetCard targetCard47 = new ProvinceCard(47, "Germania Inferior", CardType.GREEN, null, new VictoryPointsVariableCardType(2,CardType.GREEN), marker18,
                new Effect(5, "Europa =  2", EffectType.POINT));
        TargetCard targetCard48 = new SenatorCard(48, "Fuscus Tullius Cotta", new VictoryPointsVariableCardType(2,CardType.SENATOR), marker18,
                new Effect(6, "Senator = 2", EffectType.POINT));
        TargetCard targetCard49 = new ProvinceCard(49, "Cappadocia", CardType.VIOLET, null, new VictoryPointsVariableCardType(4,CardType.VIOLET), marker19,
                new Effect(7, "Kleinasien = 4", EffectType.POINT));
        TargetCard targetCard50 = new SenatorCard(50, "Silius Lucretius Nero", new VictoryPointsFixed(12), marker19, null);
        TargetCard targetCard51 = new ProvinceCard(51, "Germania Superior", CardType.GREEN, null, new VictoryPointsFixed(5), marker20, effect34/*plus 2 Legionen auf Streitwagen*/);
        TargetCard targetCard52 = new ProvinceCard(52, "Arabia Petrea", CardType.ORANGE, null, new VictoryPointsFixed(5), marker20, effect34/*plus 2 Legionen auf Streitwagen*/);
        TargetCard targetCard53 = new SenatorCard(53, "Ducenius Tarquinius Gentho", new VictoryPointsVariableMarker(3,Marker.STANDARTE,15), marker21,
                new Effect(8, "Standarte = 3 (max 15)", EffectType.POINT));
        TargetCard targetCard54 = new ProvinceCard(54, "Germania Magna", CardType.GREEN, null, new VictoryPointsVariableMarker(3,Marker.STANDARTE,15), marker21,
                new Effect(8, "Standarte = 3 (max 15)", EffectType.POINT));
        TargetCard targetCard55 = new SenatorCard(55, "Alpinus Sulpicius Fullo", new VictoryPointsFixed(5), marker22, effect22/*Streitwagen = Katapult*/);
        TargetCard targetCard56 = new ProvinceCard(56, "Pontus", CardType.VIOLET, null, new VictoryPointsFixed(5), marker22, effect22/*Streitwagen = Katapult*/);
        TargetCard targetCard57 = new SenatorCard(57, "Claudius Nonius Salinator", new VictoryPointsFixed(13), marker23, null);
        TargetCard targetCard58 = new ProvinceCard(58, "Raetia", CardType.GREEN, null, new VictoryPointsFixed(13), marker23, null);
        TargetCard targetCard59 = new ProvinceCard(59, "Armenia Superior", CardType.VIOLET, null, new VictoryPointsFixed(7), marker24, effect35/*plus 2 Legionen auf Katapult*/);
        TargetCard targetCard60 = new SenatorCard(60, "Rufinus Lunius Purpureo", new VictoryPointsFixed(7), marker24, effect35/*plus 2 Legionen auf Katapult*/);
        TargetCard targetCard61 = new ProvinceCard(61, "Normicum", CardType.GREEN, null, new VictoryPointsFixed(14), marker25, null);
        TargetCard targetCard62 = new ProvinceCard(62, "Africa", CardType.ORANGE, null, new VictoryPointsFixed(14), marker25, null);
        TargetCard targetCard63 = new ProvinceCard(63, "Pannonia", CardType.GREEN, commoditiesGoldOnly, new VictoryPointsVariableMarker(4,Marker.DOLCH,20), marker26,
                new Effect(9, "Dolch = 4 (max 20)", EffectType.POINT));
        TargetCard targetCard64 = new SenatorCard(64, "Vibulius Camilius Glabrio", new VictoryPointsVariableEffects(5,EffectType.IMMEDIATELY), marker26,
                new Effect(11, "Rote Karte = 5", EffectType.POINT)) /* TODO: EffectType=Remove */;
        TargetCard targetCard65 = new ProvinceCard(65, "Dalmatia", CardType.GREEN, null, new VictoryPointsVariableMarker(4,Marker.DOLCH,20), marker27,
                new Effect(9, "Dolch = 4 (max 20)", EffectType.POINT));
        TargetCard targetCard66 = new ProvinceCard(66, "Sophene", CardType.VIOLET, null, new VictoryPointsVariableCardType(4,CardType.VIOLET), marker27,
                new Effect(7, "Kleinasien = 4", EffectType.POINT));
        TargetCard targetCard67 = new SenatorCard(67, "Eurycles Curtius Piso", new VictoryPointsVariableCardType(2,CardType.SENATOR), marker28,
                new Effect(6, "Senator = 2", EffectType.POINT));
        TargetCard targetCard68 = new ProvinceCard(68, "Dacia", CardType.GREEN, commoditiesGoldAndWheat, new VictoryPointsVariableCardType(2,CardType.GREEN), marker28,
                new Effect(5, "Europa =  2", EffectType.POINT));
        TargetCard targetCard69 = new ProvinceCard(69, "Mauretania", CardType.ORANGE, commoditiesGoldOnly, new VictoryPointsVariableCardType(6,CardType.ORANGE), marker29,
                new Effect(10, "Afrika =  6", EffectType.POINT));
        TargetCard targetCard70 = new ProvinceCard(70, "Moesia", CardType.GREEN, null, new VictoryPointsFixed(3), marker29, effect42/*plus 2 Legionen auf beliebiges Feld*/);
        TargetCard targetCard71 = new SenatorCard(71, "Proculus Galerius Sura", new VictoryPointsVariableEffects(5,EffectType.IMMEDIATELY), marker30,
                new Effect(11, "Rote Karte = 5", EffectType.POINT)) /* TODO: EffectType=Remove */;
        TargetCard targetCard72 = new ProvinceCard(72, "Thracia", CardType.GREEN, null, new VictoryPointsFixed(3), marker30, effect42/*plus 2 Legionen auf beliebiges Feld*/);
        TargetCard targetCard73 = new ProvinceCard(73, "Osroene", CardType.VIOLET, null, new VictoryPointsFixed(7), marker31, effect23/*Katapult = Standarte*/);
        TargetCard targetCard74 = new SenatorCard(74, "Severus Marcius Rex", new VictoryPointsFixed(7), marker31, effect23/*Katapult = Standarte*/);
        TargetCard targetCard75 = new ProvinceCard(75, "Macedonia", CardType.GREEN, null, new VictoryPointsFixed(5), marker32, effect70/*alle mobilisierten Legionen von einer beliebigen Zielkarten entfernen*/);
        TargetCard targetCard76 = new ProvinceCard(76, "Cyrenaica et Creta", CardType.ORANGE, null, new VictoryPointsFixed(5), marker32, effect70/*alle mobilisierten Legionen von einer beliebigen Zielkarten entfernen*/);
        TargetCard targetCard77 = new ProvinceCard(77, "Commagene", CardType.VIOLET, null, new VictoryPointsFixed(15), marker33, null);
        TargetCard targetCard78 = new SenatorCard(78, "Helvius Quinctus Cinna", new VictoryPointsFixed(15), marker33, null);
        TargetCard targetCard79 = new ProvinceCard(79, "Epirus", CardType.GREEN, null, new VictoryPointsFixed(16), marker34, null);
        TargetCard targetCard80 = new ProvinceCard(80, "Armenia Superior", CardType.VIOLET, null, new VictoryPointsFixed(16), marker34, null);
        TargetCard targetCard81 = new SenatorCard(81, "Quintus Cassius Elva", new VictoryPointsFixed(5), marker35, effect71/*alle anderen Spieler verlieren eine eroberte Karte*/);
        TargetCard targetCard82 = new ProvinceCard(82, "Assyria", CardType.VIOLET, null, new VictoryPointsFixed(5), marker35, effect71/*alle anderen Spieler verlieren eine eroberte Karte*/);
        TargetCard targetCard83 = new ProvinceCard(83, "Numidia", CardType.ORANGE, null, new VictoryPointsVariableCardType(6,CardType.ORANGE), marker36,
                new Effect(10, "Afrika =  6", EffectType.POINT));
        TargetCard targetCard84 = new ProvinceCard(84, "Achaia", CardType.GREEN, null, new VictoryPointsFixed(12), marker36, null);
        TargetCard targetCard85 = new SenatorCard(85, "Spartacus Locundus Ludens", new VictoryPointsFixed(10), marker37, effect24/*Standarte = Dolch*/);
        TargetCard targetCard86 = new ProvinceCard(86, "Mesopotamia", CardType.VIOLET, commoditiesWheatOnly, new VictoryPointsFixed(10), marker37, effect24/*Standarte = Dolch*/);
        TargetCard targetCard87 = new ProvinceCard(87, "Aegyptus", CardType.ORANGE, commoditiesGoldAndWheat, new VictoryPointsFixed(4), marker38, effect51/*eine Zielkarte des Kartenbesitzers wird sofort erfüllt*/);
        TargetCard targetCard88 = new SenatorCard(88, "Marilinus Lenus Menus", new VictoryPointsFixed(6), marker38, effect51/*eine Zielkarte des Kartenbesitzers wird sofort erfüllt*/);

        cardsStack.add(targetCard01);
        cardsStack.add(targetCard02);
        cardsStack.add(targetCard03);
        cardsStack.add(targetCard04);
        cardsStack.add(targetCard05);
        cardsStack.add(targetCard06);
        cardsStack.add(targetCard07);
        cardsStack.add(targetCard08);
        cardsStack.add(targetCard09);
        cardsStack.add(targetCard10);
        cardsStack.add(targetCard11);
        cardsStack.add(targetCard12);
        cardsStack.add(targetCard13);
        cardsStack.add(targetCard14);
        cardsStack.add(targetCard15);
        cardsStack.add(targetCard16);
        cardsStack.add(targetCard17);
        cardsStack.add(targetCard18);
        cardsStack.add(targetCard19);
        cardsStack.add(targetCard20);
        cardsStack.add(targetCard21);
        cardsStack.add(targetCard22);
        cardsStack.add(targetCard23);
        cardsStack.add(targetCard24);
        cardsStack.add(targetCard25);
        cardsStack.add(targetCard26);
        cardsStack.add(targetCard27);
        cardsStack.add(targetCard28);
        cardsStack.add(targetCard29);
        cardsStack.add(targetCard30);
        cardsStack.add(targetCard31);
        cardsStack.add(targetCard32);
        cardsStack.add(targetCard33);
        cardsStack.add(targetCard34);
        cardsStack.add(targetCard35);
        cardsStack.add(targetCard36);
        cardsStack.add(targetCard37);
        cardsStack.add(targetCard38);
        cardsStack.add(targetCard39);
        cardsStack.add(targetCard40);
        cardsStack.add(targetCard41);
        cardsStack.add(targetCard42);
        cardsStack.add(targetCard43);
        cardsStack.add(targetCard44);
        cardsStack.add(targetCard45);
        cardsStack.add(targetCard46);
        cardsStack.add(targetCard47);
        cardsStack.add(targetCard48);
        cardsStack.add(targetCard49);
        cardsStack.add(targetCard50);
        cardsStack.add(targetCard51);
        cardsStack.add(targetCard52);
        cardsStack.add(targetCard53);
        cardsStack.add(targetCard54);
        cardsStack.add(targetCard55);
        cardsStack.add(targetCard56);
        cardsStack.add(targetCard57);
        cardsStack.add(targetCard58);
        cardsStack.add(targetCard59);
        cardsStack.add(targetCard60);
        cardsStack.add(targetCard61);
        cardsStack.add(targetCard62);
        cardsStack.add(targetCard63);
        cardsStack.add(targetCard64);
        cardsStack.add(targetCard65);
        cardsStack.add(targetCard66);
        cardsStack.add(targetCard67);
        cardsStack.add(targetCard68);
        cardsStack.add(targetCard69);
        cardsStack.add(targetCard70);
        cardsStack.add(targetCard71);
        cardsStack.add(targetCard72);
        cardsStack.add(targetCard73);
        cardsStack.add(targetCard74);
        cardsStack.add(targetCard75);
        cardsStack.add(targetCard76);
        cardsStack.add(targetCard77);
        cardsStack.add(targetCard78);
        cardsStack.add(targetCard79);
        cardsStack.add(targetCard80);
        cardsStack.add(targetCard81);
        cardsStack.add(targetCard82);
        cardsStack.add(targetCard83);
        cardsStack.add(targetCard84);
        cardsStack.add(targetCard85);
        cardsStack.add(targetCard86);
        cardsStack.add(targetCard87);
        cardsStack.add(targetCard88);
        
        Collections.shuffle(cardsStack);
    }
    
    
    public List<TargetCard> getCards() {
        return cardsStack;
    }
    
    public TargetCard removeTopCard(Integer position) {
        // TODO: check that cardsStack != null and that there is at least one card left
        return cardsStack.get(position);
    }
    
    public Long getId(){
        return id;
    }

}
