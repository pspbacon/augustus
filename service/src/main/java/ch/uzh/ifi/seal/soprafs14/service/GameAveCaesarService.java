package ch.uzh.ifi.seal.soprafs14.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.uzh.ifi.seal.soprafs14.controller.beans.game.RewardBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.TargetCardBean;
import ch.uzh.ifi.seal.soprafs14.model.Game;
import ch.uzh.ifi.seal.soprafs14.model.Reward;
import ch.uzh.ifi.seal.soprafs14.model.TargetCard;
import ch.uzh.ifi.seal.soprafs14.model.User;
import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;

public final class GameAveCaesarService {

	private GameAveCaesarService() {
	}
	
	private static Logger logger = LoggerFactory.getLogger(GameAveCaesarService.class);
	
	/** 8.1 - Ave Caesar **/
	public static void aveCaesar(Long gameId, Long playerId, Integer choosenCardNumber, UserRepository userRepo, GameRepository gameRepo) {
		// TODO: try and catch if Ids don't exist

		Game game = gameRepo.findOne(gameId);
		User user = userRepo.findOne(playerId);

		Map<Integer, TargetCard> cardRepo = new HashMap<Integer, TargetCard>();
					
		Integer neutralCardNumber = user.getNumberOfAveCaesarCard();

		// some loggers, there never can be enough loggers in this world
		logger.debug("[gameId:" + gameId + ", playerId:" + playerId + "]: aveCard " + neutralCardNumber + " and new choose " + choosenCardNumber);
		for(TargetCard logCard : user.getNeutralizedTargetCards()) {
			logger.debug("[gameId:" + gameId + ", playerId:" + playerId + "]: neutralized cards " + logCard.getNumber());
		}
		for(TargetCard log2Card : game.getDrawableTargetCards()) {
			logger.debug("[gameId:" + gameId + ", playerId:" + playerId + "]: drawable cards " + log2Card.getNumber());
		}

		// fetch neutral card and change its status to controlled
		// put the card into the controlledCardStack
		for(Entry<Integer, TargetCard> mapEntry : user.getCardMap().entrySet()) {
			if(neutralCardNumber.equals(mapEntry.getKey())) {
				user.controlCard(mapEntry.getValue());
				logger.debug("[gameId:" + gameId + ", playerId:" + playerId + "]: new card " + mapEntry.getKey() + " under control");
			}
		}

		// iterate thourgh userCardMap get all cards but the neutralized one
		for(Map.Entry<Integer, TargetCard> entry : user.getCardMap().entrySet()) {
			if(!entry.getKey().equals(neutralCardNumber)) {
				cardRepo.put(entry.getKey(), entry.getValue());
			}
		}
					
		// replace (formal) neutral card with new choosen card
		// remove drawn card from drawable cards
		//TargetCard removeCard = null;
		
		for(TargetCard choosenCard : game.getDrawableTargetCards()) {
			if(choosenCardNumber.equals(choosenCard.getNumber())) {
				cardRepo.put(choosenCard.getNumber(), choosenCard);
				//removeCard = choosenCard;
				logger.debug("[gameId:" + gameId + ", playerId:" + playerId + "]: new card " + choosenCardNumber + " assigned");
			}
		}
		try {
			game.removeDrawableCard(choosenCardNumber);
		} catch(Exception e) {
			logger.error("No card to remove from drawable cards");
		}
		user.updateTargetMap(cardRepo);
		
		// fetch new card from cardStack and put it to the drawable cards
		game.addDrawableCard();
					
		// Allfaellige Auto-Rewards verteilen
		// iteriere ueber alle verfuegbaren Belohnungen des Spiels und fuege alle diejenigen, die automatisch verteilt werden und fuer die der Spieler berechtigt ist, diesem Spieler hinzu
		Integer numberOfMovableAutoRewardsAssigned = 0;
		List<Long> assignedRewardIdList = new ArrayList<Long>();
		for (Reward aReward : game.getAllAvailableAutoRewards()) {
			if (aReward.isEligible(user, game)) {
			    try {
					user.addReward(aReward);
				} catch(Exception e) {
					logger.error(e.toString());
				}
				++numberOfMovableAutoRewardsAssigned;
				assignedRewardIdList.add(aReward.getID());
			}
		}
		String loggerText = "Checked unassigned auto rewards of game: ";
		if (numberOfMovableAutoRewardsAssigned == 0) {
			loggerText = loggerText + "none";
		} else {
			loggerText += "assigned " + numberOfMovableAutoRewardsAssigned + " rewards. IDs of assigned rewards: ";
            StringBuffer myBuffer = new StringBuffer();
			for (Long anId : assignedRewardIdList) {
                myBuffer.append(anId);
                myBuffer.append("|");
			}
			loggerText = loggerText + myBuffer.toString();
		}
		logger.debug(loggerText);

		// Save new game and user state in the repository
		gameRepo.save(game);
		userRepo.save(user);
					
		return;				
	}

	/** 8.2 - Request drawable Rewards **/
	public static List<RewardBean> getRewards(Long gameId, Long playerId, UserRepository userRepo, GameRepository gameRepo){
		if(gameId == null || playerId == null) {
			logger.error("[ERROR]: empty IDs");
			return null;
		}
		
		try {
			
			List<RewardBean> eligibleRewards = new ArrayList<RewardBean>();
			
			Game game = gameRepo.findOne(gameId);
			User player = userRepo.findOne(playerId);
			
			for(Reward tmpReward : game.getAvailableNonAutoRewards()) {
				if(tmpReward.isEligible(player, game)) {
					RewardBean tmpRewardBean = tmpReward.packIntoBean();
					
					logger.debug("[gameId: " + gameId + ", playerId: " + playerId + "]: reward " +
							tmpReward.getID() + " added");
					eligibleRewards.add(tmpRewardBean);
				}
			}
			
			player.setIsReady(false);
			userRepo.save(player);
			
			// good case
			return eligibleRewards;
			
			
		} catch(Exception e) {
			logger.error("wasn't able to fetch drawable rewards");
			logger.error(e.toString());
		}
		
		// bad case
		return null;
	}
	
	/** 8.3 - Update Conquered Rewards **/
	public static void postReward(Long gameId, Long playerId, Long rewardId, UserRepository userRepo, GameRepository gameRepo){
		try {
			
			User player = userRepo.findOne(playerId);
			Game game = gameRepo.findOne(gameId);
			
			Reward rewardToRemove = null;
			
			for(Reward tmpReward : game.getAvailableNonAutoRewards()) {
				// do something
				if(tmpReward.getID().equals(rewardId)) {
					rewardToRemove = tmpReward;
					player.addReward(tmpReward);
				}
			}
			
			if(rewardToRemove != null) {
				game.removeReward(rewardToRemove);
			}
			
			// save changes
			userRepo.save(player);
			gameRepo.save(game);
			
			logger.debug("[gameId: " + gameId + ", playerId: " + playerId + "]: reward " +
					rewardId + " was choosen");
			
			
		} catch(Exception e) {
			logger.error("Couldn't process choosen reward successfully");
			logger.error(e.toString());
		}
		
		return;
	}

	/** 8.4 - show assignedRewards **/
	public static List<List<RewardBean> > getAssignedRewards(Long gameId, GameRepository gameRepo){
		try {
			
			List<List<RewardBean> > assignedRewards = new ArrayList<List<RewardBean> >(); 
			Game game = gameRepo.findOne(gameId);
			
			
			for(User player : game.getPlayers()) {
				List<RewardBean> playerRewards = new ArrayList<RewardBean>();
				
				for(Reward reward : player.getRewardsHold()) {
					playerRewards.add(reward.packIntoBean());
				}
				if(playerRewards!=null) {
					assignedRewards.add(playerRewards);
				}
			}
			
			
			return assignedRewards;
			
		} catch(Exception e) {
			logger.error("Couldn't fetch assignedRewards");
			logger.error(e.toString());
		}
		
		return null;
	}
	
    /** 8.5 - Update Player Status After Immediate Effect Has Been Executed **/
    public static void unflagPlayerForEffectExecution(Long gameId, Long playerId, UserRepository userRepo) {
        try {
            User player = userRepo.findOne(playerId);

            // unflag player
            player.effectExecutionCompleted();

            // save changes
            userRepo.save(player);

            // write info to debug logger
            logger.debug("[gameId: " + gameId + ", playerId: " + playerId + "]: player was unflagged for immediate effect execution");
            
        } catch(Exception e) {
            logger.error("Couldn't process choosen reward successfully");
            logger.error(e.toString());
        }
        
        return;
    }
	
    /** 8.7 send playerCards (assigned and controlled) for negativeEffectActivity  **/
    public static List<List<TargetCardBean> > setNegativeEffect(Long playerId, UserRepository userRepo, GameRepository gameRepo){
    	try {
			User player = userRepo.findOne(playerId);

			List<List<TargetCardBean> > tmpPlayerCards = new ArrayList<List <TargetCardBean> >();
			List<TargetCardBean> assignedCards = new ArrayList<TargetCardBean>();
			for(Map.Entry<Integer, TargetCard> entry : player.getCardMap().entrySet()) {
				assignedCards.add(entry.getValue().packIntoBean());
			}
			tmpPlayerCards.add(assignedCards);
			List<TargetCardBean> controlledCards = new ArrayList<TargetCardBean>();
			for(TargetCard tCard : player.getControlledTargetCards()) {
				controlledCards.add(tCard.packIntoBean());
			}
			tmpPlayerCards.add(controlledCards);
			
			player.setIsReady(false);
			
			userRepo.save(player);
			
			return tmpPlayerCards;
			
			
		} catch(Exception e) {
			logger.error("Couldn't send targetCards for negative Effect");
			logger.error(e.toString());
		}
		
		return null;
    }
    
    /** 8.8 Post card if EffectId = 51 **/
    public static void submitCard(Long gameId, Long playerId, Integer choosenCardNumber, UserRepository userRepo, GameRepository gameRepo){
    	logger.debug("Player: " + playerId + " in Game: " + gameId + " submitted a card");
        
		Game game = gameRepo.findOne(gameId);
        User player = userRepo.findOne(playerId);
        TargetCard chooseCard = null;
        
        
        Map<Integer, TargetCard> newTargetCardMap = new HashMap<Integer, TargetCard>();
        
        for(Map.Entry<Integer, TargetCard> entry : player.getCardMap().entrySet()) {
			if(entry.getKey().equals(player.getNumberOfAveCaesarCard())) {
				chooseCard = entry.getValue();
			} else {
				 newTargetCardMap.put(entry.getKey(), entry.getValue());
			}
		}
        
        for(TargetCard drawnCard : game.getDrawableTargetCards()) {
        	if(drawnCard.getNumber().equals(player.getNumberOfAveCaesarCard())) {
        		chooseCard = drawnCard;
        		newTargetCardMap.put(drawnCard.getNumber(), drawnCard);
        	}
        }
        
        if(chooseCard.getEffectOfCard().getID().equals(52)) {
        	game.removeDrawableCard(chooseCard.getNumber());
        	game.addDrawableCard();
        }
        	
        if(chooseCard.getEffectOfCard().getID().equals(51)) {
        	chooseCard.neutralize();
        	try {
        	player.increaseAvailableLegionsBy(chooseCard.getNumberOfLegionFields());
        	} catch(Exception e) {logger.error(e.toString()); }
        }
        
        // TODO: add logger
        logger.debug("Set TargetCard to controlled after Effect");

        

        // update repository
        player.updateTargetMap(newTargetCardMap);
        userRepo.save(player);
    }
    
    /** 8.9 sacrifice TargetCard **/
    public static void sacrificeCrd(Long gameId, Long playerId, Integer choosenCardNumber, UserRepository userRepo, GameRepository gameRepo){
		User player = userRepo.findOne(playerId);
		
		TargetCard tmpTargetCard = null;
		
		for(TargetCard cardToRemove : player.getControlledTargetCards()) {
			if(cardToRemove.getNumber().equals(choosenCardNumber)) {
				tmpTargetCard = cardToRemove;
			}
		}
		if(tmpTargetCard != null) {
			player.removeControlledCard(tmpTargetCard);
		}
		
		userRepo.save(player);
    }
}
