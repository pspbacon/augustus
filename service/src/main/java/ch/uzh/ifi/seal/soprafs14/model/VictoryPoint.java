package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

public abstract class VictoryPoint implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public abstract Integer getPoints();
	public abstract Integer getPoints(User user);
	public abstract Boolean isVariable();
	public abstract Boolean isFixed();

}
