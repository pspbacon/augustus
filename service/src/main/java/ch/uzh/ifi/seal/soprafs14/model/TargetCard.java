package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ch.uzh.ifi.seal.soprafs14.controller.beans.game.TargetCardBean;

public abstract class TargetCard implements Serializable {
	private static final long serialVersionUID = 1L;
	
	protected Integer number;
	protected String name;
	protected VictoryPoint victoryPoints;
	protected Effect effectOfCard;
	protected List<LegionField> legionFields = new ArrayList<LegionField>();
	protected CardStatus status;
	protected CardType type;
	
	public Integer getNumber() {
		return number;
	}
	
	public String getName() {
		return name;
	}

	public CardStatus getStatus() {
		return status;
	}

	public void neutralize() {
		this.status = CardStatus.NEUTRALIZED;
	}
	
	public void ownIt() { // TODO: bad method, should not be necessary or possible
	    this.status = CardStatus.CONTROLLED;
	}
	
	public void assign() {
	    this.status = CardStatus.ASSIGNED;
	}

	public Boolean isNeutralized() {
		if (this.status == CardStatus.NEUTRALIZED) {
			return true;
		} else {
			return false;
		}
	}
	
	public Boolean isUnassigned() {
        if (this.status == CardStatus.UNASSIGNED) {
            return true;
        } else {
            return false;
        }
	}
	
	public Boolean hasImmediateEffect() {
	    if (this.effectOfCard != null) {
	        if(this.effectOfCard.isImmediate()) {
	            return true;
	        } else {
	            return false;
	        }
	    } else {
	        return false;
	    }
	}
	
	public Boolean hasPersistentEffect() {
        if (this.effectOfCard != null) {
            if(this.effectOfCard.isPersistent()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }	    
	}

    public Boolean isRed() {
        if (this.hasImmediateEffect()) {
            if (this.effectOfCard.isRed()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

	public void setStatus(CardStatus status) { // TODO: very bad method, should be removed
		this.status = status;
	}
	
	public List<LegionField> getLegionFields() { // TODO: also a bad method
		return legionFields;
	}

	/*
	 * Returns the number of legion fields associated with this target card
	 * 
	 * @pre		this.legionFields != null
	 * @post	
	 * @param	none
	 * @return	this.legionFields.size()
	 */
	public Integer getNumberOfLegionFields() throws Exception {
	    if (this.legionFields == null) {
	        throw new Exception("Error: getNumberOfLegionFields(): tried to count legion fields for a target card without legion fields");
	    } else {
	        return this.legionFields.size();	        
	    }
	}
	
    /*
     * Returns the number of legions that are mobilised (placed) on fields of this target card
     * 
     * @pre     this.legionFields != null
     * @post    
     * @param   none
     * @return  number of legion fields with attribute isSet == true
     */
	public Integer getNumberOfLegionsMobilizedOnCard() throws Exception {
        if (this.legionFields == null) {
            throw new Exception("Error: countLegionsMobilizedOnCard(): tried to count legion fields for a target card without legion fields");
        } else {
            Integer numberOfLegionsOnCard = 0;
            for (int i = 0; i < this.legionFields.size(); i++) {
                if (this.legionFields.get(i).getSet()) {
                    ++numberOfLegionsOnCard;
                }
            }
            return numberOfLegionsOnCard;            
        }
	}
	
	public Integer getPoints() {
		return victoryPoints.getPoints();
	}

    public Integer getPoints(User user) {
        return victoryPoints.getPoints(user);
    }

	public Boolean hasVariablePoints() {
		return victoryPoints.isVariable();
	}
	
	public Boolean hasFixedPoints() {
		return victoryPoints.isFixed();
	}
	
	public CardType getType() {
		return this.type;
	}
	
	public String getTypeAsString() {
		return this.type.toString();
	}

	public abstract Boolean isProvinceCard();
	public abstract Boolean isSenatorCard();
	
	protected void fillLegionFields(List<Marker> markerFields) {
		for(int i = 0; i < markerFields.size(); i++) {
			legionFields.add(new LegionField(markerFields.get(i)));
		}
	}
	
	public Effect getEffectOfCard() {
		return effectOfCard;
	}

	/*
	public void updateCurrentValue(Integer count) {
		if (this.hasVariablePoints()) {
			((VictoryPointsVariable)victoryPoints).updateCurrentValue(count);
		}
	}
	*/
	
	/*
	 * Counts the number of markers of a certain type (given by the argument of the method) of the legion fields of the target cards and returns it as an integer
	 * 
	 * @pre		this.legionFields != null
	 * @post	
	 * @param	marker type which should be counted
	 * @return	number of the markers as an integer
	 */
	public Integer getNumberOfMarkers(Marker markerToCount) {
		// TODO: check whether this.legionFields == null, if so throw exception
		Integer sumOfMarkers = 0;
		for (LegionField aLegionField : this.legionFields) {
			if (aLegionField.getMarker() == markerToCount) {
				++sumOfMarkers;
			}
		}
		return sumOfMarkers;
	}

	
	/*
	 * Packages the information from a target card into a TargetCardBean and returns this bean
	 * 
	 * @pre		this.legionFields != null
	 * @post	
	 * @param	none
	 * @return	a TargetCardBean with the information of the TargetCard object upon which this method has been applied
	 */
	public TargetCardBean packIntoBean() {
		// TODO: check whether legionFields of target card is null, if so throw exception
		
		// create helper variables
		TargetCardBean myBean = new TargetCardBean();
		List<String> aLegionField = new ArrayList<String>();
		List<Boolean> aStatusField = new ArrayList<Boolean>();

		/* set values of bean */
		// identification
		myBean.setNumber(this.number);
		myBean.setName(this.name);
        myBean.setType(this.getTypeAsString());
        
        // legion fields and assignability
        for(int i = 0; i < this.legionFields.size(); i++) {
            aLegionField.add(this.legionFields.get(i).getMarker().getName());
            aStatusField.add(this.legionFields.get(i).getSet());
        }
        myBean.setLegionFields(aLegionField);
        myBean.setStatusFields(aStatusField);
        myBean.setOpenForAssignment(null);

        // commodities
        if (this.isProvinceCard()) {
            myBean.setCommodities(((ProvinceCard)this).getCommodityNames());
        } else {
            myBean.setCommodities(null);
        }
		
        // victory points
		myBean.setPoints(this.getPoints());
		myBean.setPointType(this.hasVariablePoints());
		if (this.victoryPoints.isVariable()) {
            myBean.setPointsPerElement(((VictoryPointsVariable)this.victoryPoints).getPointsPerElement());
            myBean.setElementToCount(((VictoryPointsVariable)this.victoryPoints).getElementToCount());
		    if (this.victoryPoints.getClass() == VictoryPointsVariableMarker.class) {
		        myBean.setMaximumPoints(((VictoryPointsVariableMarker)this.victoryPoints).getMaximumPoints());
		    } else {
		        myBean.setMaximumPoints(null);
		    }
		} else {
            myBean.setPointsPerElement(null);
	        myBean.setElementToCount(null);
	        myBean.setMaximumPoints(null);
		}

		// effects
		if(this.effectOfCard != null) {
			myBean.setEffectDescription(this.effectOfCard.getDescription());
			myBean.setEffectType(this.effectOfCard.getEffectType().getLabel());
			myBean.setEffectId(this.effectOfCard.getID());
			myBean.setIsImmediatly(this.effectOfCard.isImmediate());
			myBean.setImmediateEffectType(this.effectOfCard.getImmediateEffectCategoryAsString());
			myBean.setIsRed(this.effectOfCard.isRed());
			if (this.effectOfCard.isPersistent()) {
			    String[] markers = ((EffectPersistent)this.effectOfCard).getMarkers();
	            myBean.setPersistentEffectMarkers(markers[0], markers[1]);
			}
			if (this.effectOfCard.isImmediate()) {
			    if (((EffectImmediate)this.effectOfCard).getImmediateEffectCategory() == ImmediateEffectCategory.INCREASE_LEGIONS) {
			        myBean.setImmediateEffectLegionIncreaseNumber(((EffectImmediateIncreaseLegions)this.effectOfCard).getIncreaseOfLegions());
			    } else {
			        myBean.setImmediateEffectLegionIncreaseNumber(null);
			    }
	            if (((EffectImmediate)this.effectOfCard).getImmediateEffectCategory() == ImmediateEffectCategory.MOBILIZE_LEGIONS) {
	                myBean.setImmediateEffectMobilizeLegionsNumber(((EffectImmediateMobilizeLegions)this.effectOfCard).getNumberOfLegions());
                    myBean.setImmediateEffectMobilizeLegionsMarker(((EffectImmediateMobilizeLegions)this.effectOfCard).getMarkerConstraint().getName());
	            } else {
	                myBean.setImmediateEffectMobilizeLegionsNumber(null);
	                myBean.setImmediateEffectMobilizeLegionsMarker(null);
	            }			    
			}
		}
			
		return myBean;
	}
	
}
