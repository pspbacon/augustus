package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

// Class for variable victory points that are calculated according to number of markers of fields of controlled target cards
public class VictoryPointsVariableMarker extends VictoryPointsVariable implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final Integer pointsPerElement;
	private final Marker markerToCount;
	private final Integer maximumPoints;

	public VictoryPointsVariableMarker(Integer pointsPerElement, Marker markerToCount, Integer maximumPoints) {
		this.pointsPerElement = pointsPerElement;
		this.markerToCount = markerToCount;
		this.maximumPoints = maximumPoints;
	}

	public void updateCurrentValue(Integer countElement) {
		this.currentValueForOwner = countElement * this.pointsPerElement;
	}

    public Integer getPoints(User user) {
        Integer numberOfElements = user.getNumberOfControlledMarkers(this.markerToCount);
        Integer points = numberOfElements * this.pointsPerElement;
        if (numberOfElements > this.maximumPoints) {
            return this.maximumPoints;
        } else {
            return points;
        }
    }

    protected Integer getMaximumPoints() {
        return this.maximumPoints;
    }
    
    protected String getElementToCount() {
        return this.markerToCount.getName();
    }

}
