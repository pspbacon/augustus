package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

// this Enumeration was formerly called ProvinceColor and was applicable for ProvinceCards only
public enum CardType implements Serializable {
	GREEN(1, "green", "Europa"),
	VIOLET(2, "violet", "Kleinasien"),
	ORANGE(3, "orange", "Afrika"),
	SENATOR(4, "senator", "Senator");
	
	private final Integer code;
	private final String label;
	private final String name;
	
	CardType(Integer code, String label, String name) {
		this.code = code;
		this.label = label;
		this.name = name;
	}
	
	public Integer getCode() {
		return code;
	}
	
	public String getLabel() {
		return label;
	}
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return name;
	}

}
