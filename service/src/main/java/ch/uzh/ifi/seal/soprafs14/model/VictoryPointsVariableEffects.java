package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

//Class for variable victory points that are calculated according to number of controlled target cards that contain a certain effect type
public class VictoryPointsVariableEffects extends VictoryPointsVariable implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final Integer pointsPerElement;
	private final EffectType effectTypeToCount;

	public VictoryPointsVariableEffects(Integer pointsPerElement, EffectType effectTypeToCount) {
		this.pointsPerElement = pointsPerElement;
		this.effectTypeToCount = effectTypeToCount;
	}

	public void updateCurrentValue(Integer countElement) {
		this.currentValueForOwner = countElement * this.pointsPerElement;
	}

    public Integer getPoints(User user) {
        // TODO: sollte eigentlich etwas allgemeiner gehalten sein
        Integer numberOfElements = user.getNumberOfRedCards();
        return (numberOfElements * this.pointsPerElement);
    }
    
    protected String getElementToCount() {
        // TODO: unschoen
        return "red cards";
    }

}
