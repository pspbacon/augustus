package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

public abstract class VictoryPointsVariable extends VictoryPoint implements Serializable {

	private static final long serialVersionUID = 1L;

	// Anzahl der Punkte pro Element, das gezaehlt wird, um die variablen Punkte zu berechnen
	protected Integer pointsPerElement;

	// dieses Attribut speichert den jeweils aktuellen Punktewert fuer den besitzenden Player; per Default zu Beginn auf 0 gesetzt
	protected Integer currentValueForOwner = 0;

	public abstract void updateCurrentValue(Integer countElement);

	public Integer getPoints() {
		return this.currentValueForOwner;
	}
	
	public abstract Integer getPoints(User user);

	public Boolean isVariable() {
		return true;
	}

	public Boolean isFixed() {
		return false;
	}
	
	protected abstract String getElementToCount();
	
	protected Integer getPointsPerElement() {
	    return pointsPerElement;
	}

}
