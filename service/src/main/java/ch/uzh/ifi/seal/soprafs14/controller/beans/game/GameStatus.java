package ch.uzh.ifi.seal.soprafs14.controller.beans.game;

public enum GameStatus {
	PENDING, INITIALIZED, RUNNING, FINISHED
}
