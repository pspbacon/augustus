package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

public enum CardStatus implements Serializable {
	UNASSIGNED(1, "unassigned", "target card has not been assigned to a player"), 
	ASSIGNED(2, "assigned", "target card has been assigned to a player"), 
	NEUTRALIZED(3, "neutralized", "target card has been fulfilled, meaning all legion positions are set and target card waits until it is evaluated during which time it is considered neutralized"), // NEUTRALISED = FULFILLED for the purpose of this model of the game
	CONTROLLED(4, "controlled", "target card is conquered");
	
	private final int code;
	private final String label;
	private final String description;
	
	private CardStatus(int code, String label, String description) {
		this.code = code;
		this.label = label;
		this.description = description;
	}
	
	public int getCode() {
		return code;
	}
	
	public String getLabel() {
		return label;
	}
	
	public String getDescription() {
		return description;
	}
	
}
