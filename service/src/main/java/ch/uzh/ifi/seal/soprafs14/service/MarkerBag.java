package ch.uzh.ifi.seal.soprafs14.service;

import java.io.Serializable;
import java.util.Random;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.stereotype.Service;

import ch.uzh.ifi.seal.soprafs14.model.Marker;

@Service("markerBag")
@Entity
public class MarkerBag implements Serializable {

    private static final long serialVersionUID = 6889743329410075538L;

    @Id
    @GeneratedValue
    private Long id;
    
    //@Column
    //private Marker lastDrawnMarker;

    private Marker schwert = Marker.SCHWERTER;
    private Marker schild = Marker.SCHILD;
    private Marker wagen = Marker.STREITWAGEN;
    private Marker katapult = Marker.KATAPULT;
    private Marker standarte = Marker.STANDARTE;
    private Marker dolch = Marker.DOLCH;
    private Marker joker = Marker.JOKER;

    private String[] bag;
    private int bagPosition;

    public MarkerBag() { }

    public String getMarker() {
        this.mapMarker();
        Random randomGenerator = new Random();
        int rand = randomGenerator.nextInt(this.countMarker());
        this.drawMarker(bag[rand]);
        return bag[rand];
    }

    private void mapMarker() {
        bagPosition = 0;
        bag = new String[this.countMarker()];
        fillMarkerBag(schwert.getName(), schwert.getCount());
        fillMarkerBag(schild.getName(), schild.getCount());
        fillMarkerBag(wagen.getName(), wagen.getCount());
        fillMarkerBag(katapult.getName(), katapult.getCount());
        fillMarkerBag(standarte.getName(), standarte.getCount());
        fillMarkerBag(dolch.getName(), dolch.getCount());
        fillMarkerBag(joker.getName(), joker.getCount());
    }

    private void fillMarkerBag(String name, int count) {
        for(int i = bagPosition; i < bagPosition + count; i++) {
            bag[i] = name;
        }
        bagPosition += count;
    }

    private void drawMarker(String draw) {
        switch (draw) {
        case "schwert": schwert.drawMarker();
        break;
        case "schild": schild.drawMarker();
        break;
        case "wagen": wagen.drawMarker();
        break;
        case "katapult": katapult.drawMarker();
        break;
        case "standarte": standarte.drawMarker();
        break;
        case "dolch": dolch.drawMarker();
        break;
        case "joker": {
            schwert.resetCount();
            schild.resetCount();
            wagen.resetCount();
            katapult.resetCount();
            standarte.resetCount();
            dolch.resetCount();
        }
        break;
        default: break;
    }
    }

    private int countMarker() {
        return schwert.getCount() + schild.getCount() + wagen.getCount() + katapult.getCount() +
        	    standarte.getCount() + dolch.getCount() + joker.getCount();
    }
    
    //public Marker getLastDrawnMarker() {
    //    return lastDrawnMarker;
    //}

    //private void setLastDrawnMarker(Marker lastDrawnMarker) {
    //    this.lastDrawnMarker = lastDrawnMarker;
    //}

    public Long getId() {
        return id;
    }

    public void drawMarkerPublic(String draw) {
        drawMarker(draw);
    }
}
