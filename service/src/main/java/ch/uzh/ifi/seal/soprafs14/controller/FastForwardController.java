package ch.uzh.ifi.seal.soprafs14.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;
import ch.uzh.ifi.seal.soprafs14.service.FastForwardService;

@Controller("fastForwardController")
public class FastForwardController extends GenericService {

	Logger logger = LoggerFactory.getLogger(FastForwardController.class);

	@Autowired
	private UserRepository userRepo;
	@Autowired
	private GameRepository gameRepo;

	private final String CONTEXT = "/game";

	
	/** Part 1: initialize Fastforwarding **/
	/*
	 * Context: /game/{gameId}/player/{playerId}/fastforward
	 * 
	 * Can be accessed at the end of a round. Changes the cards in the userrepo, such that each player
	 * has 6 conquered cards and one assigned card that only needs one more legion
	 * 
	 * 
	 */
	@RequestMapping(value = CONTEXT + "/{gameId}/player/{playerId}/fastforward", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void initializeFastforwarding(@PathVariable Long gameId, @PathVariable Long playerId) {
		FastForwardService.initializeFastforwarding(gameId, playerId, userRepo, gameRepo);
	}
	
	/** Part 2: initialize Fullcard **/
	/*
	 * Context: /game/{gameId}/player/{playerId}/fullcard
	 * 
	 * Cheating option for testing purposes
	 * Enable this in the MarkerActivity
	 * 
	 * 
	 */
	@RequestMapping(value = CONTEXT + "/{gameId}/player/{playerId}/fullcard", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void initializeFullcard(@PathVariable Long gameId, @PathVariable Long playerId) {
		FastForwardService.initializeFullcard(gameId, playerId, userRepo, gameRepo);
	}
	
	/** Part 3: initialize end game **/
	/*
	 * Context: /game/{gameId}/player/{playerId}/endGame
	 * 
	 * Fastforward to endofGame: Assigns 6 cards to controlled cards and provides an already full card
	 * Can be accessed via menu in MarkerActivity
	 * 
	 * 
	 */
	@RequestMapping(value = CONTEXT + "/{gameId}/player/{playerId}/endGame", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void initializeEndgame(@PathVariable Long gameId, @PathVariable Long playerId) {
		FastForwardService.initializeEndgame(gameId, playerId, userRepo, gameRepo);
	}
	
	/** Part 4: mobilize all legions **/
	/*
	 * Context: /game/{gameId}/player/{playerId}/fillcards
	 * 
	 * Cheating option for testing purposes
	 * Enable this in the MarkerActivity
	 * 
	 * Fill the cards such that no legions are left anymore in the repo
	 * 
	 * 
	 */
	@RequestMapping(value = CONTEXT + "/{gameId}/player/{playerId}/fillcards", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void fillCards(@PathVariable Long gameId, @PathVariable Long playerId) {
		
		try {
			
			FastForwardService.mobilizeAllLegions(userRepo, playerId);
			
			
		} catch(Exception e) {
			logger.error("[ERROR]: FastForwarding not successfull");
			logger.error(e.toString());
		}
		
		
		return;
	}


}
	