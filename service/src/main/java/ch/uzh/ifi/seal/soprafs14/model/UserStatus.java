package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

public enum UserStatus implements Serializable {
    // Spieler ist online, nimmt aber gerade (noch) nicht an einem Spiel teil
    ONLINE, 
    // Spieler ist online und nimmt gerade an einem Spiel teil
    PLAYING, 
    // Spieler ist online, nimmt gerade an einem Spiel teil und ist gerade dabei, einen sofortien Effekt
    // einer Zielkarte auszufuhren (als Kartenbesitzer oder von negativem Effekt Betroffener)
    EXECUTING_EFFECT, 
    // Spieler ist offline
    OFFLINE;
}
