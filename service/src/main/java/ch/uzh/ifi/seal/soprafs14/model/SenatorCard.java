package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;
import java.util.List;

public class SenatorCard extends TargetCard implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public SenatorCard(Integer number, String name, VictoryPoint victoryPoints, List<Marker> markerFields, Effect effectOfCard) {
		this.number = number;
		this.name = name;
		this.status = CardStatus.UNASSIGNED;
		this.type = CardType.SENATOR;
		this.victoryPoints = victoryPoints;
		this.fillLegionFields(markerFields);
		this.effectOfCard = effectOfCard;
	}
	
	public Boolean isProvinceCard() {
		return false;
	}

	public Boolean isSenatorCard() {
		return true;
	}

}
