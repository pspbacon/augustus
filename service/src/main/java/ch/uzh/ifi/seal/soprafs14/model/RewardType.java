package ch.uzh.ifi.seal.soprafs14.model;

public enum RewardType {
	COLOUR_OF_TARGET_CARDS(1, "Colour of target cards", "Bonuspunkte aufgrund der Farbe der Zielkarten"),
	NUMBER_OF_TARGET_CARDS(2, "Number of target cards", "Bonuspunkte aufgrund der Anzahl der eroberten Zielkarten; nur einmal pro Spieler und Spiel ziehbar"),
	CONTROL_OF_COMMODITIES(3, "Control of commodities", "Bonuspunkte aufgrund der Anzahl der kontrollierten Rohstoffe");

	private int code;
	private String label;
	private String description;

	private RewardType(int code, String label, String description) {
	this.code = code;
	this.label = label;
	this.description = description;
	}

	public int getCode() {
		return code;
	}

	public String getLabel() {
		return label;
	}

	public String getDescription() {
		return description;
	}
	
	@Override
	public String toString() {
		return label;
	}
}
