package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

public enum Marker implements Serializable {
	SCHWERTER (6, 6, "schwert"), 
	SCHILD (5, 5, "schild"), 
	STREITWAGEN (4, 4, "streitwagen"), 
	KATAPULT (3, 3, "katapult"), 
	STANDARTE (2, 2, "standarte"), 
	DOLCH (1, 1, "dolch"), 
	JOKER (2, 2, "joker");
	
	private int count;
	private final int defaultCount;
	private final String name;
	
	Marker (int count, int defaultCount, String name) {
		this.count = count;
		this.defaultCount = defaultCount;
		this.name = name;
	}
	
	public int getCount() {
		return count;
	}
	
	public void drawMarker() {
		if(count > 0) {
		    --count;
		}
	}
	
	public void resetCount() {
		count = defaultCount;
	}
	
	public String getName() {
		return name;
	}
	
	// TODO: haesslicher Hack, aber geht momentan nicht anders
	public static Marker getMarkerFromName(String name) {
	    switch(name) {
	        case "schwert": return Marker.SCHWERTER;
	        case "schild": return Marker.SCHILD;
	        case "streitwagen": return Marker.STREITWAGEN;
	        case "katapult": return Marker.KATAPULT;
	        case "standarte": return Marker.STANDARTE;
	        case "dolch": return Marker.DOLCH;
	        case "joker": return Marker.JOKER;
	        default: return null;
	    }
	}
	
}
