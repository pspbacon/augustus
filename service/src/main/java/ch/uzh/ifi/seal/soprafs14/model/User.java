package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import ch.uzh.ifi.seal.soprafs14.GameConstants;

@Entity
public class User implements Serializable {
    
    /******* 1. Attributes ********/

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false) 
	private String name;
	
	@Column(nullable = false, unique = true) 
	private String username;
	
	@Column(nullable = false, unique = true) 
	private String token;
	
	@Column(nullable = false) 
	private UserStatus status;

    @ManyToMany
    private List<Game> games;
	
    @OneToMany(mappedBy="user")
    private List<Move> moves;

    // target cards that are currently being conquered by the player
    @Column
    private Map<Integer, TargetCard> choosenTargetCards = new HashMap<Integer, TargetCard>();

    // target cards that are currently under the control of the player
    @Column
    private List<TargetCard> controlledTargetCards = new ArrayList<TargetCard>();
    
    // Liste mit allen Belohnungen, die der Spieler besitzt
    @Column
    private List<Reward> rewardsHold;

    // Flag, das beim Polling in der EndRoundActivity verwendet wird
	@Column
    private Boolean isReady;

	// Anzahl der Legionen im Vorrat der Spieler; also nur diejenigen Legionen, die sich noch nicht auf einer Zielkarte befinden
    @Column
    private Integer numberOfLegionsAvailable;

    @Column
    private Integer numberOfAveCaesarCard;


    /******* 2. Constructors ********/

    public User() {
    }
    
    public User(String name, String username) {
        this.name = name;
        this.username = username;
        this.status = UserStatus.OFFLINE;
        this.token = UUID.randomUUID().toString();
        this.numberOfLegionsAvailable = GameConstants.DEFAULT_NUMBER_OF_LEGIONS_START/* TODO: should probably be set only after user is online and joins game; take constant from game instance */;        
    }


    /******* 3. Simple getter (and some setter) methods for attributes ********/

    public Integer getNumberOfAveCaesarCard() {
		return numberOfAveCaesarCard;
	}

	public void setNumberOfAveCaesarCard(Integer numberOfAveCaesarCard) {
		this.numberOfAveCaesarCard = numberOfAveCaesarCard;
	}

	public Integer getNumberOfLegionsAvailable() {
		return numberOfLegionsAvailable;
	}

	public void setNumberOfLegionsAvailable(Integer number) {
		this.numberOfLegionsAvailable = number;
	}

	/*
	 * This method increases or decreases the number of available legions of the player.
	 * 
	 * @pre		this.numberOfLegionsAvailable must not get negative (or overflow); this.numberOfLegionsAvailable != null
	 * @post	the value of argument 'increase' is added to this.numberOfLegionsAvailable
	 * @param	Integer increase: number of legions to add to the stock of the player; negative numbers mean remove of legions
	 * @return	(void)
	 */
	public void increaseAvailableLegionsBy(Integer increase) /*throws Exception*/ {
	    // TODO: check whether this.numberOfLegionsAvailable == null
	    // TODO: activate throwing of Exception again
		//if (this.numberOfLegionsAvailable + increase < 0) {
		//    throw new Exception("Error: increaseAvailableLegionsBy(): the resulting number of legions would be negative");
			// check on overflow lassen wir mal bleiben
		//} else {
			this.numberOfLegionsAvailable = this.numberOfLegionsAvailable + increase;
		//}
	}

	public Boolean getIsReady() {
		return isReady;
	}

	public void setIsReady(Boolean isReady) {
		this.isReady = isReady;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getUsername() {
		return username;
	}

	public List<Game> getGames() {
		return games;
	}

	public void setGames(List<Game> games) {
		this.games = games;
	}

	public List<Move> getMoves() {
		return moves;
	}

	public void setMoves(List<Move> moves) {
		this.moves = moves;
	}

	public String getToken() {
		return token;
	}

	public UserStatus getStatus() {
		return status;
	}
	
	public void goOnline() {
	    this.status = UserStatus.ONLINE;
	    this.token = UUID.randomUUID().toString(); // TODO: document: why do we do this?
	}
	
	public void goOffline() {
	    this.status = UserStatus.OFFLINE;
	    // TODO: should other things be done here, e.g. delete some stuff etc.?
	}
	
	public void markForEffectExecution() {
	    this.status = UserStatus.EXECUTING_EFFECT;
	}

	public void effectExecutionCompleted() {
	    // TODO: check whether status was UserStatus.EXECUTING_EFFECT previously - if not throw exception
	    this.status = UserStatus.PLAYING;
	}
	
	public Boolean hasEffectsToExecute() {
	    return (this.status == UserStatus.EXECUTING_EFFECT);
	}
	
    /******* 4. Methods for target cards of user ********/

	public TargetCard getTargetCard(Integer cardNumber) {
		return choosenTargetCards.get(cardNumber);
	}
	
	public Map<Integer, TargetCard> getCardMap() {
		return choosenTargetCards;
	}
	
	public void addChoosenTargetCards(Integer cardNumber, TargetCard tcard) {
		this.getCardMap().put(cardNumber, tcard);
	}
	
	public void updateTargetMap(Map<Integer, TargetCard> choosenTargetCards) {
		this.choosenTargetCards = choosenTargetCards;
	}
	
	public void removeCardFromMap(Integer keynumber) {
		Map<Integer, TargetCard> tmpCards = new HashMap<Integer, TargetCard>();
		for (Map.Entry<Integer, TargetCard> entry : choosenTargetCards.entrySet()) {
			if(!entry.getKey().equals(keynumber)) {
				tmpCards.put(entry.getKey(), entry.getValue());
			}
		}
		this.choosenTargetCards = tmpCards;
	}
	
	public List<TargetCard> getControlledTargetCards() {
		return controlledTargetCards;
	}
	
    /*
     * A target card is removed from this.choosenTargetCards (i.e. target cards to conquer) and added
     * to this.controlledTargetCards; at the same time, the new card that the user seeks to conquer 
     * is added to this.choosenTargetCards
     * 
     * @pre     this.choosenTargetCards != null && this.controlledTargetCards != null
     * @post    
     * @param   Integer numberOfCardToMove is the number of the target card that should be moved to controlled cards, 
     * @return  old card is moved to controlled, new is added to choosen cards
     */
	public void replaceConqueredTargetCard(Integer numberOfCardToMove, TargetCard replacementCard) throws Exception {
	    // TODO: check whether this.choosenTargetCards == null
	    // TODO: check whether this.controlledTargetCards == null
	    
	    // store the card that should be removed from cards to be conquered in a temporary variable
	    // but first: check whether targetCardToMove.status == CardStatus.NEUTRALIZED; if not: deny action and throw exception!
	    TargetCard tmpCard = this.choosenTargetCards.get(numberOfCardToMove);
	    if (!tmpCard.isNeutralized()) {
	        throw new Exception("Error: user.replaceConqueredTargetCard: tried to moved a target card to controlled target cards that had a status other then 'neutralized' which is not allowed and was therefore denied.");
	    }
	    tmpCard.ownIt();
	    // put removed card to controlled cards
	    this.controlledTargetCards.add(tmpCard);
	    // add replacement card from card deck to cards that the user wants to conquer
        // but first: check whether the status of the replacement card equals CardStatus.UNASSIGNED; if not: deny action and throw exception!
        if (!tmpCard.isUnassigned()) {
            throw new Exception("Error: user.replaceConqueredTargetCard: tried to replace a target card to conquer with a card that had a status other then 'unassigned' which is not allowed and was therefore denied.");
        }
	    Integer numberOfReplacementCard = replacementCard.getNumber();
	    replacementCard.assign();
	    this.choosenTargetCards.put(numberOfReplacementCard, replacementCard);
	}

    /*
     * Returns the number of controlled target cards of an user
     * 
     * @pre     
     * @post    
     * @param   none
     * @return  number of controlled target cards as an integer
     */
    public Integer getNumberOfControlledTargetCards() {
        if (this.controlledTargetCards == null) {
            return 0;
        } else {
            return this.controlledTargetCards.size();
        }
    }

    /*
     * This method returns a list with all neutralised (= newly fulfilled) target cards of the user
     * 
     * @pre     this.choosenTargetCards != null
     * @post    
     * @param   none
     * @return  a list with all neutralised target cards of the player
     */
    public List<TargetCard> getNeutralizedTargetCards() {
        //TODO: check whether this.choosenTargetCards == null; if so throw exception
        List<TargetCard> neutralizedTargetCards = new ArrayList<TargetCard>();
        // iteriere ueber alle Zielkarten des Spielers, die gerade in Eroberung sind und uebernimm alle mit Status "neutralisiert" in die Rueckgabe-Liste
        for (Entry<Integer, TargetCard> aMapEntry : this.choosenTargetCards.entrySet()) {
            if (aMapEntry.getValue().isNeutralized()) {
                neutralizedTargetCards.add(aMapEntry.getValue());
            }
        }
        return neutralizedTargetCards;
    }

    /*
     * Returns the number of neutralized target cards of an user
     * 
     * @pre     
     * @post    
     * @param   none
     * @return  number of neutralized target cards as an integer; if this.choosenTargetCards == null, the value 0 is returned
     */
    public Integer getNumberOfNeutralizedTargetCards() {
        if (this.getNeutralizedTargetCards().isEmpty()) {
            return 0;
        } else {
            return this.getNeutralizedTargetCards().size();
        }
    }

    /*
     * Fuegt die als Argument uebergebene Zielkarte den kontrollierten Zielkarten des Spielers hinzu
     * 
     * @pre     newlyConqueredCard != null
     * @post    this.controlledTargetCards hat newlyConqueredCard als zusaetzliches Element
     * @param   TargetCard newlyConqueredCard: die neu hinzuzufuegende Zielkarte
     * @return  (void)
     */
    public void addControlledCard(TargetCard newlyConqueredCard) {
        // TODO: pruefen, ob das Argument newlyConqueredCard != null
        // TODO: hier auch gleich prüfen, ob der vorangehende CardStatus korrekt ist und neuen CardStatus setzen
        if (this.controlledTargetCards == null) {
            List<TargetCard> newTargetCardList = new ArrayList<TargetCard>();
            newTargetCardList.add(newlyConqueredCard);
            this.controlledTargetCards = newTargetCardList;
        } else {
            this.controlledTargetCards.add(newlyConqueredCard);
        }
    }
    
    public void removeNeutralizedCard(TargetCard cardToRemove) {
        this.controlledTargetCards.remove(cardToRemove); // TODO: shouldn't be allowed, should at least be private
    }
    
    public void removeControlledCard(TargetCard cardToRemove) {
    	this.controlledTargetCards.remove(cardToRemove);
    }

    // change status of targetCard and moves it from older containing folder to controlledCards-Stack
    public void controlCard(TargetCard targetCard) {
        if(targetCard.getStatus()!=CardStatus.CONTROLLED) {
            targetCard.setStatus(CardStatus.CONTROLLED);
        }
        
        Map<Integer, TargetCard> tmpMap = new HashMap<Integer, TargetCard>();
        for (Entry<Integer, TargetCard> mapEntry : this.choosenTargetCards.entrySet()) {
            if(!mapEntry.getValue().getNumber().equals(targetCard.getNumber())) {
                tmpMap.put(mapEntry.getKey(), mapEntry.getValue());
            }
        }
        this.updateTargetMap(tmpMap);
        this.controlledTargetCards.add(targetCard);
    }


    /******* 5. Methods for rewards of user ********/

	public List<Reward> getRewardsHold() {
		//TODO: check whether this.rewardsHold == null
		return this.rewardsHold;
	}
	
	public void addReward(Reward newReward) throws Exception {
		//TODO: check whether newReward == null
		if (this.rewardsHold == null) {
			// falls der Spieler noch keine Belohnungen hat, wird eine neue Belohnungsliste erzeugt
			List<Reward> newRewardList = new ArrayList<Reward>();
			newRewardList.add(newReward);
			this.rewardsHold = newRewardList;
		} else {
			this.rewardsHold.add(newReward);
		}
	}

    /*
     * Counts on the target cards controlled by the user the number of commodities 
     * of the type specified in the argument
     * 
     * @pre     this.controlledTargetCards != null
     * @post    
     * @param   commodityToCount is the commodity type (Commodity.GOLD, Commodity.WHEAT) that should be counted
     * @return  number of commodities on controlled cards of the user as an integer; if the user has no controlled target cards the value 0 is returned
     */
	public Integer getNumberOfCommodities(Commodity commodityToCount) {
	    if (this.controlledTargetCards == null) {
	        return 0;
	    } else {
	        Integer numberOfCommodities = 0;        
	        // iteriere durch alle kontrollierten Zielkarten des Spielers und zaehle auf den Provinz-Karten die Anzahl der als Argument uebergebenen Rohstoffe
	        for (TargetCard aTargetCard : this.controlledTargetCards) {
	            if (aTargetCard.isProvinceCard()) {
	                if (((ProvinceCard)aTargetCard).hasCommodities()) {
	                    for (Commodity myCommodity : ((ProvinceCard)aTargetCard).getCommodities()) {
	                        if (myCommodity == commodityToCount) {
	                            ++numberOfCommodities;
	                        }
	                    }
	                }
	            }
	        }
	        return numberOfCommodities;	        
	    }
	}
	
	public Integer getNumberOfCardsOfType(CardType cardTypeToCount) {
        if (this.controlledTargetCards == null) {
            return 0;
        } else {
            Integer numberOfCards = 0;
            for (TargetCard aTargetCard : this.controlledTargetCards) {
                if (aTargetCard.getType() == cardTypeToCount) {
                    ++numberOfCards;
                }
            }
            return numberOfCards;
        }	    
	}
	
	public Integer getNumberOfProvincesOfColor(CardType colorToCount) {
	    if (this.controlledTargetCards == null) {
	        return 0;
	    } else {
	        Integer numberOfProvinces = 0;
	        for (TargetCard aTargetCard : this.controlledTargetCards) {
	            if (aTargetCard.isProvinceCard()) {
                    if (((ProvinceCard)aTargetCard).getColor() == colorToCount) {
                        ++numberOfProvinces;
                    }
	            }
	        }
	        return numberOfProvinces;	        
	    }
	}
	
	public Integer getNumberOfSenators() {
        if (this.controlledTargetCards == null) {
            return 0;
        } else {
            Integer numberOfSenators = 0;
            for (TargetCard aTargetCard : this.controlledTargetCards) {
                if (aTargetCard.isSenatorCard()) {
                    ++numberOfSenators;
                }
            }
            return numberOfSenators;
        }
	}
	
	public Integer getNumberOfRedCards() {
	    if (this.controlledTargetCards == null) {
	        return 0;
	    } else {
	        Integer numberOfRedCards = 0;
	        for (TargetCard aTargetCard : this.controlledTargetCards) {
	            if (aTargetCard.isRed()) {
	                ++numberOfRedCards;
	            }
	        }
	        return numberOfRedCards;	        
	    }
	}

    /*
     * This method counts the number of markers of a certain type (given as argument) on the target cards controlled by the user
     * This method makes use of the countMarkers(..) method of class TargetCard.
     * 
     * @pre     this.controlledTargetCards != null
     * @post    
     * @param   marker for which the number should be counted
     * @return  integer with the number of the markers; if the user has no controlled target cards, the value 0 is returned
     */
    public Integer getNumberOfControlledMarkers(Marker markerToCount) {
        // TODO: check on markerToCount == null
        if (this.controlledTargetCards == null) {
            return 0;
        } else {
            Integer sumOfMarkers = 0;
            for (TargetCard aControlledCard : this.controlledTargetCards) {
                sumOfMarkers += aControlledCard.getNumberOfMarkers(markerToCount);
            }
            return sumOfMarkers;            
        }
    }


    /******* 6. Methods for counting points ********/

	/*
	 * This method returns the points which the player has acquired by rewards
	 * 
	 * @pre		
	 * @post	
	 * @param	none
	 * @return	points acquired by rewards as an integer
	 */
	public Integer getRewardPoints() {
		if (this.rewardsHold == null) {
			return 0;
		} else {
	        Integer sumOfPointsThroughRewards = 0;
			for (Reward aReward : this.rewardsHold) {
				sumOfPointsThroughRewards += aReward.getPoints();
			}
	        return sumOfPointsThroughRewards;
		}
	}
	
	/*
	 * This method returns the points which the player has acquired by victory Points on his controlled target cards
	 * 
	 * @pre		this.controlledTargetCards != null
	 * @post	
	 * @param	String which can be "fixed" or "variable" or something else; if it is "fixed" only fixed victory points are counted; if it is "variable" only variable victory points are counted; in all other cases all victory points are counted
	 * @return	points acquired by victory points (possibly constraint to a type) as an integer
	 */
	public Integer getTargetCardsPoints(String constraint) { // TODO: extrem unschoen; evtl. enum 'VictoryPointType'?
	    // wenn ein Benutzer keine kontrollierten Karten hat, dann hat er 0 Punkt von Zielkarten
	    if (this.controlledTargetCards == null) {
	        return 0;
	    }
		Integer sumOfPoints = 0;
		// iteriere ueber alle kontrollierten Zielkarten des Spielers
		for (TargetCard aTargetCard : this.controlledTargetCards) {
			// beachte ggf. die Beschraenkung auf eine bestimmte Art von Siegpunkten
			Boolean constraintCondition = true;
			if (constraint != null && constraint.equals("fixed")) {
				constraintCondition = aTargetCard.hasFixedPoints();
			}
			else if (constraint != null && constraint.equals("variable")) {
				constraintCondition = aTargetCard.hasVariablePoints();
			}
			if (constraintCondition) {
				// addiere die Punkte der Zielkarte zur Summe der Siegpunkte
				sumOfPoints += aTargetCard.getPoints(this);
			}
		}
		return sumOfPoints;
	}
	
	public Integer getTargetCardsPoints() {
		return this.getTargetCardsPoints(null);
	}
	
	public Integer getFixedTargetCardsPoints() {
		return this.getTargetCardsPoints("fixed");
	}
	
	public Integer getVariableTargetCardsPoints() {
		return this.getTargetCardsPoints("variable");
	}
	
	public Integer getPoints() {
		return this.getRewardPoints() + this.getFixedTargetCardsPoints() + this.getVariableTargetCardsPoints();
	}


    /******* 7. Methods for effects ********/

    /*
     * This method determins which markers the user is allowed to additionally set through the  
     * effects of his controlled cards a certain marker has been drawn (as specified by the argument)
     * 
     * @pre     this.controlledTargetCards != null
     * @post    
     * @param   Marker input: the marker that has been drawn and for which the effects should be evaluated
     * @return  List of Markers that can be set additionally
     */
	public List<Marker> transmuteDrawnMarker(Marker input) {
        // Idee: Iteriere über alle kontrollierten Zielkarten des Spielers;
        // prüfe bei jeder, ob sie einen entsprechenden Effekt (anhaltenden Effekt, EffectType.PERSISTENT) besitzt;
        // falls dem so ist, dann füge den entsprechenden Marker zu den zusätzlich erlaubten Markern hinzu
	    List<Marker> markersAllowedByEffect = new ArrayList<Marker>();
        if (this.controlledTargetCards != null) {
            for (TargetCard aControlledCard : this.controlledTargetCards) {
                if (aControlledCard.hasPersistentEffect()) {
                    Marker newMarker = ((EffectPersistent)aControlledCard.getEffectOfCard()).transmuteMarker(input);
                    markersAllowedByEffect.add(newMarker);
                }
            }                
        }
        return markersAllowedByEffect;
	}


    /******* 8. Other methods ********/

	public void powerwas() {
		this.setNumberOfLegionsAvailable(GameConstants.DEFAULT_NUMBER_OF_LEGIONS_START);
		this.choosenTargetCards = null;
		this.controlledTargetCards = null;
		this.setGames(null);
	}
	
}
