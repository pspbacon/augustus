package ch.uzh.ifi.seal.soprafs14.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.uzh.ifi.seal.soprafs14.controller.beans.game.TargetCardBean;
import ch.uzh.ifi.seal.soprafs14.model.Game;
import ch.uzh.ifi.seal.soprafs14.model.TargetCard;
import ch.uzh.ifi.seal.soprafs14.model.User;
import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;

public final class GameStartStopDrawService{
    
    // hide utility class public default constructor
    private GameStartStopDrawService() {
    }

    private static Logger logger = LoggerFactory.getLogger(GameAdminService.class);
    
    /** 2.1 - start a game **/
    public static Boolean startGame(Long userId, Long gameId, UserRepository userRepo, GameRepository gameRepo) {
        // TODO: nur zulaessig, falls das Spiel vorher initialisiert wurde
        try{
        logger.debug("startGame: " + gameId);

        User user = userRepo.findOne(userId);
        Game game = gameRepo.findOne(gameId);

        logger.debug("USERNAME of User: " + user.getName());
        logger.debug("USERNAME of Owner: " + game.getOwner());
        if (user.getUsername().equals(game.getOwner())) {
            // owner posts this
            logger.debug("getGame: " + gameId);

            // TODO: start card generation should be assured in this method
            game.run();

            // generate startcards in corresponding userRepos
            for (User player : game.getPlayers()) {
                generateStartcards(game, player);
                userRepo.save(player);
            }
            
            gameRepo.save(game);
            return true;
        }
        return false;
        }
        catch (Exception e){
            logger.debug("wasn't able to start the game. ");
        }
        return null;
    }
    
    /** 2.2 - stop a game **/
    public static Boolean stopGame(Long gameId, String userToken, UserRepository userRepo, GameRepository gameRepo) throws Exception{
        try{
        Game game = gameRepo.findOne(gameId);
        User owner = userRepo.findByToken(userToken);

        if (owner != null && game != null
                && game.getOwner().equals(owner.getUsername())) {
            logger.debug("delete game");
            game.delete();
            gameRepo.delete(game);
            return true;
        }
        return false;
        }
        catch(Exception e){
            logger.debug("wasn't able to stop game");
        }
        return null;
    }
    
    /** 2.3 - draw cards **/
    public static List<TargetCardBean> drawCards(Long gameId, GameRepository gameRepo){
        try{
            logger.debug("deliver drawable target cards in game: " + gameId);
            Game game = gameRepo.findOne(gameId);
        
            ArrayList<TargetCardBean> currentlyDrawableTargetCardBeans = new ArrayList<TargetCardBean>();
            // get drawable target cards for game and package them all into beans and those in turn in an ArrayList instance
            for (TargetCard tmpCard : game.getDrawableTargetCards()) {
                currentlyDrawableTargetCardBeans.add(tmpCard.packIntoBean());
            }
            return currentlyDrawableTargetCardBeans;
        }
        catch(Exception e){
            logger.debug("wasn't able to draw cards");
        }
        return null;
    }
    
    /** 2.4 - generate startcards with startGame **/
    private static void generateStartcards(Game game, User user) {
        try{
        Map<Integer, TargetCard> choosenTargetCards = new HashMap<Integer, TargetCard>();

        for (int i = 1; i <= game.numberOfStartcardsGiven; i++) {
            TargetCard tmpCard = game.getCardFromCardStack();
            logger.debug("[GameId: " + game.getId() + ", PlayerId: " + user.getId() + "]: new startcard: " + tmpCard.getNumber());
            choosenTargetCards.put(tmpCard.getNumber(), tmpCard);
        }
        
        user.updateTargetMap(choosenTargetCards);
        user.setIsReady(false);
        }
        catch(Exception e){
            logger.debug("wasn't able to generate cards");
        }
    }
    
    /** 2.5 - method to access "generateStartcards" from outside **/
    public static Boolean generateStartcardsPublic(Game game, User user){
         generateStartcards(game, user);
         return null;
    }
}