package ch.uzh.ifi.seal.soprafs14.controller.beans.game;

public class AveCaesarBean {
	
	private Integer numberOfAveCaesarCard;
	private Integer numberOfChoosenCard;
	
	public Integer getNumberOfAveCaesarCard() {
		return numberOfAveCaesarCard;
	}
	public void setNumberOfAveCaesarCard(Integer numberOfAveCaesarCard) {
		this.numberOfAveCaesarCard = numberOfAveCaesarCard;
	}
	public Integer getNumberOfChoosenCard() {
		return numberOfChoosenCard;
	}
	public void setNumberOfChoosenCard(Integer numberOfChoosenCard) {
		this.numberOfChoosenCard = numberOfChoosenCard;
	}

}
