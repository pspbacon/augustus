package ch.uzh.ifi.seal.soprafs14.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GameResponseBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GameRequestBean;
import ch.uzh.ifi.seal.soprafs14.model.Game;
import ch.uzh.ifi.seal.soprafs14.model.Move;
import ch.uzh.ifi.seal.soprafs14.model.TargetCard;
import ch.uzh.ifi.seal.soprafs14.model.User;
import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;

public final class GameAdminService{

    // hide utility class public default constructor
    private GameAdminService() {
    }

    private static Logger logger = LoggerFactory.getLogger(GameAdminService.class);
    
    /** 1.1 - listGames**/
    public static List<GameResponseBean> listGames(GameRepository gameRepo){
        logger.debug("List games: ");
        List<GameResponseBean> result = new ArrayList<>();
        
        GameResponseBean tmpGameResponseBean;
        for (Game game : gameRepo.findAll()) {
            tmpGameResponseBean = new GameResponseBean();

            tmpGameResponseBean.setId(game.getId());
            tmpGameResponseBean.setGame(game.getName());
            tmpGameResponseBean.setOwner(game.getOwner());
            tmpGameResponseBean.setStatus(game.getStatus());
            tmpGameResponseBean.setNumberOfMoves(game.getMoves().size());
            tmpGameResponseBean.setNumberOfPlayers(game.getPlayers().size());

            if (game.getNextPlayer() != null) {
                tmpGameResponseBean.setNextPlayer(game.getNextPlayer()
                        .getUsername());
            }

            result.add(tmpGameResponseBean);
        }

        return result;
    }
    
    /** 1.2 - add a new game**/
    public static Boolean addGame(GameRequestBean gameRequestBean, UserRepository userRepo, GameRepository gameRepo){
        try{
        User owner = userRepo.findByToken(gameRequestBean.getUserToken());

        if (owner != null) {
            
        	logger.debug("User was found");
            /* create new game */
            Game game = new Game(gameRequestBean.getName(), owner.getUsername());
            

            /* initialize game */
            game.initialize();
            // write drawable target cards to logger
            String loggerText = "There were " + game.getDrawableTargetCards().size() + " target cards taken from the target card deck and put as drawable cards; IDs of cards: ";
            StringBuffer myStringBuffer = new StringBuffer();
            for (TargetCard aDrawableCard : game.getDrawableTargetCards()) {
                myStringBuffer.append(aDrawableCard.getNumber().toString());
                myStringBuffer.append(" ");
            }

            loggerText = loggerText + myStringBuffer.toString();
            logger.debug(loggerText);
            
            // create 0 move
            Move tmpMove = new Move();
            Long tmpLong = Long.valueOf(0);
            tmpMove.setMoveId(tmpLong);
            game.addMove(tmpMove);

            // map the created game to the respective player in the user
            // repository and save
            game = gameRepo.save(game);
            List<Game> tmplist = owner.getGames();
            tmplist.add(game);
            owner.setGames(tmplist);
            userRepo.save(owner);
            logger.debug("new game added with ID: " + game.getId());
            // TODO Mapping into Game

            return true;
        }
        return false;
        }
        catch(Exception e){
            logger.debug("game could not be added");
        }
        return null;
    }

    /** 1.2.1 - add a new game and gameId **/
    public static Long addGame1(GameRequestBean gameRequestBean, UserRepository userRepo, GameRepository gameRepo){
        try{
        User owner = userRepo.findByToken(gameRequestBean.getUserToken());

        if (owner != null) {
            
        	logger.debug("User was found");
            /* create new game */
            Game game = new Game(gameRequestBean.getName(), owner.getUsername());
            

            /* initialize game */
            game.initialize();
            // write drawable target cards to logger
            String loggerText = "There were " + game.getDrawableTargetCards().size() + " target cards taken from the target card deck and put as drawable cards; IDs of cards: ";
            StringBuffer myStringBuffer = new StringBuffer();
            for (TargetCard aDrawableCard : game.getDrawableTargetCards()) {
                myStringBuffer.append(aDrawableCard.getNumber().toString());
                myStringBuffer.append(" ");
            }

            loggerText = loggerText + myStringBuffer.toString();
            logger.debug(loggerText);
            
            // create 0 move
            Move tmpMove = new Move();
            Long tmpLong = Long.valueOf(0);
            tmpMove.setMoveId(tmpLong);
            game.addMove(tmpMove);

            // map the created game to the respective player in the user
            // repository and save
            game = gameRepo.save(game);
            List<Game> tmplist = owner.getGames();
            tmplist.add(game);
            owner.setGames(tmplist);
            userRepo.save(owner);
            logger.debug("new game added with ID: " + game.getId());
            // TODO Mapping into Game

            return game.getId();
        }
        return null;
        }
        catch(Exception e){
            logger.debug("game could not be added");
        }
        return null;
    }
    
    /** 1.3 - get a specific game **/
    public static GameResponseBean getGame(GameRepository gameRepo, Long gameId){
        try{
            Game game = gameRepo.findOne(gameId);

            GameResponseBean gameResponseBean = new GameResponseBean();

            gameResponseBean.setId(game.getId());
            gameResponseBean.setGame(game.getName());
            gameResponseBean.setOwner(game.getOwner());
            gameResponseBean.setStatus(game.getStatus());
            gameResponseBean.setNumberOfMoves(game.getMoves().size());
            gameResponseBean.setNumberOfPlayers(game.getPlayers().size());
            if (game.getNextPlayer() != null) {
                gameResponseBean.setNextPlayer(game.getNextPlayer().getUsername());
            }
            return gameResponseBean;
        }
        catch(Exception e){
            logger.debug("was not able to locate game ");
        }
        return null;
    }
}