package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

public class LegionField implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Marker markerType;
	private Boolean isSet;
	
	public LegionField(Marker markerType) {
		this.markerType = markerType;
		this.isSet = false;
	}
	
	public Marker getMarker() {
		return markerType;
	}
	
	public Boolean getSet() {
		return isSet;
	}
	
	public void setSet(Boolean isSet) {
		this.isSet = isSet;
	}

}
