package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

public class RewardNumberOfTargetCards extends Reward implements Serializable {	
		private static final long serialVersionUID = 1L;

		private final Integer numberOfCardsToControl;
		
		public RewardNumberOfTargetCards(Long id, String description, RewardType type, Integer points, Integer numberOfCardsToControl) {
			this.id = id;
			this.description = description;
			this.type = type;
			this.points = points;
			this.isAuto = false;
			this.isMovable = false;
			this.numberOfCardsToControl = numberOfCardsToControl;
			this.isAssigned = false;
		}

		public Boolean isEligible(User anUser, Game aGame) {
			if (anUser.getNumberOfControlledTargetCards().equals(this.numberOfCardsToControl)) {
			    // check whether this player has already drawn a drawable reward
			    Boolean hasAlreadyDrawn = false;
			    for(Reward aRewardHold : anUser.getRewardsHold()) {
			        if (aRewardHold.isAuto() == false) {
			            hasAlreadyDrawn = true;
			        }
			    }
			    if (hasAlreadyDrawn) {
			        return false;
			    } else {
			        return true;
			    }
			} else {
				return false;
			}
			// TODO: das soll nur genau dann 'true' zurueckgeben, 
			// wenn der Spieler die Anzahl zum allerersten Mal erreicht... - 
			// was ein bisschen ein Problem sein koennte, falls diese Methode 
			// ausserhalb einer Ave-Caesar-Situation aufgerufen wird...
		}

}
