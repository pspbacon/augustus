package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

public class VictoryPointsFixed extends VictoryPoint implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private final Integer points;
	
	public VictoryPointsFixed(Integer points) {
		this.points = points;
	}
	
	public Integer getPoints() {
		return points;
	}
	
	public Integer getPoints(User user) {
	    return this.getPoints();
	}
	
	public Boolean isVariable() {
		return false;
	}

	public Boolean isFixed() {
		return true;
	}

}
