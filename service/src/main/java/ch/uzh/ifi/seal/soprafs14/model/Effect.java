package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

public class Effect implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private final Integer effectId;
	private final String description;
	private final EffectType effectType;
	
	public Effect(Integer effectId, String description, EffectType effectType) {
		this.effectId = effectId;
		this.description = description;
		this.effectType = effectType;
	}
	
	public Integer getID() {
		return effectId;
	}
	
	public String getDescription() {
		return description;
	}
	
	public EffectType getEffectType() {
		return effectType;
	}

	public Boolean isImmediate() {
		return (this.effectType == EffectType.IMMEDIATELY);
	}

	public Boolean isPersistent() {
		return (this.effectType == EffectType.PERSISTENT);
	}

	public Boolean isEndOfGame() {
		return (this.effectType == EffectType.POINT);
	}
	
	public Boolean isRed() {
        if (this.isImmediate()) {
            return (((EffectImmediate)this).isRed());
        } else {
            return false;
        }	    
	}
	
	protected String getImmediateEffectCategoryAsString() {
		return null;
	};

}
