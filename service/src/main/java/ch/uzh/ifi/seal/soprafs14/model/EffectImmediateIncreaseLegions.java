package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

public class EffectImmediateIncreaseLegions extends EffectImmediate implements Serializable {

    private static final long serialVersionUID = 1L;
    
    // Anzahl Legionen, die der Spieler zusätzlich für seinen Vorrat erhält
    private final Integer increaseOfLegions;

    public EffectImmediateIncreaseLegions(Integer effectId, String description, Integer increaseOfLegions) {
        super(effectId, description, ImmediateEffectCategory.INCREASE_LEGIONS);
        // TODO: optional: pruefen, ob der Wert nicht negativ ist und ggf. Warnung ausgeben
        this.increaseOfLegions = increaseOfLegions;
    }
    
    public Integer getIncreaseOfLegions() {
        return increaseOfLegions;
    }
    
}
