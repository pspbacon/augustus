package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

public class EffectImmediateRemoveLegions extends EffectImmediate implements Serializable {

    private static final long serialVersionUID = 1L;
    
    // Anzahl Legionen, die alle Gegenspieler von ihren Zielkarten entfernen müssen
    private final Integer numberOfLegionsToRemove;

    public EffectImmediateRemoveLegions(Integer effectId, String description, Integer numberOfLegionsToRemove) {
        super(effectId, description, ImmediateEffectCategory.REMOVE_LEGIONS);
        this.numberOfLegionsToRemove = numberOfLegionsToRemove;
    }

}
