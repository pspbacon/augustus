package ch.uzh.ifi.seal.soprafs14.service;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.uzh.ifi.seal.soprafs14.model.Game;
import ch.uzh.ifi.seal.soprafs14.model.LegionField;
import ch.uzh.ifi.seal.soprafs14.model.TargetCard;
import ch.uzh.ifi.seal.soprafs14.model.User;
import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;

public class FastForwardService {
	
    // hide utility class public default constructor
    private FastForwardService() {
    }

	private static Logger logger = LoggerFactory.getLogger(FastForwardService.class);
	
	/** Part 1: initialize Fastforwarding **/
	public static void initializeFastforwarding(Long gameId, Long playerId, UserRepository userRepo, GameRepository gameRepo){
		try {
			
			Game game = gameRepo.findOne(gameId);
			User player = userRepo.findOne(playerId);
			
			Integer legions = player.getNumberOfLegionsAvailable();
			
			// fetch a new card from the TargetCardStack and put it to the controlled cards
			// the amount of new cards added depends on the number of cards, that are already 
			Integer newCards = 6 -player.getNumberOfControlledTargetCards();
			
			for(int i = 0; i < newCards; i++) {
				TargetCard tmpTargetCard = game.getCardFromCardStack();
				player.addControlledCard(tmpTargetCard);
			}
			
			// fetch first targetCard from assignedCards
			int i = 1;
			for (Map.Entry<Integer, TargetCard> entry : player.getCardMap().entrySet()) {
				if(i==1) {
					// set all statusFields to assigned but the first field
					int j = 1;
					for(LegionField legionField : entry.getValue().getLegionFields()) {
						if(j==1) {
							if(legionField.getSet()) {
								legionField.setSet(false);
								++legions;
							}
						} else {
							if(!legionField.getSet()) {
								legionField.setSet(true);
								--legions;
							}
						
						}
						++j;
					}
				} else {
					// remove legions from all other cards to secure that the number
					// of available legions stay within the bounds
					for(LegionField legionField : entry.getValue().getLegionFields()) {
						if(legionField.getSet()) {
							legionField.setSet(false);
							++legions;
						}
					}
				}
				++i;
			}			
			
			// update legions
			player.setNumberOfLegionsAvailable(legions);
			
			logger.debug("Fastforwarding successful!");
			
			userRepo.save(player);
			gameRepo.save(game);
			
		} catch(Exception e) {
			logger.error("[ERROR]: FastForwarding not successfull");
			logger.error(e.toString());
		}
		
		
		return;
	}
	
	/** Part 2: initialize Fullcard **/
	public static void initializeFullcard(Long gameId, Long playerId, UserRepository userRepo, GameRepository gameRepo){
		try {
			
			Game game = gameRepo.findOne(gameId);
			User player = userRepo.findOne(playerId);
			
			Integer legions = player.getNumberOfLegionsAvailable();
			
			
			// fetch first targetCard from assignedCards
			int i = 1;
			for (Map.Entry<Integer, TargetCard> entry : player.getCardMap().entrySet()) {
				if(i==1) {
					// set all statusFields to assigned but the first field
					for(LegionField legionField : entry.getValue().getLegionFields()) {
						
						if(!legionField.getSet()) {
							legionField.setSet(true);
							--legions;
						}
					
					}
				} else {
					// remove legions from all other cards to secure that the number
					// of available legions stay within the bounds
					for(LegionField legionField : entry.getValue().getLegionFields()) {
						if(legionField.getSet()) {
							legionField.setSet(false);
							++legions;
						}
					}
				}
				++i;
			}			
			
			// update legions
			player.setNumberOfLegionsAvailable(legions);
			
			logger.debug("Fastforwarding successful!");
			
			userRepo.save(player);
			gameRepo.save(game);
			
		} catch(Exception e) {
			logger.error("[ERROR]: FastForwarding not successfull");
			logger.error(e.toString());
		}
		
		
		return;
	}
	
	/** Part 3: initialize end game **/
	public static void initializeEndgame(Long gameId, Long playerId, UserRepository userRepo, GameRepository gameRepo){
		try {
			
			Game game = gameRepo.findOne(gameId);
			User player = userRepo.findOne(playerId);
			
			Integer legions = player.getNumberOfLegionsAvailable();
			
			// fetch a new card from the TargetCardStack and put it to the controlled cards
			// the amount of new cards added depends on the number of cards, that are already 
			Integer newCards = 6 -player.getNumberOfControlledTargetCards();
			
			for(int i = 0; i < newCards; i++) {
				TargetCard tmpTargetCard = game.getCardFromCardStack();
				player.addControlledCard(tmpTargetCard);
			}
			
			// fetch first targetCard from assignedCards
			int i = 1;
			for (Map.Entry<Integer, TargetCard> entry : player.getCardMap().entrySet()) {
				if(i==1) {
					for(LegionField legionField : entry.getValue().getLegionFields()) {
						
						if(!legionField.getSet()) {
							legionField.setSet(true);
							--legions;
						}
						
						
					}
				} else {
					// remove legions from all other cards to secure that the number
					// of available legions stay within the bounds
					for(LegionField legionField : entry.getValue().getLegionFields()) {
						if(legionField.getSet()) {
							legionField.setSet(false);
							++legions;
						}
					}
				}
				++i;
			}			
			
			// update legions
			player.setNumberOfLegionsAvailable(legions);
			
			logger.debug("Fastforwarding successful!");
			
			userRepo.save(player);
			gameRepo.save(game);
			
		} catch(Exception e) {
			logger.error("[ERROR]: FastForwarding not successfull");
			logger.error(e.toString());
		}
		
		
		return;
	}
	
	/** Part 4: mobilize all legions **/
	public static void mobilizeAllLegions(UserRepository userRepo, Long playerId) {
		
		User player = userRepo.findOne(playerId);
		
		Integer legions = player.getNumberOfLegionsAvailable();
		
		
		for (Map.Entry<Integer, TargetCard> entry : player.getCardMap().entrySet()) {
			for(LegionField legionField : entry.getValue().getLegionFields()) {
				if(legions > 0 && !legionField.getSet()) {
					legionField.setSet(true);
					--legions;
				}
			}
		}
					
		
		// update legions
		// should be zero
		player.setNumberOfLegionsAvailable(legions);
		
		logger.debug("Activated all legions! - Total Warfare");
		
		userRepo.save(player);
		
		return;
	}

}
