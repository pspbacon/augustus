package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

public enum Commodity implements Serializable {
	GOLD(1, "Gold", "Gold"),
	WHEAT(2, "Wheat", "Weizen");
	
	private final Integer code;
	private final String label;
	private final String labelDe;
	
	private Commodity(int code, String label, String labelDe) {
		this.code = code;
		this.label = label;
		this.labelDe = labelDe;
	}
	
	public Integer getCode() {
		return code;
	}
	
	public String getLabel() {
		return label;
	}
	
	public String getLabelDe() {
		return labelDe;
	}

}
