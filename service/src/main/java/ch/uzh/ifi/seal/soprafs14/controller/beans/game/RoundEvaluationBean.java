package ch.uzh.ifi.seal.soprafs14.controller.beans.game;

import java.util.List;

public class RoundEvaluationBean {

    // Dieses Bean fasst die Informationen einer Rundenauswertung zusammen
    
    // Gesamtzahl der Spieler, die in dieser Runde (anfaenglich) Ave Caesar erreicht haben, i.e. mindestens eine Karte erfuellt haben
    private Integer numberOfPlayersWithAveCaesar;

    // Anzahl der Spieler, die noch abgearbeitet werden muessen
    private Integer numberOfPlayersToHandle;

    // Gesamtzahl der Zielkarten, die in dieser Runde (anfaenglich) erfuellt wurden
    private Integer numberOfTargetCardsWithAveCaesar;

    // Anzahl der Zielkarten, die noch abgearbeitet werden muessen
    private Integer numberOfTargetCardsToHandle;
    
    // Rundennummer; evtl. weglassen
    private Integer roundNumber;
    
    // selbsterkaerend
    private List<String> namesOfPlayersWithAveCaesar;

    // selbsterklaerend
    private List<List<Integer>> cardsOfPlayersWithAveCaesar;

    // naechster Spieler, der sein Ave Caesar ausfuehrt
    private String nextPlayer;

    // naechste Zielkarte, fuer die Ave Caesar ausgefuehrt wird
    private Integer nextTargetCard;

    // true genau dann, wenn ein Spieler die fuer den Sieg notwendige Anzahl an Zielkarten am Ende der aktuellen Runde erfuellt hat
    private Boolean playerFinished;

    // User-ID des Siegers, sofern dieser schon feststeht (andernfalls null)
    private Long winnerId;
    
    // Punktzahl des Siegers (sofern dieser schon feststeht; andernfalls null)
    private Integer winnerPoints;
    
    // Message für wartende Spieler
    private String message;
    
    // Flag das anzeigt, ob die als naechstes abzuabreitende Zielkarte einen sofortigen Effekt besitzt
    private Boolean hasImmediateEffect;
    
    // Flag das anzeigt, ob die wartenden Spieler einen negativen Effekt ausführen müssen
    private Boolean hasNegativeEffect;
    
    // ID des allfälligen Effekts der als nächstes abzuarbeitenden Zielkarte, falls diese einen negativen Effekt hat
    private Integer nextTargetCardNegativeEffectId;
    
    public Boolean allHandledAndReady() {
        return (numberOfPlayersToHandle == 0);
    }

    public Integer getNumberOfPlayersWithAveCaesar() {
        return numberOfPlayersWithAveCaesar;
    }
    public void setNumberOfPlayersWithAveCaesar(Integer number) {
        this.numberOfPlayersWithAveCaesar = number;
    }
    
    public Integer getNumberOfPlayersToHandle() {
        return numberOfPlayersToHandle;
    }
    public void setNumberOfPlayersToHandle(Integer number) {
        this.numberOfPlayersToHandle = number;
    }
    
    public Integer getNumberOfTargetCardsWithAveCaesar() {
        return numberOfTargetCardsWithAveCaesar;
    }
    public void setNumberOfTargetCardsWithAveCaesar(Integer number) {
        this.numberOfTargetCardsWithAveCaesar = number;
    }
    
    public Integer getNumberOfTargetCardsToHandle() {
        return numberOfTargetCardsToHandle;
    }
    public void setNumberOfTargetCardsToHandle(Integer number) {
        this.numberOfTargetCardsToHandle = number;
    }
    
    public Integer getRoundNumber() {
        return roundNumber;
    }
    public void setRoundNumber(Integer number) {
        this.roundNumber = number;
    }
    
    public List<String> getNamesOfPlayersWithAveCaesar() {
        return namesOfPlayersWithAveCaesar;
    }
    public void setNamesOfPlayersWithAveCaesar(List<String> namesList) {
        this.namesOfPlayersWithAveCaesar = namesList;
    }

    public List<List<Integer>> getCardsOfPlayersWithAveCaesar() {
        return cardsOfPlayersWithAveCaesar;
    }
    public void setCardsOfPlayersWithAveCaesar(List<List<Integer>> cardList) {
        this.cardsOfPlayersWithAveCaesar = cardList;
    }
    
    public String getNextPlayer() {
        return nextPlayer;
    }
    public void setNextPlayer(String nextOne) {
        this.nextPlayer = nextOne;
    }

    public Integer getNextTargetCard() {
        return nextTargetCard;
    }
    public void setNextTargetCard(Integer nextCard) {
        this.nextTargetCard = nextCard;
    }

    public Long getWinnerId() {
        return winnerId;
    }
    public void setWinner(Long winnerId) {
        this.winnerId = winnerId;
    }

    public Integer getWinnerPoints() {
        return winnerPoints;
    }
    public void setWinnerPoints(Integer winnerPoints) {
        this.winnerPoints = winnerPoints;
    }

    public Boolean playerFinished() {
        return playerFinished;
    }
    public void setPlayerFinished() {
        this.playerFinished = true;
    }
    public void setNobodyHasFinished() {
        this.playerFinished = false;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    public String getMessage() {
        return message;
    }

    public void setHasImmediateEffect(Boolean status) {
        this.hasImmediateEffect = status;
    }
    
    public Boolean getHasImmediateEffect() {
        return hasImmediateEffect;
    }

    public Boolean getHasNegativeEffect() {
        return hasNegativeEffect;
    }

    public void setHasNegativeEffect(Boolean hasNegativeEffect) {
        this.hasNegativeEffect = hasNegativeEffect;
    }
    
    public Integer getNextTargetCardNegativeEffectId() {
        return nextTargetCardNegativeEffectId;
    }
    
    public void setNextTargetCardNegativeEffectId(Integer nextTargetCardNegativeEffectId) {
        this.nextTargetCardNegativeEffectId = nextTargetCardNegativeEffectId;
    }

}
