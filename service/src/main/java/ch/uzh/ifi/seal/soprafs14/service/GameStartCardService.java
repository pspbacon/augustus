package ch.uzh.ifi.seal.soprafs14.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.uzh.ifi.seal.soprafs14.controller.beans.game.TargetCardBean;
import ch.uzh.ifi.seal.soprafs14.model.TargetCard;
import ch.uzh.ifi.seal.soprafs14.model.User;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;

public final class GameStartCardService {

	private GameStartCardService(){}
	
	private static Logger logger = LoggerFactory.getLogger(GameStartCardService.class);
	
	/** 6.1 - get own start cards **/
	public static List<TargetCardBean> deliverStartCards(Long gameId, Long playerId, UserRepository userRepo){
		//Game game;
				User player;
				
				try {
					//game = gameRepo.findOne(gameId);
					player = userRepo.findOne(playerId);
				
					logger.debug("[gameId: " + gameId + ", playerId: " + playerId + "]: delivering startcards");
					
					List<TargetCardBean> tmpTargetCardBean = new ArrayList<TargetCardBean>();
					
					// Iterator for map (recommended by StackOverflow)
					for (Map.Entry<Integer, TargetCard> entry : player.getCardMap().entrySet()) {
						TargetCardBean tmpBean = player.getCardMap().get(entry.getKey()).packIntoBean();
				        tmpTargetCardBean.add(tmpBean);
				        logger.debug("[gameId: " + gameId + ", playerId: " + player.getId() + "]: startCard " + entry.getKey());
					}
					
					return tmpTargetCardBean;
					
				} catch(Exception e) {
					logger.error("Couldn't deliver startcards");
					logger.error(e.toString());
				}
					
				return null;
	}
	
	/** 6.2 - post the chosen startcards from a player**/
	public static void updateStartCards(Long gameId, Long playerId, List<TargetCardBean> cardIdBean, UserRepository userRepo){
		logger.debug("Received " + cardIdBean.size() + " TargetCardRequestBeans in game: " + gameId + " from player: " + playerId);
		
		// Game game = gameRepo.findOne(gameId);
		User user = userRepo.findOne(playerId);
		
		for(TargetCardBean numbersToRemove : cardIdBean) {
			user.removeCardFromMap(numbersToRemove.getNumber());
		}
		
		user.setIsReady(true);
		userRepo.save(user);
		
		return;
	}
}