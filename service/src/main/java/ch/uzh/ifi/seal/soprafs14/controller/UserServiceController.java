package ch.uzh.ifi.seal.soprafs14.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserLoginLogoutRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserLoginLogoutResponseBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserResponseBean;

import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;
import ch.uzh.ifi.seal.soprafs14.service.UserService;

@Controller("userServiceController")
public class UserServiceController extends GenericService {

	Logger logger = LoggerFactory.getLogger(UserServiceController.class);

	@Autowired
	private UserRepository userRepo;

	private final String CONTEXT = "/user";

	/** Part 1 - Return a List with all users and their games **/
	@RequestMapping(value = CONTEXT)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<UserResponseBean> listUsers() {
		logger.debug("listUsers");
		return UserService.listUsers(userRepo);
	}

	/** Part 2 - Create new user **/
	@RequestMapping(value = CONTEXT, method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Boolean addUser(@RequestBody UserRequestBean userRequestBean) {
		logger.debug("addUser: " + userRequestBean.getName());
		UserService.addUser(userRequestBean, userRepo);
		return true;
	}

	/** Part 3 - specific user request **/
	@RequestMapping(value = CONTEXT + "/{userId}")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public UserResponseBean getUser(@PathVariable Long userId) {
		logger.debug("getUser: " + userId);
		return UserService.getUser(userId, userRepo);
	}
	
	/** Part 4 - User Login**/
	@RequestMapping(value = CONTEXT + "/{userId}/login", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public UserLoginLogoutResponseBean login(@PathVariable Long userId) {
		logger.debug("login: " + userId);
		return UserService.loginUser(userId, userRepo);
	}
	
	/** Part 5 - User logout **/
	@RequestMapping(value = CONTEXT + "/{userId}/logout", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void logout(@PathVariable Long userId, @RequestBody UserLoginLogoutRequestBean userLoginLogoutRequestBean) {
		logger.debug("getUser: " + userId);
		UserService.logoutUser(userId, userLoginLogoutRequestBean, userRepo);
	}
	
	/** Part 6 - specific user request **/
	@RequestMapping(value = CONTEXT + "/delete")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void deleteUsers() {
		logger.debug("deleting userRepo ");
		userRepo.deleteAll();;
	}
}
