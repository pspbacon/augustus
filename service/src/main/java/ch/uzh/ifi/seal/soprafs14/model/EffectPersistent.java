package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

// Diese Klasse bildet den anhaltenden Effekt ab, dass ein Spieler für einen bestimmten gezogenen Marker
// auch einen bestimmten anderen Marker belegen kann.
// Vorsicht: Die Fähigkeit ist kommutativ, wirkt aber nicht transitiv mit anderen allfällig kontrollierten Fähigkeiten
public class EffectPersistent extends Effect implements Serializable {

    private static final long serialVersionUID = 1L;

    // Marker der gezogen wurde
    private final Marker marker1;
    
    // Marker der durch die Fähigkeit ebenfalls belegt werden kann
    private final Marker marker2;

    public EffectPersistent(Integer effectId, String description, Marker marker1, Marker marker2) {
        super(effectId, description, EffectType.PERSISTENT);
        this.marker1 = marker1;
        this.marker2 = marker2;
    }
    
    // gibt den jeweils anderen Marker zurück für den als Argument übergebenen
    public Marker transmuteMarker(Marker input) {
        if (marker1 == input) {
            return marker2;
        }
        else if (marker2 == input) {
            return marker1;
        }
        else {
            return null;
        }
    }
    
    public String getImmediateEffectCategoryAsString() {
        return null;
    }
    
    protected String[] getMarkers() {
        String[] markers = new String[2];
        markers[0] = marker1.getName();
        markers[1] = marker2.getName();
        return markers;
    }

}
