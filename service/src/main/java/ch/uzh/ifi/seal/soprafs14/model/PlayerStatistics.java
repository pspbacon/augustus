package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;


/* counter class to store data like count of cardtypes (europa, kleinasien, senator and
 * schwerter, schild, etc)
*/
public class PlayerStatistics implements Serializable {

	private static final long serialVersionUID = 1L;
	// Cardtypes
	private Integer countEuropa;
	private Integer countKleinasien;
	private Integer countAfrika;
	
	// Markertypes
	private Integer countSchwerter;
	private Integer countSchild;
	private Integer countStreitwagen;
	private Integer countKatapult;
	private Integer countStandarte;
	private Integer countDolch;
	private Integer countJoker;
	
	
	//List<Reward> collectedRewards = new ArrayList<TargetCard>();
	//List<Effect> collectedEffects = new ArrayList<Effect>();
	
	public Integer getCountEuropa() {
		return countEuropa;
	}
	public void setCountEuropa(Integer countEuropa) {
		this.countEuropa = countEuropa;
	}
	public Integer getCountKleinasien() {
		return countKleinasien;
	}
	public void setCountKleinasien(Integer countKleinasien) {
		this.countKleinasien = countKleinasien;
	}
	public Integer getCountAfrika() {
		return countAfrika;
	}
	public void setCountAfrika(Integer countAfrika) {
		this.countAfrika = countAfrika;
	}
	public Integer getCountSchwerter() {
		return countSchwerter;
	}
	public void setCountSchwerter(Integer countSchwerter) {
		this.countSchwerter = countSchwerter;
	}
	public Integer getCountSchild() {
		return countSchild;
	}
	public void setCountSchild(Integer countSchild) {
		this.countSchild = countSchild;
	}
	public Integer getCountStreitwagen() {
		return countStreitwagen;
	}
	public void setCountStreitwagen(Integer countStreitwagen) {
		this.countStreitwagen = countStreitwagen;
	}
	public Integer getCountKatapult() {
		return countKatapult;
	}
	public void setCountKatapult(Integer countKatapult) {
		this.countKatapult = countKatapult;
	}
	public Integer getCountStandarte() {
		return countStandarte;
	}
	public void setCountStandarte(Integer countStandarte) {
		this.countStandarte = countStandarte;
	}
	public Integer getCountDolch() {
		return countDolch;
	}
	public void setCountDolch(Integer countDolch) {
		this.countDolch = countDolch;
	}
	public Integer getCountJoker() {
		return countJoker;
	}
	public void setCountJoker(Integer countJoker) {
		this.countJoker = countJoker;
	}
	
	
	
}
