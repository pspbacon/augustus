package ch.uzh.ifi.seal.soprafs14.service;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserLoginLogoutRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserLoginLogoutResponseBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.user.UserResponseBean;
import ch.uzh.ifi.seal.soprafs14.model.Game;
import ch.uzh.ifi.seal.soprafs14.model.User;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;

public final class UserService {

    // hide utility class public default constructor
    private UserService() {
    }

    private static Logger logger = LoggerFactory.getLogger(UserService.class);
    
    /** Part 1 - list users **/
    public static List<UserResponseBean> listUsers(UserRepository userRepo){
        try{
            logger.debug("listUsers");
            List<UserResponseBean> result = new ArrayList<>();
            UserResponseBean tmpUserResponseBean;
        
            for(User user : userRepo.findAll()) {
                tmpUserResponseBean = new UserResponseBean();
            
                tmpUserResponseBean.setId(user.getId());
                tmpUserResponseBean.setName(user.getName());
                tmpUserResponseBean.setUsername(user.getUsername());

            
                List<String>games = new ArrayList<>();
                for(Game game : user.getGames()) {
                    games.add(game.getName());

                }
            
                tmpUserResponseBean.setGames(games);
            
                result.add(tmpUserResponseBean);
            }
            return result;
        }
        catch(Exception e){
            logger.debug("failed to list users. ");
        }
        return null;
    }
    
    /** Part 2 - add user **/
    public static Boolean addUser(UserRequestBean userRequestBean, UserRepository userRepo){
        try{
            User user = new User(userRequestBean.getName(), userRequestBean.getUsername());
            user = userRepo.save(user);        
            return true;
        }
        catch(Exception e){
            logger.debug("was not able to add user. ");
        }
        return false;
    }
    
    /** Part 3 - get specific user **/
    public static UserResponseBean getUser(Long userId, UserRepository userRepo){
        try{
        User user = userRepo.findOne(userId);
        
        UserResponseBean userResponseBean = new UserResponseBean();
        if(user != null) {
            userResponseBean.setId(user.getId());
            userResponseBean.setName(user.getName());
            userResponseBean.setUsername(user.getUsername());
            
            List<String>games = new ArrayList<>();
            for(Game game : user.getGames()) {
                games.add(game.getName());
            }
            userResponseBean.setGames(games);
            logger.debug("found user: " + userResponseBean.getName());
            return userResponseBean;
        }
        }
        catch(Exception e){
            logger.debug("did not find user");
        }
        return null;
    }
    
    /** Part 4 - user login **/
    public static UserLoginLogoutResponseBean loginUser(Long userId, UserRepository userRepo){
        try{
            User user = userRepo.findOne(userId);

            if(user != null) {
                user.goOnline();
                user = userRepo.save(user);
            
                logger.debug("User " + user.getName() + " - " + user.getUsername() + " is online now");
            
                UserLoginLogoutResponseBean userLoginLogoutResponseBean = new UserLoginLogoutResponseBean();
                userLoginLogoutResponseBean.setUserToken(user.getToken());
            
                return userLoginLogoutResponseBean;
            }
            return null;
        }
        catch(Exception e){
            logger.debug("unable to login");
        }
        return null;
    }
    
    /** Part 5 - User logout **/
    public static Boolean logoutUser(Long userId, UserLoginLogoutRequestBean userLoginLogoutRequestBean, UserRepository userRepo){
        try{
        User user = userRepo.findOne(userId);
        
        if(user != null && user.getToken().equals(userLoginLogoutRequestBean.getToken())) {
            user.goOffline();
            userRepo.save(user);
            logger.debug("User " + user.getName() + " - " + user.getUsername() + " is offline now");
            return true;
        }
        return false;
        }
        catch(Exception e){
            logger.debug("unable to logout");
        }
        return null;
    }
}
