package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

//Class for variable victory points that are calculated according to number of controlled target cards of a certain card type (Senator, Europa, Afrika, Kleinasien)
public class VictoryPointsVariableCardType extends VictoryPointsVariable implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final Integer pointsPerElement;
	private final CardType cardTypeToCount;

	public VictoryPointsVariableCardType(Integer pointsPerElement, CardType cardTypeToCount) {
		this.pointsPerElement = pointsPerElement;
		this.cardTypeToCount = cardTypeToCount;
	}

	public void updateCurrentValue(Integer countElement) {
		this.currentValueForOwner = countElement * this.pointsPerElement;
	}

	public Integer getPoints(User user) {
	    Integer numberOfElements = user.getNumberOfCardsOfType(cardTypeToCount);
        return (numberOfElements * this.pointsPerElement);
	}
	
	protected String getElementToCount() {
	    return this.cardTypeToCount.getName();
	}

}
