package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ch.uzh.ifi.seal.soprafs14.controller.beans.game.RewardBean;

public abstract class Reward implements Serializable {
	private static final long serialVersionUID = 1L;
	
	protected Long id;
	protected String description;
	protected RewardType type;
	protected Integer points;
	protected Boolean isAssigned; // gibt an, ob eine Belohnung schon zugewiesen wurde
	protected Boolean isAuto; // Belohnung wird automatisch durch das Spiel zugewiesen, sobald ein Spieler dafuer berechtigt ist
	protected Boolean isMovable; // Belohnung kann von einem Spieler an einen anderen uebergehen
	
	//TODO: visibilty should be protected; only use in constructor of class Game
	public static List<Reward> createStandardRewards() { // Klassenmethode, die eine Liste mit allen bei Spielbeginn verfügbaren Rewards erzeugt
		List<Reward> rewardsList = new ArrayList<Reward>();
		// Alle zu Beginn verfügbaren Belohnungen definieren
 		Reward reward02 = new RewardNumberOfTargetCards(2L, "Spieler hat 2 Zielkarten erobert (und sich zu diesem Zeitpunkt für diese Belohnung entschieden)", RewardType.NUMBER_OF_TARGET_CARDS, 2, 2);
 		Reward reward03 = new RewardNumberOfTargetCards(3L, "Spieler hat 3 Zielkarten erobert (und sich zu diesem Zeitpunkt für diese Belohnung entschieden)", RewardType.NUMBER_OF_TARGET_CARDS, 4, 3);
 		Reward reward04 = new RewardNumberOfTargetCards(4L, "Spieler hat 4 Zielkarten erobert (und sich zu diesem Zeitpunkt für diese Belohnung entschieden)", RewardType.NUMBER_OF_TARGET_CARDS, 6, 4);
 		Reward reward05 = new RewardNumberOfTargetCards(5L, "Spieler hat 5 Zielkarten erobert (und sich zu diesem Zeitpunkt für diese Belohnung entschieden)", RewardType.NUMBER_OF_TARGET_CARDS, 8, 5);
 		Reward reward06 = new RewardNumberOfTargetCards(6L, "Spieler hat 6 Zielkarten erobert (und sich zu diesem Zeitpunkt für diese Belohnung entschieden)", RewardType.NUMBER_OF_TARGET_CARDS, 10, 6);
 		Reward reward11 = new RewardColorOfTargetCard(11L, "Spieler hat als erster drei Provinz-Zielkarten der Farbe 'grün' kontrolliert", RewardType.COLOUR_OF_TARGET_CARDS, 4, CardType.GREEN, 3);
 		Reward reward12 = new RewardColorOfTargetCard(12L, "Spieler hat als erster drei Provinz-Zielkarten der Farbe 'violett' kontrolliert", RewardType.COLOUR_OF_TARGET_CARDS, 8, CardType.VIOLET, 3);
 		Reward reward13 = new RewardColorOfTargetCard(13L, "Spieler hat als erster drei Provinz-Zielkarten der Farbe 'orange' kontrolliert", RewardType.COLOUR_OF_TARGET_CARDS, 10, CardType.ORANGE, 3);
 		Reward reward14 = new RewardColorOfTargetCard(14L, "Spieler hat als erster drei Senatoren-Zielkarten kontrolliert", RewardType.COLOUR_OF_TARGET_CARDS, 2, CardType.SENATOR, 3);
 		//TODO: anpassen, noch nicht (korrekt) implementiert
 		//Reward reward15 = new RewardColorOfTargetCard(15L, "Spieler hat als erster je eine Zielkarte jeder Art kontrolliert (je eine Provinz-Zielkarte grün, violett und orange sowie eine Senatoren-Zielkarte)", RewardType.COLOUR_OF_TARGET_CARDS, 6, null, 0);
 		Reward reward21 = new RewardControlOfCommodities(21L, "Spieler hat (als letzter) die Kontrolle über die höchste Menge des Stoffstoffs 'Gold' erreicht", RewardType.CONTROL_OF_COMMODITIES, 5, Commodity.GOLD);
 		Reward reward22 = new RewardControlOfCommodities(22L, "Spieler hat (als letzter) die Kontrolle über die höchste Menge des Stoffstoffs 'Weizen' erreicht", RewardType.CONTROL_OF_COMMODITIES, 5, Commodity.WHEAT);
 		// Alle Belohnungen in Liste des übergebenen Games schreiben
		rewardsList.add(reward02);
		rewardsList.add(reward03);
		rewardsList.add(reward04);
		rewardsList.add(reward05);
		rewardsList.add(reward06);
		rewardsList.add(reward11);
		rewardsList.add(reward12);
		rewardsList.add(reward13);
		rewardsList.add(reward14);
		//rewardsList.add(reward15);
		rewardsList.add(reward21);
		rewardsList.add(reward22);
		return rewardsList;
	}

	public Long getID() {
		return id;
	}
	
	public String getDescription() {
		return description;
	}
	
	public RewardType getType() {
		return type;
	}
	
	public Integer getPoints() {
		return points;
	}
	
	public Boolean isAuto() {
		return isAuto;
	}

	public Boolean isMovable() {
		return isMovable;
	}
	
	public Boolean getIsAssigned() {
		return isAssigned;
	}

	public void setIsAssigned(Boolean isAssigned) {
		this.isAssigned = isAssigned;
	}

	public abstract Boolean isEligible(User anUser, Game aGame);
	
	public RewardBean packIntoBean() {
		
		RewardBean myBean = new RewardBean();
		myBean.setDescription(this.getDescription());
		myBean.setId(this.getID());
		myBean.setPoints(this.getPoints());
		myBean.setRewardType(this.getType().getLabel());
		
		return myBean;
	}

}
