package ch.uzh.ifi.seal.soprafs14.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.uzh.ifi.seal.soprafs14.GameConstants;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.RoundEvaluationBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.StatisticsBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.TargetCardBean;
import ch.uzh.ifi.seal.soprafs14.model.CardType;
import ch.uzh.ifi.seal.soprafs14.model.Commodity;
import ch.uzh.ifi.seal.soprafs14.model.EffectType;
import ch.uzh.ifi.seal.soprafs14.model.Game;
import ch.uzh.ifi.seal.soprafs14.model.EffectImmediate;
import ch.uzh.ifi.seal.soprafs14.model.EffectImmediateIncreaseLegions;
import ch.uzh.ifi.seal.soprafs14.model.ImmediateEffectCategory;
import ch.uzh.ifi.seal.soprafs14.model.Marker;
import ch.uzh.ifi.seal.soprafs14.model.TargetCard;
import ch.uzh.ifi.seal.soprafs14.model.User;
import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;

public final class GameEndRoundService {

	private GameEndRoundService(){
	}
	
	private static Logger logger = LoggerFactory.getLogger(GameEndRoundService.class);
	
	/** 7.1 - evaluate a round **/
	@SuppressWarnings("unused")
    public static RoundEvaluationBean evaluateRound(Long gameId, UserRepository userRepo, GameRepository gameRepo) {
		// TODO: write Logger for debugging
				Game game;
				try {
					game = gameRepo.findOne(gameId);
				} catch(Exception e) {
					logger.error("Error: Could not find game ID: " + gameId);
					logger.error(e.toString());
					return null;
				}
				
				Integer numberOfPlayersWithAveCaesar = 0;
				
				// Spieler mit Ave Caesar und der niedrigsten Zielkarten-Nummer:
				User nextPlayer = null;
				
				// Nummer der in dieser Runde erfuellte Zielkarte mit kleinster Nummer:
				Integer minimumTargetCardNumber = GameConstants.NUMBER_OF_TARGET_CARDS + 1;
				
				List<String> namesOfPlayersWithAveCaesar = new ArrayList<String>();
				List<List<Integer>> cardsOfPlayersWithAveCaesar = new ArrayList<List<Integer>>();
				List<Integer> neutralizedCardNumbersOfPlayer = new ArrayList<Integer>();

				/* iteriere ueber alle Player und pruefe, ob sie Ave Caesar haben; alles in einer Map speichern */

				// Map: mit Paaren (User mit Ave Caesar, Zielkarte mit Ave Caesar)
				Map<User, TargetCard> aveCaesarMap = new HashMap<User, TargetCard>();
				
				for (User aPlayer : game.getPlayers()) {
					List<TargetCard> neutralizedCardsOfPlayer = aPlayer.getNeutralizedTargetCards();
					if (!neutralizedCardsOfPlayer.isEmpty()) {
						// zaehle Anzahl Spieler und Zielkarten mit Ave Caesar
						++numberOfPlayersWithAveCaesar;
                        // fuege Spieler sowie Zielkarte(n) zu entsprechenden Listen fuer Bean hinzu
                        namesOfPlayersWithAveCaesar.add(aPlayer.getUsername());
                        cardsOfPlayersWithAveCaesar.add(neutralizedCardNumbersOfPlayer);
						// iteriere ueber alle neutralisierten Zielkarten und fuege diese zusammen mit dem besitzenden User in die Map ein
						for (TargetCard aTargetCard : neutralizedCardsOfPlayer) {
							aveCaesarMap.put(aPlayer, aTargetCard);
							neutralizedCardNumbersOfPlayer.add(aTargetCard.getNumber());
			                // finde die Zielkarte mit der kleinsten Nummer sowie zugehoerigen Spieler
			                if (aTargetCard.getNumber() < minimumTargetCardNumber) {
			                    nextPlayer = aPlayer;
			                    minimumTargetCardNumber = aTargetCard.getNumber();
			                }
						}
					}
				}
				
				// zaehle Anzahl Zielkarten mit Ave Caesar (entspricht der Anzahl Eintraege in der Map)
				Integer numberOfTargetCardsWithAveCaesar = aveCaesarMap.size();

				// Legionen zuruecklegen -> bereits erledigt
				
				// Iterierien in der Reihenfolge der Nummern der Zielkarten:
				
				// Legion-Increase-Effekt der eroberten Zielkarte ausführen, falls vorhanden
				if (nextPlayer != null) {
	                TargetCard targetCardToEvaluate = nextPlayer.getCardMap().get(minimumTargetCardNumber);
				    if (targetCardToEvaluate != null) {
				        if (targetCardToEvaluate.hasImmediateEffect()) {
				            if (((EffectImmediate)(targetCardToEvaluate.getEffectOfCard())).getImmediateEffectCategory() == ImmediateEffectCategory.INCREASE_LEGIONS) {
				                Integer numberOfAdditionalLegions = ((EffectImmediateIncreaseLegions)(targetCardToEvaluate.getEffectOfCard())).getIncreaseOfLegions();
				                nextPlayer.increaseAvailableLegionsBy(numberOfAdditionalLegions);
	                            logger.debug("[gameId: " + gameId + ", playerId: " + nextPlayer +"]: found immediate legion increase effect of player identified as next player: found effect with id " + targetCardToEvaluate.getEffectOfCard().getID() + " and executed it, increasing legions of the player by " + numberOfAdditionalLegions);
				            } else {
	                            logger.debug("[gameId: " + gameId + ", playerId: " + nextPlayer +"]: checked for legion increase effect of next player: found immediate effect, but effect is not legion increase effect");
				            }
				        } else {
	                        logger.debug("[gameId: " + gameId + ", playerId: " + nextPlayer +"]: checked for legion increase effect of next player: no immediate effect found");				            
				        }
				    } else {
	                    logger.error("Error: something went terribly wrong; there is a user with ave caesar but the ave caesar card couldn't be found in his target card map.");				        
				    }
				}

                // Falls die abzuarbeitende Karte einen sofortigen Effekt besitzt, die Art des 
                // Effekts feststellen und die entsprechenden Spieler für Effekt-Abarbeitung markieren
				if (nextPlayer != null) {
	                TargetCard targetCardToEvaluate = nextPlayer.getCardMap().get(minimumTargetCardNumber);
	                if (targetCardToEvaluate.hasImmediateEffect()) {
	                    if (targetCardToEvaluate.getEffectOfCard().isRed()) {
	                        // falls der Effekt die Gegenspieler des Kartenbesitzers betrifft: 
	                        // nur diese (aber nicht den Kartenbesitzer) markieren für Effekt-Abarbeitung
	                        for (User aPlayer : game.getPlayers()) {
	                            if (aPlayer.equals(nextPlayer)) {
	                                aPlayer.markForEffectExecution();
	                            } else {
	                                aPlayer.effectExecutionCompleted();
	                            }
	                        }
	                    } else {
	                        // falls der Effekt die Gegenspieler des Kartenbesitzers betrifft: 
	                        // nur diese (aber nicht den Kartenbesitzer) markieren für Effekt-Abarbeitung
	                        for (User aPlayer : game.getPlayers()) {
	                            if (aPlayer.equals(nextPlayer)) {
	                                aPlayer.effectExecutionCompleted();
	                            } else {
	                                aPlayer.markForEffectExecution();
	                            }
	                        }                        
	                    }
	                }
				} else {
				    // TODO: not sure if this is necessary at all - probably not
                    for (User aPlayer : game.getPlayers()) {
                        aPlayer.effectExecutionCompleted();
                    }
				}

				// Allfaellige weitere Faehigkeiten der Zielkarte ausfuehren?
                // TODO: implement if necessary
				
                // Ueberpruefen, ob das Spiel fertig ist, i.e. ob ein Spieler genug Zielkarten kontrolliert (genug ist in diesem Fall durch das Attribut game.numberOfTargetCardsForVictory definiert)
                Boolean gameFinished = false;
                logger.debug("Checking whether game is finished...");
                for (User aPlayer : game.getPlayers()) {
                    // da zu diesem Zeitpunkt die AveCaesar-Karten noch nicht auf "controlled" 
                    // gesetzt sind, muss die Anzahl der bisher kontrollierten Zielkarten plus 
                    // der Anzahl der gerade neutralisierten Zielkarten gleich dem Wert 
                    // game.numberOfTargetCardsForVictory sein
                    if (((aPlayer.getNumberOfNeutralizedTargetCards() + aPlayer.getNumberOfControlledTargetCards()) == game.numberOfTargetCardsForVictory) || (aPlayer.getNumberOfControlledTargetCards() == game.numberOfTargetCardsForVictory) ) {
                        gameFinished = true;
                        logger.debug("Player " + aPlayer.getUsername() + " has " + aPlayer.getNumberOfNeutralizedTargetCards() + " neutralised cards and " + aPlayer.getNumberOfControlledTargetCards() + " controlled cards which is enough to reach the " + game.numberOfTargetCardsForVictory + " cards needed to finish the game.");
                        logger.debug("Round Evaluation: Game finished!");
                    } else {
                        logger.debug("Player " + aPlayer.getUsername() + " has " + aPlayer.getNumberOfNeutralizedTargetCards() + " neutralised cards and " + aPlayer.getNumberOfControlledTargetCards() + " controlled cards which is not enough to reach the " + game.numberOfTargetCardsForVictory + " cards needed to finish the game.");
                    }
                }
                // TODO: nochmal die Spielregeln konsultieren, was im Fall eines Spielendes noch alles ausgefuehrt wird und was nicht mehr

                // Spieler mit der höchsten Punktzahl eruieren (Sieger, falls Spiel beendet und alle Effekte ausgeführt)
                User maximusPlurissimusAugustusCaesar = new User();
                try {
                    maximusPlurissimusAugustusCaesar = game.getUserWithMaximumPoints();
                } catch (Exception e) {
                    logger.error(e.toString());
                }

				// Write debugging information to logger
				logger.debug("Round Evaluation: number of players with Ave Caesar: " + numberOfPlayersWithAveCaesar + "\n" + 
						"   number of target cards with Ave Caesar: " + numberOfTargetCardsWithAveCaesar + "\n" + 
						"   minimum target card number: " + minimumTargetCardNumber + "\n" + 
						"   game finished: " + gameFinished + "\n" + 
						"   player with most points: " + maximusPlurissimusAugustusCaesar.getUsername() + ", with " + maximusPlurissimusAugustusCaesar.getPoints() + " points (" + maximusPlurissimusAugustusCaesar.getRewardPoints() + "+" + maximusPlurissimusAugustusCaesar.getFixedTargetCardsPoints() + "+" + maximusPlurissimusAugustusCaesar.getVariableTargetCardsPoints() + ").");
				
				// Construct bean with round evaluation information // TODO: evtl. besser in Konstruktor zusammenbauen
				RoundEvaluationBean roundEvaluationBean = new RoundEvaluationBean();
				roundEvaluationBean.setNumberOfPlayersWithAveCaesar(-1); // TODO: muesste allenfalls irgendwo gespeichert werden, falls das ueberhaupt gebraucht / gewuenscht ist
				roundEvaluationBean.setNumberOfPlayersToHandle(numberOfPlayersWithAveCaesar);
				roundEvaluationBean.setNumberOfTargetCardsWithAveCaesar(-1); // TODO: wie oben
				roundEvaluationBean.setNumberOfTargetCardsToHandle(numberOfTargetCardsWithAveCaesar);
				roundEvaluationBean.setRoundNumber(-1); // TODO: do we need something like this?
				roundEvaluationBean.setNamesOfPlayersWithAveCaesar(namesOfPlayersWithAveCaesar);
				roundEvaluationBean.setCardsOfPlayersWithAveCaesar(cardsOfPlayersWithAveCaesar);				
				String message = null;
				if (nextPlayer == null) {
					roundEvaluationBean.setNextPlayer(null);
				} else {
					roundEvaluationBean.setNextPlayer(nextPlayer.getUsername());
					if (minimumTargetCardNumber < (GameConstants.NUMBER_OF_TARGET_CARDS + 1)) {
					    roundEvaluationBean.setNextTargetCard(minimumTargetCardNumber);
					    TargetCard targetCardToEvaluate = nextPlayer.getCardMap().get(minimumTargetCardNumber);
					    // Effekt-ID ins Bean abfüllen
					    if (targetCardToEvaluate.isRed()) {
					        roundEvaluationBean.setNextTargetCardNegativeEffectId(targetCardToEvaluate.getEffectOfCard().getID());
					    }
					    if (targetCardToEvaluate.hasImmediateEffect() || targetCardToEvaluate.hasImmediateEffect()) {
	                        message = nextPlayer.getUsername() + ": " + targetCardToEvaluate.getEffectOfCard().getDescription();					        
					    }
					    if (targetCardToEvaluate.hasImmediateEffect()) {
					        roundEvaluationBean.setHasImmediateEffect(true);
					        roundEvaluationBean.setHasNegativeEffect(targetCardToEvaluate.isRed());
					    } else {
					        roundEvaluationBean.setHasImmediateEffect(false);
					        roundEvaluationBean.setHasNegativeEffect(false);
					    }
					
					    // should probably cause problems
					    User player = userRepo.findByUsername(nextPlayer.getUsername());
					    player.setNumberOfAveCaesarCard(minimumTargetCardNumber);
					    userRepo.save(player);
					
					} else {
					    roundEvaluationBean.setNextTargetCard(null); // Wert fuer den Fall, dass kein Spieler Ave Caesar hat
					    roundEvaluationBean.setHasImmediateEffect(false);
					}
				}
                roundEvaluationBean.setMessage(message);
				if (gameFinished) {
					roundEvaluationBean.setPlayerFinished();
				} else {
					roundEvaluationBean.setNobodyHasFinished();
				}
				// Sieger zurückgeben, sofern bekannt (falls das Spiel beendet ist und keine weiteren Spieler zu handlen sind)
				if (gameFinished && numberOfPlayersWithAveCaesar == 0) {
				    if (maximusPlurissimusAugustusCaesar == null) {
				        logger.error("evaluateRound(): something went terribly wrong: the game is finished and all players are handled but the user ID of the winner was not determined correctly.");
				    }
                    roundEvaluationBean.setWinner(maximusPlurissimusAugustusCaesar.getId());
                    // TODO: Punktzahl des Siegers noch ins Bean packen
				} else {
	                roundEvaluationBean.setWinner(null);
				}
				return roundEvaluationBean;
	}
	
	/** 7.2 - count points **/
	public static List<StatisticsBean> countPoints(Long gameId, UserRepository userRepo, GameRepository gameRepo){

		// TODO: could also be translated into a model (e.g. PlayerStatistics)
		
		// TODO: transform to method that doesn't require the playerId
		// iterate through all player instead
		Game game;
		List<StatisticsBean> statisticBeanList = new ArrayList<StatisticsBean>();
		
		try {
			game = gameRepo.findOne(gameId);			
			Integer requestCount = game.getRequestCount() + 1;
			
			// reset counter if last player accesses this method
            if(requestCount.equals(game.getPlayers().size())) {
                requestCount = 0;
                logger.debug("reset request nr. " + requestCount);
                for(User tmpUser : game.getPlayers()) {
                	tmpUser.setIsReady(false);
                	userRepo.save(tmpUser);
                }
            }
            game.setRequestCount(requestCount);
            
            // save changes in gameRepo
            gameRepo.save(game);
			
			
			
		} catch(Exception e) {
			logger.error("Error: Could not find game ID: " + gameId);
			logger.error(e.toString());
			return null;
		}
		
		try {
		
			// TODO: Iterate through all player
			for (User tmpUser : game.getPlayers()) {

				logger.debug("[gameId: " + gameId + ", playerId: " + tmpUser.getId() +"]: calculating points...");
		
				StatisticsBean statisticsBean = new StatisticsBean();
				Integer tmpPoints = 0;
		
				User user = userRepo.findOne(tmpUser.getId());
				
				// Count information on legion fields / markers
				Integer countSchwert = user.getNumberOfControlledMarkers(Marker.SCHWERTER);
                Integer countSchild = user.getNumberOfControlledMarkers(Marker.SCHILD);
                Integer countKatapult = user.getNumberOfControlledMarkers(Marker.KATAPULT);
                Integer countStandarte = user.getNumberOfControlledMarkers(Marker.STANDARTE);
                Integer countStreitwagen = user.getNumberOfControlledMarkers(Marker.STREITWAGEN);
                Integer countDolch = user.getNumberOfControlledMarkers(Marker.DOLCH);
                logger.debug("[gameId: " + gameId + ", playerId: " + tmpUser.getId() +"]:   schwerter: " + countSchwert + "; schild: " + countSchild + 
                        "; katapult: " + countKatapult + "; standarte: " + countStandarte + "; streitwagen: " + countStreitwagen + "; dolch: " + countDolch);

				// Count information on commodities
				Integer countGold = user.getNumberOfCommodities(Commodity.GOLD);
				Integer countWheat = user.getNumberOfCommodities(Commodity.WHEAT);
                logger.debug("[gameId: " + gameId + ", playerId: " + tmpUser.getId() +"]:   gold: " + countGold + "; wheat: " + countWheat);
	
				// Count information on card type
				Integer countEuropa = user.getNumberOfProvincesOfColor(CardType.GREEN);
				Integer countKleinasien = user.getNumberOfProvincesOfColor(CardType.VIOLET);
				Integer countAfrika = user.getNumberOfProvincesOfColor(CardType.ORANGE);
				Integer countSenator = user.getNumberOfSenators();
                logger.debug("[gameId: " + gameId + ", playerId: " + tmpUser.getId() +"]:   europa: " + countEuropa + "; kleinasien: " + countKleinasien + "; afrika: " + countAfrika + "; senators: " + countSenator);
				
				// Count red cards (target cards with effect that concern other players)
				Integer countRedCards = user.getNumberOfRedCards();
                logger.debug("[gameId: " + gameId + ", playerId: " + tmpUser.getId() +"]:   red cards: " + countRedCards);

                /*
				for(TargetCard targetCard : user.getControlledTargetCards()) {
					if(targetCard.hasVariablePoints()) {
						//update points of variable point cards // TODO: move code into method since this has to be done every time a target card gets owned ;-) or is lost
						switch (targetCard.getEffectOfCard().getID()) {
						case 1: targetCard.updateCurrentValue(countSchwert);
						break;
						case 2: targetCard.updateCurrentValue(countSchild);
						break;
						case 3: targetCard.updateCurrentValue(countStreitwagen);
						break;
						case 4: targetCard.updateCurrentValue(countKatapult);
						break;
						case 5: targetCard.updateCurrentValue(countEuropa);
						break;
						case 6: targetCard.updateCurrentValue(countSenator);
						break;
						case 7: targetCard.updateCurrentValue(countKleinasien);
						break;
						case 8: targetCard.updateCurrentValue(countStandarte);
						break;
						case 9: targetCard.updateCurrentValue(countDolch);
						break;
						case 10: targetCard.updateCurrentValue(countAfrika);
						break;
						case 11: targetCard.updateCurrentValue(countRedCards);
						break;
						default: break;
						}
					}
					// calculate points
					tmpPoints += targetCard.getPoints();
				}
			
				// count points from rewards
				Integer pointsFromRewards = user.getRewardPoints();
				tmpPoints += pointsFromRewards;
				//TODO: store map rewards somewhere, probably into the bean

				userRepo.save(user);
				*/
				
				tmpPoints = user.getPoints();

				statisticsBean.setPoints(tmpPoints);
                statisticsBean.setPointsRewards(user.getRewardPoints());
                statisticsBean.setPointsTargetCards(user.getTargetCardsPoints());
                statisticsBean.setPointsTargetCardsFixed(user.getFixedTargetCardsPoints());
                statisticsBean.setPointsTargetCardsVariable(user.getVariableTargetCardsPoints());
                //TODO: momentan noch redundant

				statisticsBean.setCountGold(countGold);
				statisticsBean.setCountWheat(countWheat);
				
				statisticsBean.setName(user.getUsername());
				
		
				statisticBeanList.add(statisticsBean);
				
				
				
				
			}
			
			
		
			return statisticBeanList;
					
		} catch(Exception e) {
			logger.error("Couldn't fetch points: " + e.toString());
		}
		
		return null;
	}
	
	/** 7.3 - set ready **/
	public static void setReady(Long gameId, Long playerId, UserRepository userRepo, GameRepository gameRepo){
		// TODO: try and catch if Ids don't exist
		
		User user = userRepo.findOne(playerId);
		user.setIsReady(true);
		userRepo.save(user);
		
		return;
	}
	
	/** 7.4 - deliver card stack **/
	public static List<TargetCardBean> deliverCardStack(Long gameId, GameRepository gameRepo){
		// TODO: try and catch if Ids don't exist
		
				List<TargetCardBean> targetCardList = new ArrayList<TargetCardBean>();
				
				try {
					Game game = gameRepo.findOne(gameId);
					
					for(TargetCard targetCard : game.getDrawableTargetCards()) {
						TargetCardBean tmpBean = targetCard.packIntoBean();
						targetCardList.add(tmpBean);
					}
				
					return targetCardList;
				
				} catch(Exception e) {
					logger.error("couldn't deliver cards");
					logger.error(e.toString());
				}
				
				return null;
	}

	/** 7.5 - deliver new controlledTargetCardBean **/
	public static TargetCardBean deliverAveCard(Long gameId, Long playerId, UserRepository userRepo, GameRepository gameRepo){
		try {
			Game game = gameRepo.findOne(gameId);
			User player = userRepo.findOne(playerId);
			TargetCardBean effectBean = new TargetCardBean();

			player.getNumberOfAveCaesarCard();

			for(TargetCard neutralizedCard : player.getNeutralizedTargetCards()) {
				if(neutralizedCard.getNumber().equals(player.getNumberOfAveCaesarCard())) {
					effectBean = neutralizedCard.packIntoBean();
					
					if(neutralizedCard.getEffectOfCard() != null && neutralizedCard.getEffectOfCard().getEffectType().equals(EffectType.IMMEDIATELY)) {
						String message = player.getUsername() + ": " +
								neutralizedCard.getEffectOfCard().getDescription();
						game.setEndRoundMessage(message);
					}
				}
			}
			
			gameRepo.save(game);
			
			// set message for EndRound to 

			return effectBean;

		} catch(Exception e) {
			logger.error("couldn't send effectBean");
			logger.error(e.toString());
		}

		return null;
	}
}