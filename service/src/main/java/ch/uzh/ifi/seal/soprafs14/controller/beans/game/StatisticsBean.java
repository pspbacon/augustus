package ch.uzh.ifi.seal.soprafs14.controller.beans.game;

public class StatisticsBean {
	
	private Integer points;
    private Integer pointsTargetCards;
    private Integer pointsTargetCardsFixed;
    private Integer pointsTargetCardsVariable;
	private Integer pointsRewards;
	
	private Integer countGold;
	private Integer countWheat;
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCountGold() {
		return countGold;
	}

	public void setCountGold(Integer countGold) {
		this.countGold = countGold;
	}

	public Integer getCountWheat() {
		return countWheat;
	}

	public void setCountWheat(Integer countWheat) {
		this.countWheat = countWheat;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public Integer getPointsTargetCards() {
	        return pointsTargetCards;
    }

    public void setPointsTargetCards(Integer points) {
	        this.pointsTargetCards = points;
	}

    public Integer getPointsTargetCardsFixed() {
        return pointsTargetCardsFixed;
    }

    public void setPointsTargetCardsFixed(Integer points) {
        this.pointsTargetCardsFixed = points;
    }

    public Integer getPointsTargetCardsVariable() {
        return pointsTargetCardsVariable;
    }

    public void setPointsTargetCardsVariable(Integer points) {
        this.pointsTargetCardsVariable = points;
    }

    public Integer getPointsRewards() {
        return pointsRewards;
    }

    public void setPointsRewards(Integer points) {
        this.pointsRewards = points;
    }

}
