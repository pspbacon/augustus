package ch.uzh.ifi.seal.soprafs14.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GameMoveRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GameMoveResponseBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GamePlayerRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GamePlayerResponseBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GameRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GameResponseBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.MarkerBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.RewardBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.RoundEvaluationBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.StatisticsBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.TargetCardBean;
import ch.uzh.ifi.seal.soprafs14.model.User;
import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;
import ch.uzh.ifi.seal.soprafs14.service.GameAdminService;
import ch.uzh.ifi.seal.soprafs14.service.GameAveCaesarService;
import ch.uzh.ifi.seal.soprafs14.service.GameEndRoundService;
import ch.uzh.ifi.seal.soprafs14.service.GameLegionService;
import ch.uzh.ifi.seal.soprafs14.service.GameMoveMarkerService;
import ch.uzh.ifi.seal.soprafs14.service.GamePlayerService;
import ch.uzh.ifi.seal.soprafs14.service.GameStartCardService;
import ch.uzh.ifi.seal.soprafs14.service.GameStartStopDrawService;

/******* Index *******/

/*******  *******/
/******* Part 1: list, add, remove game		*******/
/******* Part 2: start game					*******/
/******* Part 3: Move						*******/
/******* Part 3.1 getMove					*******/
/******* Part 3.2 Marker					*******/
/******* Part 4: Legionfields				*******/
/******* Part 5: Player						*******/
/******* Part 6: Startcards					*******/
/******* Part 7: End of Round Evaluation	*******/
/******* Part 8: Ave Caesar					*******/


@Controller("gameServiceController")
public class GameServiceController extends GenericService {

	Logger logger = LoggerFactory.getLogger(GameServiceController.class);

	@Autowired
	private UserRepository userRepo;
	@Autowired
	private GameRepository gameRepo;

	private final String CONTEXT = "/game";

	
	/******* Part 1: list, add, remove game ********/
	
	/*
	 * Context: /game
	 */
	
	/** 1.1 - List all games **/
	@RequestMapping(value = CONTEXT)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<GameResponseBean> listGames() {
		logger.debug("listGames");
		return GameAdminService.listGames(gameRepo);
	}
	
	/** 1.2 - Add a new game **/
	@RequestMapping(value = CONTEXT, method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void addGame(@RequestBody GameRequestBean gameRequestBean) {
		logger.debug("addGame: " + gameRequestBean);
		GameAdminService.addGame(gameRequestBean, userRepo, gameRepo);
	}

	/** 1.2.2 - Add a new game and return gameId **/
	@RequestMapping(value = CONTEXT + "1", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Long addGame1(@RequestBody GameRequestBean gameRequestBean) {
		logger.debug("addGame: " + gameRequestBean);
		return GameAdminService.addGame1(gameRequestBean, userRepo, gameRepo);
	}
	
	/*
	 * Context: /game/{game-id}
	 */
	
	/** 1.3 - Get a specific game **/
	@RequestMapping(value = CONTEXT + "/{gameId}")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public GameResponseBean getGame(@PathVariable Long gameId) {
		return GameAdminService.getGame(gameRepo, gameId);
	}
	
	
	/******* Part 2: start game ********/
	
	/*
	 * Context: /game/{gameId}/start
	 * Initializes a new game and sets its status to RUNNING
	 * @pre		gameId != null && userId != null
	 * @post	gameStatus == RUNNING
	 * @param	
	 * @return	
	 */
	
	/** 2.1 - Start a game **/
	@RequestMapping(value = CONTEXT + "/{gameId}/start", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void startGame(@PathVariable Long gameId, @RequestBody Long userId) {
		GameStartStopDrawService.startGame(userId, gameId, userRepo, gameRepo);
	}

	/** 2.2 - Stop a game 
	 * @throws Exception **/
	@RequestMapping(value = CONTEXT + "/{gameId}/stop", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void stopGame(@PathVariable Long gameId, @RequestBody String userToken) throws Exception {
		GameStartStopDrawService.stopGame(gameId, userToken, userRepo, gameRepo);
	}

	/** 2.3 - Draw cards **/
	/*
	 * Delivers the drawable target cards for a game as an array of targetCardBeans (with length GameConstants.NUMBER_OF_DRAWABLE_TARGET_CARDS) to a player request
	 * @pre		gameId != null
	 * @post	currentlyDrawableTargetCardBeans.size() == GameConstants.NUMBER_OF_DRAWABLE_TARGET_CARDS
	 * @param	
	 * @return	Returns an array of TargetCardBeans
	*/
	@RequestMapping(value = CONTEXT + "/{gameId}/drawablecards")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<TargetCardBean> deliverDrawableTargetCards(@PathVariable Long gameId) {
		return GameStartStopDrawService.drawCards(gameId, gameRepo);
	}

	
	/******* Part 3: Move *******/
	/******* Part 3.1: getMove *******/
	
	/** 3.1.1 - initializeMove **/
	// TODO: Transform to a POST-request for security-reasons
	// only a player should be able to access this mapping successfully
	/*
	 * Context: /game/{gameId}/move
	 * Creates a new move and a new marker
	 * Includes a clever first-comes first-serves algorithm, such that for each round only
	 * one new move and one new marker is generated
	 * @pre		gameId != null
	 * @post	marker != null 
	 * @param	
	 * @return	MarkerBean with moveId and marker
	 */
	@RequestMapping(value = CONTEXT + "/{gameId}/move")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public MarkerBean initializeMove (@PathVariable Long gameId) {
		return GameMoveMarkerService.initializeMove(gameId, gameRepo);
	}	
	
	/*
	 * Context: /game/{game-id}/move
	 * 
	 */
	/** 3.1.2 - list moves **/
	@RequestMapping(value = CONTEXT + "/{gameId}/listMove")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<GameMoveResponseBean> listMoves(@PathVariable Long gameId) {
		logger.debug("listMoves");
		return GameMoveMarkerService.listMoves(gameId, gameRepo);
	}

	/** 3.1.3 - addMove **/
	// TODO: do we need this?
	@RequestMapping(value = CONTEXT + "/{gameId}/move", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void addMove(@PathVariable Long gameId, @PathVariable Long playerId, @RequestBody GameMoveRequestBean gameMoveRequestBean) {
		logger.debug("addMove: " + gameMoveRequestBean);
		GameMoveMarkerService.addMove(gameId, playerId, gameMoveRequestBean, gameRepo, userRepo);
	}

	/** 3.1.4 - delete game **/
	@RequestMapping(value = CONTEXT + "/{gameId}/delete", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Void deleteGame(@PathVariable Long gameId, @RequestBody Long userId) {
		GameMoveMarkerService.deleteGame(gameId, userId, gameRepo, userRepo);
		return null;
	}

	/** 3.1.5 - get move **/
	@RequestMapping(value = CONTEXT + "/{gameId}/move/{moveId}")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public GameMoveResponseBean getMove(@PathVariable Long gameId,
			@PathVariable Integer moveId) {
			logger.debug("getMove: " + gameId);
			return GameMoveMarkerService.getMove(gameId, moveId, gameRepo);
	}
	
	
	/******* Part 3.2: Marker *******/
	
	/*
	 * Context: /game/{gameId}/move/{moveId}/marker
	 * Creates a new marker
	 * @pre		gameId != null && moveId != null
	 * @post	marker != null 
	 * @param	tmpMarker is the marker drawn from the MarkerBag and stored in the gameRepo
	 * @return	
	 */
	
	/** 3.2.1 - create marker **/
	@RequestMapping(value = CONTEXT + "/{gameId}/move/{moveId}/createMarker")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void createMarker(@PathVariable Long gameId, @PathVariable Long moveId) {
			GameMoveMarkerService.createMarker(gameId, moveId, gameRepo);
	}
	
	/** 3.2.2 - get marker **/
	@RequestMapping(value = CONTEXT + "/{gameId}/move/{moveId}/marker")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String getMarker(@PathVariable Long gameId, @PathVariable Long moveId) {
			return GameMoveMarkerService.getMarker(gameId, moveId, gameRepo);
	}
	
	
	/******* Part 4: Legionfields *******/
	
	/** 4.1 - get available legions **/
	/*
	 * Returns the number of available legions of a player
	 * @pre		a game with game ID 'gameId' must exist, a player with player ID 'playerId' must exist
	 * @post	the value of the attribut 'numberOfLegionsAvailable' of the player with ID 'playerId' is returned
	 * @param	Long gameId, Integer playerId
	 * @return	player.getNumberOfLegionsAvailable()
	*/
	@RequestMapping(value = CONTEXT + "/{gameId}/player/{playerId}/legions")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public MarkerBean getAvailableLegions(@PathVariable Long gameId, @PathVariable Long playerId) {
		try {
			logger.debug("getAvailableLegions for game with ID " + gameId + " and player with ID " + playerId);
			return GameLegionService.getAvailableLegions(gameId, playerId, gameRepo, userRepo);
		} catch(Exception e) {
			logger.error("Couldn't fetch legions");
		}
		return null;
	}	
	
	/** 4.2 - get free legion fields **/
	/*
	 * Context: /game/{gameId}/move/{moveId}/player/{playerId}/legionfield
	 * Returns an updated version of the TargetcardBean with 
	 * information about the legionfields (if a legion can be assigned there)
	 * @pre		gameId != null && moveId != null
	 * @post	
	 * @param	marker; targetCardBean[], user
	 * @return	TargetCardBean
	 */
	@RequestMapping(value = CONTEXT + "/{gameId}/move/{moveId}/player/{playerId}/legionfield")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<TargetCardBean> getFreeLegionFields(@PathVariable Long gameId, @PathVariable Long playerId) {
		try{
			return GameLegionService.getFreeLegionFields(gameId, playerId, gameRepo, userRepo);
		}
		catch(Exception e){
			logger.error("Couldn't fetch free legions fields ");
		}
		return null;
	}

	/** 4.3 - post legion fields **/
	/*
	 * Context: /game/{gameId}/move/{moveId}/player/{playerId}/legionfield2
	 * Player posts here back their Targetcards with their modified LegionFields
	 * This method controlls the count of changes made by the user and updates the userrepo
	 * number of available legions is adjusted too
	 * 
	 * @pre		gameId != null && moveId != null && the number of targetCard in repo and bean must be equal && for each targetCard, the number of legion fields in repo and bean are equal 
	 * @post	isOpen == false for all legionfields on all target cards
	 * @param	int changeCounter: to track the count of changes made from the player
	 * @return	
	 */
	@RequestMapping(value = CONTEXT + "/{gameId}/move/{moveId}/player/{playerId}/legionfield2", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void postLegionFields(@PathVariable Long gameId, @PathVariable Long playerId, 
			@RequestBody TargetCardBean[] targetCardBeanList) {
		try {
			logger.debug("Player: " + playerId + " in Game: " + gameId + " submitted a move");
			GameLegionService.postLegionFields(gameId, playerId, targetCardBeanList, gameRepo, userRepo);
		}
		catch(Exception e) {
			logger.error("ERROR: Something went wrong - changes of legionCards not submitted");
		}
		return;
	}
	
	/** 4.4 - get player cards **/
	/*
	 * Context: /game/{gameId}/assignedCards
	 * Returns a list with the targetCards of all players
	 * Using again some black magic to send the bean as an ArrayList of ArrayList
	 * @pre		gameId != null && moveId != null
	 * @post	
	 * @param
	 * @return	List<List<TargetCardBean> >
	 */
	@RequestMapping(value = CONTEXT + "/{gameId}/assignedCards")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<List<TargetCardBean> > getPlayerCards(@PathVariable Long gameId) {
		try{
			return GameLegionService.getPlayerCards(gameId, gameRepo, userRepo);		
		}
		catch(Exception e){
			logger.error("Couldn't get player cards ");
		}
		return null;
	}
		
	/** 4.5 - get conquered cards **/
	/*
	 * Context: /game/{gameId}/conqueredCards
	 * Returns a list with the targetCards of all players
	 * Using again some black magic to send the bean as an ArrayList of ArrayList
	 * @pre		gameId != null && moveId != null
	 * @post	
	 * @param
	 * @return	List<List<TargetCardBean> >
	 */
	@RequestMapping(value = CONTEXT + "/{gameId}/conqueredCards")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<List<TargetCardBean> > getConqueredCards(@PathVariable Long gameId) {
		try{
			return GameLegionService.getConqueredCards(gameId, gameRepo, userRepo);
		}
		catch(Exception e){
			logger.error("Couldn't get conquered cards ");
		}
		return null;
	}
	
	
    /** 4.6 - get free legion fields for immediate effects 
     * @throws Exception **/
	@RequestMapping(value = CONTEXT + "/{gameId}/player/{playerId}/effectlegionfield")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<TargetCardBean> getEffectLegionFields(@PathVariable Long gameId, @PathVariable Long playerId) throws Exception {
		
		return GameLegionService.getEffectLegionFields(gameId, playerId, gameRepo, userRepo);
	}
	
	/** 4.7 - get free legion field information for immediate effects **/
	@RequestMapping(value = CONTEXT + "/{gameId}/player/{playerId}/effectlegioninfo")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
    public MarkerBean getEffectLegionCount(@PathVariable Long gameId, @PathVariable Long playerId) {
        
    	return GameLegionService.getEffectLegionCount(gameId, playerId, gameRepo, userRepo);
    }
	
	
	/******* Part 5: Player *******/
	
	/** 5.1 - list players in a game **/
	/*
	 * Context: /game/{game-id}/player
	 */
	@RequestMapping(value = CONTEXT + "/{gameId}/player")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<GamePlayerResponseBean> listPlayers(@PathVariable Long gameId) {
		try{
		return GamePlayerService.listPlayers(gameId, gameRepo);
		}
		catch(Exception e){
			logger.debug("something went unexpected");
		}
		return null;
	}

	/** 5.2 - add player to game **/
	/*
	 * Context: /game/{game-id}/player/add
	 */
	@RequestMapping(value = CONTEXT + "/{gameId}/player/add", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Void addPlayer(@PathVariable Long gameId,
			@RequestBody GamePlayerRequestBean gamePlayerRequestBean) {
		return GamePlayerService.addPlayer(gameId, gamePlayerRequestBean, userRepo, gameRepo);
	}

	/** 5.3 - remove a player **/
	/*
	 * Context: /game/{game-id}/player/remove
	 */
	@RequestMapping(value = CONTEXT + "/{gameId}/player/remove", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void removePlayer(@PathVariable Long gameId,
			@RequestBody GamePlayerRequestBean gamePlayerRequestBean) {
		GamePlayerService.removePlayer(gameId, gamePlayerRequestBean, userRepo, gameRepo);
	}
	
	/** 5.4 - get player cars **/
	@RequestMapping(value = CONTEXT + "/{gameId}/player/{playerId}")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public GamePlayerResponseBean getPlayer(@PathVariable Long gameId,
			@PathVariable Long playerId) {
		GamePlayerResponseBean gamePlayerResponseBean = new GamePlayerResponseBean();
		return gamePlayerResponseBean;
	}
	
	/** 5.5 - get player status to start the game **/
	/*
	 * Context: /game/{game-id}/player/status
	 * 
	 * @pre		
	 * @post	
	 * @param	
	 * @return	Returns Boolean allReady 
	*/
	@RequestMapping(value = CONTEXT + "/{gameId}/player/status")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Boolean getPlayerStatus(@PathVariable Long gameId) {
		return GamePlayerService.getPlayerStatus(gameId, gameRepo);
	}
	
	/** 5.6 - set player status **/
	@RequestMapping(value = CONTEXT + "/{gameId}/player/{playerId}/status", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void setPlayerStatusToFalse(@PathVariable Long gameId, @PathVariable Long playerId) {
		
		try {
			User player = userRepo.findOne(playerId);
			
			player.setIsReady(false);
			
			userRepo.save(player);
			
		} catch(Exception e) {
			logger.error("[ERROR]: status not set");
			logger.error(e.toString());
		}
		
		return;
		
	}


	/******* Part 6: Startcards *******/
	
	/** 6.1 - get own start cards **/
	/*
	 * Delivers an array of targetCardBeans (with length GameConstants.DEFAULT_NUMBER_OF_STARTCARDS_HANDOUT) to a player request
	 * It also saves the delivered targetCards into the userRepo
	 * @pre		(playerId != null) && (gameId != null)
	 * @post	(beanOfBeans.size() == GameConstants.DEFAULT_NUMBER_OF_STARTCARDS_HANDOUT) && (beanOfBeans(i) != beanOfBeans(j))
	 * @param	tmpCard is the TargetCard on top of the TargetCardStack
	 * 			tmpBean is a TargetCardBean that is filled with informations from tmpCard
	 * 			beanOfBeans is an array of TargetCardBeans that is used as return value
	 * @return	Returns an array of TargetCardBeans
	*/
	@RequestMapping(value = CONTEXT + "/{gameId}/player/{playerId}/startcard")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<TargetCardBean> deliverStartCards(@PathVariable Long gameId, @PathVariable Long playerId) {
		return GameStartCardService.deliverStartCards(gameId, playerId, userRepo);
	}
	
	/** 6.2 - post the chosen startcards from a player**/
	/*
	 * Gets TargetCardBean of choosen startcards and updates the userrepo
	 * @pre		(playerId != null) && (gameId != null) && (cardId.getCardIds().length == 3)
	 * @post	user.getCardMap.length == 3
	 * @param	
	 * @return	
	*/
	// TODO: remove implicit magic numbers
	@RequestMapping(value = CONTEXT + "/{gameId}/player/{playerId}/startcard2", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void updateStartCards(@PathVariable Long gameId, @PathVariable Long playerId,
			@RequestBody List<TargetCardBean> cardIdBean) {
		GameStartCardService.updateStartCards(gameId, playerId, cardIdBean, userRepo);
		return;
	}
	
	/******* Part 7: End of round evaluation *******/

	/** 7.1 - evaluate a round **/
	/*
	 * Evaluates round and checks whether there is more than one player with Ave Caesar
	 * @pre		..
	 * @post	
	 * @param	
	 * @return	
	*/
	@RequestMapping(value = CONTEXT + "/{gameId}/roundevaluation")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public RoundEvaluationBean evaluateRound(@PathVariable Long gameId) {
		return GameEndRoundService.evaluateRound(gameId, userRepo, gameRepo);
	}
	
	/** 7.2 - count points **/
	/*
	 * Updates the cards with variable point type and counts all points of controlled cards
	 * @pre		
	 * @post	
	 * @param	
	 * @return	Integer points
	*/
	@RequestMapping(value = CONTEXT + "/{gameId}/points")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<StatisticsBean> countPoints(@PathVariable Long gameId) {
		return GameEndRoundService.countPoints(gameId, userRepo, gameRepo);
	}
	
	/** 7.3 - set ready **/
	/*
	 * Returns a flag
	 * @pre		
	 * @post	
	 * @param	
	 * @return	Boolean 
	*/
	@RequestMapping(value = CONTEXT + "/{gameId}/player/{playerId}/ready", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void setReady(@PathVariable Long gameId, @PathVariable Long playerId) {
		GameEndRoundService.setReady(gameId, playerId, userRepo, gameRepo);
	}
	
	/** 7.4 - deliver card stack **/
	/*
	 * Returns the cards in the middle
	 * @pre		
	 * @post	
	 * @param	
	 * @return	List<TargetCardBean> 
	*/
	@RequestMapping(value = CONTEXT + "/{gameId}/newcardstack")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<TargetCardBean> deliverCardStack(@PathVariable Long gameId) {
		return GameEndRoundService.deliverCardStack(gameId, gameRepo);		
	}
	
	/** 7.5 - legion effect **/
	@RequestMapping(value = CONTEXT + "/{gameId}/player/{playerId}/legionEffect")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public TargetCardBean deliverAveCard(@PathVariable Long gameId, @PathVariable Long playerId) {
		return GameEndRoundService.deliverAveCard(gameId, playerId, userRepo, gameRepo);
	}
	
	
	/******* Part 8: Ave Caesar *******/
	/** 8.1 - Ave Caesar **/
	/*
	 * A player posts the card that she/he has choosen
	 * The neutralized targetCard is transfered to the conquered targetCards
	 * the choosen targetCard is transfered to the assigned targetCards
	 * the drawable targetCards are refilled with a new targetCard from the stack
	 * 
	 * @pre		
	 * @post	
	 * @param	
	 * @return	nothing 
	*/
	@RequestMapping(value = CONTEXT + "/{gameId}/player/{playerId}/avecaesar", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void aveCaesar(@PathVariable Long gameId, @PathVariable Long playerId, 
			@RequestBody Integer choosenCardNumber) {
		GameAveCaesarService.aveCaesar(gameId, playerId, choosenCardNumber, userRepo, gameRepo);
	}
	
	/** 8.2 - Request drawable Rewards **/
	/*
	 * Shows all drawableRewards, that a player is entitled to
	 * 
	 * @pre		
	 * @post	
	 * @param	
	 * @return	drawableRewards (only those with entitlement of the player) 
	*/
	@RequestMapping(value = CONTEXT + "/{gameId}/player/{playerId}/rewards")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<RewardBean> getRewards(@PathVariable Long gameId, @PathVariable Long playerId) {
		return GameAveCaesarService.getRewards(gameId, playerId, userRepo, gameRepo);
	}
	
	/** 8.3 - Update Conquered Rewards **/
	/*
	 * Fetches the choosenReward from the player and updates the playerRewards
	 * 
	 * @pre		
	 * @post	
	 * @param	
	 * @return	no
	*/
	@RequestMapping(value = CONTEXT + "/{gameId}/player/{playerId}/choosenReward", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public void postReward(@PathVariable Long gameId, @PathVariable Long playerId, @RequestBody Long rewardId) {
		GameAveCaesarService.postReward(gameId, playerId, rewardId, userRepo, gameRepo);
	}
	
	/** 8.4 - show assignedRewards **/
	/*
	 * Shows all assignedRewards
	 * 
	 * @pre		
	 * @post	
	 * @param	
	 * @return	all assignedRewards
	*/
	@RequestMapping(value = CONTEXT + "/{gameId}/assignedRewards")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<List<RewardBean> > getAssignedRewards(@PathVariable Long gameId) {
		return GameAveCaesarService.getAssignedRewards(gameId, gameRepo);
	}

    /** 8.5 - Update Player Status After Immediate Effect Has Been Executed **/
    /*
     * Unflags a player for 
     * 
     * @pre     
     * @post    
     * @param   
     * @return  
    */
    @RequestMapping(value = CONTEXT + "/{gameId}/player/{playerId}/negativeEffectDone", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void unflagPlayerForEffectExecution(@PathVariable Long gameId, @PathVariable Long playerId) {
        GameAveCaesarService.unflagPlayerForEffectExecution(gameId, playerId, userRepo);
    }
	
	/*
	 * some commentary
	 */
	/** 8.7 send playerCards (assigned and controlled) for negativeEffectActivity **/
	@RequestMapping(value = CONTEXT + "/{gameId}/player/{playerId}/negativeEffect")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<List<TargetCardBean> > setNegativeEffect(@PathVariable Long gameId, @PathVariable Long playerId) {
		return GameAveCaesarService.setNegativeEffect(playerId, userRepo, gameRepo);
	}
		
	/*
	 * some other commentary
	 */
	/** 8.8 Post card if EffectId = 51 **/
	@RequestMapping(value = CONTEXT + "/{gameId}/player/{playerId}/ImmediateWin", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void submitCard(@PathVariable Long gameId, @PathVariable Long playerId, 
    		@RequestBody Integer choosenCardNumber) {
		GameAveCaesarService.submitCard(gameId, playerId, choosenCardNumber, userRepo, gameRepo);
	}
	
	/** 8.9 sacrifice TargetCard **/
	@RequestMapping(value = CONTEXT + "/{gameId}/player/{playerId}/sacrifice", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void sacrificeCard(@PathVariable Long gameId, @PathVariable Long playerId, 
    		@RequestBody Integer choosenCardNumber) {
		GameAveCaesarService.sacrificeCrd(gameId, playerId, choosenCardNumber, userRepo, gameRepo);
	}
	
		
	/**** 10 Delete gameRepo ****/	
	/** 10.1 - delete gameRepo **/
	@RequestMapping(value = CONTEXT + "/delete", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void deleteGameRepo() {
		logger.error("Deleting gameRepo ");
		gameRepo.deleteAll();
	}
	
	/** 10.2 - get first gameId, assisting testing **/
	@RequestMapping(value = CONTEXT + "/{username}/gameId")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
	public Long gameId(@PathVariable String username){
		Long gameId = null;
		String nameGame = userRepo.findByUsername(username).getGames().get(0).getName();
		gameId = gameRepo.findByName(nameGame).getId();
		return gameId;
	}
}
