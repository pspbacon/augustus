package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

public enum ImmediateEffectCategory implements Serializable {
        // betrifft den Kartenbesitzer
        MOBILIZE_LEGIONS(1, "mobilize legions effect", "der Kartenbesitzer kann eine bestimmte Anzahl Legionäre auf Felder seiner in Eroberung befindlichen Zielkarten mit einem bestimmten oder beliebigen Marker legen"),
        REDISTRIBUTE_LEGIONS(2, "redistribute legions effect", "der Kartenbesitzer kann sämtliche mobilisierten (bereits gelegten) Legionen auf seinen in Eroberung befindlichen Zielkarten umverteilen"),
        INCREASE_LEGIONS(3, "increase legion effect", "der Kartenbesitzer erhält eine oder mehrere zusätzliche Legionen für seinen Vorrat an mobilisierbaren Legionen"),
        CARD_COMPLETION(4, "card completion effect", "eine Zielkarte des Kartenbesitzers wird sofort erfüllt"),
        ADD_TARGETCARD(5, "add target card effect", "der Kartenbesitzer erhält eine zusätzliche Zielkarte, die er erobern kann (die gerade eroberte Karte wird mit zwei statt nur einer neuen Zielkarte ersetzt)"),
        // betrifft die Gegenspieler
        REMOVE_LEGIONS(6, "remove legion effect", "jeder Gegenspieler des Kartenbesitzers muss eine oder mehrere mobilisierte Legionen von seinen in Eroberung befindlichen Zielkarten entfernen"),
        REMOVE_ALL_LEGIONS(7, "remove all legions effect", "jeder Gegenspieler des Kartenbesitzers muss alle seine mobilisierten Legionen von seinen in Eroberung befindlichen Zielkarten entfernen"),
        CARD_LOSS(8, "card loss effect", "jeder Gegenspieler des Kartenbesitzers verliert eine kontrollierte Zielkarte");

        private final Integer code;
        private final String label;
        private final String description;
        
        private ImmediateEffectCategory(int code, String label, String description) {
            this.code = code;
            this.label = label;
            this.description = description;
        }
        
        public Integer getCode() {
            return code;
        }
        
        public String getLabel() {
            return label;
        }
        
        public String getDescription() {
            return description;
        }

}
