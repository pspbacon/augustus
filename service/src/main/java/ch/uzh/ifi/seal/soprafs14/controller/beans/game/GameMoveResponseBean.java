package ch.uzh.ifi.seal.soprafs14.controller.beans.game;


public class GameMoveResponseBean {
	//TODO GameMoveResponseBean
	
	private Integer moveId;
	private String DrawnMarker;
	
	public Integer getMoveId() {
		return moveId;
	}
	public void setMoveId(Integer moveId) {
		this.moveId = moveId;
	}
	public String getDrawnMarker() {
		return DrawnMarker;
	}
	public void setDrawnMarker(String drawnMarker) {
		DrawnMarker = drawnMarker;
	}
	
}