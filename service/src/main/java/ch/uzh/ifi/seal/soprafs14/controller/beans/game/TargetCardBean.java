package ch.uzh.ifi.seal.soprafs14.controller.beans.game;

import java.util.List;

public class TargetCardBean {

    // identification
	private Integer number;
	private String name;
	private String type;

	// legion fields
	private List<String> legionFields;
	private List<Boolean> statusFields;
	private List<Boolean> openForAssignment;

	// commodities
	private List<String> commodities;

	// victory points
    private Integer points;
	private Boolean pointType;
	private String elementToCount;
	private Integer pointsPerElement;
	private Integer maximumPoints;

	// effects
	private String effectDescription;
	private String effectType;
	private Integer effectId;
	private Boolean isImmediatly;
	private String immediateEffectType;
	private Boolean isRed;
	private String[] persistentEffectMarkers = new String[2];
	private Integer immediateEffectLegionIncreaseNumber;
	private Integer immediateEffectMobilizeLegionsNumber;
	private String immediateEffectMobilizeLegionsMarker;
    private Integer immediateEffectRemoveLegionsNumber;

    // identification
    public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
    // commodities
	public List<String> getCommodities() {
		return commodities;
	}
	public void setCommodities(List<String> commodities) {
		this.commodities = commodities;
	}
	
	// legion fields
	public List<String> getLegionFields() {
		return legionFields;
	}
	public void setLegionFields(List<String> legionFields) {
		this.legionFields = legionFields;
	}
	public List<Boolean> getStatusFields() {
		return statusFields;
	}
	public void setStatusFields(List<Boolean> statusFields) {
		this.statusFields = statusFields;
	}
	public List<Boolean> getOpenForAssignment() {
		return openForAssignment;
	}
	public void setOpenForAssignment(List<Boolean> openForAssignment) {
		this.openForAssignment = openForAssignment;
	}

	// victory points
    public Integer getPoints() {
        return points;
    }
    public void setPoints(Integer points) {
        this.points = points;
    }
	public Boolean getPointType() {
		return pointType;
	}
	public void setPointType(Boolean pointType) {
		this.pointType = pointType;
	}
    public String getElementToCount() {
        return elementToCount;
    }
    public void setElementToCount(String elementToCount) {
        this.elementToCount = elementToCount;
    }
    public Integer getPointsPerElement() {
        return pointsPerElement;
    }
    public void setPointsPerElement(Integer pointsPerElement) {
        this.pointsPerElement = pointsPerElement;
    }
    public Integer getMaximumPoints() {
        return maximumPoints;
    }
    public void setMaximumPoints(Integer maximumPoints) {
        this.maximumPoints = maximumPoints;
    }

    // effects
    public Integer getEffectId() {
        return effectId;
    }
    public void setEffectId(Integer effectId) {
        this.effectId = effectId;
    }
	public String getEffectDescription() {
		return effectDescription;
	}
	public void setEffectDescription(String effectDescription) {
		this.effectDescription = effectDescription;
	}
	public String getEffectType() {
		return effectType;
	}
	public void setEffectType(String effectType) {
		this.effectType = effectType;
	}
    public Boolean getIsImmediatly() {
        return isImmediatly;
    }
    public void setIsImmediatly(Boolean isImmediatly) {
        this.isImmediatly = isImmediatly;
    }
    public String getImmediateEffectType() {
        return immediateEffectType;
    }
    public void setImmediateEffectType(String immediateEffectType) {
        this.immediateEffectType = immediateEffectType;
    }
    public Boolean isRed() {
        return isRed;
    }
    public void setIsRed(Boolean isRed) {
        this.isRed = isRed;
    }
    public String[] getPersistentEffectMarkers() {
        return this.persistentEffectMarkers;
    }
    public void setPersistentEffectMarkers(String marker1, String marker2) {
        this.persistentEffectMarkers[0] = marker1;
        this.persistentEffectMarkers[1] = marker2;
    }
    public Integer getImmediateEffectLegionIncreaseNumber() {
        return immediateEffectLegionIncreaseNumber;
    }
    public void setImmediateEffectLegionIncreaseNumber(Integer immediateEffectLegionIncreaseNumber) {
        this.immediateEffectLegionIncreaseNumber = immediateEffectLegionIncreaseNumber;
    }
    public Integer getImmediateEffectMobilizeLegionsNumber() {
        return immediateEffectMobilizeLegionsNumber;
    }
    public void setImmediateEffectMobilizeLegionsNumber(Integer immediateEffectMobilizeLegionsNumber) {
        this.immediateEffectMobilizeLegionsNumber = immediateEffectMobilizeLegionsNumber;
    }    
    public String getImmediateEffectMobilizeLegionsMarker() {
        return immediateEffectMobilizeLegionsMarker;
    }
    public void setImmediateEffectMobilizeLegionsMarker(String immediateEffectMobilizeLegionsMarker) {
        this.immediateEffectMobilizeLegionsMarker = immediateEffectMobilizeLegionsMarker;
    }
    public Integer getImmediateEffectRemoveLegionsNumber() {
        return immediateEffectRemoveLegionsNumber;
    }
    public void setImmediateEffectRemoveLegionsNumber(Integer immediateEffectRemoveLegionsNumber) {
        this.immediateEffectRemoveLegionsNumber = immediateEffectRemoveLegionsNumber;
    }    

}
