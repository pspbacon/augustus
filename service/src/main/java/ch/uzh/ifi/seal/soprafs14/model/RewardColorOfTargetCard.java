package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

public class RewardColorOfTargetCard extends Reward implements Serializable {	
		private static final long serialVersionUID = 1L;

		private final CardType cardTypeToAchieve;
		private final Integer countToAchieve;
	
		public RewardColorOfTargetCard(Long id, String description, RewardType type, Integer points, CardType cardTypeToAchieve, Integer countToAchieve) {
			this.id = id;
			this.description = description;
			this.type = type;
			this.points = points;
			this.isAuto = true;
			this.isMovable = false;
			this.cardTypeToAchieve = cardTypeToAchieve;
			this.countToAchieve = countToAchieve;
			this.isAssigned = false;
		}

		public Boolean isEligible(User anUser, Game aGame) {
			return (anUser.getNumberOfProvincesOfColor(this.cardTypeToAchieve) >= this.countToAchieve);
		}

}
