package ch.uzh.ifi.seal.soprafs14.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;

import ch.uzh.ifi.seal.soprafs14.controller.beans.game.MarkerBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.TargetCardBean;
import ch.uzh.ifi.seal.soprafs14.model.Game;
import ch.uzh.ifi.seal.soprafs14.model.LegionField;
import ch.uzh.ifi.seal.soprafs14.model.Marker;
import ch.uzh.ifi.seal.soprafs14.model.TargetCard;
import ch.uzh.ifi.seal.soprafs14.model.User;
import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;

public final class GameLegionService {
    
    // hide utility class public default constructor
    private GameLegionService() {
    }
    
    private static Logger logger = LoggerFactory.getLogger(GameLegionService.class);
    
    /** 4.1 - get available legions **/
    public static MarkerBean getAvailableLegions(Long gameId, Long playerId, GameRepository gameRepo, UserRepository userRepo){
        try {
        // Aufruf der Methode im Logger festhalten mit uebergebenen Argumenten
            logger.debug("getAvailableLegions for game with ID " + gameId + " and player with ID " + playerId);

            MarkerBean tmpMarkerBean = new MarkerBean();
            
        // Anhand der uebergebenen Game-ID und Player-ID entsprechende Instanzen mit den Informationen aus dem Repository erzeugen
            Game game = gameRepo.findOne(gameId);
            User player = userRepo.findOne(playerId);
            
            tmpMarkerBean.setMarker(game.getLastDrawnMarker());
            tmpMarkerBean.setMoveId(game.getMoves().get(game.getMoves().size()-1).getMoveId());
            tmpMarkerBean.setLegion(player.getNumberOfLegionsAvailable());
            
            player.setIsReady(false);
            userRepo.save(player);
        
        // Anzahl der verfuegbaren Legionen eines Spieler zurueckgeben
            return tmpMarkerBean;
        } catch(Exception e) {
            logger.error("Couldn't fetch legions");
        }
        return null;
    }
    
    /** 4.2 - get free legion fields **/
    public static List<TargetCardBean> getFreeLegionFields(Long gameId, Long playerId, GameRepository gameRepo, UserRepository userRepo) {
        try{
            // TODO: Logic if player has no legionfields to assign
            
            Game game = gameRepo.findOne(gameId);
            User user = userRepo.findOne(playerId);
            List<Integer> targetCardKeys = new ArrayList<Integer>();
            List<TargetCardBean> targetCardBeanArray = new ArrayList<TargetCardBean>();
            String marker = game.getLastDrawnMarker();
            // TODO: haesslicher Hack, to be refactored
            Marker drawnMarker = Marker.getMarkerFromName(marker);

            // Liste mit allen Marker-Typen, die belegt werden dürfen
            List<Marker> allowedMarkers = new ArrayList<Marker>();
            allowedMarkers.add(drawnMarker);
            
            // identifiziere zusätzliche erlaubte Marker durch Fähigkeiten auf kontrollierten Karten
            if (drawnMarker != Marker.JOKER) {
                List<Marker> markersAdditionallyAllowedByEffect = user.transmuteDrawnMarker(drawnMarker);
                if (markersAdditionallyAllowedByEffect != null) {
                    if (!markersAdditionallyAllowedByEffect.isEmpty()) {
                        // durch Effekte ermoeglichte Marker zu erlaubten Markern hinzufuegen
                        allowedMarkers.addAll(markersAdditionallyAllowedByEffect);
                        logger.debug(loggerHeader(gameId, playerId) + "checked on persistent effects: found effects"); // TODO: write number of cards and transmuted markers to log
                    } else {
                        logger.debug(loggerHeader(gameId, playerId) + "checked on persistent effects: none found");
                    }
                }
            }

            for ( Integer key : user.getCardMap().keySet() ) {
                targetCardKeys.add(key);
                logger.debug(loggerHeader(gameId, playerId) + "targetCardNumber: " + key);
            }

            for(int i = 0; i < targetCardKeys.size(); i++) {
                TargetCard tmpTargetCard = user.getTargetCard(targetCardKeys.get(i));

                List<Boolean> openFields = new ArrayList<Boolean>();
                Boolean isOpen;

                for(int j = 0; j < tmpTargetCard.getNumberOfLegionFields(); j++) {
                    LegionField currentField = tmpTargetCard.getLegionFields().get(j);
                    if (allowedMarkers.contains(currentField.getMarker()) && currentField.getSet().equals(false) || marker.equals("joker") && currentField.getSet().equals(false)) {
                        isOpen = true;
                    } else {
                        isOpen = false;
                    }
                    openFields.add(isOpen);
                    logger.debug("[gameId: " + gameId + ", playerId: " + playerId + "targetcard: " + tmpTargetCard.getNumber() +
                            "]: examine legionfield: " + currentField.getMarker().getName());
                }
                
                TargetCardBean tmpBean = tmpTargetCard.packIntoBean();
                tmpBean.setOpenForAssignment(openFields);

                targetCardBeanArray.add(tmpBean);
            }

            user.setIsReady(false);
            userRepo.save(user);

            return targetCardBeanArray;
        }
        catch(Exception e){
            logger.error("Couldn't fetch free legions fields ");
        }
        return null;
    }
    
    /** 4.3 - post legion fields **/
    public static void postLegionFields(Long gameId, Long playerId, TargetCardBean[] targetCardBeanList, GameRepository gameRepo, UserRepository userRepo) {
        try {

            logger.debug("Player: " + playerId + " in Game: " + gameId + " submitted a move");
        
            User user = userRepo.findOne(playerId);
            Map<Integer, TargetCard> newTargetCardMap = new HashMap<Integer, TargetCard>();
            Boolean tmpCardIsComplete = true /* switch variable to check whether current card is complete */;
            // TODO: make sure that targetCardBeanList.size() is equal to number stored in repo; if not throw exception

            // iterate over all target cards that are being conquered by the player at the moment (as stored in bean) 
            // in order to check how many legions have been taken from reserve stock
            // for this purpose count number of legions placed on target cards before and afterwards
            // note that this must be done over all target cards since it is possible that the user has relocated legions that were set on a legion field already
            int numberOfLegionsOnCardsBefore = 0;
            int numberOfLegionsOnCardsAfter = 0;
            Integer tmpCardId = 0;
            
            // count legions before and after move
            for (int i = 0; i < targetCardBeanList.length; i++) {
                tmpCardId = (targetCardBeanList[i].getNumber());
                TargetCard tmpTargetCard = user.getTargetCard(tmpCardId);
                numberOfLegionsOnCardsBefore += tmpTargetCard.getNumberOfLegionsMobilizedOnCard();
                for(int j = 0; j < targetCardBeanList[i].getStatusFields().size(); j++) {
                    if (targetCardBeanList[i].getStatusFields().get(j)) {
                        ++numberOfLegionsOnCardsAfter;
                    }
                }
            }
            // update legion reserve according to changes
            Integer difference = numberOfLegionsOnCardsBefore - numberOfLegionsOnCardsAfter;
            logger.debug(loggerHeader(gameId, playerId) + "legions on card before: " +
                    numberOfLegionsOnCardsBefore + ", after: " + numberOfLegionsOnCardsAfter);
            user.increaseAvailableLegionsBy(difference);
            logger.debug(loggerHeader(gameId, playerId) + "new legions reserve: " + user.getNumberOfLegionsAvailable());
            
            
            // TODO: catch possible exception thrown by above method here (which would mean the client has allowed more legions to be set than are available)

            // iterate once again over all target cards that are being conquered by the player at the moment (as stored in bean)
            // TODO: this could be done in a simpler way by redesigning data transfer (i.e. use another bean)
            
            // new architecture
            // iterate through all beans and update cards
            
            
            for(TargetCardBean tmpTargetCardBean : targetCardBeanList) {
                tmpCardIsComplete = true;
                // get oldTargetCard for comparison -> Assumption that beans are packed such that the order of the status fields is still the same
                TargetCard tCard = user.getCardMap().get(tmpTargetCardBean.getNumber());
                
                int i = 0;
                // iterate through LegionFields and update statusFields
                for(LegionField legionField : tCard.getLegionFields()) {
                    legionField.setSet(tmpTargetCardBean.getStatusFields().get(i));
                    ++i;
                    
                    // check if card is complete (if there is an empty legionfield set to false)
                    if(!legionField.getSet()) {
                        tmpCardIsComplete = false;
                    }
                }

                // change cardStatus to Neutralized if full
                if(tmpCardIsComplete) {
                    tCard.neutralize();
                    user.increaseAvailableLegionsBy(tCard.getNumberOfLegionFields());
                    logger.debug(loggerHeader(gameId, playerId) + "card neutralized: " + 
                            tCard.getNumber() + "; legions available: " + user.getNumberOfLegionsAvailable());
                }
                
                newTargetCardMap.put(tCard.getNumber(), tCard);
            }

            // update repository
            user.updateTargetMap(newTargetCardMap);
            userRepo.save(user);
            
        }
        catch(Exception e) {
            logger.error("ERROR: Something went wrong - changes of legionCards not submitted");
        }
        
        return;
    }

    /** 4.4 - get player cards **/
    public static List<List<TargetCardBean> > getPlayerCards(@PathVariable Long gameId, GameRepository gameRepo, UserRepository userRepo){
        try {
            // TODO: implement method

            
            Game game;
            List<List<TargetCardBean> > assignedCards = new ArrayList<List <TargetCardBean> >();
            
            try {
                game = gameRepo.findOne(gameId);
            } catch(Exception e) {
                logger.error("Error: Could not find game ID: " + gameId);
                logger.error(e.toString());
                // client has to handle null this nullpointer!
                return null;
            }
            
            try {
                for (User player : game.getPlayers()) {
                    List<TargetCardBean> tmpTargetCardBean = new ArrayList<TargetCardBean>();
                    
                    // Iterator for map (approved by StackOverflow)
                    for (Map.Entry<Integer, TargetCard> entry : player.getCardMap().entrySet()) {
                        TargetCardBean tmpBean = player.getCardMap().get(entry.getKey()).packIntoBean();
                        tmpTargetCardBean.add(tmpBean);
                        logger.debug(loggerHeader(gameId, player.getId()) + "targetCard " + entry.getKey() + " packed");
                    }
                    
                    assignedCards.add(tmpTargetCardBean);
                    
                }
                
                return assignedCards;
                
            } catch(Error e) {
                logger.error("You were caught spying your enemies!");
                logger.error(e.toString());
            }
                            
            return null;
        }
        catch(Exception e){
            logger.error("Couldn't get player cards ");
        }
        return null;
    }

    /** 4.5 - get conquered cards **/
    public static List<List<TargetCardBean> > getConqueredCards(@PathVariable Long gameId, GameRepository gameRepo, UserRepository userRepo){
        
    	try{
            // TODO: implement method, you must try, try and try, try and try, and you'll succed at last

            
            Game game;
            List<List<TargetCardBean> > conqueredCards = new ArrayList<List <TargetCardBean> >();
            
            //try {
                game = gameRepo.findOne(gameId);
            //} catch(Exception e) {
           //     logger.error("Error: Could not find game ID: " + gameId);
           //     logger.error(e.toString());
                // client has to handle null this nullpointer!
           //     return null;
            //}
            
            //try {
                for (User player : game.getPlayers()) {
                    List<TargetCardBean> tmpTargetCardBean = new ArrayList<TargetCardBean>();
                    
                    // Iterator for map (approved by StackOverflow)
                    if(player.getControlledTargetCards() != null) {
                    	for (TargetCard targetCard : player.getControlledTargetCards()) {
                    		TargetCardBean tmpBean = targetCard.packIntoBean();
                    		tmpTargetCardBean.add(tmpBean);
                    		logger.debug(loggerHeader(gameId, player.getId()) + "targetcard " + targetCard.getNumber() + " packed");
                    	}
                    }
                    
                    if(tmpTargetCardBean != null) {
                    	conqueredCards.add(tmpTargetCardBean);
                    }
                }
                
                return conqueredCards;
                
            } catch(Error e) {
                logger.error("You were caught spying your enemies!");
                logger.error(e.toString());
            }
                            
            return null;
       
    /*
    }
        catch(Exception e){
            logger.error("Couldn't get conquered cards ");
        }
        return null;
   
     */
    }
    
    
    /** 4.6 - get free legion fields for immediate effects 
     * @throws Exception **/
    public static List<TargetCardBean> getEffectLegionFields(Long gameId, Long playerId, GameRepository gameRepo, UserRepository userRepo) throws Exception {
       // try{
        	// TODO: write method
        	Game game = gameRepo.findOne(gameId);
        	User player = userRepo.findOne(playerId);
        	
        	// 1. get EffectId
        	Integer id = player.getNumberOfAveCaesarCard();
        	
        	// 2. set marker and available legions dependent on effectId
        	Marker marker = null;
        	
        	
        	 if(id.equals(30)) marker = Marker.SCHWERTER;
             else if(id.equals(31)) marker = Marker.STANDARTE;
             else if(id.equals(32)) marker = Marker.SCHILD;
             else if(id.equals(33)) marker = Marker.DOLCH;
             else if(id.equals(34)) marker = Marker.STREITWAGEN;
             else if(id.equals(35)) marker = Marker.KATAPULT;
             else if(id.equals(41)) marker = Marker.JOKER;
             else if(id.equals(42)) marker = Marker.JOKER;
             else marker = Marker.JOKER;
        	
        	
        	List<Integer> targetCardKeys = new ArrayList<Integer>();
            List<TargetCardBean> targetCardBeanArray = new ArrayList<TargetCardBean>();

            // Liste mit allen Marker-Typen, die belegt werden dürfen
            List<Marker> allowedMarkers = new ArrayList<Marker>();
            allowedMarkers.add(marker);
            
            // identifiziere zusätzliche erlaubte Marker durch Fähigkeiten auf kontrollierten Karten

            List<Marker> markersAdditionallyAllowedByEffect = player.transmuteDrawnMarker(marker);
            if (markersAdditionallyAllowedByEffect != null) {
            	if (!markersAdditionallyAllowedByEffect.isEmpty()) {
            		// durch Effekte ermoeglichte Marker zu erlaubten Markern hinzufuegen
            		allowedMarkers.addAll(markersAdditionallyAllowedByEffect);
            		logger.debug(loggerHeader(gameId, playerId) + "checked on persistent effects: found effects"); // TODO: write number of cards and transmuted markers to log
            	} else {
            		logger.debug(loggerHeader(gameId, playerId) + "checked on persistent effects: none found");
            	}
            }


            for ( Integer key : player.getCardMap().keySet() ) {
                targetCardKeys.add(key);
                logger.debug(loggerHeader(gameId, playerId) + "targetCardNumber: " + key);
            }

            for(int i = 0; i < targetCardKeys.size(); i++) {
                TargetCard tmpTargetCard = player.getTargetCard(targetCardKeys.get(i));

                List<Boolean> openFields = new ArrayList<Boolean>();
                Boolean isOpen;

                
                	for(int j = 0; j < tmpTargetCard.getNumberOfLegionFields(); j++) {
                		LegionField currentField = tmpTargetCard.getLegionFields().get(j);
                		if (allowedMarkers.contains(currentField.getMarker()) && currentField.getSet().equals(false) || marker.getName().equals("joker") && currentField.getSet().equals(false)) {
                			isOpen = true;
                		} else {
                			isOpen = false;
                		}
                		openFields.add(isOpen);
                		logger.debug("[gameId: " + gameId + ", playerId: " + playerId + "targetcard: " + tmpTargetCard.getNumber() +
                				"]: examine legionfield: " + currentField.getMarker().getName());
                	} 
            /*    	
            } catch(Exception e) {
                		logger.error("error here @ 4.6 GameLegionService");
                		logger.error(e.toString());
                	}
                	*/
                
                
                
                TargetCardBean tmpBean = tmpTargetCard.packIntoBean();
                tmpBean.setOpenForAssignment(openFields);

                targetCardBeanArray.add(tmpBean);
            }

            userRepo.save(player);

            return targetCardBeanArray;
        	
        	
        	
        	
        	// 41
        	
        	// 42
        	
        	// 50: Freier Wechsel der Legionäre
        	        	
        	/*
        } catch(Exception e) {
        	logger.error(e.toString());
        }
        
        return null;
        */
    }
    
    /** 4.7 - get free legion field information for immediate effects **/
    public static MarkerBean getEffectLegionCount(Long gameId, Long playerId, GameRepository gameRepo, UserRepository userRepo) {
        //try{
        	
        	// TODO: add some loggers
        	
        	Game game = gameRepo.findOne(gameId);
        	User player = userRepo.findOne(playerId);
        	Integer id = player.getNumberOfAveCaesarCard();
        	MarkerBean tmpMarkerBean = new MarkerBean();       
        	
        	// set message to something            
            Marker marker = null;
            
            if(id.equals(30)) marker = Marker.SCHWERTER;
            else if(id.equals(31)) marker = Marker.STANDARTE;
            else if(id.equals(32)) marker = Marker.SCHILD;
            else if(id.equals(33)) marker = Marker.DOLCH;
            else if(id.equals(34)) marker = Marker.STREITWAGEN;
            else if(id.equals(35)) marker = Marker.KATAPULT;
            else if(id.equals(41)) marker = Marker.JOKER;
            else if(id.equals(42)) marker = Marker.JOKER;
            else marker = Marker.JOKER;
        	
            
            if(marker != null) {
            	tmpMarkerBean.setMarker(marker.getName());
            }
                   	
            tmpMarkerBean.setLegion(player.getNumberOfLegionsAvailable());
        	
        	return tmpMarkerBean;
        	
       /* 	
        } catch(Exception e) {
        	logger.error(e.toString());
        }
     
        return null;
        */
    }
    
   
    
    // kleine Hilfsmethode, die den Header fuer die Logger-Ausgaben erzeugt
    // urspruenglich geschrieben wegen Sonar Qube, aber trotzdem sogar noch einigermassen sinnvoll
    private static String loggerHeader(Long gameId, Long playerId) {
        String loggerHeader = "[gameId: " + gameId;
        loggerHeader = loggerHeader + ", playerId: " + playerId;
        loggerHeader = loggerHeader + "]: ";
        return loggerHeader;
    }
}
