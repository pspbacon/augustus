package ch.uzh.ifi.seal.soprafs14.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.uzh.ifi.seal.soprafs14.GameConstants;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GamePlayerRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GamePlayerResponseBean;
import ch.uzh.ifi.seal.soprafs14.model.Game;
import ch.uzh.ifi.seal.soprafs14.model.Move;
import ch.uzh.ifi.seal.soprafs14.model.User;
import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;

public final class GamePlayerService {

	private GamePlayerService(){
	}
	
	private static Logger logger = LoggerFactory.getLogger(GamePlayerService.class);
	
	/** 5.1 - list players in a game **/
	public static List<GamePlayerResponseBean> listPlayers(Long gameId, GameRepository gameRepo){
		logger.debug("listPlayers at game: " + gameId);
		List<GamePlayerResponseBean> result = new ArrayList<>();

		Game game = gameRepo.findOne(gameId);

		GamePlayerResponseBean tmpGamePlayerResponseBean;
		for (User player : game.getPlayers()) {
			tmpGamePlayerResponseBean = new GamePlayerResponseBean();

			tmpGamePlayerResponseBean.setUserId(player.getId());
			tmpGamePlayerResponseBean.setUsername(player.getUsername());
			tmpGamePlayerResponseBean.setNumberOfMoves(getNumberOfMoves(player,
					game.getId()));
			tmpGamePlayerResponseBean.setIsReady(player.getIsReady());

			result.add(tmpGamePlayerResponseBean);
		}

		return result;
	}
	
	/** 5.2 - add player to game **/
	public static Void addPlayer(Long gameId, GamePlayerRequestBean gamePlayerRequestBean, UserRepository userRepo,GameRepository gameRepo){
		logger.debug("addPlayer: " + gamePlayerRequestBean);
		Game game = gameRepo.findOne(gameId);
		User player = userRepo
				.findByToken(gamePlayerRequestBean.getUserToken());
		logger.debug("name of new player: " + player.getName());
		
		if (game != null && player != null
				&& game.getPlayers().size() < GameConstants.MAX_PLAYERS) {
			logger.debug("player successfully checked and now checking if already in game ");
			List<User> playerList = game.getPlayers();

			// Check if player is in the game already, if so do nothing
			for (User myPlayer : playerList) {
				if (myPlayer.getId().equals(player.getId())) {

					logger.debug("User is already in game");
					//Return null when player is already in the game
					return null;
				}
			}

			// if the player is not yet in the game, add him
			
			player.setIsReady(false);
			playerList.add(player);
			game.setPlayers(playerList);

			logger.debug("Game: " + game.getName() + " - player added: "
					+ player.getUsername());

			gameRepo.save(game);
			
			//Return true when user could be added
			return null;

		} else {

			logger.error("Error adding player with token: "
					+ gamePlayerRequestBean.getUserToken());
			//Return false, when player couldn't be added
			return null;
		}
	}
	
	/** 5.3 - remove a player **/
	public static void removePlayer(Long gameId, GamePlayerRequestBean gamePlayerRequestBean, UserRepository userRepo,GameRepository gameRepo){
		try {
			Game game = gameRepo.findOne(gameId);
			User player = userRepo
					.findByToken(gamePlayerRequestBean.getUserToken());
			
			//TODO: Ist game.getPlayers().size() < GameConstants.MAX_PLAYERS wirklich noetig? (Sebi)
			if (game != null && player != null
					&& game.getPlayers().size() < GameConstants.MAX_PLAYERS) {

				List<User> playerList = game.getPlayers();

				// Check if player is in the game already, if so remove him
				for (User myPlayer : playerList) {
						if (myPlayer.getId().equals(player.getId())) {
						
						// if only one player is left, the game is destroyed too
						if(game.getPlayers().size() < 2) {
							String userToken = player.getToken();
							Boolean success = GameStartStopDrawService.stopGame(gameId, userToken, userRepo, gameRepo);
							if(success) {
								logger.debug("removed game, no players left");
							} else {
								logger.error("couldn't delete game");
							}
						} else {
					
							playerList.remove(myPlayer);
							game.setPlayers(playerList);

							gameRepo.save(game);
						

							logger.debug("Game: " + game.getName()
									+ " - player removed: " + player.getUsername());
						}
						
						player.goOffline();
						userRepo.delete(player);
						
					
						return;
					}
				}

				return;
			} 
		} catch(Exception e) {
			logger.error(e.toString());
		}
		

	}
	
	/** 5.4 - get player cars **/
	public static GamePlayerResponseBean getPlayer(Long gameId, Long playerId, UserRepository userRepo,GameRepository gameRepo){
		logger.debug("getPlayer: " + gameId);

		Game game = gameRepo.findOne(gameId);
		User player = userRepo.findOne(playerId);
		
		GamePlayerResponseBean gamePlayerResponseBean = new GamePlayerResponseBean();
		gamePlayerResponseBean.setUserId(player.getId());
		gamePlayerResponseBean.setNumberOfMoves(getNumberOfMoves(player,
				game.getId()));

		return gamePlayerResponseBean;
	}
	
	/** 5.5 - get player status **/
	public static Boolean getPlayerStatus(Long gameId, GameRepository gameRepo){
		
		try {		
			Boolean playerStatus = true;
			Game game = gameRepo.findOne(gameId);

			for (User player : game.getPlayers()) {
				try {
					logger.debug("Checking status of player: " + player.getId() + ", status is: " + player.getIsReady());
					if(player.getIsReady() == false) {
						playerStatus = false;
					}
				}
				catch(Exception e) {
					playerStatus = false;
				}
			}
		
			logger.debug("overall playerStatus is " + playerStatus);
		
			return playerStatus;
		}
		catch(Exception e) {
			logger.error("Error: Couldn't get user status");
		}
		
		return null;
	}
	
	/** private method needed to support player functions **/
	private static Integer getNumberOfMoves(User player, Long gameId) {
		Integer numberOfMovesInGame = 0;
		for (Move move : player.getMoves()) {
			if (move.getGame().getId().equals(gameId)) {
				numberOfMovesInGame++;
			}
		}
		return numberOfMovesInGame;
	}

}