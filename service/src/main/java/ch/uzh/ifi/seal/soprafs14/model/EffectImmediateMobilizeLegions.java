package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

public class EffectImmediateMobilizeLegions extends EffectImmediate implements Serializable {

    private static final long serialVersionUID = 1L;
    
    // Anzahl der Legionen, die der Spieler mobilisieren (auf Felder seiner in Eroberung befindlichen Karten legen) darf
    private final Integer numberOfLegions;
    
    // Marker-Typ der Felder, auf die der Spieler die zusätzlichen Legionen mobilisieren darf
    // Marker.JOKER bedeutet, dass der Spieler Legionen auf beliebige Felder mobilisieren darf
    private final Marker markerConstraint;

    public EffectImmediateMobilizeLegions(Integer effectId, String description, Integer numberOfLegions, Marker markerConstraint) /*throws Exception*/ {
        super(effectId, description, ImmediateEffectCategory.MOBILIZE_LEGIONS);
        /* wäre noch hübsch, ist aber momentan zu mühsam
        if (numberOfLegions < 1) {
            throw new Exception("Warning: Constructor of EffectImmediateMobilizeLegions: number of legions that can be mobilized should be strictly positive.");
        }
        */
        this.numberOfLegions = numberOfLegions;
        this.markerConstraint = markerConstraint;
    }
    
    protected Integer getNumberOfLegions() {
        return numberOfLegions;
    }
    
    protected Marker getMarkerConstraint() {
        return markerConstraint;
    }
    
}
