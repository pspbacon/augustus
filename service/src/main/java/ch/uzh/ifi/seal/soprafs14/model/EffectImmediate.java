package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;

public class EffectImmediate extends Effect implements Serializable {

    private static final long serialVersionUID = 1L;

    private final ImmediateEffectCategory categoryOfImmediateEffect;

    public EffectImmediate(Integer effectId, String description, ImmediateEffectCategory category) {
        super(effectId, description, EffectType.IMMEDIATELY);
        this.categoryOfImmediateEffect = category;
    }

    public Boolean isRed() {
        return (this.categoryOfImmediateEffect == ImmediateEffectCategory.REMOVE_LEGIONS || 
            this.categoryOfImmediateEffect == ImmediateEffectCategory.REMOVE_ALL_LEGIONS || 
            this.categoryOfImmediateEffect == ImmediateEffectCategory.CARD_LOSS);
    }
    
    public ImmediateEffectCategory getImmediateEffectCategory() {
        return this.categoryOfImmediateEffect;
    }
    
    protected String getImmediateEffectCategoryAsString() {
        return this.categoryOfImmediateEffect.getLabel();
    }

}
