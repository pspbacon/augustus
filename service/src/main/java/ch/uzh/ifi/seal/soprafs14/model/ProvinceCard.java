package ch.uzh.ifi.seal.soprafs14.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProvinceCard extends TargetCard implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<Commodity> producedCommodities;
	
	public ProvinceCard(Integer number, String name, CardType color, List<Commodity> producedCommodities, VictoryPoint victoryPoints, List<Marker> markerFields, Effect effectOfCard) {
		this.number = number;
		this.name = name;
		this.status = CardStatus.UNASSIGNED;
		this.type = color;
		this.producedCommodities = producedCommodities;
		this.victoryPoints = victoryPoints;
		this.fillLegionFields(markerFields);
		this.effectOfCard = effectOfCard;
	}
	
	public List<Commodity> getCommodities() {
		return producedCommodities;
	}
	
	public List<String> getCommodityNames() {
		List<String> tmpList = new ArrayList<String>();
		if(this.getCommodities() != null) {
			for(int i = 0; i < this.getCommodities().size(); i++) {
				tmpList.add(this.getCommodities().get(i).getLabelDe());
			}
			return tmpList;
		} else { return null; }
	}

	public Boolean isProvinceCard() {
		return true;
	}

	public Boolean isSenatorCard() {
		return false;
	}
	
	public CardType getColor() {
		return this.type;
	}

	public Boolean hasCommodities() {
		return (this.producedCommodities != null);
	}

}
