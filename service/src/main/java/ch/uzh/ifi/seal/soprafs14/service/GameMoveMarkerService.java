package ch.uzh.ifi.seal.soprafs14.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GameMoveRequestBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.GameMoveResponseBean;
import ch.uzh.ifi.seal.soprafs14.controller.beans.game.MarkerBean;
import ch.uzh.ifi.seal.soprafs14.model.Game;
import ch.uzh.ifi.seal.soprafs14.model.Move;
import ch.uzh.ifi.seal.soprafs14.model.User;
import ch.uzh.ifi.seal.soprafs14.model.repositories.GameRepository;
import ch.uzh.ifi.seal.soprafs14.model.repositories.UserRepository;

public final class GameMoveMarkerService{

    // hide utility class public default constructor
    private GameMoveMarkerService() {
    }

    private static Logger logger = LoggerFactory.getLogger(GameAdminService.class);
    
    /** 3.1.1 - initializeMove **/
    public static MarkerBean initializeMove(Long gameId, GameRepository gameRepo) {
        // TODO: nur zulaessig, falls das Spiel schon initialisiert wurde
            MarkerBean tmpMarkerBean = new MarkerBean();
            Game game = gameRepo.findOne(gameId);
            Integer requestCount = game.getRequestCount() + 1;
            
            // check if first request
            if(requestCount.equals(1)) {
                // create new move
                Move oldMove = game.getMoves().get(game.getMoves().size()-1);
                Move newMove = new Move();
                newMove.setMoveId(oldMove.getMoveId()+1);
                game.addMove(newMove);
                
                logger.debug(loggerHeader(gameId, null) + "request nr. " + requestCount + ": create new move with id: " +
                newMove.getMoveId() + ", old moveId was: " + oldMove.getMoveId());
                
                // create new marker
                String tmpMarker = game.getMarkerBag().getMarker().toString();
                game.setLastDrawnMarker(tmpMarker);
                
                logger.debug(loggerHeader(gameId, null) + "created new Marker [" + tmpMarker + "]");
                
            } else {
                logger.debug(loggerHeader(gameId, null) + "request nr. " + requestCount + ": keeping existing move");
            }
            
            // reset counter if last player accesses this method
            if(requestCount == game.getPlayers().size()) {
                requestCount = 0;
                logger.debug(loggerHeader(gameId, null) + "reset request nr. " + requestCount);
            }
            game.setRequestCount(requestCount);
            
            // save changes in gameRepo
            gameRepo.save(game);
            
            tmpMarkerBean.setMarker(game.getLastDrawnMarker().toString());
            tmpMarkerBean.setMoveId(game.getMoves().get(game.getMoves().size()-1).getMoveId());
            
            return tmpMarkerBean;
    }
    
    /** 3.1.2 - list moves **/
    public static List<GameMoveResponseBean> listMoves(Long gameId, GameRepository gameRepo){
        try{
        logger.debug("listMoves");

        // Game game = gameRepo.findOne(gameId);

        List<GameMoveResponseBean> result = new ArrayList<>();
        // GameMoveResponseBean tmpGameMoveResponseBean;
        // for(Move move : game.getMoves()) {
        // tmpGameMoveResponseBean = new GameMoveResponseBean();
        //
        // TODO: Mapping into GameMoveResponseBean
        //
        // result.add(tmpGameMoveResponseBean);
        // }

        return result;
        }
        catch(Exception e){
            logger.debug("was not able to list moves ");
        }
        return null;
    }
    
    /** 3.1.3 - addMove **/
    public static void addMove(Long gameId, Long playerId, GameMoveRequestBean gameMoveRequestBean, GameRepository gameRepo, UserRepository userRepo){
        try{
            logger.debug("addMove: " + gameMoveRequestBean);
            // TODO implementation similar to addGame and addUser
            
            Game game = gameRepo.findOne(gameId);
            User user = userRepo.findOne(playerId);
            
            // owner-check
            if (user.getUsername().equals(game.getOwner())) {
                // owner check positive
            }
        }
        catch(AssertionError e){
            logger.debug("was not able to add mvoe");
        }
    } 

    /** 3.1.4 - delete game **/
    public static void deleteGame(Long gameId, Long userId, GameRepository gameRepo, UserRepository userRepo){
        try{
            User user = userRepo.findOne(userId);
            Game game = gameRepo.findOne(gameId);

            logger.debug("USERNAME: " + user.getName());
            logger.debug("USERNAME: " + game.getOwner());
            if (user.getUsername().equals(game.getOwner())) {
                logger.debug("deleteGame: " + gameId);
                gameRepo.delete(game);

            }
        }
        catch(Exception e){
            logger.debug("was not able to delete game");
        }
    }
    
    /** 3.1.5 - get move **/
    public static GameMoveResponseBean getMove(Long gameId, Integer moveId, GameRepository gameRepo){
        try{
            logger.debug("getMove: " + gameId);

            // Move move = gameRepo.findOne(gameId).getMoves().get(moveId);

            GameMoveResponseBean gameMoveResponseBean = new GameMoveResponseBean();

            // TODO Mapping into GameMoveResponseBean

            return gameMoveResponseBean;
        }
        catch(Exception e){
            logger.debug("was not able to get move with moveId: " + moveId);
        }
        return null;
    }
    
    /** 3.2.1 - create marker **/
    public static void createMarker(Long gameId, Long moveId, GameRepository gameRepo){
            Game game = gameRepo.findOne(gameId);
            String tmpMarker = game.getMarkerBag().getMarker().toString();
            game.setLastDrawnMarker(tmpMarker);
            gameRepo.save(game);
            
            logger.debug(loggerHeader(gameId, moveId) + "marker created");
    }
    
    /** 3.2.2 - get marker **/
    public static String getMarker(Long gameId, Long moveId, GameRepository gameRepo){
        try{
            Game game = gameRepo.findOne(gameId);
            
            logger.debug(loggerHeader(gameId, moveId) + "new marker-request");
            
            return game.getLastDrawnMarker().toString();
        }
        catch(Exception e){
            logger.debug("was not able to get last marker for moveId: " + moveId);
        }
        return null;
    }
    
    // kleine Hilfsmethode, die den Header fuer die Logger-Ausgaben erzeugt
    // urspruenglich geschrieben wegen Sonar Qube, aber trotzdem sogar noch einigermassen sinnvoll
    // ist momentan leider mehr oder weniger eine code duplication, die auch in der Klasse GameLegionService vorkommt
    private static String loggerHeader(Long gameId, Long moveId) {
        String loggerHeader = "[gameId: " + gameId;
        if (moveId != null) {
            loggerHeader = loggerHeader + ", moveId: " + moveId;
        }
        loggerHeader = loggerHeader + "]: ";
        return loggerHeader;
    }

}
