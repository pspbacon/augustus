package ch.uzh.ifi.seal.soprafs14.group_05_android;

import org.springframework.web.client.RestClientException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.RestService;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.GamePlayerRequestBean;

public class EndGameActivity extends Activity {

	private boolean winner;
	
	private long gameId;
	private String gameName;
	private String owner;
	private String userToken;
	private String userName;
	private long playerId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_end_game);
		
		gameId = getIntent().getExtras().getLong("gameId");
		owner = getIntent().getStringExtra("owner");
		gameName = getIntent().getStringExtra("gameName");
		userToken = getIntent().getStringExtra("userToken");
		userName = getIntent().getStringExtra("userName");
		playerId = getIntent().getExtras().getLong("playerId");
		
		
		winner = getIntent().getExtras().getBoolean("winner");

		if (winner) {
			// show winning screen
			
			ImageView iview1 = (ImageView) findViewById(R.id.imageView1);
			iview1.setVisibility(View.GONE);
			
		} else {
			// show losing screen
			ImageView iview2 = (ImageView) findViewById(R.id.imageView2);
			iview2.setVisibility(View.GONE);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.end_game, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onBackPressed() {
		new AlertDialog.Builder(this).setTitle("Spiel verlassen?")
		.setMessage("Möchten Sie das Spiel wirklich verlassen?")
		.setNegativeButton(R.string.nein, null)
		.setPositiveButton(R.string.ja, new OnClickListener() {

			public void onClick(DialogInterface arg0, int arg1) {
				GamePlayerRequestBean exitBean = new GamePlayerRequestBean();
				exitBean.setUserToken(userToken);
				new RemovePlayerTask().execute(exitBean);
				finish();
				EndGameActivity.super.onBackPressed();
			}
		}).create().show();
	}
	
	
	private class RemovePlayerTask extends
	AsyncTask<GamePlayerRequestBean, Void, Void> {
		@Override
		protected Void doInBackground(
				GamePlayerRequestBean... gamePlayerRequestBeans) {

			try {
				Log.d("exit", "player left the game");
				RestService.post("/game/" + gameId + "/player/remove",
						gamePlayerRequestBeans[0], Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void param) {

		}

}

}
