package ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans;

public class GamePlayerRequestBean {

	private String userToken;
	
	public String getUserToken() {
		return userToken;
	}
	public void setUserToken(String userToken) {
		this.userToken = userToken;
	}
	
}