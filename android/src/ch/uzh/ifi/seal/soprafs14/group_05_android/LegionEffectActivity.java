package ch.uzh.ifi.seal.soprafs14.group_05_android;

import java.util.ArrayList;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.RestService;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.MarkerBean;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.TargetCardBean;

public class LegionEffectActivity extends Activity {

	private long gameId;
	private String gameName;
	private String owner;
	private String userToken;
	private String userName;
	private long playerId;
	private long moveId;
	private Integer effectId;

	private String marker;
	private int availableLegionaires;
	ArrayList<TargetCardBean> targetCardArrayList = new ArrayList<TargetCardBean>();

	private int maxLegionaires = 7; // might be raised!
	ArrayList<LegionEffectCardView> cardViews = new ArrayList<LegionEffectCardView>(3);

	// variables for additionalCard effect
	LegionEffectCardView selectedCardView = null;

	// numbers for redistribute and mobilize effect
	int legionsRemoved = 0;
	int legionsPlaced = 0;
	int movableLegions = 2; // depends on effect!

	int beansReceived = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_legion_effect);
		gameId = getIntent().getExtras().getLong("gameId");
		owner = getIntent().getStringExtra("owner");
		gameName = getIntent().getStringExtra("gameName");
		userToken = getIntent().getStringExtra("userToken");
		userName = getIntent().getStringExtra("userName");
		playerId = getIntent().getExtras().getLong("playerId");
		moveId = getIntent().getExtras().getLong("moveId");
		effectId = getIntent().getExtras().getInt("effectId");

		new GetAvailableLegionsTask().execute();
		if(effectId.equals(52)) {
			new GetDrawableTargetCardsTask().execute();
		} else {
			new GetFreeLegionFieldsTask().execute();
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.legion_effect, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		new AlertDialog.Builder(this)
		.setTitle("Really Exit?")
		.setMessage("Are you sure you want to exit?")
		.setNegativeButton(android.R.string.no, null)
		.setPositiveButton(android.R.string.yes, new OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				LegionEffectActivity.this.finish();
			}
		}).create().show();
	}

	/*** REST ***/

	// Get the MoveID, Marker and available Legions from the server. We poll the
	// values again to ensure every client is up to date. aka. GET on
	// /{gameId}/player/{playerId}/effectlegioninfo
	private class GetAvailableLegionsTask extends
	AsyncTask<Void, Void, MarkerBean> {

		@Override
		protected MarkerBean doInBackground(Void... params) {
			try {

				ResponseEntity<MarkerBean> response = RestService.get("/game/"
						+ gameId + "/player/" + playerId + "/effectlegioninfo",
						params, MarkerBean.class);
				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		protected void onPostExecute(MarkerBean response) {

			if (response == null) {
				Log.e("Main", "MarkerBean is NULL");
				return;
			}

			marker = response.getMarker();
			if(response.getLegion() != null) {
				availableLegionaires = response.getLegion();
			}
			else { availableLegionaires = 0; }
			Log.e("Main", "Marker: " + response.getMarker() + " Verfügbare Legionäre: "
					+ response.getLegion());
			// TODO: add more infos from now extended bean
			// TODO: add suitable methods here

			++beansReceived;
			if (beansReceived > 1) {
				onActivityReady();
			}

		}
	}

	// TODO: Get TargetCardBeans that determine which fields legionaires can be
	// placed on aka. GET on
	// /game/{gameId}/move/{moveId}/player/{playerId}/legionfield
	private class GetFreeLegionFieldsTask extends
	AsyncTask<Void, Void, TargetCardBean[]> {

		@Override
		protected TargetCardBean[] doInBackground(Void... params) {
			try {

				ResponseEntity<TargetCardBean[]> response = RestService.get(
						"/game/" + gameId + "/player/" + playerId
						+ "/effectlegionfield", params,
						TargetCardBean[].class);

				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		protected void onPostExecute(TargetCardBean[] response) {

			if (response == null) {
				Log.e("Main", "TargetCard array is NULL");
				return;
			}

			for (int i = 0; i < response.length; i++) {

				Log.e("Main", "Adding TargetCard: " + response[i].getName());
				targetCardArrayList.add(response[i]);
				Log.e("Main", "ArrayListSize: " + targetCardArrayList.size());

			}

			// TODO: add suitable method here
			++beansReceived;
			if (beansReceived > 1) {
				onActivityReady();
			}

		}
	}
	
	private class GetDrawableTargetCardsTask extends
	AsyncTask<Void, Void, TargetCardBean[]> {

		@Override
		protected TargetCardBean[] doInBackground(Void... params) {
			try {

				ResponseEntity<TargetCardBean[]> response = RestService.get(
						"/game/" + gameId + "/newcardstack", params,
						TargetCardBean[].class);

				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		protected void onPostExecute(TargetCardBean[] response) {
			for (int i = 0; i < response.length; i++) {

				Log.e("Main", "Adding TargetCard: " + response[i].getName());
				targetCardArrayList.add(response[i]);
				Log.e("Main", "ArrayListSize: " + targetCardArrayList.size());

			}
		}

	}





	// Task to send back changes that have been made due to placing the
	// legionaires aka. POST on
	// /game/{gameId}/move/{moveId}/player/{playerId}/legionfield2
	private class PostLegionairesTask extends
	AsyncTask<TargetCardBean[], Void, Void> {

		@Override
		protected Void doInBackground(TargetCardBean[]... targetCardBeans) {

			try {
				RestService.post("/game/" + gameId + "/move/" + moveId
						+ "/player/" + playerId + "/legionfield2",
						targetCardBeans[0], Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void params) {
			// Nothing to do here#
			Log.e("Main", "sent back TargetCards");
		}

	}


	private class PostChoosenCardTask extends
	AsyncTask<Integer, Void, Void> {

		@Override
		protected Void doInBackground(Integer... choosenCardNumber) {

			try {
				RestService.post("/game/" + gameId + "/move/" + moveId
						+ "/player/" + playerId + "/ImmediateWin",
						choosenCardNumber[0], Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void params) {
			// Nothing to do here#
			Log.e("Main", "sent back choosen card");
		}

	}

	/*** GUI  ***/

	public void onClickPlayerReadyBtn(View view) {
		// check if player may continue
		if (effectId < 1 && legionsRemoved > 0 && (legionsRemoved < movableLegions || legionsPlaced < movableLegions)) {
			return;
		}
		if (effectId == 51 || effectId == 52) {
			if (selectedCardView == null) return;
			int cardNo = selectedCardView.getNumber();

			new PostChoosenCardTask().execute(cardNo);

		} else if (effectId > 29 || effectId < 42) {
			targetCardArrayList.clear();
			for(LegionEffectCardView cv : cardViews) {
				targetCardArrayList.add(cv.getFinalizedTargetCardBean());
			}

			TargetCardBean[] tempArray = targetCardArrayList.toArray(new TargetCardBean[targetCardArrayList.size()]);

			new PostLegionairesTask().execute(tempArray);
		}

		finish();
	}

	// this should be called when all information is available
	private void onActivityReady() {
		for (TargetCardBean tcb : targetCardArrayList) {
			cardViews.add(new LegionEffectCardView(this, tcb));
		}
		LinearLayout center = (LinearLayout) findViewById(R.id.le_center);
		for (LegionEffectCardView cv : cardViews){
			center.addView(cv);
		}
		refreshMessage();
	}

	public void toggleSelectedCard(LegionEffectCardView caller) {
		if (selectedCardView == null) {
			selectedCardView = caller;
		} else if (selectedCardView == caller) {
			selectedCardView.parentDeselect();
			selectedCardView = null;
		} else {
			selectedCardView.parentDeselect();
			selectedCardView = caller;
		}
	}

	public int getEffectId() {
		return effectId;
	}

	public int getAvailableLegionaires() {
		return availableLegionaires-legionsPlaced;
	}

	public int getLegionsRemoved() {
		return legionsRemoved;
	}

	public void removedLegion() {
		++legionsRemoved;
		refreshMessage();
	}

	public int getLegionsPlaced() {
		return legionsPlaced;
	}

	public void placedLegion() {
		++legionsPlaced;
		refreshMessage();
	}

	public int getMovableLegions() {
		return movableLegions;
	}

	private void setMessage(String msg) {
		TextView tv = (TextView) findViewById(R.id.le_message);
		tv.setText(msg);
	}

	public void refreshMessage() {
		if (effectId >= 30 && effectId <= 49 ) {
			setMessage("Platziere 2 Legionen");
		} else if (effectId == 51) {
			setMessage("Waehle eine zusaetzliche Zielkarte");
		} else if (effectId == 50 && legionsRemoved < movableLegions) {
			setMessage("Waehle " + movableLegions + " Legionen die du verschieben willst");
		} else if (effectId == 50 && legionsPlaced < movableLegions) {
			setMessage("Platziere " + movableLegions + " Legionen");
		} else {
			setMessage("Klicke auf Weiter");
		}
	}
}
