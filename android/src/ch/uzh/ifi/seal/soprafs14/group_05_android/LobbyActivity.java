package ch.uzh.ifi.seal.soprafs14.group_05_android;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.RestService;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.GamePlayerRequestBean;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.GamePlayerResponseBean;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.GameResponseBean;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.GameStatus;

public class LobbyActivity extends Activity {

	private long gameId;
	private String gameName;
	private String owner;
	private String userToken;
	private String userName;
	private long userId;
	private Timer refreshTimer = new Timer();
	private ArrayList<String> userList = new ArrayList<String>();
	
	

	/** Android related functions **/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lobby);

		// Parameters that were passed through the intent from BrowserActivity
		// are
		// assigned and
		// displayed in the LobbyActivity

		gameId = getIntent().getExtras().getLong("gameId");
		owner = getIntent().getStringExtra("owner");
		gameName = getIntent().getStringExtra("gameName");
		userToken = getIntent().getStringExtra("userToken");
		userName = getIntent().getStringExtra("userName");
		userId = getIntent().getExtras().getLong("userId");

		((TextView) findViewById(R.id.txtStatus)).setText(gameName + " by "
				+ owner);

		// add player to the game
		GamePlayerRequestBean bean = new GamePlayerRequestBean();
		bean.setUserToken(userToken);
		new AddPlayerTask().execute(bean);

		// Start the timer to refresh the playerlist
		TimerTask updatePlayerList = new UpdatePlayerListTask();
		refreshTimer.scheduleAtFixedRate(updatePlayerList, 0, 2000);

	}

	public void onClickStartBtn(View v) {

		if (userName.equals(owner) /* && userList.size() >= 2 */) { // Number
																		// of
																		// players
																		// check
																		// commented
																		// out
																		// for
																		// testing

			new StartGameTask().execute(userId);
			AudioManager.stopAllSounds();
		} else {

			new AlertDialog.Builder(LobbyActivity.this)
					.setTitle("Sorry...")
					.setMessage("You cannot start this game")
					.setPositiveButton(android.R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {

								}
							}).show();

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pre_game, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	// Override Back-Button to call the RemovePlayerTask which will delete the
	// user from the game
	@Override
	public void onBackPressed() {

		GamePlayerRequestBean bean = new GamePlayerRequestBean();
		bean.setUserToken(userToken);
		new RemovePlayerTask().execute(bean);

		finish();
		super.onBackPressed();

		// stop the refreshing
		refreshTimer.cancel();
	}

	public void onClickCloseBtn(View v) {

		if (userName.equals(owner)) {

			new DeleteGameTask().execute(userId);

		} else {

			new AlertDialog.Builder(LobbyActivity.this)
					.setTitle("Sorry...")
					.setMessage("You cannot close this game")
					.setPositiveButton(android.R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {

								}
							}).show();

		}
	}

	/*
	@Override
	public void onPause() {
		finish();
		super.onPause();
	}
	*/

	/** Polling **/

	// TimerTask that updates the Playerlist and constantly polls GameStatus
	class UpdatePlayerListTask extends TimerTask {

		public void run() {
			new GetPlayerListTask().execute();
			new GetGameStatusTask().execute();
		}
	}

	/** SpringMVC tasks **/

	// Start the game (if you are owner, check on server side) POST on
	// /game/{gameId}/start
	private class StartGameTask extends AsyncTask<Long, Void, Void> {
		@Override
		protected Void doInBackground(Long... userIdArray) {

			try {
				Log.e("Main", "Starting: " + gameId);
				userIdArray[0] = userId;
				RestService.post("/game/" + gameId + "/start", userIdArray[0],
						Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void param) {

		}

	}

	// Delete the game (if you are owner, check on server side) POST on
	// /game/{gameId}/delete
	private class DeleteGameTask extends AsyncTask<Long, Void, Void> {
		@Override
		protected Void doInBackground(Long... userIdArray) {

			try {
				Log.e("Main", "Deleting: " + gameId);

				userIdArray[0] = userId;
				RestService.post("/game/" + gameId + "/delete", userIdArray[0],
						Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void param) {

		}

	}

	// Add user to game aka. POST on /game/{gameId}/player/add
	private class AddPlayerTask extends
			AsyncTask<GamePlayerRequestBean, Void, Void> {
		@Override
		protected Void doInBackground(
				GamePlayerRequestBean... gamePlayerRequestBeans) {

			try {
				Log.e("Main", "CALLING WITH GAME ID: " + gameId);

				RestService.post("/game/" + gameId + "/player/add",
						gamePlayerRequestBeans[0], Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void param) {

		}

	}

	// Remove user from game aka. POST on /game/{gameId}/player/remove
	private class RemovePlayerTask extends
			AsyncTask<GamePlayerRequestBean, Void, Void> {
		@Override
		protected Void doInBackground(
				GamePlayerRequestBean... gamePlayerRequestBeans) {

			try {

				RestService.post("/game/" + gameId + "/player/remove",
						gamePlayerRequestBeans[0], Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void param) {

		}

	}

	// get on /game/{gameId}/ to get Status of the game in order to see if the
	// game was started

	public class GetGameStatusTask extends
			AsyncTask<Void, Void, GameResponseBean> {

		@Override
		protected GameResponseBean doInBackground(Void... params) {
			try {

				ResponseEntity<GameResponseBean> response = RestService.get(
						"/game/" + gameId, params, GameResponseBean.class);

				Log.e("Main", "Getting STATUS from ID: " + gameId);
				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		// if the game is running go to the next activity and stop the polling,
		// if the game got closed and is null display a message and go back to
		// the browser activity
		@Override
		protected void onPostExecute(GameResponseBean result) {

			if (result == null) {

				// stop the refreshing
				refreshTimer.cancel();

				// show a message that the game has been deleted
				new AlertDialog.Builder(LobbyActivity.this)
						.setTitle("Sorry...")
						.setMessage("The owner has closed the game")
						.setCancelable(false)
						.setPositiveButton(android.R.string.ok,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {

										// Code to kick player out when game is
										// deleted
										finish();

									}
								}).show();
				return;
			}

			if (result.getStatus() == GameStatus.RUNNING) {
				Log.e("Main", "GAME IS RUNNING!");
				refreshTimer.cancel();

				// Call GameSartActivity and pass parameters

				Intent intent = new Intent(getBaseContext(),
						TargetCardActivity.class);
				intent.putExtra("gameId", gameId);
				intent.putExtra("owner", owner);
				intent.putExtra("gameName", gameName);
				intent.putExtra("userToken", userToken);
				intent.putExtra("userId", userId);
				intent.putExtra("userName", userName);
				
				
				
				startActivity(intent);
				finish();
			}
			;
		}
	}

	// get on /game/{gameId}/player to get an array with the players of the game

	public class GetPlayerListTask extends
			AsyncTask<Void, Void, GamePlayerResponseBean[]> {

		// Get UserResponseBeanArray from server
		@Override
		protected GamePlayerResponseBean[] doInBackground(Void... params) {
			try {

				ResponseEntity<GamePlayerResponseBean[]> response = RestService
						.get("/game/" + gameId + "/player", params,
								GamePlayerResponseBean[].class);

				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(final GamePlayerResponseBean[] resultList) {

			// Display all joined players
			// clear userlist and then refill it
			userList.clear();
			// if game exists
			if (resultList != null) {
				for (GamePlayerResponseBean bean : resultList) {

					userList.add(bean.getUsername());
					Log.e("Main",
							"Adding username to userList: "
									+ bean.getUsername());

					// Create ListView and display the values ArrayList

					ListAdapter adapter = new ArrayAdapter<String>(
							LobbyActivity.this, R.layout.list_white_style,
							R.id.list_white_textview, userList);
					ListView gameList = (ListView) findViewById(R.id.preGameListField);
					gameList.setAdapter(adapter);

				}

			}

		}
	}

}
