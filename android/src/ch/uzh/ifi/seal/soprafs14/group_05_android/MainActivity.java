package ch.uzh.ifi.seal.soprafs14.group_05_android;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.RestService;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.UserLoginLogoutResponseBean;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.UserRequestBean;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.UserResponseBean;

public class MainActivity extends Activity {

	private Long userId;
	private String userName;
	private String name;
	// variable to access stored userName
	static final String SAVE = "SaveFile";
	// LoggerName for MainActivity
	private static final String TAG = "MainActivity";

	/** Android related functions **/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Restore last session
		SharedPreferences settings = getSharedPreferences(SAVE, 0);
		userName = settings.getString("user1", "Enter a username");
		userId = settings.getLong("id1", 0);
		name = settings.getString("name1", "Enter your name");

		Log.v(TAG, "restoreUser: " + userName);
		Log.v(TAG, "restoreID: " + userId);
		Log.v(TAG, "restoreName: " + name);

		// Adapt view with existing information
		TextView text_username = (TextView) findViewById(R.id.username);
		text_username.setText(userName);
		TextView text_name = (TextView) findViewById(R.id.name);
		text_name.setText(name);
		
		AudioManager.playSound(this, "got");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	// Login button
	public void onClickLoginBtn(View v) {

		// Execute FetchUserTask first to find userID based on Name and
		// Username, then UserLoginTask is called onPostExecute to finish the
		// login procedure

		new FetchUserTask().execute();

	}

	public void onClickCreateUserBtn(View v) {

		UserRequestBean userRequestBean = new UserRequestBean();
		userRequestBean.setName(((EditText) findViewById(R.id.name)).getText()
				.toString());
		userRequestBean.setUsername(((EditText) findViewById(R.id.username))
				.getText().toString());
		new CreateUserTask().execute(userRequestBean);

	}

	@Override
	protected void onPause() {
		super.onPause();

		name = ((EditText) findViewById(R.id.name)).getText().toString();

		SharedPreferences settings = getSharedPreferences(SAVE, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("user1", userName);
		editor.putString("name1", name);
		editor.putLong("id1", userId);

		Log.v(TAG, "username to store: " + userName);
		Log.v(TAG, "id to store: " + userId);
		Log.v(TAG, "name to store: " + name);

		// Commit the edits
		editor.commit();
	}

	// prevent user from using the back-key to cheat the game
	@Override
	public void onBackPressed() {
		moveTaskToBack(true);
	}

	/** SpringMVC tasks **/

	// Creates new User
	private class CreateUserTask extends
			AsyncTask<UserRequestBean, Void, Boolean> {
		@Override
		protected Boolean doInBackground(UserRequestBean... userRequestBeans) {
			try {
				ResponseEntity<Boolean> response = RestService.post("/user",
						userRequestBeans[0], Boolean.class);
				if (HttpStatus.OK.equals(response.getStatusCode())) {

					return response.getBody();
				}
			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return false;
		}

		// show success/failure messages after AsyncCall
		@Override
		protected void onPostExecute(Boolean result) {
			if (result) {

				new AlertDialog.Builder(MainActivity.this)
						.setTitle("Success")
						.setMessage("User created")
						.setPositiveButton(android.R.string.ok,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {

									}
								}).show();

			} else {

				new AlertDialog.Builder(MainActivity.this)
						.setTitle("Sorry...")
						.setMessage("User creation failed")
						.setPositiveButton(android.R.string.ok,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {

									}
								}).show();

			}
		}

	}

	// Sets the variable userId and userName if an account exists (based on name
	// + username) and continues
	// with the login procedure
	public class FetchUserTask extends
			AsyncTask<Void, Void, UserResponseBean[]> {

		// Get UserResponseBeanArray from server
		@Override
		protected UserResponseBean[] doInBackground(Void... params) {
			try {

				ResponseEntity<UserResponseBean[]> response = RestService.get(
						"/user/", params, UserResponseBean[].class);

				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(UserResponseBean[] resultList) {
			ArrayList<String> values = new ArrayList<String>();

			// Extract information from each bean contained in the list-Object
			// that has been sent by the server

			if (resultList != null) {

				for (UserResponseBean bean : resultList) {

					// compare each bean with the entered user login details
					// until corresponding bean is found then call LobbyActivity
					// and pass the extracted userID
					if (bean.getName().equals(
							((EditText) findViewById(R.id.name)).getText()
									.toString())
							&& bean.getUsername().equals(
									((EditText) findViewById(R.id.username))
											.getText().toString())) {

						userId = bean.getId();
						userName = bean.getUsername();
						Log.e("Main", "" + userId);

						// After the userId was fetched, execute the Login
						// request
						new UserLoginTask().execute();
						return;

					}

				}

				// Show error message if login fails
				new AlertDialog.Builder(MainActivity.this)
						.setTitle("Sorry...")
						.setMessage("Login Failed")
						.setPositiveButton(android.R.string.ok,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {

									}
								}).show();

			}
		}
	}

	// Completes the login request based on the userId and gets a Token in
	// return. Token and userName are passed to LobbyActivity.
	private class UserLoginTask extends
			AsyncTask<Void, Void, UserLoginLogoutResponseBean> {

		// Get UserResponseBeanArray from server
		@Override
		protected UserLoginLogoutResponseBean doInBackground(Void... params) {
			try {

				ResponseEntity<UserLoginLogoutResponseBean> response = RestService
						.post("/user/" + userId + "/login", params,
								UserLoginLogoutResponseBean.class);

				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(UserLoginLogoutResponseBean result) {

			// Extract information from each bean contained in the list-Object
			// that has been sent by the server

			if (result != null) {
				Intent intent = new Intent(getBaseContext(),
						BrowserActivity.class);
				intent.putExtra("userToken", result.getUserToken());
				intent.putExtra("userId", userId);
				intent.putExtra("userName", userName);
				startActivity(intent);

			}

		}
	}

}
