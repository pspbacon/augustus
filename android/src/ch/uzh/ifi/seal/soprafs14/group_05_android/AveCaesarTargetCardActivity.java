package ch.uzh.ifi.seal.soprafs14.group_05_android;

import java.util.ArrayList;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.RestService;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.AveCaesarBean;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.GamePlayerRequestBean;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.TargetCardBean;

public class AveCaesarTargetCardActivity extends Activity {

	public static final String cTag = "AveCaesarTargetCardActivity";

	private long gameId;
	private String gameName;
	private String owner;
	private String userToken;
	private String userName;
	private long playerId;
	private long moveId;
	private String effectType;
	private Integer effectId;
	ArrayList<TargetCardBean> targetCardArrayList = new ArrayList<TargetCardBean>();

	private AveCaesarBean aveCaesarBean = new AveCaesarBean();

	private AveCardView selectedCardView = null;
	private ArrayList<AveCardView> cardViews = new ArrayList<AveCardView>();
	private int onResumeCount = 0;

	/** Android related functions **/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ave_caesar_target_card);
		gameId = getIntent().getExtras().getLong("gameId");
		owner = getIntent().getStringExtra("owner");
		gameName = getIntent().getStringExtra("gameName");
		userToken = getIntent().getStringExtra("userToken");
		userName = getIntent().getStringExtra("userName");
		playerId = getIntent().getExtras().getLong("playerId");
		moveId = getIntent().getExtras().getLong("moveId");

		new GetEffectTask().execute();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ave_caesar_target_card, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}

		if (id == R.id.Spiel_verlassen) {
			onBackPressed();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	public void onBackPressed() {
		new AlertDialog.Builder(this).setTitle("Spiel verlassen?")
				.setMessage("Moechten Sie das Spiel wirklich verlassen?")
				.setNegativeButton(R.string.nein, null)
				.setPositiveButton(R.string.ja, new OnClickListener() {

					public void onClick(DialogInterface arg0, int arg1) {
						GamePlayerRequestBean exitBean = new GamePlayerRequestBean();
						exitBean.setUserToken(userToken);
						new RemovePlayerTask().execute(exitBean);
						finish();
						AveCaesarTargetCardActivity.super.onBackPressed();
					}
				}).create().show();
	}

	public void onResume() {
		if (onResumeCount == 0) {
			Log.e("Main", "Getting TargetCards");
			new GetTargetCardsTask().execute();
		}
		++onResumeCount;
		super.onResume();
	}

	/** GUI **/

	// buttons
	public void onAveCaesarPress(View view) {
		AudioManager.playSound(this, "ave"); // THIS SOUND IS NOT IMPLEMENTED
		// YET --> APP NOT FEATURE
		// COMPLETE
		// should short-circuit if list is null, i.e. second condition not
		// checked (would throw error - safe programming inc.)
		if (targetCardArrayList == null || targetCardArrayList.isEmpty()) {
			return;
		}

		Button btn = (Button) findViewById(R.id.button_avecaesar);
		btn.setVisibility(View.GONE);

		LinearLayout cardLayout = (LinearLayout) findViewById(R.id.ac_cardlayout);
		cardViews.clear();
		cardLayout.removeAllViews();
		for (TargetCardBean tcb : targetCardArrayList) {
			cardViews.add(new AveCardView(this, new Card(tcb)));
			Log.d(cTag, "PlayingCardView added");
		}
		for (AveCardView acv : cardViews) {
			cardLayout.addView(acv);
		}
	}

	public void onSelectPress(View view) {
		if (selectedCardView == null) {
			new AlertDialog.Builder(this)
					.setTitle("Karte Auswaehlen")
					.setMessage("Du musst eine Karte auswaehlen")
					.setPositiveButton("Comprehendo",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									// nothing much happening, could be deleted
									// i guess
								}
							}).setIcon(android.R.drawable.ic_dialog_alert)
					.show();
			return;
		} else if (selectedCardView.hasTargetCardBean()) {
			// Log.d(cTag, "Selected Card " + selectedCardView.getName() +
			// " has TargetCardBean");
			// TargetCardBean selectedTCB =
			// selectedCardView.getTargetCardBean();
			// Log.d(cTag, "Selected Card " + selectedCardView.getName() +
			// " TCB has been stored");

			/** umweg über TCB nicht nötig wenn nur die Number benötigt wird. **/
			// aveCaesarBean.setNumberOfChoosenCard(selectedCardView.getNumber());
			// aveCaesarBean.setNumberOfChoosenCard(selectedTCB.getNumber());
			new ChooseCardTask().execute(selectedCardView.getNumber());

		} else {
			Log.e(cTag, "Selected Card " + selectedCardView.getName()
					+ " has no TargetCardBean!");
		}
	}

	public void toggleSelection(AveCardView callingView) {
		if (selectedCardView == null) {
			selectedCardView = callingView;
		} else if (selectedCardView == callingView) {
			selectedCardView = null;
		} else {
			selectedCardView.parentDeselect();
			selectedCardView = callingView;
		}
	}

	/** SpringMVC Tasks **/

	// Get the TargetCardStack from the server aka GET on
	// game/{gameId}/newcardstack
	private class GetTargetCardsTask extends
			AsyncTask<Void, Void, TargetCardBean[]> {

		@Override
		protected TargetCardBean[] doInBackground(Void... params) {
			try {

				ResponseEntity<TargetCardBean[]> response = RestService.get(
						"/game/" + gameId + "/newcardstack", params,
						TargetCardBean[].class);

				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		protected void onPostExecute(TargetCardBean[] response) {
			targetCardArrayList.clear();
			for (int i = 0; i < response.length; i++) {

				Log.e("Main", "Adding TargetCard: " + response[i].getName());
				targetCardArrayList.add(response[i]);
				Log.e("Main", "ArrayListSize: " + targetCardArrayList.size());

			}
		}

	}

	// Get a TargetCardBean from the server that includes the completed
	// AveCaesarCard and its effect aka GET on
	// game/{gameId}/player/{playerId}/legionEffect

	private class GetEffectTask extends AsyncTask<Void, Void, TargetCardBean> {

		@Override
		protected TargetCardBean doInBackground(Void... params) {
			try {

				ResponseEntity<TargetCardBean> response = RestService
						.get("/game/" + gameId + "/player/" + playerId
								+ "/legionEffect", params, TargetCardBean.class);

				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		protected void onPostExecute(TargetCardBean response) {
			if (response != null) {
				effectType = response.getEffectType();
				effectId = response.getEffectId();

				// Effect logic if effecType is immediate

				if (effectType != null && effectType.equals("immediately")) {

					Log.e("Main", "Immediate effect wit effectId " + effectId
							+ " Player: " + playerId);
					// LegionEffectActivity related effects (Mobilisierung von
					// Legionaeren)
					if (effectId <= 50 && effectId >= 30) {

						Log.e("Main", "Immediate effect " + effectId
								+ " Calling LegionEffectActivity for player: "
								+ playerId);
						Intent intent = new Intent(getBaseContext(),
								LegionEffectActivity.class);
						intent.putExtra("gameId", gameId);
						intent.putExtra("owner", owner);
						intent.putExtra("gameName", gameName);
						intent.putExtra("userToken", userToken);
						intent.putExtra("playerId", playerId);
						intent.putExtra("userName", userName);
						intent.putExtra("moveId", moveId);
						intent.putExtra("effectId", effectId);
						startActivity(intent);

					}

					// Zusaetzliche Karte sofort fertig
					if (effectId == 51) {

						Log.e("Main",
								"Immediate effect: Zusaetzliche Karte sofort fertig");
						Intent intent = new Intent(getBaseContext(),
								LegionEffectActivity.class);
						intent.putExtra("gameId", gameId);
						intent.putExtra("owner", owner);
						intent.putExtra("gameName", gameName);
						intent.putExtra("userToken", userToken);
						intent.putExtra("playerId", playerId);
						intent.putExtra("userName", userName);
						intent.putExtra("moveId", moveId);
						intent.putExtra("effectId", effectId);
						startActivity(intent);

					}

					// Zusaetzliche Zielkarte erhalten
					if (effectId.equals(52)) {

						// handle effect 52 directly here
						Log.e("Main",
								"Immediate effect: Zusaetzliche Zielkarte erhalten");

						Intent intent = new Intent(getBaseContext(),
								LegionEffectActivity.class);
						intent.putExtra("gameId", gameId);
						intent.putExtra("owner", owner);
						intent.putExtra("gameName", gameName);
						intent.putExtra("userToken", userToken);
						intent.putExtra("playerId", playerId);
						intent.putExtra("userName", userName);
						intent.putExtra("moveId", moveId);
						intent.putExtra("effectId", effectId);
						startActivity(intent);

					}

					// Zusaetzliche Legionen erhalten
					if (effectId == 60 || effectId == 61) {
						Log.e("Main",
								"Immediate effect: Zusaetzliche Legionen erhalten");

						// TODO: controller + GUI message
						new GetTargetCardsTask().execute();

					}

					// Gegenspieler verlieren mobilisierte Legionen
					if (effectId == 80 || effectId == 81) {
						Log.e("Main",
								"Immediate effect: Gegenspieler verlieren mobilisierte Legionen");
						// TODO: GUI message
						new GetTargetCardsTask().execute();

					}

					// Gegenspieler muss umgehend alle mobilisierten Legionen
					// von
					// einer beliebigen seiner Zielkarten entfernen
					if (effectId == 70) {
						Log.e("Main",
								"Immediate effect: Gegenspieler muss umgehend alle mobilisierten Legionen von einer beliebigen seiner Zielkarten entfernen");
						// TODO: GUI message
						new GetTargetCardsTask().execute();

					}

					// Gegenspieler verlieren eine eroberte Zielkarte

					if (effectId == 71) {
						Log.e("Main",
								"Immediate effect: Gegenspieler verlieren eine eroberte Zielkarte");
						// TODO: GUI message
						new GetTargetCardsTask().execute();

					}

				}

				// if effect is not immediate go to DrawableRewardActivity
				else {
					Log.e("Main", "No immediate effect!");
					new GetTargetCardsTask().execute();

				}
			}
		}

	}

	// Send the selected Target Card that will replace the ave caesar card to
	// the server aka. POST on game/{gameId}/player/{playerId}/avecaesar
	private class ChooseCardTask extends AsyncTask<Integer, Void, Void> {

		@Override
		protected Void doInBackground(Integer... choosenCardNumber) {

			try {
				RestService.post("/game/" + gameId + "/player/" + playerId
						+ "/avecaesar", choosenCardNumber[0], Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void params) {

			Log.e("Main", "AveCaesar: sent back TargetCard: "
					+ selectedCardView.getName());

			Log.e("Main", "Calling DrawableRewardActivity for player: "
					+ playerId);
			Intent intent = new Intent(getBaseContext(),
					DrawableRewardActivity.class);
			intent.putExtra("gameId", gameId);
			intent.putExtra("owner", owner);
			intent.putExtra("gameName", gameName);
			intent.putExtra("userToken", userToken);
			intent.putExtra("playerId", playerId);
			intent.putExtra("userName", userName);
			intent.putExtra("moveId", moveId);
			startActivity(intent);
			finish();

		}

	}

	// Remove user from game aka. POST on /game/{gameId}/player/remove
	private class RemovePlayerTask extends
			AsyncTask<GamePlayerRequestBean, Void, Void> {
		@Override
		protected Void doInBackground(
				GamePlayerRequestBean... gamePlayerRequestBeans) {

			try {
				Log.d("exit", "player left the game");
				RestService.post("/game/" + gameId + "/player/remove",
						gamePlayerRequestBeans[0], Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void param) {

		}

	}

}
