package ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans;

public class UserLoginLogoutRequestBean {

	private String token;
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
}
