package ch.uzh.ifi.seal.soprafs14.group_05_android;

import java.util.ArrayList;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
//import ch.uzh.ifi.seal.soprafs14.group_05_android.MainActivity.CreateUserTask;
//import ch.uzh.ifi.seal.soprafs14.group_05_android.MainActivity.LoginUserTask;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.RestService;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.GameRequestBean;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.GameResponseBean;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.GameStatus;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.UserLoginLogoutRequestBean;

public class BrowserActivity extends Activity {
	private String userToken;
	private String userName;
	private long userId;
	private ArrayList<GameResponseBean> pendingGames = new ArrayList<GameResponseBean>();

	/** Android related functions **/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_browser);

		// Parameters that were passed through the intent at the MainActivity
		// are assigned and
		// displayed in the BrowserActivity

		userToken = getIntent().getStringExtra("userToken");
		userName = getIntent().getStringExtra("userName");
		userId = getIntent().getLongExtra("userId", 99);

		((TextView) findViewById(R.id.txtStatus)).setText("Logged in as "
				+ userName);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.lobby, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	// Create Game button
	public void onClickCreateBtn(View v) {

		if (!((EditText) findViewById(R.id.txtGameName)).getText().toString()
				.isEmpty()) {
			GameRequestBean gameRequestBean = new GameRequestBean();
			gameRequestBean.setName(((EditText) findViewById(R.id.txtGameName))
					.getText().toString());
			gameRequestBean.setUserToken(userToken);
			Log.e("Main", userName + " " + userToken);
			new CreateGameTask().execute(gameRequestBean);
		} else {
			new AlertDialog.Builder(BrowserActivity.this)
					.setTitle("Error")
					.setMessage("Please enter a name for your game")
					.setPositiveButton(android.R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {

								}
							}).show();
		}

	}

	// Refresh button
	public void onClickRefreshBtn(View v) {

		new GetGamesListTask().execute();
	}

	// Log the user out when the back button is pressed, also destroy the
	// activity
	@Override
	public void onBackPressed() {
		UserLoginLogoutRequestBean bean = new UserLoginLogoutRequestBean();
		bean.setToken(userToken);
		new UserLogoutTask().execute(bean);
		finish();
		super.onBackPressed();
	}

	/** SpringMVC Tasks **/

	// Create game on the server aka. POST on /game
	private class CreateGameTask extends AsyncTask<GameRequestBean, Void, Void> {
		@Override
		protected Void doInBackground(GameRequestBean... gameRequestBeans) {

			try {
				RestService.post("/game", gameRequestBeans[0], Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void params) {

			// refresh the GameList
			new GetGamesListTask().execute();
		}

	}

	// Calling GET on /games returning a list of all open games
	public class GetGamesListTask extends
			AsyncTask<Void, Void, GameResponseBean[]> {

		// Get UserResponseBeanArray from server
		@Override
		protected GameResponseBean[] doInBackground(Void... params) {
			try {

				ResponseEntity<GameResponseBean[]> response = RestService.get(
						"/game/", params, GameResponseBean[].class);

				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(final GameResponseBean[] resultList) {
			ArrayList<String> values = new ArrayList<String>();

			// Display all open games

			// this needs to be done due to the AndroidLifeCycle, we have to
			// delete previously gathered information that is now outadted.
			pendingGames.clear();

			if (resultList != null) {
				for (GameResponseBean bean : resultList) {

					if (bean.getStatus() == GameStatus.PENDING) {
						values.add("GameName:[" + bean.getGame() + "] Owner:["
								+ bean.getOwner() + "] Players: ["
								+ bean.getNumberOfPlayers() + "]");
						pendingGames.add(bean);
					}
				}

				// Create ListView and display the values ArrayList -
				// Referenziert custom styles in list_white_style.xml für
				// weisse
				// Schrift

				ListAdapter adapter = new ArrayAdapter<String>(
						BrowserActivity.this, R.layout.list_white_style,
						R.id.list_white_textview, values);

				ListView gameList = (ListView) findViewById(R.id.preGameListField);
				gameList.setAdapter(adapter);

				// Create listener to detect clicking on List, and refer user to
				// the game he clicked on

				gameList.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long id) {

						// Call PreGame Activity and pass parameters

						Intent intent = new Intent(getBaseContext(),
								LobbyActivity.class);
						intent.putExtra("gameId",
								(long) pendingGames.get(position).getId());

						Log.e("Main", "SENDING GAME ID: "
								+ (long) pendingGames.get(position).getId()
								+ " Position: " + position);

						intent.putExtra("owner", pendingGames.get(position)
								.getOwner());

						intent.putExtra("gameName", pendingGames.get(position)
								.getGame());
						intent.putExtra("userToken", userToken);
						intent.putExtra("userId", userId);
						intent.putExtra("userName", userName);
						startActivity(intent);
						finish();
					}

				});

			}
		}
	}

	// logout the user aka. POST on /user/{userId}/logout
	private class UserLogoutTask extends
			AsyncTask<UserLoginLogoutRequestBean, Void, Void> {
		@Override
		protected Void doInBackground(
				UserLoginLogoutRequestBean... userLoginLogoutRequestBeans) {

			try {
				RestService.post("/user/" + userId + "/logout",
						userLoginLogoutRequestBeans[0], Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void params) {

		}

	}
}
