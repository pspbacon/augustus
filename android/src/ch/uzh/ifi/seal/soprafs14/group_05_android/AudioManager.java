package ch.uzh.ifi.seal.soprafs14.group_05_android;

import java.util.ArrayList;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

public class AudioManager {
	public static final String cTag = "AudioManager";
	
	private static int activeManagers = 0; // count to prevent too many managers active at the same time
	private static final int maxManagers = 2; // currently maximum allowed managers running in parallel
	private static ArrayList<MediaPlayer> mediaPlayers = new ArrayList<MediaPlayer>();

	public static void playSound(Context ctx, String sound) {
		if (activeManagers < maxManagers) {
			++activeManagers;
			MediaPlayer mp;
			if (sound.equalsIgnoreCase("accept"))
				mp = MediaPlayer.create(ctx, R.raw.coin);
			else if (sound.equalsIgnoreCase("cancel"))
				mp = MediaPlayer.create(ctx, R.raw.cancel);
			else if (sound.equalsIgnoreCase("select"))
				mp = MediaPlayer.create(ctx,  R.raw.select);
			else if (sound.equalsIgnoreCase("got")) {
				mp = MediaPlayer.create(ctx, R.raw.got8b);
				mp.setLooping(true);
			}
			else if (sound.equalsIgnoreCase("ave")) {
				mp = MediaPlayer.create(ctx,  R.raw.ave);
			}
			else
				mp = MediaPlayer.create(ctx,  R.raw.ponch);
	
			mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					--activeManagers;
					mp.stop();
					mediaPlayers.remove(mp);
					if (mp != null) {
						mp.release();
						mp = null;
					}
					Log.d(cTag, "Mediaplayer Released, List size: " + mediaPlayers.size());
					
				}
			});
			mp.start();
			mediaPlayers.add(mp);
		}
	}
	
	public static void stopAllSounds() {
		for (MediaPlayer mp : mediaPlayers) {
			mp.stop();
			if (mp != null) {
				mp.release();
				mp = null;
			}
			activeManagers = 0;
			mediaPlayers.clear();
		}
	}
}
