package ch.uzh.ifi.seal.soprafs14.group_05_android;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import ch.uzh.ifi.seal.soprafs14.group_05_android.Card.Banner;
import ch.uzh.ifi.seal.soprafs14.group_05_android.Card.Marker;
import ch.uzh.ifi.seal.soprafs14.group_05_android.Card.Province;
import ch.uzh.ifi.seal.soprafs14.group_05_android.Card.Resource;
import ch.uzh.ifi.seal.soprafs14.group_05_android.Card.Type;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.TargetCardBean;

public class PlayingCardView extends View {
	
	public static final String cTag = "PlayingCardView";
	
	private Paint nameTextPaint, pointsTextPaint;
	private Paint selectStrokePaint;
	private Paint assignablePaint;
	private int nameTextColor = Color.DKGRAY;
	private int pointsTextColor = Color.RED;
	
	private String currentMarker;
	
	private boolean selected = false;
	
	// Information about Card to be drawn
	private Card card;
	private int points, number;
	private String name;
	private Boolean pointType;
	
	public static final int def_small = 192; // size of card when not selected - pretty much max size possible with 5 cards on screen
	public static final int def_large = 270; // size of card when selected
	
	// Size of whole Card, start out with unselected size
	private int size = def_small;
	
	// sizes of individual elements - determined by testing and fine tuning (fuck me)
	private int r_width = 105;
	private int r_height = 46;
	private int r_offset = 10;
	private int r_spacing = 30;
	private int m_size = 30;  // only one because square
	private int b_width = 50;
	private int b_height = 100;
	
	private float points_x = size - 39;
	private float points_y = size - 20;
	private float name_x = 10;
	private float name_y = size - 10;
	
	private int nameTextSize = 12;
	private final int defaultTextSize = 12;
	private int pointsTextSize = 26;
	private final int defaultPointsSize = 26;
	
	// Rectangles to draw the Bitmaps to
	private Rect drawRect;
	private ArrayList<Rect> drawMarkerRects;
	private ArrayList<Rect> drawResourceRects;
	private Rect drawBannerRect;
	private ArrayList<Integer> indicesOfNewlyAssignedFields; // 0 - (numberOfFields-1), stores all fields that are newly occupied
	private ArrayList<Integer> indicesOfNewlyRemovedFields = new ArrayList<Integer>(); // , stores all fields that are newly NOT occupied
	private ArrayList<Integer> indicesOfOccupiedFields; // 0 - (numberOfFields - 1), stores all fields that are occupied
	private ArrayList<Integer> indicesOfAssignable;
	
	// Rectangles on the source .png to draw the Bitmap from, need not be altered
	private final Rect srcTypeRect = new Rect(0, 0, 400, 400); 
	private final Rect srcMarkerRect = new Rect(0, 0, 200, 200);
	private final Rect srcResourceRect = new Rect(0, 0, 210, 92);
	private final Rect srcBannerRect = new Rect(0, 0, 138, 273);
	private final Rect srcLegionRect = new Rect(0, 0, 53, 94);
	
	// Storage for Bitmaps
	private HashMap<String, Bitmap> bitmaps;
	private ArrayList<Bitmap> markers;
	private ArrayList<Bitmap> resources;

	
	// Constructor to be used, passing a Card
	public PlayingCardView(Context context, Card card) {
		super(context);
		this.card = card;
		init();
	}
	
	// Constructor taking a TargetCardBean, works also
	public PlayingCardView(Context context, TargetCardBean tcb) {
		super(context);
		card = new Card(tcb);
		init();
	}
	
	public String getName() {
		return this.card.getName();
	}
	
	public int getNumber() {
		return this.card.getNumber();
	}
	
	public TargetCardBean getTargetCardBean() {
		return this.card.getTargetCardBean();
	}
	
	public boolean hasTargetCardBean() {
		return this.card.hasTargetCardBean();
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

	    int desiredWidth = size;


	    int widthMode = MeasureSpec.getMode(widthMeasureSpec);
	    int widthSize = MeasureSpec.getSize(widthMeasureSpec);

	    //Measure Width
	    if (widthMode == MeasureSpec.EXACTLY) {
	        //Must be this size
	        size = widthSize;
	    } else if (widthMode == MeasureSpec.AT_MOST) {
	        //Can't be bigger than...
	        size = Math.min(desiredWidth, widthSize);
	    } else {
	        //Be whatever you want
	        size = desiredWidth;
	    }

	    //MUST CALL THIS
	    setMeasuredDimension(size, size);
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldW, int oldH) {
		
		size = w;
		Log.d("Card", "onSizeChanged called with (w: " + w + ", h: " + h + ", oldW: " + oldW + "oldH: " + oldH + ")");
		// only do the following if previous size wasn't 0 i.e. just created
		if ((oldH != 0) && (oldW != 0)) {
			double ratio = (double)h / (double)oldH;  // the horizontal change in scale
			Log.d("Card", "ratio is" + ratio);
			
			// TODO: scaling verbessern, wiederholtes scalen laesst alles nach und nach schrumpfen
			
			r_width *= ratio;
			r_height *= ratio;
			r_offset *= ratio;
			r_spacing *= ratio;
			
			b_width *= ratio;
			b_height *= ratio;
			
			m_size *= ratio;
			
			points_x *= ratio;
			points_y *= ratio;
			name_x *= ratio;
			name_y *= ratio;
			
			// text scaling manually because problems
			if (oldW > w) {
				nameTextSize = defaultTextSize;
				pointsTextSize = defaultPointsSize;
			} else  {
				nameTextSize *= ratio;
				pointsTextSize *= ratio;
			}
			nameTextPaint.setTextSize(nameTextSize);
			pointsTextPaint.setTextSize(pointsTextSize);
			
			// recalculate all rectangles

			drawRect.set(0, 0, size, size);

			drawBannerRect.set((size-b_width), 0, size, b_height);
			
			// adds a rectangle for every marker, one below another
			int mk_count = card.getMarkers().size();
			drawMarkerRects.clear();
			for (int i = 0; i<mk_count; ++i) {
				drawMarkerRects.add(new Rect(0, m_size*i, m_size, m_size*(i+1)));
			}
			// add new rectangles for every resource
			int res_count = card.getResources().size();
			drawResourceRects.clear();
			for (int i = 0; i<res_count; ++i) {
				drawResourceRects.add(new Rect((size-r_width)/2, r_offset*(i+1), (size+r_width)/2, r_offset*(i+1)+r_height));
			}
		}
		invalidate();
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		Bitmap bmp = null;
		bmp = bitmaps.get("type");
		canvas.drawBitmap(bmp, srcTypeRect, drawRect, null);

		bmp = bitmaps.get("banner");
		if (bmp != null) canvas.drawBitmap(bmp,  srcBannerRect, drawBannerRect, null);
		
		for (int i = 0; i<resources.size(); ++i) {
			canvas.drawBitmap(resources.get(i), srcResourceRect, drawResourceRects.get(i), null);
		}
		
		for (int i = 0; i<markers.size(); ++i) {
			canvas.drawBitmap(markers.get(i), srcMarkerRect, drawMarkerRects.get(i), null);
		}
		// replaced by code 238ff
//		boolean[] occ = card.getOccupied();
//		for (int i = 0; i<occ.length; ++i) {
//			if (occ[i])
//				canvas.drawBitmap(bitmaps.get("legion"), srcLegionRect, drawMarkerRects.get(i), null);
//		}
//		boolean[] ofa = card.getAssignable();
//		for (int i = 0; i<ofa.length; ++i) {
//
//			if (ofa[i]) {
//				Rect r = drawMarkerRects.get(i);
//				canvas.drawCircle(r.exactCenterX(), r.exactCenterY(), r.width()/2, selectStrokePaint);
//			}
//				
//		}
		
		for (Integer i : indicesOfAssignable) {
			Rect r = drawMarkerRects.get(i);
			canvas.drawCircle(r.exactCenterX(), r.exactCenterY(), r.width()/2, selectStrokePaint);
		}
		
		for (Integer i : indicesOfOccupiedFields) {
			canvas.drawBitmap(bitmaps.get("legion"), srcLegionRect, drawMarkerRects.get(i.intValue()), null);
		}
		
		// draws a legion on every field that has been newly assigned
		for (Integer i : indicesOfNewlyAssignedFields) {
			canvas.drawBitmap(bitmaps.get("legion"), srcLegionRect, drawMarkerRects.get(i.intValue()), null);
		}
				
		if(!pointType) {
			canvas.drawText(String.valueOf(points), points_x, points_y, pointsTextPaint);
		} else {
			canvas.drawText("?", points_x, points_y, pointsTextPaint);
		}
		
		canvas.drawText(name, name_x, name_y, nameTextPaint);
		
		
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent e) {
		
		int action = e.getAction();

		if (selected){
			switch (action) {
			case MotionEvent.ACTION_DOWN:
				if (!((PlayingFieldActivity)getContext()).getBarracksEmpty()) {
					AudioManager.playSound(getContext(), "select");
					float x = e.getX();
					float y = e.getY();
					Log.d(cTag, "Card selected, touch registered at :" + x + " " + y);
					int i = getNewlyAssignedField(x, y);
					if (i<0) { // no field has been clicked - could be refined but gotta go fast
						toggleSelected();
					} else {
						Log.d(cTag, "Great Success: " + getNewlyAssignedField(x,y));
						assignMarker(i);
					}
				} else {
					AudioManager.playSound(getContext(),  "select");
					float x = e.getX();
					float y = e.getY();
					int i = getOccupiedField(x, y);
					if (i<0) {
						toggleSelected();
					} else {
						Log.d(cTag, "Barracks empty, " + i + " selected for moving");
						indicesOfOccupiedFields.remove(Integer.valueOf(i));
						removeMarker(i);
					}
				}
				break;
			}
		} else {
			switch (action) {
			case MotionEvent.ACTION_DOWN:
				AudioManager.playSound(getContext(), "select");
				toggleSelected();
				break;
			}
		}
		return true;
	}
	
	public void toggleSelected() {
		PlayingFieldActivity parent = (PlayingFieldActivity) this.getContext();
		if (selected) {
			size = def_small; 
			selected = false;
			parent.toggleSelection(this);

		} else {
			size = def_large;
			selected = true;
			parent.toggleSelection(this);

		}
		requestLayout();
		invalidate();
	}
	
	// parent calls this if another card is selected while this one was selected
	public void parentDeselect() {
		selected = false;
		size = def_small;
		requestLayout();
		invalidate();
	}
	
	public void parentRemoveSetLegion() {
		indicesOfNewlyAssignedFields.remove(0); // remove item that has been inserted least recently
		invalidate();
	}
	
	private void init() {
		// reads current Marker and if barracks are empty from parent
		getCurrentMarkerandBarracks();
		
		// not yet sure if will be utilized
		nameTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		nameTextPaint.setColor(nameTextColor);
		nameTextPaint.setTextSize(nameTextSize);
		
		pointsTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		pointsTextPaint.setColor(pointsTextColor);
		pointsTextPaint.setTextSize(pointsTextSize);
		
		selectStrokePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		selectStrokePaint.setColor(Color.GREEN);
		selectStrokePaint.setStyle(Paint.Style.STROKE);
		selectStrokePaint.setStrokeWidth(3.0f);
		
		assignablePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		assignablePaint.setColor(Color.WHITE);
		assignablePaint.setAlpha(100);
		
		points = card.getPoints();
		number = card.getNumber();
		name = String.valueOf(number) + " " + card.getName();
		
		// setting the rectangles to which bitmaps should be drawn - might be used to register touch actions?
		drawRect = new Rect(0, 0, size, size);
		//drawResourceRect = new Rect((size-r_width)/2, r_offset, (size+r_width)/2, r_offset+r_height);
		
		int res_count = card.getResources().size();
		drawResourceRects = new ArrayList<Rect>(res_count);
		for (int i = 0; i<res_count; ++i) {
			drawResourceRects.add(new Rect((size-r_width)/2, r_offset+r_spacing*i, (size+r_width)/2, r_offset+r_spacing*i+r_height));
		}
		
		drawBannerRect = new Rect((size-b_width), 0, size, b_height);
		
		// adds a rectangle for every marker, one below another
		int mk_count = card.getMarkers().size();
		drawMarkerRects = new ArrayList<Rect>(mk_count);
		for (int i = 0; i<mk_count; ++i) {
			drawMarkerRects.add(new Rect(0, m_size*i, m_size, m_size*(i+1)));
		}
		
		indicesOfNewlyAssignedFields = new ArrayList<Integer>();
		// add indices of assignable fields to list
		indicesOfAssignable = new ArrayList<Integer>();
		boolean[] assignable = card.getAssignable();
		for (int i = 0; i<assignable.length; ++i) {
			if (assignable[i]) {
				indicesOfAssignable.add(i);
			}
		}
		
		// add indices of occupied fields to list
		indicesOfOccupiedFields = new ArrayList<Integer>();
		boolean[] occupied = card.getOccupied();
		for (int i = 0; i<occupied.length; ++i) {
			if (occupied[i]) {
				indicesOfOccupiedFields.add(i);
			}
		}
		
		bitmaps = new HashMap<String, Bitmap>();
		Bitmap bmp = null;
		
		Type t = card.getType();
		Province p = card.getProvince();
		if (t == Type.REGION) {
			if (p == Province.EUROPE)
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_zielkarte_europa);
			else if (p == Province.AFRICA)
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_zielkarte_afrika);
			else
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_zielkarte_kleinasien);
		}
		else
			bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_zielkarte_senator);
		bitmaps.put("type", bmp);
		
		ArrayList<Resource> r = card.getResources();
		resources = new ArrayList<Bitmap>(r.size());
		for (Resource res : r) {
			if (res == Resource.GOLD){
				bmp = BitmapFactory.decodeResource(getResources(),  R.drawable.augustus_gold);
			} else {
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_getreide);
			}
			resources.add(bmp);
		}
		
		Banner b = card.getBanner();
		if (b == Banner.LIGHT)
			bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_banner_hell);
		else if (b == Banner.DARK)
			bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_banner_dunkel);
		else if (b == Banner.RED)
			bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_banner_rot);
		else
			bmp = null;
		bitmaps.put("banner", bmp);
		
		bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_legion_rot);
		bitmaps.put("legion", bmp);
		
		ArrayList<Marker> mks = card.getMarkers();
		markers = new ArrayList<Bitmap>(mks.size());  // preallocate size, not necessary, but yeah
		for (Marker m : mks) {
			if (m == Marker.SWORD)
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_mobilisierung_schwerter);
			else if (m == Marker.SHIELD)
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_mobilisierung_schild);
			else if (m == Marker.CART)
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_mobilisierung_wagen);
			else if (m == Marker.DAGGER)
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_mobilisierung_dolch);
			else if (m == Marker.HURRICAN)
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_mobilisierung_hurrican);
			else
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_mobilisierung_katapult);
			markers.add(bmp);
		}
		
		pointType = card.getPointType();
		
	}

	private void getCurrentMarkerandBarracks() {
		PlayingFieldActivity parent = (PlayingFieldActivity) this.getContext();
		currentMarker = parent.getMarker();
	}
	
	// returns index of field when it is assignable and has been clicked, returns -1 else
	private int getNewlyAssignedField(float x, float y) {
		for (Integer i : indicesOfAssignable) {
			if (drawMarkerRects.get(i).contains((int)x, (int)y))
				return i;
		}
		return -1;
	}
	
	// returns index of field when it is occupied and has been clicked, -1 else
	private int getOccupiedField(float x, float y) {
		for (Integer i : indicesOfOccupiedFields) {
			if (drawMarkerRects.get(i).contains((int)x, (int)y))
				return i;
		}
		return -1;
	}
	
	private void assignMarker(int indexOfField) {
		PlayingFieldActivity parent = (PlayingFieldActivity) this.getContext();
		if (indicesOfNewlyAssignedFields.contains(indexOfField)) {
			indicesOfNewlyAssignedFields.remove(Integer.valueOf(indexOfField)); // pitfall, remove(int i) removes am index i, nicht das objekt i
			parent.removeLegion(this);
		} else if (!indicesOfOccupiedFields.contains(Integer.valueOf(indexOfField))){
			indicesOfNewlyAssignedFields.add(indexOfField);
			parent.setLegion(this);
		}
		invalidate();
	}
	
	// called when barracks are empty and occupied field should be removed
	private void removeMarker(int indexOfField) {
		PlayingFieldActivity parent = (PlayingFieldActivity) this.getContext();
		indicesOfOccupiedFields.remove(Integer.valueOf(indexOfField));
		indicesOfNewlyRemovedFields.add(Integer.valueOf(indexOfField));
		parent.removeWhenBarracksEmpty(this);
		checkForAssignableFields();
		invalidate();
	}
	
	// call to update assignableFields, in case server didnt send any
	public void checkForAssignableFields() {
		ArrayList<Marker> mks = card.getMarkers();
		indicesOfAssignable.clear();
		for (int i = 0; i < mks.size(); ++i) {
			if(currentMarker.equalsIgnoreCase(getStringOfMarker(mks.get(i))))
				indicesOfAssignable.add(i);
		}
		invalidate();
	}
	
	// careful, doesnt work for Joker token
	private String getStringOfMarker(Marker mk) {
		if (mk == Marker.SWORD)
			return "schwert";
		else if (mk == Marker.SHIELD)
			return "schild";
		else if (mk == Marker.DAGGER)
			return "dolch";
		else if (mk == Marker.CART)
			return "streitwagen";
		else if (mk == Marker.CATAPULT)
			return "katapult";
		else
			return "standarte";
	}
	
	// gibt ein TCB zurück das geupdated wurde mit den neuen legionen
	public TargetCardBean getFinalizedTargetCardBean() {
		TargetCardBean tcb = new TargetCardBean();
		if (card.hasTargetCardBean()) { // safety check
			tcb = card.getTargetCardBean();
			// read statusFields of targetCardBean, update newly assigned ones and set it
			List<Boolean> statusFields = tcb.getStatusFields();
			for (Integer i : indicesOfNewlyAssignedFields) {
				statusFields.set(i, true);
			}
			for (Integer i : indicesOfNewlyRemovedFields) {
				statusFields.set(i, false);
			}
			tcb.setStatusFields(statusFields);
		}
		return tcb;
	}
}

