package ch.uzh.ifi.seal.soprafs14.group_05_android;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import ch.uzh.ifi.seal.soprafs14.group_05_android.Card.Banner;
import ch.uzh.ifi.seal.soprafs14.group_05_android.Card.Marker;
import ch.uzh.ifi.seal.soprafs14.group_05_android.Card.Province;
import ch.uzh.ifi.seal.soprafs14.group_05_android.Card.Resource;
import ch.uzh.ifi.seal.soprafs14.group_05_android.Card.Type;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.TargetCardBean;

public class LegionEffectCardView extends View {
	
	// Information about Effect pertaining to the Activity
	private int effectId = 0;
	LegionEffectActivity parent = null;
	private ArrayList<Integer> indexOfNewlyAssigned = new ArrayList<Integer>();
	private ArrayList<Integer> indexOfNewlyRemoved = new ArrayList<Integer>();
	private boolean[] fieldOccupied;
	private boolean[] fieldAssignable;
	
	private Paint nameTextPaint, pointsTextPaint;
	private Paint selectStrokePaint;
	private int nameTextColor = Color.DKGRAY;
	private int pointsTextColor = Color.RED;
	
	private boolean selected = false;
	
	// Information about Card to be drawn
	private Card card;
	private int points, number;
	private Boolean pointType;
	private String name;
	
	public static final int def_small = 160; // size of card when not selected - pretty much max size possible with 5 cards on screen
	public static final int def_large = 300; // size of card when selected
	
	// Size of whole Card, start out with unselected size
	private int size = def_small;
	
	// sizes of individual elements - determined by testing and fine tuning (fuck me)
	private int r_width = 87; //105;
	private int r_height = 38; //46;
	private int r_offset = 8; //10;
	private int r_spacing = 30;
	private int m_size = 25; //30;  // only one because square
	private int b_width = 42; //50;
	private int b_height = 83; //100;
	
	private float points_x = size - 32; //39;
	private float points_y = size - 16; // 20;
	private float name_x = 4; //10;
	private float name_y = size - 8; // 10;
	
	private int nameTextSize = 11; //12;
	private int pointsTextSize = 22; //26;
	
	// Rectangles to draw the Bitmaps to
	private Rect drawRect;
	private RectF outline;
	private ArrayList<Rect> drawMarkerRects;
	private Rect drawBannerRect;
	//private Rect drawResourceRect;
	private ArrayList<Rect> drawResourceRects;
	
	// Rectangles on the source .png to draw the Bitmap from, need not be altered
	private final Rect srcTypeRect = new Rect(0, 0, 400, 400); 
	private final Rect srcMarkerRect = new Rect(0, 0, 200, 200);
	private final Rect srcResourceRect = new Rect(0, 0, 210, 92);
	private final Rect srcBannerRect = new Rect(0, 0, 138, 273);
	
	// Storage for Bitmaps
	private HashMap<String, Bitmap> bitmaps;
	private ArrayList<Bitmap> markers;
	private ArrayList<Bitmap> resources;
	
	// Constructor to be used, passing a Card
	public LegionEffectCardView(Context context, Card card) {
		super(context);
		this.card = card;
		parent = (LegionEffectActivity) context;
		effectId = parent.getEffectId();
		init();
	}
	
	// Constructor taking a TargetCardBean
	public LegionEffectCardView(Context context, TargetCardBean tcb) {
		super(context);
		card = new Card(tcb);
		parent = (LegionEffectActivity) context;
		effectId = parent.getEffectId();
		init();
	}
	
	public String getName() {
		return this.card.getName();
	}
	
	public int getNumber() {
		return this.card.getNumber();
	}
	
	public TargetCardBean getTargetCardBean() {
		return this.card.getTargetCardBean();
	}
	
	public boolean hasTargetCardBean() {
		return this.card.hasTargetCardBean();
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

	    int desiredWidth = size;

	    int widthMode = MeasureSpec.getMode(widthMeasureSpec);
	    int widthSize = MeasureSpec.getSize(widthMeasureSpec);

	    //Measure Width
	    if (widthMode == MeasureSpec.EXACTLY) {
	        //Must be this size
	        size = widthSize;
	    } else if (widthMode == MeasureSpec.AT_MOST) {
	        //Can't be bigger than...
	        size = Math.min(desiredWidth, widthSize);
	    } else {
	        //Be whatever you want
	        size = desiredWidth;
	    }

	    //MUST CALL THIS
	    setMeasuredDimension(size, size);
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldW, int oldH) {
		
		size = w;
		
		// only do the following if previous size wasn't 0 i.e. just created
		if ((oldH != 0) && (oldW != 0)) {
			double ratio = (double)h / (double)oldH;  // the horizontal change in scale
			//double w_ratio = (double)w / (double)oldW;  // the vertical change in scale
			Log.d("Card", "ratio is" + ratio);
			
			r_width *= ratio;
			r_height *= ratio;
			r_offset *= ratio;
			
			b_width *= ratio;
			b_height *= ratio;
			
			m_size *= ratio;
			
			points_x *= ratio;
			points_y *= ratio;
			name_x *= ratio;
			name_y *= ratio;
			
			nameTextSize *= ratio;
			nameTextPaint.setTextSize(nameTextSize);
			pointsTextSize *= ratio;
			pointsTextPaint.setTextSize(pointsTextSize);
			
			// recalculate all rectangles
			drawRect.set(0, 0, size, size);;
			drawBannerRect.set((size-b_width), 0, size, b_height);

			int mk_count = card.getMarkers().size();
			drawMarkerRects.clear();
			for (int i = 0; i<mk_count; ++i) {
				drawMarkerRects.add(new Rect(0, m_size*i, m_size, m_size*(i+1)));
			}
			int res_count = card.getResources().size();
			drawResourceRects.clear();
			for (int i = 0; i<res_count; ++i) {
				drawResourceRects.add(new Rect((size-r_width)/2, r_offset*(i+1), (size+r_width)/2, r_offset*(i+1)+r_height));
			}
		}
		invalidate();
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		Bitmap bmp = null;
		bmp = bitmaps.get("type");
		canvas.drawBitmap(bmp, srcTypeRect, drawRect, null);
		for (int i = 0; i<resources.size(); ++i) {
			canvas.drawBitmap(resources.get(i), srcResourceRect, drawResourceRects.get(i), null);
		}
		bmp = bitmaps.get("banner");
		if (bmp != null) canvas.drawBitmap(bmp,  srcBannerRect, drawBannerRect, null);
		
		for (int i = 0; i<markers.size(); ++i) {
			canvas.drawBitmap(markers.get(i), srcMarkerRect, drawMarkerRects.get(i), null);
			if (i < fieldOccupied.length) { // prevent oufOfBounds
				if (fieldOccupied[i] && !indexOfNewlyRemoved.contains(Integer.valueOf(i)))
					canvas.drawBitmap(bitmaps.get("legion"), srcMarkerRect, drawMarkerRects.get(i), null);
			}
			if (indexOfNewlyAssigned.contains(Integer.valueOf(i))){
				canvas.drawBitmap(bitmaps.get("legion"), srcMarkerRect, drawMarkerRects.get(i), null);
			}
		}
		
		if(!pointType) {
			canvas.drawText(String.valueOf(points), points_x, points_y, pointsTextPaint);
		} else {
			canvas.drawText("?", points_x, points_y, pointsTextPaint);
		}
		canvas.drawText(name, name_x, name_y, nameTextPaint);
		
		if (selected)
			canvas.drawRoundRect(outline, 3.0f, 3.0f, selectStrokePaint);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent e) {
		
		int action = e.getAction();
		
		switch (action) {
		case MotionEvent.ACTION_DOWN:
			if (effectId == 51 || effectId == 52) {
				selectCardTouch();
			} else if (effectId == 50){
				redistributeTouch(e);
			} else {
				mobilizeTouch(e);
			}
			break;
		}
		return true;
	}
	
	// parent calls this if 3 cards are selected and another one is selected
	public void parentDeselect() {
		selected = false;
		invalidate();
	}
	
	
	private void init() {
		// not yet sure if will be utilized
		nameTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		nameTextPaint.setColor(nameTextColor);
		nameTextPaint.setTextSize(nameTextSize);
		
		pointsTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		pointsTextPaint.setColor(pointsTextColor);
		pointsTextPaint.setTextSize(pointsTextSize);
		
		selectStrokePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		selectStrokePaint.setColor(Color.GREEN);
		selectStrokePaint.setStyle(Paint.Style.STROKE);
		selectStrokePaint.setStrokeWidth(3.0f);
		
		outline = new RectF(1, 1, size-2, size-2);
		
		
		points = card.getPoints();
		pointType = card.getPointType();
		
		number = card.getNumber();
		name = String.valueOf(number) + " " + card.getName();
		
		fieldOccupied = card.getOccupied();
		fieldAssignable = card.getAssignable();

		drawRect = new Rect(0, 0, size, size);
		//drawResourceRect = new Rect((size-r_width)/2, r_offset, (size+r_width)/2, r_offset+r_height);
		drawBannerRect = new Rect((size-b_width), 0, size, b_height);
		
		// add rectangles to draw resources to
		int res_count = card.getResources().size();
		drawResourceRects = new ArrayList<Rect>(res_count);
		for (int i = 0; i<res_count; ++i) {
			drawResourceRects.add(new Rect((size-r_width)/2, r_offset+r_spacing*i, (size+r_width)/2, r_offset+r_spacing*i+r_height));
		}
		
		// adds a rectangle for every marker, one below another
		int mk_count = card.getMarkers().size();
		drawMarkerRects = new ArrayList<Rect>(mk_count);
		for (int i = 0; i<mk_count; ++i) {
			drawMarkerRects.add(new Rect(0, m_size*i, m_size, m_size*(i+1)));
		}
		
		bitmaps = new HashMap<String, Bitmap>();
		Bitmap bmp = null;
		
		Type t = card.getType();
		Province p = card.getProvince();
		if (t == Type.REGION){
			if (p == Province.EUROPE)
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_zielkarte_europa);
			else if (p == Province.AFRICA)
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_zielkarte_afrika);
			else
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_zielkarte_kleinasien);
		}	
		else
			bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_zielkarte_senator);
		
		bitmaps.put("type", bmp);
		
		bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_legion_rot);
		bitmaps.put("legion", bmp);
		
		ArrayList<Resource> r = card.getResources();
		resources = new ArrayList<Bitmap>(r.size());
		for (Resource res : r) {
			if (res == Resource.GOLD){
				bmp = BitmapFactory.decodeResource(getResources(),  R.drawable.augustus_gold);
			} else {
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_getreide);
			}
			resources.add(bmp);
		}
		
		Banner b = card.getBanner();
		if (b == Banner.LIGHT)
			bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_banner_hell);
		else if (b == Banner.DARK)
			bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_banner_dunkel);
		else if (b == Banner.RED)
			bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_banner_rot);
		else
			bmp = null;
		bitmaps.put("banner", bmp);
		
		ArrayList<Marker> mks = card.getMarkers();
		markers = new ArrayList<Bitmap>(mks.size());  // preallocate size, not necessary, but yeah
		for (Marker m : mks) {
			if (m == Marker.SWORD)
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_mobilisierung_schwerter);
			else if (m == Marker.SHIELD)
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_mobilisierung_schild);
			else if (m == Marker.CART)
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_mobilisierung_wagen);
			else if (m == Marker.DAGGER)
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_mobilisierung_dolch);
			else if (m == Marker.HURRICAN)
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_mobilisierung_hurrican);
			else
				bmp = BitmapFactory.decodeResource(getResources(), R.drawable.augustus_mobilisierung_katapult);
			markers.add(bmp);
		}
		
	}
	
	private void selectCardTouch() {
		selected = !selected;
		parent.toggleSelectedCard(this);
		invalidate();
	}

	private void mobilizeTouch(MotionEvent e) {
		int index = getClickedField(e.getX(), e.getY());
		if (index < 0) 
			return;
		int available = parent.getAvailableLegionaires();
		if (available > 0) {
			if (fieldAssignable.length > index && fieldAssignable[index]){ // first check to prevent indexOutOfBounds
				if (!indexOfNewlyAssigned.contains(Integer.valueOf(index))){
					indexOfNewlyAssigned.add(Integer.valueOf(index));
					parent.placedLegion();
					invalidate();
				}
			}
		}
	}
	
	private void redistributeTouch(MotionEvent e) {
		int index = getClickedField(e.getX(), e.getY());
		if (index < 0) return;
		// removing phase
		if (parent.getLegionsRemoved()<parent.getMovableLegions()){
			if (index < fieldOccupied.length){ // prevent indexOutOfBounds
				if (fieldOccupied[index] && !indexOfNewlyRemoved.contains(Integer.valueOf(index))) {
					indexOfNewlyRemoved.add(Integer.valueOf(index));
					parent.removedLegion();
					invalidate();
				}
			}
		}
		// placing phase
		else if (parent.getLegionsPlaced()<parent.getMovableLegions()){
			if (index < fieldAssignable.length) {
				if (fieldAssignable[index] && !indexOfNewlyAssigned.contains(Integer.valueOf(index))) {
					indexOfNewlyAssigned.add(Integer.valueOf(index));
					parent.placedLegion();
					invalidate();
				}
			}
		}
	}

	private int getClickedField(float xf, float yf) {
		int x = Math.round(xf);
		int y = Math.round(yf);
		for (int i = 0; i<drawMarkerRects.size(); ++i) {
			if (drawMarkerRects.get(i).contains(x, y))
				return i;
		}
		return -1;
	}
	
	// call when activity is ready
	public TargetCardBean getFinalizedTargetCardBean() {
		if (!card.hasTargetCardBean()) return null;
		TargetCardBean fin = card.getTargetCardBean();
		List<Boolean> occupiedField = fin.getStatusFields();
		for (Integer i : indexOfNewlyRemoved) {
			occupiedField.set(Integer.valueOf(i),  Boolean.valueOf(false));
		}
		for (Integer i : indexOfNewlyAssigned) {
			occupiedField.set(Integer.valueOf(i), Boolean.valueOf(true));
		}
		fin.setStatusFields(occupiedField);
		// TODO: set final setup of legions
		return fin;
	}
}
