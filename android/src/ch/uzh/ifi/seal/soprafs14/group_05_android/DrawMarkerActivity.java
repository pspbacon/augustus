package ch.uzh.ifi.seal.soprafs14.group_05_android;

import java.util.Timer;
import java.util.TimerTask;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.RestService;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.GamePlayerRequestBean;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.MarkerBean;

public class DrawMarkerActivity extends Activity {

	private ImageView markerView;
	private String marker = null;
	private long gameId;
	private String gameName;
	private String owner;
	private String userToken;
	private String userName;
	private long playerId;
	private long moveId;
	private Timer waitTimer = new Timer();

	/** Android related functions **/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_draw_marker);

		gameId = getIntent().getExtras().getLong("gameId");
		owner = getIntent().getStringExtra("owner");
		gameName = getIntent().getStringExtra("gameName");
		userToken = getIntent().getStringExtra("userToken");
		userName = getIntent().getStringExtra("userName");
		playerId = getIntent().getExtras().getLong("playerId");

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.draw_marker, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		
		if(id == R.id.kartefertig) {
			
			new FinishCardTask().execute();
			
			return true;
		}
		
		if(id == R.id.gamefertig) {
			
			new FinishGameTask().execute();
			
			return true;
		}
		
		if(id == R.id.kartenvoll) {
			
			new MobilizeAllLegionsTask().execute();
			
			return true;
		}
		
		
		if(id == R.id.Spiel_verlassen) {
			onBackPressed();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		new AlertDialog.Builder(this).setTitle("Spiel verlassen?")
			.setMessage("Möchten Sie das Spiel wirklich verlassen?")
			.setNegativeButton(R.string.nein, null)
			.setPositiveButton(R.string.ja, new OnClickListener() {

					public void onClick(DialogInterface arg0, int arg1) {
						
						GamePlayerRequestBean exitBean = new GamePlayerRequestBean();
						exitBean.setUserToken(userToken);
						new RemovePlayerTask().execute(exitBean);
						finish();
						DrawMarkerActivity.super.onBackPressed();
					}
				}).create().show();
	}

	/** Timer **/

	// task that is performed to wait 5 seconds and then go to the next activity
	class WaitTask extends TimerTask {

		public void run() {
			waitTimer.cancel();
			Log.e("Main", "Calling next activity for player: " + playerId);

			// TODO: next activity
			Intent intent = new Intent(getBaseContext(),
					PlayingFieldActivity.class);
			intent.putExtra("gameId", gameId);
			intent.putExtra("owner", owner);
			intent.putExtra("gameName", gameName);
			intent.putExtra("userToken", userToken);
			intent.putExtra("playerId", playerId);
			intent.putExtra("userName", userName);
			startActivity(intent);

			finish();

		}
	}
	
	/** SpringMVC tasks **/

	// get moveId and Marker from Server aka. GET on /game/{gameID}/move/
	public class GetMoveTask extends AsyncTask<Void, Void, MarkerBean> {

		@Override
		protected MarkerBean doInBackground(Void... params) {
			try {

				ResponseEntity<MarkerBean> response = RestService.get("/game/"
						+ gameId + "/move/", params, MarkerBean.class);
				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		protected void onPostExecute(MarkerBean response) {

			if (response == null) {
				Log.e("Main", "Marker ist NULL");
				return;
			}

			marker = response.getMarker();
			moveId = response.getMoveId();
			Log.e("Main", "Marker " + response.getMarker() + " gezogen.");
			onMarkerReadyCallback();

		}
	}
	
	// Remove user from game aka. POST on /game/{gameId}/player/remove
	private class RemovePlayerTask extends
	AsyncTask<GamePlayerRequestBean, Void, Void> {
		@Override
		protected Void doInBackground(
				GamePlayerRequestBean... gamePlayerRequestBeans) {

			try {
				Log.d("exit", "player left the game");
				RestService.post("/game/" + gameId + "/player/remove",
						gamePlayerRequestBeans[0], Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void param) {

		}

	}
	
	
	private class FinishCardTask extends
	AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			
			try {
				Log.d("fastforward", "started FastCardTask");
				RestService.post("/game/" + gameId + "/player/" + playerId + "/fullcard", params, Void.class);
				
			} catch(RestClientException rce) {
				Log.e("ERROR", rce.getMessage());
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void param) {
			
		}
	}
	
	private class FinishGameTask extends
	AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			
			try {
				Log.d("fastforward", "started FinishGameTask");
				RestService.post("/game/" + gameId + "/player/" + playerId + "/endGame", params, Void.class);
				
			} catch(RestClientException rce) {
				Log.e("ERROR", rce.getMessage());
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void param) {
			
		}
	}
	
	
	private class MobilizeAllLegionsTask extends
	AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			
			try {
				Log.d("fastforward", "mobilized all legions");
				RestService.post("/game/" + gameId + "/player/" + playerId + "/fillcards", params, Void.class);
			
			} catch(RestClientException rce) {
				Log.e("ERROR", rce.getMessage());
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void param) {
			
		}
	}
	

	/** GUI **/

	// will be called when button is pressed
	public void onClickDrawBtn(View v) {
		AudioManager.playSound(this, "ponch"); // immediate responsiveness inc.
		new GetMoveTask().execute();
		Button btn = (Button) findViewById(R.id.btnDrawMarker);
		btn.setVisibility(Button.GONE); // to prevent double clicks

		// wait 5 seconds then move on to the next task
		TimerTask waitTask = new WaitTask();
		waitTimer.schedule(waitTask, 5000);
	}

	public void onMarkerReadyCallback() {
		markerView = new ImageView(this);
		markerView.setScaleX(0.5f);
		markerView.setScaleY(0.5f);
		// markerView.getLayoutParams().width = 50;
		loadMarkerImage();
		Button btn = (Button) findViewById(R.id.btnDrawMarker);
		ViewGroup vg = (ViewGroup) findViewById(android.R.id.content);
		vg.removeView(btn);
		vg.addView(markerView);
	}

	private void loadMarkerImage() {
		if (marker == null) // = something went terribly wrong
			return;
		if (marker.equalsIgnoreCase("schwert")) {
			markerView
					.setImageResource(R.drawable.augustus_mobilisierung_schwerter);
		} else if (marker.equalsIgnoreCase("schild")) {
			markerView
					.setImageResource(R.drawable.augustus_mobilisierung_schild);
		} else if (marker.equalsIgnoreCase("streitwagen")) {
			markerView
					.setImageResource(R.drawable.augustus_mobilisierung_wagen);
		} else if (marker.equalsIgnoreCase("katapult")) {
			markerView
					.setImageResource(R.drawable.augustus_mobilisierung_katapult);
		} else if (marker.equalsIgnoreCase("dolch")) {
			markerView
					.setImageResource(R.drawable.augustus_mobilisierung_dolch);
		} else if (marker.equalsIgnoreCase("standarte")) {
			markerView
					.setImageResource(R.drawable.augustus_mobilisierung_hurrican);
		} else {
			markerView
					.setImageResource(R.drawable.augustus_mobilisierung_spezial);
		}
	}

}
