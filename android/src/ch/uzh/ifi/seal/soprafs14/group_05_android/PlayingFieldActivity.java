package ch.uzh.ifi.seal.soprafs14.group_05_android;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.RestService;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.GamePlayerRequestBean;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.MarkerBean;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.TargetCardBean;

public class PlayingFieldActivity extends Activity {

	private static final String cTag = "PlayingFieldActivity";

	private long gameId;
	private String gameName;
	private String owner;
	private String userToken;
	private String userName;
	private long playerId;
	private long moveId;
	private String marker;
	private int availableLegionaires; // stores the amt of legions available for
										// distribution
	ArrayList<TargetCardBean> targetCardArrayList = new ArrayList<TargetCardBean>();
	private ArrayList<PlayingCardView> cardViews = new ArrayList<PlayingCardView>();
	private PlayingCardView selectedCard = null;
	private Timer waitTimer = new Timer();

	private int assignableLegions; // stores the number of legions that can be
									// placed this round
	private boolean barracksEmpty = false; // checks if there are any legions
											// left to place
	private ArrayList<PlayingCardView> viewsWithNewlyAssignedLegion = new ArrayList<PlayingCardView>();
	
	private int totalNumberOfLegions = 7;
	private ArrayList<ImageView> barrackViews = new ArrayList<ImageView>(totalNumberOfLegions);

	/** Android related functions **/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_playing_field);

		gameId = getIntent().getExtras().getLong("gameId");
		owner = getIntent().getStringExtra("owner");
		gameName = getIntent().getStringExtra("gameName");
		userToken = getIntent().getStringExtra("userToken");
		userName = getIntent().getStringExtra("userName");
		playerId = getIntent().getExtras().getLong("playerId");

		// Draw available legionaires, also update marker and moveId
		new GetAvailableLegionsTask().execute();

		// Draw available fields that legionaires can be placed on
		new GetFreeLegionFieldsTask().execute();

		// Set maximum number of Legions that may be placed in this round,
		// currently 1
		assignableLegions = 1;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.playing_field, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}

		if (id == R.id.Spiel_verlassen) {
			onBackPressed();
			return true;
		}

		if (id == R.id.gegner_karten) {

			// call EnemyCardsActivity

			Log.e("Main", "Calling EnemyCardsActivity for UserId: " + playerId);
			Intent intent = new Intent(getBaseContext(),
					EnemyCardsActivity.class);
			intent.putExtra("gameId", gameId);
			intent.putExtra("owner", owner);
			intent.putExtra("gameName", gameName);
			intent.putExtra("userToken", userToken);
			intent.putExtra("playerId", playerId);
			intent.putExtra("userName", userName);
			intent.putExtra("moveId", moveId);
			startActivity(intent);

			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	public void onBackPressed() {
		new AlertDialog.Builder(this).setTitle("Spiel verlassen?")
				.setMessage("Möchten Sie das Spiel wirklich verlassen?")
				.setNegativeButton(android.R.string.no, null)
				.setPositiveButton(android.R.string.yes, new OnClickListener() {

					public void onClick(DialogInterface arg0, int arg1) {
						GamePlayerRequestBean exitBean = new GamePlayerRequestBean();
						exitBean.setUserToken(userToken);
						new RemovePlayerTask().execute(exitBean);
						finish();
						PlayingFieldActivity.super.onBackPressed();
					}
				}).create().show();
	}

	/** Spring MVC Tasks **/

	// Get the MoveID, Marker and available Legions from the server. We poll the
	// values again to ensure every client is up to date. aka. GET on
	// /{gameId}/player/{playerId}/legions
	private class GetAvailableLegionsTask extends
			AsyncTask<Void, Void, MarkerBean> {

		@Override
		protected MarkerBean doInBackground(Void... params) {
			try {

				ResponseEntity<MarkerBean> response = RestService.get("/game/"
						+ gameId + "/player/" + playerId + "/legions", params,
						MarkerBean.class);
				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		protected void onPostExecute(MarkerBean response) {

			if (response == null) {
				Log.e("Main", "MarkerBean is NULL");
				return;
			}

			marker = response.getMarker();
			moveId = response.getMoveId();
			availableLegionaires = response.getLegion();
			Log.e("Main", "Marker: " + response.getMarker() + " MoveID: "
					+ response.getMoveId() + " Verfügbare Legionäre: "
					+ response.getLegion());

			onMarkerLegionairesReady();

		}
	}

	// TODO: Get TargetCardBeans that determine which fields legionaires can be
	// placed on aka. GET on
	// /game/{gameId}/move/{moveId}/player/{playerId}/legionfield
	private class GetFreeLegionFieldsTask extends
			AsyncTask<Void, Void, TargetCardBean[]> {

		@Override
		protected TargetCardBean[] doInBackground(Void... params) {
			try {

				ResponseEntity<TargetCardBean[]> response = RestService.get(
						"/game/" + gameId + "/move/" + moveId + "/player/"
								+ playerId + "/legionfield", params,
						TargetCardBean[].class);

				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		protected void onPostExecute(TargetCardBean[] response) {

			if (response == null) {
				Log.e("Main", "TargetCard array is NULL");
				return;
			}

			for (int i = 0; i < response.length; i++) {

				Log.e("Main", "Adding TargetCard: " + response[i].getName());
				targetCardArrayList.add(response[i]);
				Log.e("Main", "ArrayListSize: " + targetCardArrayList.size());

			}

			onTargetCardsReady();

		}
	}

	// Task to send back changes that have been made due to placing the
	// legionaires aka. POST on
	// /game/{gameId}/move/{moveId}/player/{playerId}/legionfield2
	private class PostLegionairesTask extends
			AsyncTask<TargetCardBean[], Void, Void> {

		@Override
		protected Void doInBackground(TargetCardBean[]... targetCardBeans) {

			try {
				RestService.post("/game/" + gameId + "/move/" + moveId
						+ "/player/" + playerId + "/legionfield2",
						targetCardBeans[0], Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void params) {
			// Nothing to do here#
			Log.e("Main", "sent back TargetCards");
		}

	}

	/** GUI **/
	private void onMarkerLegionairesReady() {

		ImageView mkView = (ImageView) findViewById(R.id.currentMarker);
		if (marker.equalsIgnoreCase("schwert")) {
			mkView.setImageResource(R.drawable.augustus_mobilisierung_schwerter);
		} else if (marker.equalsIgnoreCase("schild")) {
			mkView.setImageResource(R.drawable.augustus_mobilisierung_schild);
		} else if (marker.equalsIgnoreCase("streitwagen")) {
			mkView.setImageResource(R.drawable.augustus_mobilisierung_wagen);
		} else if (marker.equalsIgnoreCase("dolch")) {
			mkView.setImageResource(R.drawable.augustus_mobilisierung_dolch);
		} else if (marker.equalsIgnoreCase("katapult")) {
			mkView.setImageResource(R.drawable.augustus_mobilisierung_katapult);
		} else if (marker.equalsIgnoreCase("standarte")) {
			mkView.setImageResource(R.drawable.augustus_mobilisierung_hurrican);
		} else {
			mkView.setImageResource(R.drawable.augustus_mobilisierung_spezial);
		}

		// fill all ImageViews into barrackViews to centrally store them
		barrackViews.add((ImageView) findViewById(R.id.legion1));
		barrackViews.add((ImageView) findViewById(R.id.legion2));
		barrackViews.add((ImageView) findViewById(R.id.legion3));
		barrackViews.add((ImageView) findViewById(R.id.legion4));
		barrackViews.add((ImageView) findViewById(R.id.legion5));
		barrackViews.add((ImageView) findViewById(R.id.legion6));
		barrackViews.add((ImageView) findViewById(R.id.legion7));
		for (int i = 0; i < barrackViews.size(); ++i) {
			if (availableLegionaires > i) {
				barrackViews.get(i).setImageResource(
						R.drawable.augustus_legion_rot);
			} else {
				barrackViews.get(i).setImageResource(
						R.drawable.augustus_legion_dunkel);
			}
		}

		if (availableLegionaires > 0) {
			assignableLegions = 1;
			barracksEmpty = false;
		} else {
			assignableLegions = 0;
			barracksEmpty = true;
		}
	}

	private void onTargetCardsReady() {

		LinearLayout center = (LinearLayout) findViewById(R.id.playing_field_center);
		for (TargetCardBean tcb : targetCardArrayList) {
			cardViews.add(new PlayingCardView(this, new Card(tcb)));
			Log.d(cTag, "PlayingCardView added");
		}
		for (PlayingCardView pcv : cardViews) {
			center.addView(pcv);
		}
	}

	public void toggleSelection(PlayingCardView callingView) {

		if (selectedCard == null) { // no card is selected
			selectedCard = callingView;
		} else if (selectedCard == callingView) { // selected card already
													// selected, i.e. is
													// deselected directly
			selectedCard = null;
		} else { // another view is selected, must be deselected
			selectedCard.parentDeselect();
			selectedCard = callingView;
		}
	}

	// helper methods
	public String getMarker() {
		return marker;
	}

	public int getNumberOfAssignableLegions() {
		return assignableLegions;
	}

	public void setLegion(PlayingCardView callingView) {
		if (assignableLegions > 0) {
			--assignableLegions;
			--availableLegionaires;
		} else {
			viewsWithNewlyAssignedLegion.get(0).parentRemoveSetLegion();
			viewsWithNewlyAssignedLegion.remove(0);
		}
		viewsWithNewlyAssignedLegion.add(callingView);
		updateBarracks();
	}

	public void removeLegion(PlayingCardView callingView) {
		viewsWithNewlyAssignedLegion.remove(callingView);
		++assignableLegions;
		++availableLegionaires;
		updateBarracks();
	}

	public void removeWhenBarracksEmpty(PlayingCardView callingView) {
		++assignableLegions;
		++availableLegionaires;
		updateBarracks();
		callingView.checkForAssignableFields();
	}

	public boolean getBarracksEmpty() {
		return barracksEmpty;
	}

	private void updateBarracks() {
		Log.d(cTag, "Barracks being updated, available legions: "
				+ availableLegionaires);
		int avl = availableLegionaires;
		for (int i = 0; i < barrackViews.size(); ++i) {
			if (avl > i) {
				barrackViews.get(i).setImageResource(
						R.drawable.augustus_legion_rot);
				Log.d(cTag, "Barrack " + i + " set to Red");
			} else {
				barrackViews.get(i).setImageResource(
						R.drawable.augustus_legion_dunkel);
				Log.d(cTag, "Barrack " + i + " set to Black");
			}
		}
		if (avl < 1) {
			barracksEmpty = true;
		} else {
			barracksEmpty = false;
		}
	}

	/** buttons **/
	public void nextPlayer(View view) {

	}

	public void onClickPlayerReadyBtn(View v) {
		Log.e("Main", "Calling EndRoundActivity for player: " + playerId);

		// Post updated List of TCB
		List<TargetCardBean> updatedTCB = new ArrayList<TargetCardBean>();
		for (PlayingCardView pcv : cardViews) {
			updatedTCB.add(pcv.getFinalizedTargetCardBean());
		}

		TargetCardBean[] tmpArray = updatedTCB
				.toArray(new TargetCardBean[updatedTCB.size()]);

		new PostLegionairesTask().execute(tmpArray);

		// call EndRound intent
		Intent intent = new Intent(getBaseContext(), EndRoundActivity.class);
		intent.putExtra("gameId", gameId);
		intent.putExtra("owner", owner);
		intent.putExtra("gameName", gameName);
		intent.putExtra("userToken", userToken);
		intent.putExtra("playerId", playerId);
		intent.putExtra("userName", userName);
		intent.putExtra("moveId", moveId);
		startActivity(intent);

		finish();
	}

	private class RemovePlayerTask extends
			AsyncTask<GamePlayerRequestBean, Void, Void> {
		@Override
		protected Void doInBackground(
				GamePlayerRequestBean... gamePlayerRequestBeans) {

			try {
				Log.d("exit", "player left the game");
				RestService.post("/game/" + gameId + "/player/remove",
						gamePlayerRequestBeans[0], Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void param) {

		}

	}

}
