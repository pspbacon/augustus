package ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans;

public class MarkerBean {
	
	private Long moveId;
	private String marker;
	private Integer legion;
	
	// additional variables for extra-effect-round
	private Integer legionCountEffect;

	public Integer getLegionCountEffect() {
		return legionCountEffect;
	}
	public void setLegionCountEffect(Integer legionCountEffect) {
		this.legionCountEffect = legionCountEffect;
	}
	
	public Integer getLegion() {
		return legion;
	}
	public void setLegion(Integer legion) {
		this.legion = legion;
	}
	
	public Long getMoveId() {
		return moveId;
	}
	public void setMoveId(Long moveId) {
		this.moveId = moveId;
	}
	public String getMarker() {
		return marker;
	}
	public void setMarker(String marker) {
		this.marker = marker;
	}

}
