package ch.uzh.ifi.seal.soprafs14.group_05_android;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.TargetCardBean;


public class Card {
	
	public enum Type {REGION, SENATOR}
	public enum Marker {SWORD, SHIELD, DAGGER, CART, CATAPULT, HURRICAN}
	public enum Resource {WHEAT, GOLD, NONE}
	public enum Banner {RED, LIGHT, DARK, NONE}
	public enum Province {NONE, AFRICA, ASIA, EUROPE}
	public enum ImmediateEffectCategory {
        // betrifft den Kartenbesitzer
        MOBILIZE_LEGIONS(1, "mobilize legions effect", "der Kartenbesitzer kann eine bestimmte Anzahl Legionäre auf Felder seiner in Eroberung befindlichen Zielkarten mit einem bestimmten oder beliebigen Marker legen"),
        REDISTRIBUTE_LEGIONS(2, "redistribute legions effect", "der Kartenbesitzer kann sämtliche mobilisierten (bereits gelegten) Legionen auf seinen in Eroberung befindlichen Zielkarten umverteilen"),
        INCREASE_LEGIONS(3, "increase legion effect", "der Kartenbesitzer erhält eine oder mehrere zusätzliche Legionen für seinen Verrat an mobilisierbaren Legionen"),
        CARD_COMPLETION(4, "card completion effect", "eine Zielkarte des Kartenbesitzers wird sofort erfüllt"),
        ADD_TARGETCARD(5, "add target card effect", "der Kartenbesitzer erhält eine zusätzliche Zielkarte, die er erobern kann (die gerade eroberte Karte wird mit zwei statt nur einer neuen Zielkarte ersetzt)"),
        // betrifft die Gegenspieler
        REMOVE_LEGIONS(6, "remove legion effect", "jeder Gegenspieler des Kartenbesitzers muss eine oder mehrere mobilisierte Legionen von seinen in Eroberung befindlichen Zielkarten entfernen"),
        REMOVE_ALL_LEGIONS(7, "remove all legions effect", "jeder Gegenspieler des Kartenbesitzers muss alle seine mobilisierten Legionen von seinen in Eroberung befindlichen Zielkarten entfernen"),
        CARD_LOSS(8, "card loss effect", "jeder Gegenspieler des Kartenbesitzers verliert eine kontrollierte Zielkarte");

        private final Integer code;
        private final String label;
        private final String description;
        
        private ImmediateEffectCategory(int code, String label, String description) {
            this.code = code;
            this.label = label;
            this.description = description;
        }
        
        public Integer getCode() {
            return code;
        }
        
        public String getLabel() {
            return label;
        }
        
        public String getDescription() {
            return description;
        }
	}
	
	private ArrayList<Marker> markers;
	private boolean[] assignable, occupied;
	private Type type;
	private Boolean isVarPoints;
	//private Resource resource;
	private ArrayList<Resource> resources = new ArrayList<Resource>();
	private Banner banner;
	private int points;
	private int number;
	private String name;
	private Province province;
	private TargetCardBean targetCardBean;
	
	public Type getType() {
		return type;
	}
	public ArrayList<Resource> getResources() {
		return resources;
	}
	public Banner getBanner() {
		return banner;
	}
	public int getPoints() {
		return points;
	}
	public ArrayList<Marker> getMarkers() {
		return markers;
	}
	public int getNumber() {
		return number;
	}
	public String getName() {
		return name;
	}
	public Province getProvince() {
		return province;
	}
	public TargetCardBean getTargetCardBean() {
		return targetCardBean;
	}
	public boolean hasTargetCardBean() {
		return targetCardBean != null;
	}
	public boolean[] getAssignable() {
		return assignable;
	}
	public boolean[] getOccupied() {
		return occupied;
	}
	public boolean getPointType() {
		return isVarPoints;
	}
		
//	@Deprecated
//	public Card(ArrayList<Marker> markers, Type type, Resource resource, Banner banner, int points, int number, String name) {
//		this.markers = markers; 
//		this.type = type; 
//		this.resources.add(resource); 
//		this.banner=banner; 
//		this.points = points; 
//		this.number = number; 
//		this.name = name;
//		this.province = Province.NONE;
//		targetCardBean = null; // ACHTUNG, FüHRT ZU FEHLERN, diesen constructor nicht mehr verwenden!
//	}
	
	public Card(TargetCardBean tcb) {
		targetCardBean = tcb;
		points = tcb.getPoints();
		number = tcb.getNumber();
		name = tcb.getName();
		
		// TODO: Effects implementation
		if(tcb.getEffectId() != null) {
			int effectId = tcb.getEffectId();
			// card-holder effect -> beige
			if (1 <= effectId && effectId <= 5) {
				banner = Banner.LIGHT;
				// enemies effect -> red
			} else if ( effectId <= 8 ) {
				banner = Banner.RED;
			} else {
				banner = Banner.DARK;
			}
		} else {
			banner = Banner.NONE;
		}
		
		
		String tcb_type = tcb.getType();
		if (tcb_type.equalsIgnoreCase("senator")) {
			type = Type.SENATOR;
			province = Province.NONE;
		}
		else if (tcb_type.equalsIgnoreCase("europa")) {
			type = Type.REGION;
			province = Province.EUROPE;
		}
		else if (tcb_type.equalsIgnoreCase("kleinasien")) {
			type = Type.REGION;
			province = Province.ASIA;
		}
		else {
			type = Type.REGION;
			province = Province.AFRICA;
		}
		
		// TODO: Include option for two commodities
		if (tcb.getCommodities() != null) {

			List<String> comm = tcb.getCommodities();
			for (String str_comm : comm) {
				if (str_comm.equalsIgnoreCase("gold")) {
					resources.add(Resource.GOLD);
				} else {
					resources.add(Resource.WHEAT);
				}
			}
//			String tcb_comm = tcb.getCommodities().get(0);
//			if (tcb_comm.equalsIgnoreCase("gold")) {
//				resource = Resource.GOLD;
//			}
//			else {
//				resource = Resource.WHEAT;
//			}
		}
		
		markers = new ArrayList<Marker>();
		// wahrscheinlich nicht noetig als neue AL zu initialisieren
		ArrayList<String> mks = new ArrayList<String>(tcb.getLegionFields());
		for (String m : mks) {
			if (m.equalsIgnoreCase("schwert")) {
				markers.add(Marker.SWORD);
			} else if (m.equalsIgnoreCase("schild")) {
				markers.add(Marker.SHIELD);
			} else if (m.equalsIgnoreCase("streitwagen")) {
				markers.add(Marker.CART);
			} else if (m.equalsIgnoreCase("dolch")) {
				markers.add(Marker.DAGGER);
			} else if (m.equalsIgnoreCase("katapult")) {
				markers.add(Marker.CATAPULT);
			} else {
				markers.add(Marker.HURRICAN);
			}
		}
		
		isVarPoints = tcb.getPointType();

		
		List<Boolean> bools = tcb.getOpenForAssignment();
		assignable = new boolean[markers.size()];
		if (bools != null) {
			for (int i = 0; i<assignable.length; ++i) {
				assignable[i] = bools.get(i).booleanValue();
			}
		} else {
			for (int i = 0; i<assignable.length; ++i) {
				assignable[i] = true;
			}
		}
		
		bools = tcb.getStatusFields();
		occupied = new boolean[markers.size()];
		if (bools != null) {
			for (int i = 0; i<occupied.length; ++i) {
				occupied[i] = bools.get(i).booleanValue();
			}
		} else {
			for (int i = 0; i<occupied.length; ++i) {
				occupied[i] = false;
			}
		}
	}
		
}