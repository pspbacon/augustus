package ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans;

public class AveCaesarBean {
	
	Integer numberOfAveCaesarCard;
	Integer numberOfChoosenCard;
	
	public Integer getNumberOfAveCaesarCard() {
		return numberOfAveCaesarCard;
	}
	public void setNumberOfAveCaesarCard(Integer numberOfAveCaesarCard) {
		this.numberOfAveCaesarCard = numberOfAveCaesarCard;
	}
	public Integer getNumberOfChoosenCard() {
		return numberOfChoosenCard;
	}
	public void setNumberOfChoosenCard(Integer numberOfChoosenCard) {
		this.numberOfChoosenCard = numberOfChoosenCard;
	}

}
