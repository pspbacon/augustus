package ch.uzh.ifi.seal.soprafs14.group_05_android;

import java.util.ArrayList;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.RestService;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.GamePlayerRequestBean;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.RewardBean;

public class DrawableRewardActivity extends Activity {

	private long gameId;
	private String gameName;
	private String owner;
	private String userToken;
	private String userName;
	private long playerId;
	private long moveId;
	private ArrayList<RewardBean> rewardList = new ArrayList<RewardBean>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_drawable_reward);

		gameId = getIntent().getExtras().getLong("gameId");
		owner = getIntent().getStringExtra("owner");
		gameName = getIntent().getStringExtra("gameName");
		userToken = getIntent().getStringExtra("userToken");
		userName = getIntent().getStringExtra("userName");
		playerId = getIntent().getExtras().getLong("playerId");
		moveId = getIntent().getExtras().getLong("moveId");

		new GetRewardsTask().execute();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ave_caesar_target_card, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}

		if (id == R.id.Spiel_verlassen) {
			onBackPressed();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	public void onBackPressed() {
		new AlertDialog.Builder(this).setTitle("Spiel verlassen?")
				.setMessage("Moechten Sie das Spiel wirklich verlassen?")
				.setNegativeButton(R.string.nein, null)
				.setPositiveButton(R.string.ja, new OnClickListener() {

					public void onClick(DialogInterface arg0, int arg1) {
						GamePlayerRequestBean exitBean = new GamePlayerRequestBean();
						exitBean.setUserToken(userToken);
						new RemovePlayerTask().execute(exitBean);
						finish();
						DrawableRewardActivity.super.onBackPressed();
					}
				}).create().show();
	}

	// Remove user from game aka. POST on /game/{gameId}/player/remove
	private class RemovePlayerTask extends
			AsyncTask<GamePlayerRequestBean, Void, Void> {
		@Override
		protected Void doInBackground(
				GamePlayerRequestBean... gamePlayerRequestBeans) {

			try {
				Log.d("exit", "player left the game");
				RestService.post("/game/" + gameId + "/player/remove",
						gamePlayerRequestBeans[0], Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void param) {

		}

	}

	/** REST **/

	// 1. fetch rewards
	private class GetRewardsTask extends AsyncTask<Void, Void, RewardBean[]> {

		@Override
		protected RewardBean[] doInBackground(Void... params) {
			try {

				ResponseEntity<RewardBean[]> response = RestService.get(
						"/game/" + gameId + "/player/" + playerId + "/rewards",
						params, RewardBean[].class);

				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		protected void onPostExecute(RewardBean[] response) {
			if (response == null || response.length == 0) {
				// nothing to execute go directly to next intent
				nextIntent();
			} else {

				ArrayList<String> values = new ArrayList<String>();

				for (int i = 0; i < response.length; i++) {
					rewardList.add(response[i]);
					values.add(response[i].getId() + ": "
							+ response[i].getDescription() + "; points: "
							+ response[i].getPoints());
				}

				// primitive anzeige der rewards mittels liste
				ListAdapter adapter = new ArrayAdapter<String>(
						DrawableRewardActivity.this, R.layout.list_white_style,
						R.id.list_white_textview, values);

				ListView rewards = (ListView) findViewById(R.id.listView1);
				rewards.setAdapter(adapter);

				rewards.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long id) {

						// eine Box wäre hier wohl noch angebracht
						new ChooseRewardTask().execute(rewardList.get(position)
								.getId());
						nextIntent();
					}

				});

			}
		}
	}

	// 2. update rewards
	private class ChooseRewardTask extends AsyncTask<Long, Void, Void> {

		@Override
		protected Void doInBackground(Long... choosenRewardId) {

			try {
				RestService.post("/game/" + gameId + "/player/" + playerId
						+ "/choosenReward", choosenRewardId[0], Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void params) {
			// do nothing

		}

	}

	// TODO: Einbetten in den AveCaesar-Komplex
	public void nextIntent() {

		// EndRoundActivity wird nicht beendet, sondern beibehalten
		// dadurch reicht hier ein finish() aus
		/*
		 * Intent intent = new Intent(getBaseContext(), EndRoundActivity.class);
		 * intent.putExtra("gameId", gameId); intent.putExtra("owner", owner);
		 * intent.putExtra("gameName", gameName); intent.putExtra("userToken",
		 * userToken); intent.putExtra("playerId", playerId);
		 * intent.putExtra("userName", userName);
		 * 
		 * startActivity(intent);
		 */

		finish();
	}

	/** GUI **/

	// add behaviour for button and list

	// keine Rewards auswählen
	public void onClickOhneWeiter(View v) {
		nextIntent();
	}

}
