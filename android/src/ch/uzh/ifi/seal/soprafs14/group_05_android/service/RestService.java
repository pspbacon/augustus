package ch.uzh.ifi.seal.soprafs14.group_05_android.service;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

public class RestService {

	//private final static String baseUrl = "http://10.0.2.2:26051";
	private final static String baseUrl = "https://seal-students.ifi.uzh.ch/soprafs14-group-05-service/";
	private final static RestTemplate restTemplate;

	// add-on for slow ifi-Server
	private final static HttpHeaders headers = new HttpHeaders();

	static {
		restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new GsonHttpMessageConverter());

		// add-ons for slow ifi-Server
		headers.set("Connection", "Close");
		restTemplate
				.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
	}

	public static <T> ResponseEntity<T> get(String resource,
			Object requestData, Class<T> responseType)
			throws RestClientException {
		return restTemplate.getForEntity(baseUrl + resource, responseType,
				requestData);
	}

	public static <T> ResponseEntity<T> post(String resource,
			Object requestData, Class<T> responseType)
			throws RestClientException {
		return restTemplate.postForEntity(baseUrl + resource, requestData,
				responseType);
	}

}
