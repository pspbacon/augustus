package ch.uzh.ifi.seal.soprafs14.group_05_android;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.RestService;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.GamePlayerRequestBean;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.GamePlayerResponseBean;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.TargetCardBean;

public class EnemyCardsActivity extends Activity {
	
	public static final String cTag = "EnemyCardsActivity";

	private long gameId;
	private String gameName;
	private String owner;
	private String userToken;
	private String userName;
	private long playerId;
	private long moveId;

	ArrayList<ArrayList<TargetCardBean>> assignedTargetCards = new ArrayList<ArrayList<TargetCardBean>>();
	ArrayList<ArrayList<TargetCardBean>> controlledTargetCards = new ArrayList<ArrayList<TargetCardBean>>();
	ArrayList<TargetCardBean> selectedTargetCards = new ArrayList<TargetCardBean>();
	
	List<String> usernameList;

	private Integer userSelected = 0;
	private int viewSelected = 0; // 0 == assigned, 1 == controlled
	private ArrayList<EnemyCardView> cardViews = new ArrayList<EnemyCardView>();
	AveCardView aveCardView = null;
	
	LinearLayout cardLayout = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_enemy_cards);

		gameId = getIntent().getExtras().getLong("gameId");
		owner = getIntent().getStringExtra("owner");
		gameName = getIntent().getStringExtra("gameName");
		userToken = getIntent().getStringExtra("userToken");
		userName = getIntent().getStringExtra("userName");
		playerId = getIntent().getExtras().getLong("playerId");
		moveId = getIntent().getExtras().getLong("moveId");
		
		cardLayout = (LinearLayout) findViewById(R.id.ec_cardlayout);

		
		// TODO: Selection of player
		// TODO: Selection of Card-Mode (Assigned Cards and Controlled Cards)
		
		//new GetControlledCardsTask().execute();
		new GetControlledCardsTask().execute();
		new GetAssignedCardsTask().execute();
		new GetUsernames().execute();

		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.enemycards, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}

		if (id == R.id.Punkteansicht) {

			finish();
			return true;
		}

		if (id == R.id.Spiel_verlassen) {
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {

		EnemyCardsActivity.super.onBackPressed();
	}

	/** SpringMVC tasks **/

	// Get conquered cards of all player aka. GET on
	// /game/{gameId}/conqueredCards
	private class GetControlledCardsTask extends
			AsyncTask<Void, Void, TargetCardBean[][]> {

		@Override
		protected TargetCardBean[][] doInBackground(Void... params) {
			try {

				ResponseEntity<TargetCardBean[][]> response = RestService.get(
						"/game/" + gameId + "/conqueredCards", params,
						TargetCardBean[][].class);
				Log.d("REST", "Requested all controlled Cards with gameID:"
						+ gameId + ", playerId:" + playerId);
				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		protected void onPostExecute(TargetCardBean[][] response) {

			// unpack the bean and assign each bean to the corresponding user
			controlledTargetCards = new ArrayList<ArrayList<TargetCardBean>>();

			// prevent crashing
			if (response != null) {
				Log.d("REST", "unpacking all controlled Cards");
				for (int i = 0; i < response.length; i++) {
					ArrayList<TargetCardBean> tmpTargetCardList = new ArrayList<TargetCardBean>();
					for (int j = 0; j < response[i].length; j++) {
						tmpTargetCardList.add(response[i][j]);
					}
					controlledTargetCards.add(tmpTargetCardList);
				}
			}

			if (controlledTargetCards != null) {
				//selectedControlledTargetCards = controlledTargetCards.get(userSelected);
				Log.d("Felix", "controlledTargetCards read" + controlledTargetCards.size());
			}

		}
	}

	// Get assigned cards of all player aka. GET on /game/{gameId}/assignedCards
	private class GetAssignedCardsTask extends
			AsyncTask<Void, Void, TargetCardBean[][]> {

		@Override
		protected TargetCardBean[][] doInBackground(Void... params) {
			try {

				ResponseEntity<TargetCardBean[][]> response = RestService.get(
						"/game/" + gameId + "/assignedCards", params,
						TargetCardBean[][].class);
				Log.d("REST", "Requested all controlled Cards with gameID:"
						+ gameId + ", playerId:" + playerId);
				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		protected void onPostExecute(TargetCardBean[][] response) {

			// unpack the bean and assign each bean to the corresponding user
			assignedTargetCards = new ArrayList<ArrayList<TargetCardBean>>();

			// prevent crashing
			if (response != null) {
				Log.d("REST", "unpacking all assigned Cards");
				for (int i = 0; i < response.length; i++) {
					ArrayList<TargetCardBean> tmpTargetCardList = new ArrayList<TargetCardBean>();
					for (int j = 0; j < response[i].length; j++) {
						tmpTargetCardList.add(response[i][j]);
					}
					assignedTargetCards.add(tmpTargetCardList);
				}
			}

			if (!assignedTargetCards.isEmpty()) {
				selectedTargetCards = assignedTargetCards.get(userSelected);
				Log.d("Felix", "EnemyCards are going to be rendered");
				showCardTask();
			}

		}
	}

	// TODO: create Task to fetch all userNames of the game
	// get usernames of all players aka. GET on /game/{game-id}/player

	private class GetUsernames extends
			AsyncTask<Void, Void, GamePlayerResponseBean[]> {

		@Override
		protected GamePlayerResponseBean[] doInBackground(Void... params) {
			try {

				ResponseEntity<GamePlayerResponseBean[]> response = RestService
						.get("/game/" + gameId + "/player", params,
								GamePlayerResponseBean[].class);
				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		protected void onPostExecute(GamePlayerResponseBean[] response) {

			// unpack the bean and assign each bean to the corresponding user
			usernameList = new ArrayList<String>();
			for (int i = 0; i < response.length; i++) {
				usernameList.add(response[i].getUsername());
			}
			
			//create ListAdapter to fill usernameList into playerList ListView
			ListAdapter adapter = new ArrayAdapter<String>(
					EnemyCardsActivity.this, R.layout.list_white_style,
					R.id.list_white_textview, usernameList);

			ListView playerList = (ListView) findViewById(R.id.ec_playerList);
			playerList.setAdapter(adapter);
			
			playerList.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long id) {
					userSelected = position;
					selectedTargetCards = assignedTargetCards.get(userSelected);
					showCardTask();
				}

			});
			
			Log.d("Felix", "usernames fetched: size " + usernameList.size() );
		}
	}

	private class RemovePlayerTask extends
			AsyncTask<GamePlayerRequestBean, Void, Void> {
		@Override
		protected Void doInBackground(
				GamePlayerRequestBean... gamePlayerRequestBeans) {

			try {
				Log.d("exit", "player left the game");
				RestService.post("/game/" + gameId + "/player/remove",
						gamePlayerRequestBeans[0], Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void param) {

		}

	}

	/** GUI **/

	private void showCardTask() {
		// should short-circuit if list is null, i.e. second condition not
		// checked (would throw error - safe programming inc.)
		cardLayout.removeAllViews();
		
		if (selectedTargetCards == null || selectedTargetCards.isEmpty()) {
			Log.d("EnemyCards", "No cards to show");
			return;
		}
		
		cardViews.clear();

		for (TargetCardBean tcb : selectedTargetCards) {
			cardViews.add(new EnemyCardView(this, new Card(tcb)));
			Log.d(cTag, "EnemyCardView added");
		}
		for (EnemyCardView ecv : cardViews) {
			cardLayout.addView(ecv);
		}
		
		
	}
	
	public void onConqueredPress(View view) {
		// TODO: load conquered cards of selected player
		if (viewSelected == 0) {
			viewSelected = 1;
			selectedTargetCards = controlledTargetCards.get(userSelected);
			showCardTask();
		}
	}
	
	public void onConquestedPress(View view) {
		if (viewSelected == 1) {
			viewSelected = 0;
			selectedTargetCards = assignedTargetCards.get(userSelected);
			showCardTask();
		}
	}

}
