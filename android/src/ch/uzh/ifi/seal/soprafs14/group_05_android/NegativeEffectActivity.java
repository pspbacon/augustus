package ch.uzh.ifi.seal.soprafs14.group_05_android;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.RestService;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.TargetCardBean;

public class NegativeEffectActivity extends Activity {
	private long gameId;
	private String gameName;
	private String owner;
	private String userToken;
	private String userName;
	private long playerId;
	private long moveId;
	private Integer effectId;

	private ArrayList<TargetCardBean> assignedCards;
	private ArrayList<TargetCardBean> controlledCards;
	private ArrayList<TargetCardBean> finalizedCards = new ArrayList<TargetCardBean>();
	private ArrayList<NegativeEffectCardView> cardViews = new ArrayList<NegativeEffectCardView>();

	private int removedLegions = 0;
	private int legionsToRemove = 0;
	private NegativeEffectCardView selectedCard = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_negative_effect);
		gameId = getIntent().getExtras().getLong("gameId");
		owner = getIntent().getStringExtra("owner");
		gameName = getIntent().getStringExtra("gameName");
		userToken = getIntent().getStringExtra("userToken");
		userName = getIntent().getStringExtra("userName");
		playerId = getIntent().getExtras().getLong("playerId");
		moveId = getIntent().getExtras().getLong("moveId");
		effectId = getIntent().getExtras().getInt("effectId");

		if (effectId == 80) {
			legionsToRemove = 1;
		} else if (effectId == 81) {
			legionsToRemove = 2;
		}

		refreshMessage();
		new GetPlayerCardsTask().execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.negative_effect, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}



	/** REST **/

	// 1. get TargetCardBean[][] with own and controlled cards (response[0] for assigned
	// response[1] for controlled cards
	// server does automatically set playerStatus to not ready
	private class GetPlayerCardsTask extends
	AsyncTask<Void, Void, TargetCardBean[][]> {

		@Override
		protected TargetCardBean[][] doInBackground(Void... params) {
			try {

				ResponseEntity<TargetCardBean[][]> response = RestService.get(
						"/game/" + gameId + "/player/" + playerId +"/negativeEffect", params,
						TargetCardBean[][].class);
				Log.d("REST", "Requested all Cards with gameID:"
						+ gameId + ", playerId:" + playerId);
				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		protected void onPostExecute(TargetCardBean[][] response) {

			// unpack the bean and assign each bean to the corresponding user
			ArrayList<ArrayList<TargetCardBean>> userTargetCards = new ArrayList<ArrayList<TargetCardBean>>();

			// prevent crashing
			if (response != null) {
				Log.d("REST", "unpacking all controlled Cards");
				for (int i = 0; i < response.length; i++) {
					ArrayList<TargetCardBean> tmpTargetCardList = new ArrayList<TargetCardBean>();
					for (int j = 0; j < response[i].length; j++) {
						tmpTargetCardList.add(response[i][j]);
					}
					userTargetCards.add(tmpTargetCardList);
				}
			}

			if (userTargetCards != null) {

				// TODO: do logic here
				if (userTargetCards.size() > 1) {
					assignedCards = userTargetCards.get(0);
					controlledCards = userTargetCards.get(1);

					onActivityReady();
				}
			}

		}
	}


	// Task to send back changes that have been made due to placing the
	// legionaires aka. POST on
	// /game/{gameId}/move/{moveId}/player/{playerId}/legionfield2
	private class PostLegionairesTask extends
	AsyncTask<TargetCardBean[], Void, Void> {

		@Override
		protected Void doInBackground(TargetCardBean[]... targetCardBeans) {

			try {
				RestService.post("/game/" + gameId + "/move/" + moveId
						+ "/player/" + playerId + "/legionfield2",
						targetCardBeans[0], Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void params) {
			// Nothing to do here#
			Log.e("Main", "sent back TargetCards");
		}

	}
	
	// Task to send back targetCard to sacrifice
	// POST on:
	// /game/{gameId}/player/{playerId}/sacrifice
	private class SacrificeTargetCardTask extends
	AsyncTask<Integer, Void, Void> {
		
		@Override
		protected Void doInBackground(Integer... numberOfSacrifice) {
			
			try{
				RestService.post("/game/" + gameId + "/player/" + playerId + "/sacrifice", numberOfSacrifice[0], Void.class);
				Log.d("REST", "sending card to sacrifice");
			} catch(RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void params) {
			// Nothing to do here#
			Log.e("Main", "sacrificed targetCard");
		}
	}

	/** GUI **/
	public int getEffectId() {
		return effectId;
	}

	public int getRemovedLegions() {
		return removedLegions;
	}

	public int getLegionsToRemove() {
		return legionsToRemove;
	}

	public void removeLegion() {
		++removedLegions;
	}

	public void onClickPlayerReadyBtn(View v) {
		if (effectId == 80 || effectId == 81) {
			if (removedLegions == legionsToRemove) {
				// TODO: pack up cards and post
				for (NegativeEffectCardView cv : cardViews) {
					finalizedCards.add(cv.getFinalizedTargetCardBean());
				}
				TargetCardBean[] tempArr = finalizedCards.toArray(new TargetCardBean[finalizedCards.size()]);
				new PostLegionairesTask().execute(tempArr);
				finish();
			}
		} else if (effectId == 70) {
			stripLegionsOfSelected();
			TargetCardBean[] tempArr = finalizedCards.toArray(new TargetCardBean[finalizedCards.size()]);
			new PostLegionairesTask().execute(tempArr);
			finish();
		}
		else if (effectId == 71) {
			if (selectedCard != null) {
				// TODO: post index of selected Card
				int index = selectedCard.getNumber();
				new SacrificeTargetCardTask().execute(Integer.valueOf(index));
				finish();
			}
		} else {
			Log.e("NegativeEffect", "NegativeEffectActivity, effectId scheint nicht zu stimmen, nicht 70, 71, 80, 81");
			finish();
		}

	}

	public void toggleSelect(NegativeEffectCardView caller) {
		if (selectedCard == null) {
			selectedCard = caller;
		} else if (selectedCard == caller) {
			selectedCard = null;
		} else {
			selectedCard.parentDeselect();
			selectedCard = caller;
		}
	}

	private void onActivityReady() {
		// spieler verlieren eine eroberte karte
		if (effectId == 71) {
			for (TargetCardBean tcb : controlledCards) {
				cardViews.add(new NegativeEffectCardView(this, tcb));
			}
		} // effekte die Karten in eroberung betreffen
		else if (effectId == 70 || effectId == 80 || effectId == 81) {
			for (TargetCardBean tcb : assignedCards) {
				cardViews.add(new NegativeEffectCardView(this, tcb));
			}
		} else {
			Log.e("NegativeEffect", "effectId seems incorrect, is neither 70, 71, 80 or 81");
		}

		LinearLayout center = (LinearLayout) findViewById(R.id.ne_center);
		for (NegativeEffectCardView cv : cardViews) {
			center.addView(cv);
		}
	}

	private void refreshMessage() {
		TextView tv = (TextView) findViewById(R.id.ne_message);

		switch(effectId) {
		case 70:
			tv.setText("Pech: Waehle eine Karte von der alle Legionen abgezogen werden");
			break;
		case 71:
			tv.setText("Miseria: Waehle eine Eroberte Karte die du wieder verlierst");
			break;
		case 80:
			tv.setText("Intrigen: Waehle eine Legion die abgezogen wird");
			break;
		case 81:
			tv.setText("Betrüger: Waehle zwei Legionen die abgezogen werden");
			break;
		default:
			tv.setText("Something went horribly wrong");
			break;
		}
	}
	
	private void stripLegionsOfSelected() {
		TargetCardBean tcb = selectedCard.getTargetCardBean();
		List<Boolean> occupied = tcb.getStatusFields();
		
		for (int i = 0; i<occupied.size(); ++i) {
			occupied.set(i,  Boolean.valueOf(false));
		}
		tcb.setStatusFields(occupied);
		
		for (NegativeEffectCardView cv : cardViews) {
			if (cv == selectedCard) {
				finalizedCards.add(tcb);
			} else {
				finalizedCards.add(cv.getTargetCardBean());
			}
		}
	}
}
