package ch.uzh.ifi.seal.soprafs14.group_05_android;

import java.util.ArrayList;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.RestService;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.GamePlayerRequestBean;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.StatisticsBean;

public class StatsActivity extends Activity {

	private long gameId;
	private String gameName;
	private String owner;
	private String userToken;
	private String userName;
	private long playerId;
	private long moveId;

	/** Android related functions **/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stats);

		gameId = getIntent().getExtras().getLong("gameId");
		owner = getIntent().getStringExtra("owner");
		gameName = getIntent().getStringExtra("gameName");
		userToken = getIntent().getStringExtra("userToken");
		userName = getIntent().getStringExtra("userName");
		playerId = getIntent().getExtras().getLong("playerId");
		moveId = getIntent().getExtras().getLong("moveId");

		new GetStatsTask().execute();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.stats, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		if (id == R.id.FastForward) {
			new FastForwardTask().execute();

			// Automatically go into the next Activity
			Intent intent = new Intent(getBaseContext(),
					DrawMarkerActivity.class);
			intent.putExtra("gameId", gameId);
			intent.putExtra("owner", owner);
			intent.putExtra("gameName", gameName);
			intent.putExtra("userToken", userToken);
			intent.putExtra("playerId", playerId);
			intent.putExtra("userName", userName);

			startActivity(intent);
			finish();

			return true;
		}

		if (id == R.id.Kartenansicht) {
			Intent intent = new Intent(getBaseContext(),
					EnemyCardsActivity.class);
			intent.putExtra("gameId", gameId);
			intent.putExtra("owner", owner);
			intent.putExtra("gameName", gameName);
			intent.putExtra("userToken", userToken);
			intent.putExtra("playerId", playerId);
			intent.putExtra("userName", userName);

			startActivity(intent);

			return true;
		}

		if (id == R.id.aufgeben) {
			Intent intent = new Intent(getBaseContext(), EndGameActivity.class);
			intent.putExtra("winner", false);

			startActivity(intent);
			finish();
		}

		if (id == R.id.Spiel_verlassen) {
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		new AlertDialog.Builder(this).setTitle("Spiel verlassen?")
				.setMessage("Möchten Sie das Spiel wirklich verlassen?")
				.setNegativeButton(R.string.nein, null)
				.setPositiveButton(R.string.ja, new OnClickListener() {

					public void onClick(DialogInterface arg0, int arg1) {
						GamePlayerRequestBean exitBean = new GamePlayerRequestBean();
						exitBean.setUserToken(userToken);
						new RemovePlayerTask().execute(exitBean);
						finish();
						StatsActivity.super.onBackPressed();
					}
				}).create().show();
	}

	/** SpringMVC tasks **/

	// Get statistics data aka. GET on /game/{gameId}/points
	// TODO: adapt task to fetch StatisticsBean[] instead
	private class GetStatsTask extends AsyncTask<Void, Void, StatisticsBean[]> {

		@Override
		protected StatisticsBean[] doInBackground(Void... params) {
			try {

				ResponseEntity<StatisticsBean[]> response = RestService.get(
						"/game/" + gameId + "/points", params,
						StatisticsBean[].class);
				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		protected void onPostExecute(StatisticsBean[] response) {

			ArrayList<String> values = new ArrayList<String>();

			for (int i = 0; i < response.length; i++) {
				Log.e("Main", "Player: " + response[i].getName() + " Points: "
						+ response[i].getPoints());
				int gold;
				int weizen;
				if(response[i].getCountGold() != null) {
					gold = response[i].getCountGold();
				} else gold = 0;
				if(response[i].getCountWheat() != null) {
					weizen = response[i].getCountWheat();
				} else weizen = 0;
				
				values.add(response[i].getName() + ", Punkte: "
						+ response[i].getPoints().toString() + ", Gold:" +
				+ gold + ", Weizen: " + weizen);
			}

			ListAdapter adapter = new ArrayAdapter<String>(StatsActivity.this,
					R.layout.list_white_style, R.id.list_white_textview, values);

			ListView pointList = (ListView) findViewById(R.id.statslist);
			pointList.setAdapter(adapter);

		}
	}

	/** GUI **/

	public void onClickNextRoundBtn(View v) {

		// Call DrawMarkerActivity

		Intent intent = new Intent(getBaseContext(), DrawMarkerActivity.class);
		intent.putExtra("gameId", gameId);
		intent.putExtra("owner", owner);
		intent.putExtra("gameName", gameName);
		intent.putExtra("userToken", userToken);
		intent.putExtra("playerId", playerId);
		intent.putExtra("userName", userName);

		startActivity(intent);
		finish();
	}

	private class FastForwardTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {

			try {
				Log.d("fastforward", "started FastforwardingTask");
				RestService.post("/game/" + gameId + "/player/" + playerId
						+ "/fastforward", params, Void.class);

			} catch (RestClientException rce) {
				Log.e("ERROR", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void param) {

		}
	}

	private class RemovePlayerTask extends
			AsyncTask<GamePlayerRequestBean, Void, Void> {
		@Override
		protected Void doInBackground(
				GamePlayerRequestBean... gamePlayerRequestBeans) {

			try {
				Log.d("exit", "player left the game");
				RestService.post("/game/" + gameId + "/player/remove",
						gamePlayerRequestBeans[0], Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void param) {

		}

	}
}
