package ch.uzh.ifi.seal.soprafs14.group_05_android;

import java.util.Timer;
import java.util.TimerTask;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.RestService;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.GamePlayerRequestBean;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.RoundEvaluationBean;

public class EndRoundActivity extends Activity {

	private long gameId;
	private String gameName;
	private String owner;
	private String userToken;
	private String userName;
	private long playerId;
	private long moveId;
	private boolean allArrived;
	private boolean timerActive = false;
	private Timer refreshTimer;
	private boolean negativeEffectDone = false;

	private String debugMessage = "";

	/** Android related functions **/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_end_round);

		gameId = getIntent().getExtras().getLong("gameId");
		owner = getIntent().getStringExtra("owner");
		gameName = getIntent().getStringExtra("gameName");
		userToken = getIntent().getStringExtra("userToken");
		userName = getIntent().getStringExtra("userName");
		playerId = getIntent().getExtras().getLong("playerId");
		moveId = getIntent().getExtras().getLong("moveId");
		allArrived = false;

	}

	@Override
	public void onStart() {

		// Tell the server that the user has arrived in the EndOfRoundActivity,
		// to synchronize all players
		new PlayerReadyTask().execute();

		new GetPlayerStatusTask().execute();
		new GetEndRoundDataTask().execute();

		super.onStart();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.end_round, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}

		if (id == R.id.gamefertig) {

			Log.e("Main", "Calling EndGameActivity for player: " + playerId);

			if (timerActive) {
				refreshTimer.cancel();
				timerActive = false;
			}
			// TODO: logic for winner
			Intent intent = new Intent(getBaseContext(), EndGameActivity.class);

			intent.putExtra("winner", true);

			startActivity(intent);
			finish();

			return true;
		}

		if (id == R.id.Spiel_verlassen) {
			onBackPressed();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	public void onBackPressed() {
		new AlertDialog.Builder(this).setTitle("Spiel verlassen?")
				.setMessage("Möchten Sie das Spiel wirklich verlassen?")
				.setNegativeButton(R.string.nein, null)
				.setPositiveButton(R.string.ja, new OnClickListener() {

					public void onClick(DialogInterface arg0, int arg1) {
						GamePlayerRequestBean exitBean = new GamePlayerRequestBean();
						exitBean.setUserToken(userToken);
						new RemovePlayerTask().execute(exitBean);
						finish();
						EndRoundActivity.super.onBackPressed();
					}
				}).create().show();
	}

	/** Polling **/

	// the timer task to fetch the user stati
	class GetEndRoundDataTimerTask extends TimerTask {

		public void run() {

			new GetPlayerStatusTask().execute();
			new GetEndRoundDataTask().execute();

		}
	}

	/** Spring MVC Tasks **/

	// get on /game/{gameId}/player/status to get the boolean which determines
	// if all players are ready for the next round

	private class GetPlayerStatusTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			try {

				ResponseEntity<Boolean> response = RestService.get("/game/"
						+ gameId + "/player/status", params, Boolean.class);

				Log.e("Main", "EndRoundActivity: Polling Playerstati");

				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		protected void onPostExecute(Boolean response) {

			// set allArrived to true, when server acknowledges that all players
			// are in the EndRoundActivity
			if (response == null) {
				return;
			}

			else if (response == true) {

				allArrived = true;

			} else {

				allArrived = false;

			}
		}
	}

	// Set user ready aka. POST on /game/{gameId}/player/{playerId}/ready
	private class PlayerReadyTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {

			try {

				RestService.post("/game/" + gameId + "/player/" + playerId
						+ "/ready", params, Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void param) {
			Log.e("Main", "Player: " + userName + " with ID: " + playerId
					+ " is ready.");
		}

	}

	// Get relevant data aka. GET on /game/{gameId}/roundevaluation
	private class GetEndRoundDataTask extends
			AsyncTask<Void, Void, RoundEvaluationBean> {

		@Override
		protected RoundEvaluationBean doInBackground(Void... params) {
			try {

				ResponseEntity<RoundEvaluationBean> response = RestService.get(
						"/game/" + gameId + "/roundevaluation", params,
						RoundEvaluationBean.class);
				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		protected void onPostExecute(RoundEvaluationBean response) {

			// check if all players have arrived in the EndOfRoundActivity

			if (allArrived) {

				// if all players are in the EndOfRoundActivity and have no Ave
				// Caesars

				/*
				 * ORDER: 1. player has aveCaesar (nextPlayer != null) 2. a
				 * player has 7 cards 3. allhandled and ready 4. waiting for
				 * more player
				 */

				// if this client has ave caesar
				if (response.getNextPlayer() != null
						&& response.getNextPlayer().equals(userName)) {

					Log.e("Main",
							"Calling AveCaesarTargetCardActivity for player: "
									+ playerId);

					if (timerActive) {
						refreshTimer.cancel();
						timerActive = false;
					}

					Intent intent = new Intent(getBaseContext(),
							AveCaesarTargetCardActivity.class);
					intent.putExtra("gameId", gameId);
					intent.putExtra("owner", owner);
					intent.putExtra("gameName", gameName);
					intent.putExtra("userToken", userToken);
					intent.putExtra("playerId", playerId);
					intent.putExtra("userName", userName);
					intent.putExtra("moveId", moveId);
					startActivity(intent);

					// Let it be and return to it later and restart it
					// finish();
					return;

					// if a different client has ave caesar
				} else if (response.getNextPlayer() != null) {

					Log.e("Main", "Player " + response.getNextPlayer()
							+ " has Ave Caesar");

					printText("Waiting for " + response.getNextPlayer()
							+ " to execute Ave Caesar");

					if (response.getMessage() != null) {
						appendLine("\n" + response.getMessage());
					}
					Log.e("Main", "getMessage():" + response.getMessage());

					if (!timerActive) {
						refreshTimer = new Timer();
						TimerTask getEndRoundData = new GetEndRoundDataTimerTask();
						refreshTimer.scheduleAtFixedRate(getEndRoundData, 0,
								2000);
						timerActive = true;
					}

					// TODO: logik für Negative Effect activity

					if (response.getHasNegativeEffect() != null) {
						if (response.getHasNegativeEffect() && !negativeEffectDone) {

							if (timerActive) {
								refreshTimer.cancel();
								timerActive = false;
							}
							
							negativeEffectDone = true;

							Log.e("Main",
									"Calling NegativeEffectActivity for player: "
											+ playerId);

							Intent intent = new Intent(getBaseContext(),
									NegativeEffectActivity.class);
							intent.putExtra("gameId", gameId);
							intent.putExtra("owner", owner);
							intent.putExtra("gameName", gameName);
							intent.putExtra("userToken", userToken);
							intent.putExtra("playerId", playerId);
							intent.putExtra("userName", userName);
							intent.putExtra("moveId", moveId);

							if (response.getNextTargetCardNegativeEffectId() != null) {
								intent.putExtra("effectId", response
										.getNextTargetCardNegativeEffectId());
							}
							startActivity(intent);

						} else {
							Log.e("Main", "No negative effects for the player");
						}
					} else {
						Log.e("Main", "getHasNegativeEffect == null ");
					}

				}

				// A player has won the game
				else if (response.getWinnerId() != null) {

					Log.e("Main", "Calling EndGameActivity for player: "
							+ playerId);

					if (timerActive) {
						refreshTimer.cancel();
						timerActive = false;
					}
					// TODO: logic for winner
					Intent intent = new Intent(getBaseContext(),
							EndGameActivity.class);

					if (response.getWinnerId().equals(playerId)) {

						intent.putExtra("winner", true);

					} else {
						intent.putExtra("winner", false);
					}

					startActivity(intent);
					finish();

					// All AveCaesars are handled
				} else if (response.allHandledAndReady()) {
					Log.e("Main", "Calling StatsActivity for player: "
							+ playerId);

					if (timerActive) {
						refreshTimer.cancel();
						timerActive = false;
					}

					Intent intent = new Intent(getBaseContext(),
							StatsActivity.class);
					intent.putExtra("gameId", gameId);
					intent.putExtra("owner", owner);
					intent.putExtra("gameName", gameName);
					intent.putExtra("userToken", userToken);
					intent.putExtra("playerId", playerId);
					intent.putExtra("userName", userName);
					intent.putExtra("moveId", moveId);

					startActivity(intent);
					finish();
					return;
				}

				// if not all players are in EndRoundActivity yet
			} else {
				Log.e("Main", "Waiting for players to finish their move");
				printText("Waiting for players to finish their move");

				if (!timerActive) {

					refreshTimer = new Timer();
					TimerTask getEndRoundData = new GetEndRoundDataTimerTask();
					refreshTimer.scheduleAtFixedRate(getEndRoundData, 0, 2000);

					timerActive = true;
				}
			}
		}
	}

	// Remove user from game aka. POST on /game/{gameId}/player/remove
	private class RemovePlayerTask extends
			AsyncTask<GamePlayerRequestBean, Void, Void> {
		@Override
		protected Void doInBackground(
				GamePlayerRequestBean... gamePlayerRequestBeans) {

			try {
				Log.d("exit", "player left the game");
				RestService.post("/game/" + gameId + "/player/remove",
						gamePlayerRequestBeans[0], Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void param) {

		}

	}

	/** debugging **/
	// sets debug message to specified string
	private void printText(String str) {
		TextView debugText = (TextView) findViewById(R.id.er_logMessage);
		debugText.setText(str);
		debugMessage = str;
	}

	// appends specified line to already written string
	private void appendLine(String str) {
		debugMessage = debugMessage.concat(str + "\n");
		printText(debugMessage);
	}


}
