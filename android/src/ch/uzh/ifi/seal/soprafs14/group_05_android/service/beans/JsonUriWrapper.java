package ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans;

public class JsonUriWrapper {

	private String uri;

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}
}
