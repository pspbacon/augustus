package ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans;

public enum GameStatus {
	PENDING, RUNNING, FINISHED
}
