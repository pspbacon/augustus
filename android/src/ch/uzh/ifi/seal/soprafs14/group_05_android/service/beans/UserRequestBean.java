package ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans;

public class UserRequestBean {

	private String name;
	private String username;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}
