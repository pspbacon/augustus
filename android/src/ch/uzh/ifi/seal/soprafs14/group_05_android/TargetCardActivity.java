package ch.uzh.ifi.seal.soprafs14.group_05_android;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.RestService;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.GamePlayerRequestBean;
import ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans.TargetCardBean;

public class TargetCardActivity extends Activity {
	// tag for debug filtering
	public static final String cTag = "GameStartActivity";

	private long gameId;
	private String gameName;
	private String owner;
	private String userToken;
	private String userName;
	private long playerId;
	ArrayList<TargetCardBean> targetCardArrayList = new ArrayList<TargetCardBean>();
	ArrayList<TargetCardBean> nonSelectedTargetCards = new ArrayList<TargetCardBean>();
	private Timer refreshTimer = new Timer();

	// represents a card that is currently selected / enlarged, null if none is
	// selected
	private CardView selectedCard = null;
	ArrayList<CardView> cardViews;
	ArrayList<CardView> selectedCards = new ArrayList<CardView>();

	GamePlayerRequestBean seanBean = new GamePlayerRequestBean();

	/** Android related functions **/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_targetcard);

		gameId = getIntent().getExtras().getLong("gameId");
		owner = getIntent().getStringExtra("owner");
		gameName = getIntent().getStringExtra("gameName");
		userToken = getIntent().getStringExtra("userToken");
		userName = getIntent().getStringExtra("userName");
		playerId = getIntent().getExtras().getLong("userId");

		Log.d(cTag,
				"Wenn diese Botschaft korrekt angezeigt wird, liegt der Fehler bei Felix");
		// get 6 target cards and save them in an ArrayList
		seanBean.setUserToken(userToken);

		new GetTargetCardTask().execute(seanBean);

		// Start the timer to refresh the player stati

		TimerTask getPlayerStatus = new GetPlayerStatusTimerTask();
		refreshTimer.scheduleAtFixedRate(getPlayerStatus, 0, 1000);

		// Task created for sending back the cards
		// TODO: Some method is needed to make the user choose between the three
		// card

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.game, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		
		if(id == R.id.Spiel_verlassen) {
			onBackPressed();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}

	public void onBackPressed() {
		new AlertDialog.Builder(this).setTitle("Spiel verlassen?")
				.setMessage("Möchten Sie das Spiel wirklich verlassen?")
				.setNegativeButton(R.string.nein, null)
				.setPositiveButton(R.string.ja, new OnClickListener() {

					public void onClick(DialogInterface arg0, int arg1) {
						refreshTimer.cancel();
						new RemovePlayerTask().execute(seanBean);
						finish();
						TargetCardActivity.super.onBackPressed();
					}
				}).create().show();
	}

	/** Polling **/

	// the timer task to fetch the user stati
	class GetPlayerStatusTimerTask extends TimerTask {

		public void run() {
			new GetPlayerStatusTask().execute();
		}
	}

	/** SpringMVC tasks **/

	// Remove user from game aka. POST on /game/{gameId}/player/remove
	private class RemovePlayerTask extends
			AsyncTask<GamePlayerRequestBean, Void, Void> {
		@Override
		protected Void doInBackground(
				GamePlayerRequestBean... gamePlayerRequestBeans) {

			try {

				RestService.post("/game/" + gameId + "/player/remove",
						gamePlayerRequestBeans[0], Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void param) {

		}

	}

	// Fetch cards from game aka. POST on
	// /game/{gameId}/player/{playerId}/startcard

	private class GetTargetCardTask extends
			AsyncTask<GamePlayerRequestBean, Void, TargetCardBean[]> {
		@Override
		protected TargetCardBean[] doInBackground(
				GamePlayerRequestBean... seanBean) {
			try {

				ResponseEntity<TargetCardBean[]> response = RestService.post(
						"/game/" + gameId + "/player/" + playerId
								+ "/startcard", seanBean[0],
						TargetCardBean[].class);

				return response.getBody();
			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		// after TargetCardBeans have been sent save them in the
		// targetCardArrayList for client side interaction
		// @Override
		protected void onPostExecute(TargetCardBean[] response) {

			for (int i = 0; i < response.length; i++) {

				Log.e("Main", "Adding TargetCard: " + response[i].getName());
				targetCardArrayList.add(response[i]);
				Log.e("Main", "ArrayListSize: " + targetCardArrayList.size());

			}

			onCardsReceivedCallback();

		}

	}

	// Send selected cards (that are stored as beans in targetCardArrayList) as
	// a TargetCardBean[] back to the server
	// aka. POST on /game/{gameId}/player/{userId}/startcard2

	private class ChooseCardsTask extends
			AsyncTask<TargetCardBean[], Void, Void> {

		@Override
		protected Void doInBackground(TargetCardBean[]... targetCardBeans) {

			try {
				RestService.post("/game/" + gameId + "/player/" + playerId
						+ "/startcard2", targetCardBeans[0], Void.class);

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void params) {
			// Nothing to do here#
			Log.e("Main", "sent back TargetCards");
		}

	}

	// get on /game/{gameId}/player/status to get the boolean which determines
	// if all players have chosen their starting targetcards

	private class GetPlayerStatusTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			try {

				ResponseEntity<Boolean> response = RestService.get("/game/"
						+ gameId + "/player/status", params, Boolean.class);

				Log.e("Main", "TargetCardActivity: Polling Playerstati");

				return response.getBody();

			} catch (RestClientException rce) {
				Log.e("Main", rce.getMessage());
			}

			return null;
		}

		protected void onPostExecute(Boolean response) {

			// if all players have selected their cards
			if (response == null) {
				return;
			}

			if (response == true) {
				// TODO: Next game activity
				Log.e("Main", "Calling DrawMarkerActivity for player: "
						+ playerId);

				refreshTimer.cancel();

				// Call GameSartActivity and pass parameters

				Intent intent = new Intent(getBaseContext(),
						DrawMarkerActivity.class);
				intent.putExtra("gameId", gameId);
				intent.putExtra("owner", owner);
				intent.putExtra("gameName", gameName);
				intent.putExtra("userToken", userToken);
				intent.putExtra("playerId", playerId);
				intent.putExtra("userName", userName);
				startActivity(intent);
				finish();
			}
		}
	}

	/** GUI **/

	// selected Card is set to the new card, and if one was selected, that one
	// is deselected
	public void switchCards(CardView newSelected) {
		if (selectedCard != null)
			selectedCard.toggleSelected();
		selectedCard = newSelected;
	}

	// allows calling card to deselect (onyl) themselves. scaling etc. is
	// handled card-internally
	public void deselectCard(CardView callingView) {
		if (selectedCard == callingView)
			selectedCard = null;
	}

	// horrible code, i'm so ugly, DON'T LOOK AT ME!
	public void toggleSelection(CardView callingView) {
		if (!selectedCards.contains(callingView)) {
			selectedCards.add(callingView);
			if (selectedCards.size() == 3) { // if 3rd card is selected
				// show dialog to let player accept or undo his selection
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("Auswahl bestätigen")
						.setMessage("Willst du diese Karten wählen?")
						.setPositiveButton("Ja",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int id) {
										AudioManager.playSound(
												getGameStartActivityContext(),
												"accept");
										for (CardView cardview : cardViews) {
											if (!selectedCards
													.contains(cardview)) {
												if (cardview
														.hasTargetCardBean()) // sicherheitscheck,
																				// aber
																				// sollte
																				// immer
																				// haben
													nonSelectedTargetCards.add(cardview
															.getTargetCardBean());
											}
										}
										Log.d(cTag,
												"nonSelectedTargetCards numel: "
														+ nonSelectedTargetCards
																.size());
										onCardsSelectedCallback();
									}
								})
						.setNegativeButton("Nein",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										for (CardView card : selectedCards) {
											card.parentDeselect();
										}
										selectedCards.clear();
										AudioManager.playSound(
												getGameStartActivityContext(),
												"cancel");
										Log.d(cTag, "SelectedCards cleared");
									}
								});
				builder.show();
			}
			Log.d(cTag, "Card selected: " + callingView.getName());
		} else {
			selectedCards.remove(callingView);
			Log.d(cTag, "Card deselected: " + callingView.getName());
		}

		// DEBUG
		String debug = "";
		for (CardView cv : selectedCards) {
			debug += cv.getName() + " ";
		}
		Log.d(cTag, "ArrayList contains: " + debug);
	}

	// gets called after card have been loaded into targetCardArrayList
	private void onCardsReceivedCallback() {
		LinearLayout root = (LinearLayout) findViewById(R.id.container);
		cardViews = new ArrayList<CardView>();

		for (TargetCardBean tcb : targetCardArrayList) {
			cardViews.add(new CardView(this, new Card(tcb)));
		}

		for (CardView cardv : cardViews) {
			root.addView(cardv);
		}

	}

	private void onCardsSelectedCallback() {
		// send cards from ArrayList nonSelectedTargetCards back
		TargetCardBean[] tmpArray = nonSelectedTargetCards
				.toArray(new TargetCardBean[nonSelectedTargetCards.size()]);
		new ChooseCardsTask().execute(tmpArray);
	}

	// might be a highly dangerous and utterly and irredeemably bad hack. but it
	// werks.
	// TODO: what is this hack here doing anyways???
	private Context getGameStartActivityContext() {
		return this;
	}

}
