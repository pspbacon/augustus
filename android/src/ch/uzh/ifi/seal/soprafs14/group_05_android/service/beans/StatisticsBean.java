package ch.uzh.ifi.seal.soprafs14.group_05_android.service.beans;

public class StatisticsBean {
    
    Integer points;
    Integer pointsTargetCards;
    Integer pointsTargetCardsFixed;
    Integer pointsTargetCardsVariable;
    Integer pointsRewards;
    
    Integer countGold;
    Integer countWheat;
    
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCountGold() {
        return countGold;
    }

    public void setCountGold(Integer countGold) {
        this.countGold = countGold;
    }

    public Integer getCountWheat() {
        return countWheat;
    }

    public void setCountWheat(Integer countWheat) {
        this.countWheat = countWheat;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getPointsTargetCards() {
            return pointsTargetCards;
    }

    public void setPointsTargetCards(Integer points) {
            this.pointsTargetCards = points;
    }

    public Integer getPointsTargetCardsFixed() {
        return pointsTargetCardsFixed;
    }

    public void setPointsTargetCardsFixed(Integer points) {
        this.pointsTargetCardsFixed = points;
    }

    public Integer getPointsTargetCardsVariable() {
        return pointsTargetCardsVariable;
    }

    public void setPointsTargetCardsVariable(Integer points) {
        this.pointsTargetCardsVariable = points;
    }

    public Integer getPointsRewards() {
        return pointsRewards;
    }

    public void setPointsRewards(Integer points) {
        this.pointsRewards = points;
    }

}
