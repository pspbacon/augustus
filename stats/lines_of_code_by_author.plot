set terminal png transparent size 640,240
set size 1.0,1.0

set terminal png transparent size 640,480
set output 'lines_of_code_by_author.png'
set key left top
set xdata time
set timefmt "%s"
set format x "%Y-%m-%d"
set grid y
set ylabel "Lines"
set xtics rotate
set bmargin 6
plot 'lines_of_code_by_author.dat' using 1:2 title "pspbacon" w lines, 'lines_of_code_by_author.dat' using 1:3 title "Sebastian" w lines, 'lines_of_code_by_author.dat' using 1:4 title "Christoph Vogel" w lines, 'lines_of_code_by_author.dat' using 1:5 title "Felix Kieber" w lines, 'lines_of_code_by_author.dat' using 1:6 title "Lars" w lines, 'lines_of_code_by_author.dat' using 1:7 title "Martin Brandtner" w lines, 'lines_of_code_by_author.dat' using 1:8 title "larsw" w lines, 'lines_of_code_by_author.dat' using 1:9 title "Trolleybus" w lines, 'lines_of_code_by_author.dat' using 1:10 title "publicstaticdouble" w lines, 'lines_of_code_by_author.dat' using 1:11 title "Felix" w lines
